import { combineReducers } from "redux";
import UIReducer from "./UIReducer";
import AccountReducer from "./AccountReducer";
import FileUploadReducer from "./FileUploadReducer";

export default combineReducers({
  ui: UIReducer,
  account: AccountReducer,
  fileupload: FileUploadReducer
});
