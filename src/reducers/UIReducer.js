import { UI_LOADING_MODAL, UI_NOTIFICATION_SNACK } from "../api/ActionTypes";

const initialState = {
  loadingModal: false,
  notificationSnack: {
    open: false,
    notification: "",
    type: ""
  }
};

export default (state = initialState, action) => {
  const { payload } = action;
  switch (action.type) {
    case UI_LOADING_MODAL:
      return Object.assign({}, state, { loadingModal: payload.open });

    case UI_NOTIFICATION_SNACK:
      return Object.assign({}, state, {
        notificationSnack: Object.assign(
          {},
          state.notificationSnack,
          payload.open
            ? {
                open: payload.open,
                notification: payload.notification,
                type: payload.type
              }
            : { open: payload.open }
        )
      });
    default:
      return state;
  }
};
