import {
  FILEUPLOAD_NEW,
  FILEUPLOAD_UPDATE,
  FILEUPLOAD_END
} from "../api/ActionTypes";

const initialState = {
  uploading: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FILEUPLOAD_NEW:
      return Object.assign({}, initialState, {
        uploading: Object.assign({}, initialState.uploading, {
          [action.fileId]: 0
        })
      });

    case FILEUPLOAD_UPDATE:
      return Object.assign({}, initialState, {
        uploading: Object.assign({}, initialState.uploading, {
          [action.fileId]: action.progress
        })
      });

    case FILEUPLOAD_END:
      let {
        [action.fileId]: temp,
        ...removedUploading
      } = initialState.uploading;
      return Object.assign({}, initialState, {
        uploading: removedUploading
      });

    default:
      return state;
  }
};
