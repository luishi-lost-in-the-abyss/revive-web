import { ACCOUNT_LOGIN, ACCOUNT_LOGOUT } from "../api/ActionTypes";

const initialState = {
  token: "",
  authenticated: false,
  role: "none"
};

export default (state = initialState, action) => {
  const { payload } = action;
  switch (action.type) {
    case ACCOUNT_LOGIN:
      return Object.assign({}, state, {
        token: payload.token,
        role: payload.role,
        authenticated: true
      });
    case ACCOUNT_LOGOUT:
      return Object.assign({}, state, initialState);
    default:
      return state;
  }
};
