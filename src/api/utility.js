import Router from "next/router";
import { ACCOUNT_LOGOUT } from "./ActionTypes";
import { blue, red, teal, green, yellow } from "@material-ui/core/colors";

export const validateEmail = function(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

export function redirectTo(destination, { res, status } = {}) {
  if (res) {
    res.writeHead(status || 302, { Location: destination });
    res.end();
  } else {
    if (destination[0] === "/" && destination[1] !== "/") {
      Router.push(destination);
    } else {
      window.location = destination;
    }
  }
}

export const initializeAuthClient = (client, dispatch) => {
  client.on("logout", () => {
    window.localStorage.removeItem("token");
    dispatch({ type: ACCOUNT_LOGOUT });
  });
  client.on("reauthentication-error", () => {
    window.localStorage.removeItem("token");
    dispatch({ type: ACCOUNT_LOGOUT });
  });
};

export const extractLocationDetails = barangayObj => {
  const barangay = barangayObj;
  const municipality = barangay.municipality;
  const province = municipality.province;
  const region = province.region;
  const country = region.country;
  return { barangay, municipality, province, region, country };
};

export const START_HOUR = 6; // 6 am
export const END_HOUR = 18; // 6 pm
export const ADMIN_START_HOUR = 0; // 12 am
export const ADMIN_END_HOUR = 24; // 12 am
export const START_END_HOURS = END_HOUR - START_HOUR;

export function exportToCsv(filename, rows) {
  let processRow = function(row) {
    let finalVal = "";
    for (let j = 0; j < row.length; j++) {
      let innerValue = row[j] === null ? "" : row[j].toString();
      if (row[j] instanceof Date) {
        innerValue = row[j].toLocaleString();
      }
      let result = innerValue.replace(/"/g, '""');
      if (result.search(/("|,|\n)/g) >= 0) result = '"' + result + '"';
      if (j > 0) finalVal += ",";
      finalVal += result;
    }
    return finalVal + "\n";
  };

  let csvFile = "";
  for (let i = 0; i < rows.length; i++) {
    csvFile += processRow(rows[i]);
  }

  let blob = new Blob([csvFile], { type: "text/csv;charset=utf-8;" });
  if (navigator.msSaveBlob) {
    // IE 10+
    navigator.msSaveBlob(blob, filename);
  } else {
    let link = document.createElement("a");
    if (link.download !== undefined) {
      // feature detection
      // Browsers that support HTML5 download attribute
      let url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", filename);
      link.style.visibility = "hidden";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
}

export const engagementObj = {
  directBuying: { type: "fss", label: "Direct Buying", color: blue[400] },
  growership: { type: "fss", label: "Growership", color: red[400] },
  financing: { type: "fss", label: "Financing", color: teal[400] },
  inhouse: { type: "lmf", label: "In-House", color: green[400] },
  lease: { type: "lmf", label: "Lease", color: yellow[400] }
};
