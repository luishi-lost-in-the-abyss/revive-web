import io from "socket.io-client";
import feathers from "@feathersjs/feathers";
import socketio from "@feathersjs/socketio-client";
import auth from "@feathersjs/authentication-client";

import { API_SERVER } from "./EndPoints";

const socket = io(API_SERVER, {
  transports: ["websocket"],
  forceNew: true
});

let client = null;

export default {
  getClient: () => {
    if (!client) {
      client = feathers()
        .configure(
          socketio(socket, {
            timeout: 60000
          })
        )
        .configure(
          auth({
            cookie: "revive-jwt"
          })
        );
    }
    return client;
  },
  getSocket: () => {
    return socket;
  },
  getNewSocket: () => {
    return io(API_SERVER, {
      transports: ["websocket"],
      forceNew: true
    });
  }
};
