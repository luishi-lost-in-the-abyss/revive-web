import React from "react";
import {
  Grid,
  Typography,
  Table,
  TableRow,
  TableCell,
  TableBody
} from "@material-ui/core";
import VerticalTable from "../common/VerticalTable";
import { DOWNLOAD_SERVER } from "../../api/EndPoints";

const getLabelasValue = function(registrationParams, name, key) {
  const keyValue = {};
  const arr = registrationParams[name];
  arr.map(({ label, value }) => {
    keyValue[value] = label;
  });
  return keyValue[key] ? keyValue[key] : key;
};
export default ({
  disableGrid = false,
  plots,
  registrationParams,
  barangays,
  ...application
}) => {
  const ParticipationInfo = (
    <React.Fragment>
      <Typography variant="h5" gutterBottom>
        <b>Participation Info</b>
      </Typography>
      <VerticalTable
        id="participantInfo"
        colWidth={[200, null]}
        data={application}
        tableValues={[
          {
            label: "Formal Cluster Code",
            key: null,
            render: ({ cluster, formal_cluster }) =>
              formal_cluster ? formal_cluster.code : cluster ? cluster.code : ""
          }
        ]}
      />
    </React.Fragment>
  );
  const FarmerInfo = (
    <React.Fragment>
      <Typography variant="h5" gutterBottom>
        <b>Farmer Info</b>
      </Typography>
      <VerticalTable
        id="farmerInfo"
        colWidth={[200, null]}
        data={application}
        tableValues={[
          {
            label: "Name",
            key: null,
            render: obj => {
              return obj.name
                ? obj.name
                : `${obj.firstName} ${obj.middleName} ${obj.lastName}`;
            }
          },
          { label: "Birth Date", key: "birthdate" },
          { label: "Occupation", key: "occupation" },
          { label: "Civil Status", key: "civil" },
          { label: "Contact Number", key: "contact" },
          { label: "Mobile Operator", key: "operator" },
          { label: "Gender", key: "gender" },
          { label: "Resident Address", key: "residentAddress" },
          { label: "Bank Name", key: "bankName" },
          { label: "Bank Account Type", key: "bankAccType" },
          { label: "Bank Account Name", key: "bankAccName" },
          { label: "Bank TIN Number", key: "bankTinNo" },
          {
            label: "Health Condition",
            key: null,
            render: ({ healthConditions, healthOthers }) => {
              return (
                <div>
                  {healthConditions.length === 0
                    ? "None"
                    : healthConditions.join(", ")}
                  {healthConditions.indexOf("others") > -1 ? (
                    <React.Fragment>
                      <br />
                      Others: {healthOthers}
                    </React.Fragment>
                  ) : null}
                </div>
              );
            }
          },
          {
            label: "ID Image",
            key: null,
            render: ({ id, file }) => (
              <img
                id="profile-image"
                src={
                  file
                    ? file.toDataURL()
                    : id
                    ? `${DOWNLOAD_SERVER}/farmer/${id}.jpg?timestamp=${new Date().getTime()}`
                    : "/static/images/id-card.png"
                }
                onError={e => {
                  e.target.onerror = null;
                  e.target.src = "/static/images/id-card.png";
                }}
                style={{ width: "100%" }}
              />
            )
          }
        ]}
      />
    </React.Fragment>
  );

  const PlotInfo = (
    <React.Fragment>
      <Typography variant="h5" gutterBottom>
        <b>Plot Info</b>
      </Typography>
      {plots.map((plot, i) => (
        <div key={`review-plot-${i}`}>
          <Typography variant="h6" gutterBottom>
            Plot {i + 1}
          </Typography>

          <VerticalTable
            id={`plot-${i}`}
            colWidth={[200, null]}
            data={plot}
            tableValues={[
              {
                label: "Code",
                key: "code"
              },
              {
                label: "Description",
                key: "description"
              },
              {
                label: "Informal Cluster",
                key: "cluster",
                render: cluster => (cluster ? cluster.code : "")
              },
              {
                label: "Participation Type",
                key: "type"
              },
              {
                label: "Engagement Scheme",
                key: "engagementScheme"
              },
              {
                label: "Location",
                key: null,
                render: data => {
                  const barangay = barangays[data.barangay];
                  const municipality = barangay.municipality;
                  const province = municipality.province;
                  const region = province.region;
                  const country = region.country;
                  return `${country.name} ${province.name} ${region.name} ${
                    municipality.name
                  } ${barangay.name} ${data.purok}`;
                }
              },
              {
                label: "Tenure Status",
                key: "tenure",
                render: tenure =>
                  getLabelasValue(registrationParams, "tenureStatus", tenure)
              },
              {
                label: "Commodity",
                key: null,
                render: ({ commodity, commodityOthers }) =>
                  commodity === "others"
                    ? `Others - ${commodityOthers}`
                    : getLabelasValue(registrationParams, "cropType", commodity)
              },
              {
                label: "Water System",
                key: "waterSystem",
                render: waterSystem =>
                  getLabelasValue(
                    registrationParams,
                    "waterSystem",
                    waterSystem
                  )
              },
              { label: "Coordinates", key: "coordinates" },
              { label: "Previous Buyer", key: "prevBuyer" },
              { label: "Previous Price", key: "prevPrice" },
              { label: "Previous Payment Mode", key: "prevPaymentMode" },
              {
                label: "Source of Financing",
                key: "sourceFinance",
                render: financialSource =>
                  getLabelasValue(
                    registrationParams,
                    "financialSource",
                    financialSource
                  )
              },
              {
                label: "Crop Insurance",
                key: null,
                render: ({ cropInsurance, cropInsuranceDetails }) => {
                  return cropInsurance && cropInsurance === true
                    ? cropInsuranceDetails
                    : cropInsurance
                    ? cropInsurance
                    : "No";
                }
              }
            ]}
          />
        </div>
      ))}
    </React.Fragment>
  );
  /*
  const SocioInfo = (
    <React.Fragment>
      <Typography variant="h5" gutterBottom>
        <b>Social Economic Info</b>
      </Typography>
      <VerticalTable
        id="socioInfo"
        colWidth={[200, null]}
        data={application}
        tableValues={[
          { label: "Spouse's Name", key: "spouseName" },
          { label: "Spouse's Occupation", key: "spouseOccupation" },
          {
            label: "Beneficiary",
            key: "beneficiary",
            render: children =>
              children.length > 0
                ? children.map((child, i) => (
                    <p key={`review-child-${i}`}>
                      {child.name} - {child.education}
                    </p>
                  ))
                : "None"
          }
        ]}
      />
    </React.Fragment>
  );
  */

  return disableGrid ? (
    <React.Fragment>
      {ParticipationInfo}
      {FarmerInfo}
      {PlotInfo}
      {/*SocioInfo*/}
    </React.Fragment>
  ) : (
    <Grid container>
      <Grid item xs={12}>
        <Typography
          variant="h4"
          gutterBottom
          style={{ marginTop: 20, marginBottom: 20 }}
        >
          Review Application Details
        </Typography>
      </Grid>
      <Grid item xs={12} sm={10} md={8}>
        {ParticipationInfo}
      </Grid>

      <Grid item xs={12} sm={10} md={8}>
        {FarmerInfo}
      </Grid>

      <Grid item xs={12} sm={10} md={8}>
        {PlotInfo}
      </Grid>

      <Grid item xs={12} sm={10} md={8}>
        {/*SocioInfo*/}
      </Grid>
    </Grid>
  );
};
