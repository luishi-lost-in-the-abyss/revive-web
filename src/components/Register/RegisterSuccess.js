import React from "react";
import { Typography } from "@material-ui/core";

export default ({}) => {
  return (
    <Typography variant="h6">Application Successfully Submitted</Typography>
  );
};
