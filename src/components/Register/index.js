import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import {
  Stepper,
  Step,
  StepLabel,
  Grid,
  Typography,
  StepConnector,
  Button
} from "@material-ui/core";
import RegisterFarmerStep from "./RegisterFarmerStep";
import RegisterStepFinal from "./RegisterStepFinal";
import AccountActions from "../../actions/AccountActions";
import RegisterLandStep from "./RegisterLandStep";

import RegisterSocioStep from "./RegisterSocioStep";
import RegisterAccountStep from "./RegisterAccountStep";

import { questions as socioQuestions } from "../../api/socio";
import Pica from "pica/dist/pica";
import FileUploadActions from "../../actions/FileUploadActions";
import UIActions from "../../actions/UIActions";
const pica = Pica();

const steps = [
  "Farmer Info",
  "Land Info",
  "Socio-economic Info",
  "Confirmation"
];
function getStepContent(step) {
  switch (step) {
    case 1:
      return "Your personal particulars.";
    case 2:
      return "Details of your plots.";
    case 3:
      return "Other details about you.";
    case 4:
      return "Review and confirm application details.";
    default:
      return "Unknown step";
  }
}

class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    const { plotDetail } = props.registrationParams;
    this.state = {
      type: "",
      isCluster: false,
      cluster: "",
      email: "",
      username: "",
      password: "",
      confirmPassword: "",
      firstName: "",
      middleName: "",
      lastName: "",
      birthdate: "",
      occupation: "",
      civil: "",
      operator: "",
      contact: "",
      gender: "",
      residentAddress: "",
      bankName: "",
      bankAccType: "",
      bankAccName: "",
      bankTinNo: "",
      healthConditions: [],
      healthOthers: "",
      plots: [Object.assign({}, plotDetail)],
      socio: socioQuestions,
      beneficiary: [],
      activeStep: 0,
      errors: {},
      errorType: "none",
      file: ""
    };
    this.onFileChange = this.onFileChange.bind(this);
  }

  async componentDidUpdate(prevProps, prevState) {
    if (
      this.state.uploadingFile &&
      Object.keys(prevProps.uploading).length > 0 &&
      Object.keys(this.props.uploading).length === 0
    ) {
      //finished uploading
      this.props.showLoadingModal(false);
      Router.push("/technician/registration/application_submit_success");
    }
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  onFileChange(evt) {
    const file = evt.target.files[0];
    evt.target.value = "";
    if (file.size > 5000000) {
      alert("Image size must be below 5mb!");
      return;
    }
    let img = new Image();
    img.src = window.URL.createObjectURL(file);
    img.onload = () => {
      let src = document.createElement("canvas");
      src.width = img.width;
      src.height = img.height;
      let ctx = src.getContext("2d");
      ctx.drawImage(img, 0, 0);
      let dest = document.createElement("canvas");
      dest.width = img.width;
      dest.height = img.height;

      pica.resize(src, dest).then(result => {
        this.setState({ file: result });
      });
    };
  }

  onClickUpload() {
    const input = document.getElementById("upload-profile");
    input.click();
  }

  onPlotInputChange(i, evt, cb = null) {
    const { name, value } = evt.target;
    const { plots } = this.state;

    const newPlots = plots.map((plot, index) => {
      if (index !== i) return plot;
      return Object.assign({}, plot, { [name]: value });
    });
    if (typeof cb === "function") {
      this.setState({ plots: newPlots }, cb);
    } else {
      this.setState({ plots: newPlots });
    }
  }

  onChildInputChange(i, evt) {
    const { name, value } = evt.target;
    const { beneficiary } = this.state;

    const newChildren = beneficiary.map((child, index) => {
      if (index !== i) return child;
      return Object.assign({}, child, { [name]: value });
    });
    this.setState({ beneficiary: newChildren });
  }

  onSocioInputChange(evt) {
    const { name, value } = evt.target;
    const { socio } = this.state;
    this.setState({ socio: Object.assign({}, socio, { [name]: value }) });
  }

  onAddPlot() {
    const { plotDetail } = this.props.registrationParams;
    this.setState({
      plots: [
        ...this.state.plots,
        Object.assign({}, plotDetail, { type: this.state.type })
      ]
    });
  }

  onRemovePlot(i) {
    const { plots } = this.state;
    this.setState({ plots: [...plots.slice(0, i), ...plots.slice(i + 1)] });
  }

  onAddChild() {
    const { childDetail } = this.props.registrationParams;
    this.setState({
      beneficiary: [...this.state.beneficiary, Object.assign({}, childDetail)]
    });
  }

  onRemoveChild(i) {
    const { beneficiary } = this.state;
    this.setState({
      beneficiary: [...beneficiary.slice(0, i), ...beneficiary.slice(i + 1)]
    });
  }

  registerUser() {
    const { username, password, email, confirmPassword } = this.state;
    if (!username || !confirmPassword) {
      this.setState({
        errors: {
          username: !username,
          confirmPassword: !confirmPassword
        },
        errorType: "blank"
      });
      return;
    }

    if (password !== confirmPassword) {
      this.setState({
        errors: { password: true, confirmPassword: true },
        errorType: "passwordMatch"
      });
      return;
    }

    this.setState({ errors: {}, errorType: "none" });
    this.props.registerUser(this.state, async (err, res) => {
      if (err) {
        this.setState({
          errors: { username: true },
          errorType: "duplicateUser"
        });
        console.log(err);
        return;
      }
      if (res.profile.fileUploadToken) {
        // need to upload file
        const uploadFile = await pica.toBlob(
          this.state.file,
          "image/jpeg",
          0.75
        );
        this.props.uploadFile(
          {
            token: this.props.token,
            type: "farmerId",
            typeId: res.id, //this doesn't matter, will be replaced
            file: uploadFile,
            uploadToken: res.profile.fileUploadToken
          },
          res => {
            this.setState({ uploadingFile: true });
            this.props.showLoadingModal(true);
          }
        );
      } else {
        Router.push("/technician/registration/application_submit_success");
      }
    });
  }

  validateFarmerInfo() {
    const {
      firstName,
      middleName,
      lastName,
      birthdate,
      occupation,
      civil,
      contact,
      operator,
      gender,
      residentAddress,
      bankName,
      bankAccType,
      bankAccName,
      bankTinNo,
      healthConditions,
      healthOthers
    } = this.state;

    let errors = {
      firstName: !firstName,
      middleName: !middleName,
      lastName: !lastName,
      birthdate: !birthdate,
      occupation: !occupation,
      civil: !civil,
      contact: !contact,
      operator: !operator,
      gender: !gender,
      residentAddress: !residentAddress,
      bankName: !bankName,
      bankAccType: !bankAccType,
      bankAccName: !bankAccName,
      bankTinNo: !bankTinNo,
      healthOthers: healthConditions.indexOf("others") > -1 && !healthOthers
    };

    let hasErrors = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasErrors) {
      this.setState({ errors, errorType: "blank" });
      return false;
    }

    this.setState({ errors: {}, errorType: "none" });
    return true;
  }

  validateAccountStep() {
    const { type, isCluster, cluster } = this.state;
    if (!type || (isCluster && !cluster)) {
      this.setState({
        errors: {
          type: !type,
          cluster: isCluster && !cluster
        },
        errorType: "blank"
      });
      return false;
    }

    this.setState({ errors: {}, errorType: "none" });
    return true;
  }

  validateLandInfo() {
    const { plotDetail } = this.props.registrationParams;
    const { plots } = this.state;
    let errors = {};
    const keys = Object.keys(plotDetail);
    plots.map((plot, i) => {
      errors[i] = {};
      keys.map(key => {
        if (key === "cropInsuranceDetails") {
          //only need to fill up insurance details if have insurance
          errors[i][key] = plot["cropInsurance"] && !plot[key];
          return;
        }
        if (key === "commodityOthers") {
          //only need to fill up other commodity if select other
          errors[i][key] = plot["commodity"] === "others" && !plot[key];
          return;
        }
        if (key === "clusterId") return;
        if (typeof plot[key] === "boolean") return;
        errors[i][key] = !plot[key];
      });
    });
    let hasError = Object.values(errors)
      .map(errObj => {
        return Object.values(errObj).reduce((a, b) => a || b, false);
      })
      .reduce((a, b) => a || b, false);

    if (hasError) {
      this.setState({ errors, errorType: "blank" });
      return false;
    }

    plots.map(({ plantableArea, totalArea }, i) => {
      try {
        plantableArea = parseFloat(plantableArea);
        totalArea = parseFloat(totalArea);
      } catch (err) {
        plantableArea = 0;
        totalArea = 0;
      }
      errors[i]["totalArea"] = totalArea < plantableArea || totalArea == 0;
      errors[i]["plantableArea"] = plantableArea > totalArea;
    });
    hasError = Object.values(errors)
      .map(errObj => {
        return Object.values(errObj).reduce((a, b) => a || b, false);
      })
      .reduce((a, b) => a || b, false);
    if (hasError) {
      this.setState({ errors, errorType: "area" });
      return false;
    }

    this.setState({ errors: {}, errorType: "none" });
    return true;
  }

  validateSocioInfo() {
    const { childDetail } = this.props.registrationParams;
    const { socio, beneficiary } = this.state;
    let errors = { beneficiary: {} };
    const childKeys = Object.keys(childDetail);
    if (beneficiary.length > 0) {
      beneficiary.map((child, i) => {
        errors.beneficiary[i] = {};
        childKeys.map(key => {
          if (typeof child[key] === "boolean") return;
          errors.beneficiary[i][key] = !child[key];
        });
      });
    }
    let hasError = Object.values(errors.beneficiary)
      .map(errObj => {
        return Object.values(errObj).reduce((a, b) => a || b, false);
      })
      .reduce((a, b) => a || b, false);
    if (hasError) {
      this.setState({ errors, errorType: "blank" });
      return false;
    }

    this.setState({ errors: {}, errorType: "none" });
    return true;
  }

  changeStep(next = true) {
    const { activeStep } = this.state;
    if (!next) {
      this.setState({ activeStep: activeStep - 1 });
      return;
    }

    //do validation before going to next page
    let error = false;
    switch (activeStep) {
      case 0:
        error = !this.validateAccountStep();
        break;
      case 1:
        error = !this.validateFarmerInfo();
        break;
      case 2:
        error = !this.validateLandInfo();
        break;
      case 3:
        error = !this.validateSocioInfo();
        break;
    }
    if (!error) {
      this.setState({ activeStep: activeStep + 1 }, () => {
        window.scrollTo(0, 0);
        const { plots, type } = this.state;
        if (!plots[0].type && activeStep === 0) {
          const plot = plots[0];
          plot.type = type;
          this.setState({
            plots: [plot, ...plots.slice(1)]
          });
        }
      });
    }
  }

  render() {
    const { activeStep, errors, errorType } = this.state;
    const finalStep = activeStep === 4;
    const commonStepProps = {
      registrationParams: this.props.registrationParams,
      onInputChange: this.onInputChange.bind(this),
      rootState: this.state,
      errors: errors,
      errorType: errorType
    };
    return (
      <React.Fragment>
        <Grid container>
          {activeStep === 0 ? null : (
            <Grid item xs={12}>
              <Stepper
                activeStep={activeStep - 1}
                connector={<StepConnector />}
                style={{ marginBottom: 50 }}
              >
                {steps.map((label, index) => {
                  return (
                    <Step key={`step-${index}`}>
                      <StepLabel
                        optional={
                          <Typography variant="caption">
                            {getStepContent(index + 1)}
                          </Typography>
                        }
                      >
                        {label}
                      </StepLabel>
                    </Step>
                  );
                })}
              </Stepper>
            </Grid>
          )}
          <Grid item xs={12} style={{ marginBottom: 50 }}>
            {activeStep === 0 ? (
              <RegisterAccountStep {...commonStepProps} />
            ) : activeStep === 1 ? (
              <RegisterFarmerStep
                {...commonStepProps}
                onClickUpload={this.onClickUpload.bind(this)}
              />
            ) : activeStep === 2 ? (
              <RegisterLandStep
                disableInformalCluster={!!this.state.isCluster} //shouldn't show input if participant is part of formal cluster
                {...Object.assign({}, commonStepProps, {
                  onAddPlot: this.onAddPlot.bind(this),
                  onRemovePlot: this.onRemovePlot.bind(this),
                  onInputChange: this.onPlotInputChange.bind(this)
                })}
              />
            ) : activeStep === 3 ? (
              <RegisterSocioStep
                onAddChild={this.onAddChild.bind(this)}
                onRemoveChild={this.onRemoveChild.bind(this)}
                onChildInputChange={this.onChildInputChange.bind(this)}
                {...Object.assign({}, commonStepProps, {
                  onInputChange: this.onSocioInputChange.bind(this)
                })}
              />
            ) : (
              <RegisterStepFinal {...commonStepProps} />
            )}
          </Grid>

          <Grid item xs={12} sm={10} md={8}>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button
                variant="outlined"
                color="secondary"
                style={{ marginRight: 50 }}
                disabled={activeStep === 0}
                onClick={this.changeStep.bind(this, false)}
              >
                Back
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={
                  finalStep
                    ? this.registerUser.bind(this)
                    : this.changeStep.bind(this, true)
                }
              >
                {finalStep ? "Submit" : "Next"}
              </Button>
            </div>
          </Grid>
        </Grid>
        <input
          style={{ display: "none" }}
          id="upload-profile"
          accept="image/x-png,image/gif,image/jpeg"
          type="file"
          onChange={this.onFileChange}
        />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { account, fileupload } = state;
  return {
    token: account ? account.token : "",
    uploading: fileupload.uploading
  };
}

export default connect(
  mapStateToProps,
  Object.assign({}, AccountActions, FileUploadActions, UIActions)
)(RegisterForm);
