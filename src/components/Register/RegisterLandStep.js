import React, { ReactDOM } from "react";
import { Typography, Grid, TextField, Button } from "@material-ui/core";
import SelectInput from "../common/SelectInput";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../theme";
import FeathersClient from "../../api/FeathersClient";

class RegisterLandStep extends React.Component {
  constructor(props) {
    super(props);

    const plotLocation = {};
    if (props.editMode) {
      Object.keys(props.rootState.plots).map((obj, i) => {
        plotLocation[i] = {
          countryArr: [],
          regionArr: [],
          provinceArr: [],
          municipalityArr: [],
          barangayArr: []
        };
      });
    } else {
      plotLocation[0] = {
        countryArr: [],
        regionArr: [],
        provinceArr: [],
        municipalityArr: [],
        barangayArr: []
      };
    }
    this.state = {
      labelWidth: 0,
      plotLocation,
      clusters: []
    };
  }

  async componentDidMount() {
    const { editMode, rootState } = this.props;
    if (editMode) {
      rootState.plots.map((plot, i) => {
        this.getLocation("country", null, null, i);
        this.getLocation("region", plot.country, "country", i);
        this.getLocation("province", plot.region, "region", i);
        this.getLocation("municipality", plot.province, "province", i);
        this.getLocation("barangay", plot.municipality, "municipality", i);
      });
    } else {
      this.getLocation("country", null, null, 0);
    }
    const client = FeathersClient.getClient();
    const clusters = await client
      .service("clusters")
      .find({ query: { $limit: "-1" } });
    this.setState({
      clusters: clusters.filter(cluster => !cluster.formal_cluster)
    });
  }
  async getLocation(type, value = null, parent = null, i) {
    const client = FeathersClient.getClient();
    const service = client.service(`location-${type}`);
    const results =
      !value && !parent
        ? await service.find({ query: { $limit: "-1" } }) // this will always be the country search
        : await service.find({
            query: { $limit: "-1", [`${parent}Id`]: value, $sort: { name: 1 } }
          });
    const plotLocation = this.state.plotLocation[i];

    this.setState({
      plotLocation: Object.assign({}, this.state.plotLocation, {
        [i]: Object.assign({}, plotLocation, {
          [`${type}Arr`]: results.map(({ id, name }) => ({
            label: name,
            value: id
          }))
        })
      })
    });
  }

  onLocationChange(i, evt) {
    const { name, value } = evt.target;
    switch (name) {
      case "country":
        this.getLocation("region", value, "country", i);
        break;
      case "region":
        this.getLocation("province", value, "region", i);
        break;
      case "province":
        this.getLocation("municipality", value, "province", i);
        break;
      case "municipality":
        this.getLocation("barangay", value, "municipality", i);
        break;
    }

    this.props.onInputChange(i, evt, () => {
      switch (name) {
        case "country":
          this.props.onInputChange(i, {
            target: { name: "region", value: "" }
          });
        case "region":
          this.props.onInputChange(i, {
            target: { name: "province", value: "" }
          });
        case "province":
          this.props.onInputChange(i, {
            target: { name: "municipality", value: "" }
          });
        case "municipality":
          this.props.onInputChange(i, {
            target: { name: "barangay", value: "" }
          });
          break;
        default:
          break;
      }
    });
  }

  onAddPlot() {
    const plots = this.props.rootState.plots.length;
    this.setState({
      plotLocation: Object.assign({}, this.state.plotLocation, {
        [plots]: {
          countryArr: [],
          regionArr: [],
          provinceArr: [],
          municipalityArr: [],
          barangayArr: []
        }
      })
    });
    this.getLocation("country", null, null, plots);
    this.props.onAddPlot();
  }

  onRemovePlot(i, evt) {
    const plot = this.state.plotLocation;
    const newPlotLocation = {};
    Object.keys(plot).map(index => {
      if (index < i) {
        newPlotLocation[index] = plot[index];
      } else if (index > i) {
        newPlotLocation[index - 1] = plot[index];
      }
    });
    this.setState({ plotLocation: newPlotLocation });
    this.props.onRemovePlot(i, evt);
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.props;
    const plotError = errors[i] ? errors[i] : {};
    if (!plotError[name]) return null;
    if (
      errorType === "blank" &&
      (name === "totalArea" || name === "plantableArea")
    )
      return "Area cannot be 0.";
    if (errorType === "blank") return "This field cannot be left blank.";

    if (errorType === "area")
      return "Plantable area cannot be more than total area";

    return null;
  }

  render() {
    const {
      classes,
      onInputChange,
      onRemovePlot,
      onAddPlot,
      rootState,
      errors,
      errorType,
      registrationParams,
      disableInformalCluster
    } = this.props;
    const commonTextFieldProps = (name, i) => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: onInputChange.bind(null, i),
      name,
      value: rootState.plots[i][name],
      error: errors[i] && errors[i][name],
      helperText: this.getHelperText(name, i)
    });

    const commonSelectFieldProps = (name, i) => ({
      id: `plot-${i}-${name}`,
      name,
      value: rootState.plots[i][name],
      labelWidth: 100,
      onInputChange: onInputChange.bind(null, i),
      error: errors[i] && errors[i][name],
      helperText: this.getHelperText(name, i)
    });

    const commonLocationFieldProps = (name, i) =>
      Object.assign({}, commonSelectFieldProps(name, i), {
        onInputChange: this.onLocationChange.bind(this, i)
      });

    const gridContainerWidth = this.props.fullWidth
      ? { xs: 12 }
      : { xs: 12, sm: 10, md: 8 };
    return (
      <Grid container>
        <Grid item {...gridContainerWidth}>
          {rootState.plots.map((plot, i) => (
            <div key={`plot-${i}`}>
              <Typography variant="h5" gutterBottom>
                <b>Plot {i + 1}</b>
              </Typography>
              <TextField label="Code" {...commonTextFieldProps("code", i)} />
              <TextField
                label="Description"
                {...commonTextFieldProps("description", i)}
              />
              {!disableInformalCluster && (
                <Grid container spacing={16}>
                  <Grid item xs={9}>
                    <SelectInput
                      label="Informal Cluster (Optional)"
                      menuArr={this.state.clusters.map(cluster => ({
                        label: cluster.code,
                        value: cluster.id
                      }))}
                      {...commonSelectFieldProps("clusterId", i)}
                    />
                  </Grid>
                  <Grid item xs={3} style={{ justifyContent: "center" }}>
                    <Button
                      onClick={() => {
                        onInputChange(i, {
                          target: { value: "", name: "clusterId" }
                        });
                      }}
                    >
                      REMOVE
                    </Button>
                  </Grid>
                </Grid>
              )}
              <SelectInput
                {...Object.assign(
                  {
                    menuArr: [
                      { label: "FSS", value: "fss" },
                      { label: "LMF", value: "lmf" }
                    ],
                    label: "Type"
                  },
                  commonLocationFieldProps("type", i)
                )}
              />
              <SelectInput
                {...Object.assign(
                  {
                    menuArr:
                      rootState.plots[i]["type"] === "fss"
                        ? [
                            { label: "Financing", value: "financing" },
                            { label: "Growership", value: "growership" },
                            { label: "Direct Buying", value: "directBuying" }
                          ]
                        : [
                            { label: "In-house", value: "inhouse" },
                            { label: "Lease", value: "lease" }
                          ],
                    label: "Engagement Scheme"
                  },
                  commonLocationFieldProps("engagementScheme", i)
                )}
              />

              <SelectInput
                {...Object.assign(
                  {
                    menuArr: this.state.plotLocation[i].countryArr,
                    label: "Country"
                  },
                  commonLocationFieldProps("country", i)
                )}
              />

              <SelectInput
                {...Object.assign(
                  {
                    menuArr: this.state.plotLocation[i].regionArr,
                    label: "Region"
                  },
                  commonLocationFieldProps("region", i)
                )}
              />

              <SelectInput
                {...Object.assign(
                  {
                    menuArr: this.state.plotLocation[i].provinceArr,
                    label: "Province"
                  },
                  commonLocationFieldProps("province", i)
                )}
              />

              <SelectInput
                {...Object.assign(
                  {
                    menuArr: this.state.plotLocation[i].municipalityArr,
                    label: "Municipality"
                  },
                  commonLocationFieldProps("municipality", i)
                )}
              />

              <SelectInput
                {...Object.assign(
                  {
                    menuArr: this.state.plotLocation[i].barangayArr,
                    label: "Barangay"
                  },
                  commonLocationFieldProps("barangay", i)
                )}
              />

              <TextField label="Purok" {...commonTextFieldProps("purok", i)} />

              <SelectInput
                {...Object.assign(
                  {
                    menuArr: registrationParams.tenureStatus,
                    label: "Tenure Status"
                  },
                  commonSelectFieldProps("tenure", i)
                )}
              />

              <SelectInput
                {...Object.assign(
                  {
                    menuArr: registrationParams.cropType,
                    label: "Commodity"
                  },
                  commonSelectFieldProps("commodity", i)
                )}
              />

              {rootState.plots[i].commodity === "others" && (
                <TextField
                  label="Specify Commodity"
                  {...commonTextFieldProps("commodityOthers", i)}
                />
              )}

              <TextField
                label="Total Area"
                step={0.01}
                type="number"
                {...commonTextFieldProps("totalArea", i)}
              />

              <TextField
                label="Plantable Area"
                step={0.01}
                type="number"
                {...commonTextFieldProps("plantableArea", i)}
              />

              <SelectInput
                {...Object.assign(
                  {
                    menuArr: registrationParams.waterSystem,
                    label: "Water System"
                  },
                  commonSelectFieldProps("waterSystem", i)
                )}
              />

              <TextField
                label="Coordinates"
                {...commonTextFieldProps("coordinates", i)}
              />
              <Grid container justify="space-evenly" spacing={16}>
                <Grid item xs={12} sm={4}>
                  <TextField
                    label="Previous Buyer"
                    {...commonTextFieldProps("prevBuyer", i)}
                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <TextField
                    label="Previous Price"
                    type="number"
                    {...commonTextFieldProps("prevPrice", i)}
                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <SelectInput
                    {...Object.assign(
                      {
                        menuArr: registrationParams.modeOfPayment,
                        label: "Payment Mode"
                      },
                      commonSelectFieldProps("prevPaymentMode", i)
                    )}
                  />
                </Grid>
              </Grid>

              <SelectInput
                {...Object.assign(
                  {
                    menuArr: registrationParams.financialSource,
                    label: "Source of Financing"
                  },
                  commonSelectFieldProps("sourceFinance", i)
                )}
              />

              <SelectInput
                {...Object.assign(
                  {
                    menuArr: registrationParams.cropInsurance,
                    label: "Crop Insurance"
                  },
                  commonSelectFieldProps("cropInsurance", i)
                )}
              />
              {plot.cropInsurance && (
                <TextField
                  label="Crop Insurance Details"
                  {...commonTextFieldProps("cropInsuranceDetails", i)}
                />
              )}
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between"
                }}
              >
                {rootState.plots.length > 1 ? (
                  <Button
                    variant="outlined"
                    onClick={this.onRemovePlot.bind(this, i)}
                  >
                    Remove Plot {i + 1}
                  </Button>
                ) : (
                  <div />
                )}
                {i === rootState.plots.length - 1 ? (
                  <Button
                    variant="outlined"
                    color="primary"
                    onClick={this.onAddPlot.bind(this)}
                  >
                    Add Plot
                  </Button>
                ) : null}
              </div>
            </div>
          ))}
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(RegisterLandStep);
