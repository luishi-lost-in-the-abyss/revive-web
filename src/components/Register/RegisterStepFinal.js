import React from "react";
import {
  Typography,
  Grid,
  TextField,
  CircularProgress
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../theme";
import ReviewApplicationTable from "./ReviewApplicationTable";
import FeathersClient from "../../api/FeathersClient";

class RegisterStepFinal extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true, barangays: {} };
  }
  getHelperText(name) {
    const { errors, errorType } = this.props;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";
    if (errorType === "passwordMatch") return "Passwords do not match";
    if (errorType === "duplicateUser") return "Username already in use";

    return null;
  }

  async componentDidMount() {
    const client = FeathersClient.getClient();
    //fetch all the relevant barangays etc.
    const barangayIds = this.props.rootState.plots.map(
      ({ barangay }) => barangay
    );
    const barangayArr = await client
      .service("location-barangay")
      .find({ query: { $limit: "-1", id: { $in: barangayIds } } });
    const barangays = {};
    barangayArr.map(obj => {
      barangays[obj.id] = obj;
    });
    this.setState({ barangays, loading: false });
  }

  render() {
    const {
      classes,
      onInputChange,
      rootState,
      registrationParams,
      errors,
      errorType
    } = this.props;
    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: onInputChange,
      value: rootState[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });
    if (this.state.loading) {
      return <CircularProgress />;
    }
    return (
      <React.Fragment>
        <ReviewApplicationTable
          {...rootState}
          registrationParams={registrationParams}
          barangays={this.state.barangays}
        />
        <Grid container>
          <Grid item xs={12}>
            <Typography
              variant="h4"
              gutterBottom
              style={{ marginTop: 20, marginBottom: 20 }}
            >
              Setup User Account
            </Typography>
          </Grid>
          <Grid item xs={12} sm={10} md={8}>
            <TextField
              label="Username"
              name="username"
              {...commonTextFieldProps("username")}
            />
            <TextField
              label="Email"
              name="email"
              helperText="Optional"
              {...commonTextFieldProps("email")}
            />
            <TextField
              label="Password"
              name="password"
              value={rootState.password}
              type="password"
              helperText="Minimum length 8 characters"
              {...commonTextFieldProps("password")}
            />
            <TextField
              label="Confirm Password"
              name="confirmPassword"
              value={rootState.confirmPassword}
              type="password"
              helperText="Please re-enter your password above"
              {...commonTextFieldProps("confirmPassword")}
            />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(RegisterStepFinal);
