import React, { ReactDOM } from "react";
import dynamic from "next/dynamic";
import { Typography, Grid, TextField, Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../theme";
import SelectInput from "../common/SelectInput";
import { DOWNLOAD_SERVER } from "../../api/EndPoints";
const MuiPhoneInput = dynamic(import("material-ui-phone-number"), {
  ssr: false
});
class RegisterFarmerStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = { labelWidth: 0 };
    this.timestamp = new Date().getTime();
  }

  componentDidMount() {
    this.setState({
      labelWidth: document.getElementById("register-civil-label").offsetWidth
    });
  }

  getHelperText(name) {
    const { errors, errorType } = this.props;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";

    return null;
  }

  render() {
    const {
      classes,
      onInputChange,
      rootState,
      errors,
      errorType,
      registrationParams
    } = this.props;
    const { labelWidth } = this.state;
    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: onInputChange,
      name,
      value: rootState[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });
    const noVerticalPadding = { paddingBottom: 0, paddingTop: 0 };
    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <Grid container justify="space-evenly" spacing={16}>
            <Grid item xs={4}>
              <TextField
                label="First Name"
                value={rootState.firstName}
                {...commonTextFieldProps("firstName")}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                label="Middle Name"
                value={rootState.middleName}
                {...commonTextFieldProps("middleName")}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                label="Last Name"
                value={rootState.lastName}
                {...commonTextFieldProps("lastName")}
              />
            </Grid>
          </Grid>
          <Grid container justify="space-evenly" spacing={16}>
            <Grid item xs={12} sm={4} style={noVerticalPadding}>
              <TextField
                label="Birthdate"
                type="date"
                value={rootState.birthdate}
                InputLabelProps={{
                  shrink: true
                }}
                {...commonTextFieldProps("birthdate")}
              />
            </Grid>
            <Grid item xs={12} sm={4} style={noVerticalPadding}>
              <TextField
                label="Occupation"
                value={rootState.occupation}
                {...commonTextFieldProps("occupation")}
              />
            </Grid>
            <Grid item xs={12} sm={4} style={noVerticalPadding}>
              <SelectInput
                {...{
                  id: "register-civil",
                  name: "civil",
                  onInputChange,
                  value: rootState.civil,
                  menuArr: registrationParams.civilStatus,
                  error: errors["civil"],
                  helperText: this.getHelperText("civil"),
                  label: "Civil Status"
                }}
              />
            </Grid>
          </Grid>

          <Grid container spacing={16}>
            {/*
            <Grid item xs={2} style={noVerticalPadding}>
              <TextField
                disabled={true}
                {...Object.assign({}, commonTextFieldProps("prefix"), {
                  value: "+63"
                })}
              />
            </Grid> */}
            <Grid item xs={12} md={4} style={noVerticalPadding}>
              <MuiPhoneInput
                variant="outlined"
                enableLongNumbers={true}
                onlyCountries={["ph"]}
                countryCodeEditable={false}
                defaultCountry="ph"
                label="Contact Number"
                value={rootState.contact}
                {...Object.assign({}, commonTextFieldProps("contact"), {
                  onChange: value => {
                    onInputChange({ target: { name: "contact", value } });
                  }
                })}
              />
            </Grid>
            <Grid item xs={12} md={4} style={noVerticalPadding}>
              <SelectInput
                {...{
                  id: "register-operator",
                  name: "operator",
                  onInputChange,
                  value: rootState.operator,
                  menuArr: registrationParams.mobileOperators,
                  error: errors["operator"],
                  helperText: this.getHelperText("operator"),
                  label: "Operator"
                }}
              />
            </Grid>
            <Grid item xs={12} md={4} style={noVerticalPadding}>
              <SelectInput
                {...{
                  id: "register-gender",
                  name: "gender",
                  onInputChange,
                  value: rootState.gender,
                  menuArr: [
                    { label: "Male", value: "m" },
                    { label: "Female", value: "f" }
                  ],
                  error: errors["gender"],
                  helperText: this.getHelperText("gender"),
                  label: "Gender"
                }}
              />
            </Grid>
          </Grid>
          <TextField
            label="Residential Address (Province, Municipality, Barangay, Sitio)"
            value={rootState.residentAddress}
            helperText="(Province, Municipality, Barangay, Sitio)"
            {...commonTextFieldProps("residentAddress")}
          />

          <Grid container justify="space-evenly" spacing={16}>
            <Grid item xs={12} style={noVerticalPadding}>
              <TextField
                label="Bank Name"
                value={rootState.bankName}
                {...commonTextFieldProps("bankName")}
              />
            </Grid>

            <Grid item xs={12} style={noVerticalPadding}>
              <TextField
                label="TIN Number"
                value={rootState.bankTinNo}
                {...commonTextFieldProps("bankTinNo")}
              />
            </Grid>

            <Grid item xs={12} sm={6} style={noVerticalPadding}>
              <TextField
                label="Account Type"
                value={rootState.bankAccType}
                {...commonTextFieldProps("bankAccType")}
              />
            </Grid>
            <Grid item xs={12} sm={6} style={noVerticalPadding}>
              <TextField
                label="Account Name"
                value={rootState.bankAccName}
                {...commonTextFieldProps("bankAccName")}
              />
            </Grid>
          </Grid>
          <Grid container justify="center">
            <Grid item xs={12} sm={4}>
              <img
                id="sidebar-profile-image"
                src={
                  rootState.file
                    ? rootState.file.toDataURL()
                    : `${DOWNLOAD_SERVER}/farmer/${
                        rootState.id ? rootState.id : 0
                      }.jpg?timestamp=${this.timestamp}`
                }
                onError={e => {
                  e.target.onerror = null;
                  e.target.src = "/static/images/id-card.png";
                }}
                style={{ width: "100%" }}
              />
              <Button
                fullWidth
                variant="outlined"
                onClick={this.props.onClickUpload}
              >
                Upload Farmer ID
              </Button>
            </Grid>
          </Grid>
          <Typography variant="h5" gutterBottom>
            <b>Health</b>
          </Typography>
          <Typography>
            Leave blank if there are no health conditions.
          </Typography>
          <SelectInput
            {...{
              id: "register-medical",
              name: "healthConditions",
              onInputChange,
              value: rootState.healthConditions,
              menuArr: registrationParams.medicalConditions,
              error: errors["medicalConditions"],
              helperText: this.getHelperText("medical"),
              label: "Health Conditions",
              multiple: true
            }}
          />
          {rootState.healthConditions.indexOf("others") > -1 ? (
            <TextField
              label="Other Conditions"
              value={rootState.healthOthers}
              {...commonTextFieldProps("healthOthers")}
            />
          ) : null}
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(RegisterFarmerStep);
