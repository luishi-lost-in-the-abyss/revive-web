import React, { ReactDOM } from "react";
import { Typography, Grid, TextField, Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../theme";
import SelectInput from "../common/SelectInput";
import FeathersClient from "../../api/FeathersClient";

const noVerticalPadding = { paddingBottom: 0, paddingTop: 0 };

class RegisterAccountStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = { labelWidth: 0, clusters: [] };
  }

  async componentDidMount() {
    const client = FeathersClient.getClient();
    const clusters = await client
      .service("clusters")
      .find({ query: { $limit: "-1" } });
    this.setState({
      clusters: clusters
        .filter(({ formal_cluster }) => !!formal_cluster)
        .map(cluster => ({
          label: cluster.code,
          value: cluster.formal_cluster.id
        }))
    });
  }

  getHelperText(name) {
    const { errors, errorType } = this.props;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";

    return null;
  }
  render() {
    const {
      classes,
      registrationParams,
      onInputChange,
      rootState,
      errors,
      errorType
    } = this.props;

    const { labelWidth, clusters } = this.state;
    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <SelectInput
            {...{
              id: "register-type",
              name: "type",
              onInputChange,
              value: rootState.type,
              menuArr: [
                { label: "FSS", value: "fss" },
                { label: "LMF", value: "lmf" }
              ],
              error: errors["type"],
              helperText: this.getHelperText("type"),
              label: "Main Participation Type"
            }}
          />

          <SelectInput
            {...{
              id: "register-isCluster",
              name: "isCluster",
              onInputChange,
              value: rootState.isCluster,
              menuArr: [
                { label: "No", value: false },
                { label: "Yes", value: true }
              ],
              error: errors["isCluster"],
              helperText: this.getHelperText("isCluster"),
              label: "Part of Formal Cluster?"
            }}
          />
          {rootState.isCluster ? (
            <SelectInput
              {...{
                id: "register-cluster",
                name: "cluster",
                onInputChange,
                value: rootState.cluster,
                menuArr: clusters,
                error: errors["cluster"],
                helperText: this.getHelperText("cluster"),
                label: "Formal Cluster Code"
              }}
            />
          ) : null}
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(RegisterAccountStep);
