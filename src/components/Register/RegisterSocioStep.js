import React, { ReactDOM } from "react";
import { Typography, Grid, TextField, Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../theme";
import SelectInput from "../common/SelectInput";
import Questionnaire from "../socio/Questionnaire";

const noVerticalPadding = { paddingBottom: 0, paddingTop: 0 };

class RegisterSocioStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = { labelWidth: 0 };
  }

  componentDidMount() {}

  getHelperText(name) {
    const { errors, errorType } = this.props;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";

    return null;
  }

  getHelperTextChild(name, i) {
    const { errors, errorType } = this.props;
    const beneficiaryErrs = errors.beneficiary;
    if (!beneficiaryErrs || !beneficiaryErrs[i] || !beneficiaryErrs[i][name])
      return null;
    if (errorType === "blank") return "This field cannot be left blank.";

    return null;
  }

  render() {
    const {
      classes,
      onInputChange,
      rootState,
      errors,
      errorType,
      onChildInputChange,
      onAddChild,
      onRemoveChild
    } = this.props;

    const addChildButton = (
      <Button variant="outlined" color="primary" onClick={onAddChild}>
        Add Beneficiary
      </Button>
    );
    const { labelWidth } = this.state;
    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: onInputChange,
      name,
      value: rootState["socio"][name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    const childFieldProps = (name, i) =>
      Object.assign({}, commonTextFieldProps(name), {
        onChange: onChildInputChange.bind(null, i),
        value: rootState.beneficiary[i][name],
        error:
          errors.beneficiary &&
          errors.beneficiary[i] &&
          errors.beneficiary[i][name],
        helperText: this.getHelperTextChild(name, i)
      });
    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <Questionnaire
            state={rootState.socio}
            errors={errors}
            onInputChange={onInputChange}
          />
          <Grid container spacing={16}>
            {rootState.beneficiary.length === 0 ? (
              <Grid item xs={12}>
                <Typography variant="h5" gutterBottom>
                  <b>Beneficiary</b>
                </Typography>
                <div style={{ display: "flex", justifyContent: "flex-end" }}>
                  {addChildButton}
                </div>
              </Grid>
            ) : (
              rootState.beneficiary.map((child, i) => (
                <Grid item xs={12} key={`child-${i}`}>
                  <Typography variant="h5" gutterBottom>
                    <b>Beneficiary {i + 1}</b>
                  </Typography>
                  <Grid container spacing={16}>
                    <Grid item sm={12} md={6} style={noVerticalPadding}>
                      <TextField label="Name" {...childFieldProps("name", i)} />
                    </Grid>
                    <Grid item sm={12} md={6} style={noVerticalPadding}>
                      <TextField
                        label="Education"
                        {...childFieldProps("education", i)}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between"
                        }}
                      >
                        <Button
                          variant="outlined"
                          onClick={onRemoveChild.bind(null, i)}
                        >
                          Remove Beneficiary {i + 1}
                        </Button>
                        {i === rootState.beneficiary.length - 1
                          ? addChildButton
                          : null}
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
              ))
            )}
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(RegisterSocioStep);
