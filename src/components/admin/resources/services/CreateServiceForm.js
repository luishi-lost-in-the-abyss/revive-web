import React from "react";
import Link from "next/link";
import {
  Grid,
  Button,
  TextField,
  withStyles,
  Typography,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction
} from "@material-ui/core";
import SelectInput from "../../../common/SelectInput";
import { styles } from "../../../../theme";
import FeathersClient from "../../../../api/FeathersClient";
import ServiceCategoryBrowser from "../service_category/ServiceCategoryBrowser";
import DataTable from "../../../common/DataTable";
import VerticalTable from "../../../common/VerticalTable";

class CreateServiceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: "",
      name: "",
      description: "",
      price: 0,
      equipment: {},
      categoryId: "",
      errors: {},
      errorType: "none"
    };

    if (props.editMode) {
      const { service } = props;
      service.equipment.map(equip => {
        this.state.equipment[equip.id] = equip;
      });
      this.state.code = service.code;
      this.state.name = service.name;
      this.state.description = service.description;
      this.state.price = parseInt(service.price);
      this.state.categoryId = service.serviceCategoryId;
    }

    this.onCategoryChange = this.onCategoryChange.bind(this);
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  onCategoryChange(id) {
    this.setState({ categoryId: id });
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";
    return null;
  }

  selectEquipment(equipObj) {
    const { equipment } = this.state;
    if (equipment[equipObj.id]) {
      const { [equipObj.id]: toRemove, ...finalObj } = equipment;
      this.setState({ equipment: finalObj });
    } else {
      this.setState({
        equipment: Object.assign({}, equipment, { [equipObj.id]: equipObj })
      });
    }
  }

  onSubmit() {
    const {
      categoryId,
      code,
      name,
      equipment,
      description,
      price
    } = this.state;
    if (!name || !code || Object.keys(equipment).length === 0) {
      this.setState({
        errors: {
          name: !name,
          code: !code,
          equipment: Object.keys(equipment).length === 0
        },
        errorType: "blank"
      });
      return;
    }
    this.setState({ errors: {} });
    this.props.onSubmit({
      code,
      name,
      price,
      equipment: Object.keys(equipment),
      description,
      categoryId
    });
  }

  render() {
    const { classes, submitError, editMode, userRole } = this.props;
    const { equipment, errors } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <form>
            <ServiceCategoryBrowser
              onCategoryChange={this.onCategoryChange}
              {...(editMode
                ? { initialPath: this.props.service.parent_category }
                : {})}
            />
            <TextField
              label="Code"
              {...Object.assign(
                {},
                commonTextFieldProps("code"),
                submitError
                  ? { error: true, helperText: "Duplicated service code" }
                  : null
              )}
            />
            <TextField label="Name" {...commonTextFieldProps("name")} />
            <TextField
              multiline
              rows={4}
              rowsMax={4}
              label="Description"
              {...commonTextFieldProps("description")}
            />
            <TextField
              disabled={this.props.disablePrice}
              label="Price / ha"
              {...commonTextFieldProps("price")}
              type="number"
            />

            <Typography variant="h6" gutterBottom>
              Selected Equipment
            </Typography>
            {errors.equipment && Object.keys(equipment).length === 0 ? (
              <Typography color="error">
                Service requires at least one equipment added!
              </Typography>
            ) : null}
            <List>
              {Object.values(equipment).map(equip => (
                <ListItem key={`selectedequipment-${equip.id}`}>
                  <ListItemText primary={equip.name} />
                  <ListItemSecondaryAction>
                    <Button
                      variant="outlined"
                      onClick={this.selectEquipment.bind(this, equip)}
                    >
                      Remove
                    </Button>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </List>

            <Typography variant="h6" gutterBottom>
              Equipment Browser
            </Typography>
            <DataTable
              colWidths={[400, null, 150]}
              service="equipment"
              dataCols={[
                { content: "Equipment Name", data: "name" },
                { content: "Description", data: "description" },
                {
                  content: "Actions",
                  data: null,
                  render: equip => {
                    return (
                      <Button
                        variant="outlined"
                        onClick={this.selectEquipment.bind(this, equip)}
                      >
                        {equipment[equip.id] ? "Remove" : "Add"}
                      </Button>
                    );
                  }
                }
              ]}
              searchCategory={[{ label: "Name", value: "name" }]}
              defaultSearchCategory="name"
            />

            <Grid container justify="space-between" style={{ marginTop: 20 }}>
              <Grid item>
                <Link
                  href={`/${userRole ? userRole : "admin"}/resources/services${
                    editMode ? `/service?id=${this.props.service.id}` : ""
                  }`}
                >
                  <Button color="secondary" variant="outlined">
                    Cancel
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={this.onSubmit.bind(this)}
                >
                  {editMode ? "Edit" : "Create"}
                </Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(CreateServiceForm);
