import React from "react";
import {} from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
export default ({ service }) => {
  return (
    <React.Fragment>
      <VerticalTable
        data={service}
        tableValues={[
          {
            label: "Category",
            key: "parent_category",
            render: arr => arr.map(({ name }) => name).join(" > ")
          },
          { label: "Code", key: "code" },
          { label: "Name", key: "name" },
          {
            label: "Description",
            key: "description"
          }
        ]}
      />
    </React.Fragment>
  );
};
