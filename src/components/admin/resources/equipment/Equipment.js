import React from "react";
import {} from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
export default ({ equipment }) => (
  <React.Fragment>
    <VerticalTable
      data={equipment}
      tableValues={[
        { label: "Name", key: "name" },
        {
          label: "Description",
          key: "description"
        }
      ]}
    />
  </React.Fragment>
);
