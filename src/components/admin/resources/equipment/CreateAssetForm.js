import React from "react";
import Link from "next/link";
import {
  Grid,
  Button,
  TextField,
  withStyles,
  Typography
} from "@material-ui/core";
import SelectInput from "../../../common/SelectInput";
import { styles } from "../../../../theme";
import FeathersClient from "../../../../api/FeathersClient";

class CreateAssetForm extends React.Component {
  constructor(props) {
    super(props);
    const { equipmentId } = props;
    this.state = {
      serial: "",
      fecId: "",
      equipmentId: equipmentId ? equipmentId : "",
      fixedEquipment: !!equipmentId,
      equipmentArr: [],
      fecArr: [],
      equipment: {},
      errors: {},
      errorType: "none"
    };

    if (props.editMode) {
      const { asset } = props;
      this.state.serial = asset.serial;
      this.state.fecId = asset.fecId;
    }
  }

  async componentDidMount() {
    const { userRole, equipmentId, editMode } = this.props;
    const client = FeathersClient.getClient();
    const equipment = equipmentId
      ? await client.service("equipment").get(equipmentId)
      : null;
    let equipmentArr = [];
    if (!equipment) {
      equipmentArr = await client
        .service("equipment")
        .find({ query: { $limit: "-1" } });
    } else {
      equipmentArr.push(equipment);
    }

    const isDispatcherAndNew = userRole === "dispatcher" && !editMode;
    const fecArr = await client.service("fec").find({
      query: Object.assign(
        { $limit: "-1" },
        isDispatcherAndNew ? { getOwnFec: true } : {}
      )
    });
    this.setState({
      equipmentArr: equipment ? equipmentArr : equipmentArr,
      equipment: equipment ? equipment : {},
      fecId: isDispatcherAndNew ? fecArr[0].id : this.state.fecId,
      fecArr
    });
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";
    return null;
  }

  onSubmit() {
    const { serial, equipmentId, fecId } = this.state;
    if (!equipmentId || !serial || !fecId) {
      this.setState({
        errors: { serial: !serial, equipmentId: !equipmentId, fecId: !fecId },
        errorType: "blank"
      });
      return;
    }
    this.setState({ errors: {} });
    this.props.onSubmit({ serial, equipmentId, fecId });
  }

  render() {
    const { classes, submitError, editMode, userRole } = this.props;
    const { fixedEquipment, equipmentArr, fecArr, errors } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <form>
            <SelectInput
              {...Object.assign({}, commonTextFieldProps("equipmentId"), {
                id: "equipmentasset-equipment",
                label: "Equipment",
                onInputChange: this.onInputChange.bind(this),
                disabled: fixedEquipment || editMode,
                menuArr: equipmentArr.map(equip => ({
                  label: equip.name,
                  value: equip.id
                }))
              })}
            />
            <SelectInput
              {...Object.assign({}, commonTextFieldProps("fecId"), {
                id: "equipmentasset-fec",
                label: "FEC",
                onInputChange: this.onInputChange.bind(this),
                menuArr: fecArr.map(fec => ({
                  label: fec.name,
                  value: fec.id
                }))
              })}
            />
            <TextField
              label="Serial"
              disabled={editMode}
              {...Object.assign(
                {},
                commonTextFieldProps("serial"),
                submitError
                  ? { error: true, helperText: "Duplicate serial number" }
                  : null
              )}
            />

            <Grid container justify="space-between">
              <Grid item>
                <Link
                  href={
                    editMode
                      ? `/${
                          userRole ? userRole : "admin"
                        }/resources/equipment/asset?id=${this.state.serial}`
                      : `/${userRole ? userRole : "admin"}/resources/equipment${
                          fixedEquipment
                            ? `/equipment?id=${this.state.equipmentId}`
                            : ""
                        }`
                  }
                >
                  <Button color="secondary" variant="outlined">
                    Cancel
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={this.onSubmit.bind(this)}
                >
                  {editMode ? "Edit" : "Create"}
                </Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(CreateAssetForm);
