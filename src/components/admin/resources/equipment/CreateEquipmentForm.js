import React from "react";
import Link from "next/link";
import {
  Grid,
  Button,
  TextField,
  withStyles,
  Typography
} from "@material-ui/core";
import SelectInput from "../../../common/SelectInput";
import { styles } from "../../../../theme";
import FeathersClient from "../../../../api/FeathersClient";

class CreateEquipmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      description: "",
      errors: {},
      errorType: "none"
    };

    if (props.editMode) {
      const { equipment } = props;
      this.state.name = equipment.name;
      this.state.description = equipment.description;
    }
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";
    return null;
  }

  onSubmit() {
    const { name, description } = this.state;
    if (!name) {
      this.setState({
        errors: { name: !name },
        errorType: "blank"
      });
      return;
    }
    this.setState({ errors: {} });
    this.props.onSubmit({ name, description });
  }

  render() {
    const { classes, editMode, userRole } = this.props;
    const { errors } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <form>
            <TextField label="Name" {...commonTextFieldProps("name")} />
            <TextField
              multiline
              rows={4}
              rowsMax={4}
              label="Description"
              {...commonTextFieldProps("description")}
            />

            <Grid container justify="space-between">
              <Grid item>
                <Link
                  href={`/${userRole ? userRole : "admin"}/resources/equipment${
                    editMode ? `/equipment?id=${this.props.equipment.id}` : ""
                  }`}
                >
                  <Button color="secondary" variant="outlined">
                    Cancel
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={this.onSubmit.bind(this)}
                >
                  {editMode ? "Edit" : "Create"}
                </Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(CreateEquipmentForm);
