import React from "react";
import Link from "next/link";
import {
  Grid,
  Button,
  TextField,
  withStyles,
  Typography
} from "@material-ui/core";
import { styles } from "../../../../theme";

class CreateAssetMaintenanceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date().toISOString().split("T")[0],
      endDate: new Date().toISOString().split("T")[0],
      errors: {},
      errorType: "none"
    };
  }

  async componentDidMount() {}

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";
    if (errorType === "dateRange")
      return "Start date cannot be after the end date.";
    return null;
  }

  onSubmit() {
    const { startDate, endDate } = this.state;
    if (!startDate || !endDate) {
      this.setState({
        errors: { startDate: !startDate, endDate: !endDate },
        errorType: "blank"
      });
      return;
    }
    const start = new Date(startDate),
      end = new Date(endDate);
    if (end.getTime() < start.getTime()) {
      this.setState({
        errors: { startDate: true, endDate: true },
        errorType: "dateRange"
      });
      return;
    }
    this.setState({ errors: {} });
    this.props.onSubmit({ startDate: start, endDate: end });
  }

  render() {
    const { classes, submitError, assetId, userRole } = this.props;
    const { errors } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <form>
            <TextField
              label="Start Date"
              type="date"
              {...commonTextFieldProps("startDate")}
            />
            <TextField
              label="End Date"
              type="date"
              {...commonTextFieldProps("endDate")}
            />

            <Grid container justify="space-between">
              <Grid item>
                <Link
                  href={`/${
                    userRole ? userRole : "admin"
                  }/resources/equipment/asset?id=${assetId}`}
                >
                  <Button color="secondary" variant="outlined">
                    Cancel
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={this.onSubmit.bind(this)}
                >
                  Create
                </Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(CreateAssetMaintenanceForm);
