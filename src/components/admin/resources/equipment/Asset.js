import React from "react";
import {} from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
import { Interval, DateTime } from "luxon";
export default ({ asset }) => (
  <React.Fragment>
    <VerticalTable
      data={asset}
      tableValues={[
        { label: "Serial", key: "serial" },
        {
          label: "Current Condition",
          key: "conditionText"
        },
        {
          label: "FEC",
          key: "fec",
          render: fec => fec.name
        },
        {
          label: "Equipment Type",
          key: "equipment",
          render: equipment => equipment.name
        }
      ]}
    />
  </React.Fragment>
);
