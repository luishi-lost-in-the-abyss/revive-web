import {
  Grid,
  Typography,
  TextField,
  withStyles,
  Button,
  InputAdornment,
  IconButton,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";
import { styles } from "../../../../theme";
import CancelIcon from "@material-ui/icons/Cancel";
import FeathersClient from "../../../../api/FeathersClient";
import SelectInput from "../../../common/SelectInput";

const initialState = {
  maintenanceOnly: false,
  fecId: "",
  equipmentId: "",
  forceRefresh: new Date().getTime()
};

class AssetDashboardFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = Object.assign({}, initialState);
    this.onInputChange = this.onInputChange.bind(this);
    this.onCheckChange = this.onCheckChange.bind(this);
    this.resetAll = this.resetAll.bind(this);
    this.applyFilters = this.applyFilters.bind(this);
    this.fecArr = [];
    this.equipmentArr = [];
  }

  async componentDidMount() {
    const client = FeathersClient.getClient();
    this.fecArr = await client.service("fec").find({ query: { $limit: "-1" } });
    this.equipmentArr = await client
      .service("equipment")
      .find({ query: { $limit: "-1" } });
    this.setState({ forceRefresh: new Date().getTime() });
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }
  onCheckChange(evt) {
    this.setState({ [evt.target.name]: evt.target.checked });
  }

  resetField(name) {
    this.setState({ [name]: "" });
  }

  resetAll() {
    this.setState(Object.assign({}, initialState));
    this.props.applyFilters(null);
  }

  applyFilters() {
    const findQuery = {};
    const { maintenanceOnly, fecId, equipmentId } = this.state;
    if (maintenanceOnly) {
      findQuery.maintenanceOnly = true;
    }
    if (fecId) {
      findQuery.fecId = fecId;
    }
    if (equipmentId) {
      findQuery.equipmentId = equipmentId;
    }
    this.props.applyFilters(findQuery);
  }

  render() {
    const { classes } = this.props;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange,
      name,
      value: this.state[name],
      InputLabelProps: { shrink: true },
      InputProps: {
        endAdornment: (
          <InputAdornment position="end">
            <IconButton onClick={this.resetField.bind(this, name)}>
              <CancelIcon />
            </IconButton>
          </InputAdornment>
        )
      }
    });
    return (
      <React.Fragment>
        <Typography variant="h5" gutterBottom>
          Filters
        </Typography>
        <Grid container spacing={8}>
          <Grid item xs={12}>
            <FormControlLabel
              control={
                <Checkbox
                  name="maintenanceOnly"
                  checked={this.state.maintenanceOnly}
                  onChange={this.onCheckChange}
                />
              }
              label="Show only assets under maintenance"
            />
          </Grid>
          <Grid item xs={6}>
            <SelectInput
              {...Object.assign({}, commonTextFieldProps("fecId"), {
                id: "assetdashboard-fec",
                label: "FEC",
                onInputChange: this.onInputChange,
                menuArr: this.fecArr.map(fec => ({
                  label: fec.name,
                  value: fec.id
                }))
              })}
            />
          </Grid>
          <Grid item xs={6}>
            <SelectInput
              {...Object.assign({}, commonTextFieldProps("equipmentId"), {
                id: "assetdashboard-equipment",
                label: "Equipment",
                onInputChange: this.onInputChange,
                menuArr: this.equipmentArr.map(equip => ({
                  label: equip.name,
                  value: equip.id
                }))
              })}
            />
          </Grid>
        </Grid>
        <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <div>
            <Button onClick={this.applyFilters}>Apply Filters</Button>
            <Button onClick={this.resetAll}>Reset All</Button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(AssetDashboardFilter);
