import React from "react";
import Router from "next/router";
import { Button, Typography } from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
import DataTable from "../../../common/DataTable";
export default ({ serviceCategory }) => {
  return (
    <React.Fragment>
      <VerticalTable
        data={serviceCategory}
        tableValues={[
          {
            label: "Parent Category",
            key: "parent_category",
            render: cat => {
              if (!cat) return "-";
              return cat.name;
            }
          },
          {
            label: "Category",
            key: "name"
          }
        ]}
      />
      <Typography variant="h6" gutterBottom style={{ marginTop: 20 }}>
        Sub Categories
      </Typography>
      <DataTable
        service="service-category"
        findQuery={{ parentCategoryId: serviceCategory.id }}
        disableSearch={true}
        reloadData={serviceCategory.id}
        dataCols={[
          { content: "Name", data: "name" },
          {
            content: "Actions",
            data: null,
            render: data => (
              <Button
                onClick={() => {
                  Router.push(
                    `/admin/resources/service_category/category?id=${data.id}`
                  );
                }}
              >
                View
              </Button>
            )
          }
        ]}
      />
      <Typography variant="h6" gutterBottom style={{ marginTop: 20 }}>
        Services
      </Typography>
      <DataTable
        offline={true}
        dataArr={serviceCategory.services}
        service="Services"
        disableSearch={true}
        reloadData={serviceCategory.id}
        dataCols={[
          { content: "Code", data: "code" },
          { content: "Name", data: "name" },
          { content: "Description", data: "description" },
          {
            content: "Actions",
            data: null,
            render: data => (
              <Button
                onClick={() => {
                  Router.push(
                    `/admin/resources/services/service?id=${data.code}`
                  );
                }}
              >
                View
              </Button>
            )
          }
        ]}
      />
    </React.Fragment>
  );
};
