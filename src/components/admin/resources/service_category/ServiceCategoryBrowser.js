import React from "react";
import Link from "next/link";
import { withStyles, Typography } from "@material-ui/core";
import SelectInput from "../../../common/SelectInput";
import { styles } from "../../../../theme";
import FeathersClient from "../../../../api/FeathersClient";

class ServiceCategoryBrowser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryId: "",
      category: [],
      editingCategoryId: "",
      path: [],
      errors: {},
      errorType: "none"
    };

    if (props.initialPath) {
      this.state.path = [null, ...props.initialPath];
      if (props.editingCategory) {
        this.state.editingCategoryId = this.state.path.pop().id;
      }
    }

    this.onSelectParent = this.onSelectParent.bind(this);
  }
  async componentDidMount() {
    if (this.props.initialPath) {
      this.getChildCategory(
        this.props.initialPath[this.props.initialPath.length - 1].id
      );
    } else {
      this.getChildCategory(null);
    }
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  async getChildCategory(id, current = null) {
    const { path, category, editingCategoryId } = this.state;
    if (id && !current) {
      current = category.reduce((a, b) => (a.id === id ? a : b), {});
    }
    const client = FeathersClient.getClient();
    const res = await client.service("service-category").find({
      query: Object.assign(
        { parentCategoryId: id, $limit: "-1" },
        this.props.editingCategory ? { id: { $ne: editingCategoryId } } : {}
      )
    });
    this.setState({
      categoryId: id,
      category: res,
      path: [...path, current]
    });
  }

  onSelectParent(evt) {
    const { value } = evt.target;
    this.setState({ categoryId: value }, () => {
      this.props.onCategoryChange(value);
      this.getChildCategory(value);
    });
  }

  goBackCategory(index) {
    const { path } = this.state;
    const cat = path[index];
    this.setState({ path: path.slice(0, index), categoryId: cat }, () => {
      this.props.onCategoryChange(cat ? cat.id : null);
      this.getChildCategory(cat ? cat.id : null, cat);
    });
  }

  render() {
    const { classes } = this.props;
    const { categoryId, path, category, errors } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name]
    });

    return (
      <React.Fragment>
        <Typography variant="h6" gutterBottom>
          Category:
        </Typography>
        <div
          style={{
            marginBottom: 20,
            display: "flex",
            alignItems: "center"
          }}
        >
          {path.map((p, i) => {
            return (
              <React.Fragment key={`path-${p ? p.id : "root"}`}>
                <Typography
                  variant="h5"
                  {...(p && p.id === categoryId
                    ? {}
                    : {
                        style: { cursor: "pointer" },
                        onClick: this.goBackCategory.bind(this, i)
                      })}
                >
                  {p ? p.name : "Home"}
                </Typography>
                {i !== path.length - 1 ? (
                  <Typography
                    variant="h5"
                    style={{ marginLeft: 5, marginRight: 5 }}
                  >
                    >
                  </Typography>
                ) : null}
              </React.Fragment>
            );
          })}
          {category.length > 0 && (
            <React.Fragment>
              <Typography
                variant="h5"
                style={{ marginLeft: 5, marginRight: 5 }}
              >
                >
              </Typography>
              <div style={{ width: 200 }}>
                <SelectInput
                  {...Object.assign(
                    {},
                    commonTextFieldProps("categoryId"),
                    {
                      id: "servicecategory-category",
                      label:
                        path.length === 1
                          ? "Service Type"
                          : path.length === 2
                          ? "Phase"
                          : "Category Browser",
                      onInputChange: this.onSelectParent,
                      menuArr: category.map(cat => ({
                        label: cat.name,
                        value: cat.id
                      }))
                    },
                    this.state.categoryId ? null : { value: "" }
                  )}
                />
              </div>
            </React.Fragment>
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(ServiceCategoryBrowser);
