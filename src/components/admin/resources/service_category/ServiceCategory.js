import React from "react";
import Router from "next/router";
import { Button, Typography } from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
import DataTable from "../../../common/DataTable";
export default ({ serviceCategory }) => {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom style={{ marginTop: 20 }}>
        Services
      </Typography>
      <DataTable
        offline={true}
        dataArr={serviceCategory.services}
        service="Services"
        disableSearch={true}
        reloadData={serviceCategory.id}
        dataCols={[
          { content: "Code", data: "code" },
          { content: "Name", data: "name" },
          { content: "Description", data: "description" },
          {
            content: "Actions",
            data: null,
            render: data => (
              <Button
                onClick={() => {
                  Router.push(
                    `/admin/resources/services/service?id=${data.id}`
                  );
                }}
              >
                View
              </Button>
            )
          }
        ]}
      />
    </React.Fragment>
  );
};
