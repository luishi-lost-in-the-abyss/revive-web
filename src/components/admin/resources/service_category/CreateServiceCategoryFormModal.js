import React from "react";
import Link from "next/link";
import {
  Grid,
  Button,
  TextField,
  withStyles,
  Typography,
  Dialog,
  DialogContent,
  DialogActions,
  DialogTitle
} from "@material-ui/core";
import SelectInput from "../../../common/SelectInput";
import { styles } from "../../../../theme";
import FeathersClient from "../../../../api/FeathersClient";
import ServiceCategoryBrowser from "./ServiceCategoryBrowser";
import ConfirmationPopup from "../../../common/ConfirmationPopup";

const initialState = {
  name: "",
  parentCategory: "",
  errors: {},
  errorType: "none",
  openConfirmation: false,
  popperPosition: null
};

class CreateServiceCategoryFormModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      parentCategory: "",
      errors: {},
      errorType: "none",
      editMode: null,
      openConfirmation: false,
      popperPosition: null
    };
    this.onClose = this.onClose.bind(this);
    this.toggleConfirmation = this.toggleConfirmation.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  static getDerivedStateFromProps(props, prevState) {
    const { parentCategory, editMode } = props;
    const results = { parentCategory, editMode };
    if (editMode && editMode !== prevState.editMode) {
      results.name = editMode.name;
    }
    return results;
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";
    return null;
  }

  onSubmit() {
    const { name, parentCategory } = this.state;
    if (!name) {
      this.setState({
        errors: { name: !name },
        errorType: "blank"
      });
      return;
    }
    this.onClose();
    this.props.onSubmit({
      edit: this.props.editMode,
      name,
      parentCategoryId: parentCategory ? parentCategory.id : null
    });
  }

  onClose() {
    this.setState(initialState);
    this.props.onClose();
  }

  onDelete() {
    this.onClose();
    this.props.onDelete();
  }

  toggleConfirmation(evt) {
    this.setState({
      openConfirmation: !this.state.openConfirmation,
      popperPosition: evt.target
    });
  }

  render() {
    const { classes, parentCategory, open, editMode } = this.props;
    const { errors, openConfirmation, popperPosition } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    return (
      <React.Fragment>
        <ConfirmationPopup
          dialogMode={true}
          open={openConfirmation}
          anchorEl={popperPosition}
          onClickAway={this.toggleConfirmation}
          title="Delete Category?"
          onCancel={this.toggleConfirmation}
          onConfirm={this.onDelete}
        />

        <Dialog open={open} onClose={this.onClose} fullWidth>
          <DialogTitle>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <div style={{ display: "flex" }}>
                {editMode ? "Edit " : "Create New "}
                {parentCategory
                  ? `${parentCategory.name} Phase`
                  : "Service Type"}
              </div>
              <div style={{ display: "flex" }}>
                {editMode && (
                  <Button
                    variant="outlined"
                    className={classes.endComponentButton}
                    onClick={this.toggleConfirmation}
                  >
                    Delete
                  </Button>
                )}
              </div>
            </div>
          </DialogTitle>
          <DialogContent>
            <Grid container style={{ marginTop: 10 }}>
              <Grid item xs={12}>
                <TextField
                  label={`${
                    parentCategory
                      ? `${parentCategory.name} Phase`
                      : "Service Type"
                  } Name`}
                  {...commonTextFieldProps("name")}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Grid container justify="space-between">
              <Grid item>
                <Button
                  color="secondary"
                  variant="outlined"
                  onClick={this.onClose}
                >
                  Cancel
                </Button>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={this.onSubmit.bind(this)}
                >
                  Create
                </Button>
              </Grid>
            </Grid>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(CreateServiceCategoryFormModal);
