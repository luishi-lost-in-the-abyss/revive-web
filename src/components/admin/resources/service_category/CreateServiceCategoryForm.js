import React from "react";
import Link from "next/link";
import {
  Grid,
  Button,
  TextField,
  withStyles,
  Typography
} from "@material-ui/core";
import SelectInput from "../../../common/SelectInput";
import { styles } from "../../../../theme";
import FeathersClient from "../../../../api/FeathersClient";
import ServiceCategoryBrowser from "./ServiceCategoryBrowser";

class CreateServiceCategoryForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      parentCategory: "",
      errors: {},
      errorType: "none"
    };

    if (props.editMode) {
      const { category } = props;
      this.state.name = category.name;
      this.state.parentCategory = category.parentCategoryId;
    }

    this.onCategoryChange = this.onCategoryChange.bind(this);
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }
  onCategoryChange(id) {
    this.setState({ parentCategory: id });
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";
    return null;
  }

  onSubmit() {
    const { name, parentCategory } = this.state;
    if (!name) {
      this.setState({
        errors: { name: !name },
        errorType: "blank"
      });
      return;
    }
    this.setState({ errors: {} });
    this.props.onSubmit({ name, parentCategoryId: parentCategory });
  }

  render() {
    const { classes, editMode } = this.props;
    const { errors } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <form>
            <ServiceCategoryBrowser
              onCategoryChange={this.onCategoryChange}
              {...(editMode
                ? {
                    initialPath: this.props.category.parent_category,
                    editingCategory: true
                  }
                : {})}
            />
            <TextField
              label="Category Name"
              {...commonTextFieldProps("name")}
            />

            <Grid container justify="space-between">
              <Grid item>
                <Link
                  href={`/admin/resources/service_category${
                    editMode ? `/category?id=${this.props.category.id}` : ""
                  }`}
                >
                  <Button color="secondary" variant="outlined">
                    Cancel
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={this.onSubmit.bind(this)}
                >
                  {editMode ? "Edit" : "Create"}
                </Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(CreateServiceCategoryForm);
