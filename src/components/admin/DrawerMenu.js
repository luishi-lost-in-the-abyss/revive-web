import React from "react";
import Link from "next/link";
import Router, { withRouter } from "next/router";
import { Divider, withStyles, Button } from "@material-ui/core";
import { styles } from "../../theme";
import LogoutButton from "../common/LogoutButton";
import DrawerMenuItem from "../common/DrawerMenuItem";

const DrawerMenu = ({ classes, router }) => {
  return (
    <div style={{ display: "flex", flexDirection: "column", height: "100%" }}>
      <Link passHref href="/admin">
        <a>
          <img
            src="/static/images/LOGO_2.jpg"
            style={{ width: "100%", padding: 10 }}
          />
        </a>
      </Link>
      <Divider />
      <DrawerMenuItem
        title="Users"
        path="/admin/registration"
        otherPaths={["/admin/users"]}
        items={[
          {
            title: "Pending Appli.",
            path: "/admin/registration/applications"
          },
          {
            title: "Participants",
            path: "/admin/users/participants"
          },
          {
            title: "FECs",
            path: "/admin/users/fecs"
          },
          {
            title: "Supervisors",
            path: "/admin/users/supervisors"
          },
          {
            title: "Technicians",
            path: "/admin/users/technicians"
          },
          {
            title: "Dispatchers",
            path: "/admin/users/dispatchers"
          },
          {
            title: "Clusters",
            path: "/admin/users/clusters"
          }
        ]}
      />
      <Divider />
      <DrawerMenuItem
        title="Agri-Ops"
        path="/admin/agri_ops"
        items={[
          {
            title: "Place Holder",
            path: "#"
          }
        ]}
      />
      <Divider />

      <DrawerMenuItem
        title="Bookings"
        path="/admin/bookings"
        items={[
          {
            title: "Dashboard",
            path: "/admin/bookings/dashboard"
          },
          {
            title: "View Bookings",
            path: "/admin/bookings/view"
          },
          {
            title: "Create Bookings",
            path: "/admin/bookings/create_booking"
          },
          {
            title: "Price Agreements",
            path: "/admin/bookings/price_agreements"
          },
          {
            title: "Asset Availability",
            path: "/admin/bookings/check_asset_availability"
          }
        ]}
      />
      <Divider />

      <DrawerMenuItem
        title="Resources"
        path="/admin/resources"
        items={[
          {
            title: "Service Types",
            path: "/admin/resources/service_category"
          },
          {
            title: "Services",
            path: "/admin/resources/services"
          },
          {
            title: "Equipment",
            path: "/admin/resources/equipment"
          },
          {
            title: "Assets Dashboard",
            path: "/admin/resources/asset_dashboard"
          }
        ]}
      />
      <Divider />

      <DrawerMenuItem
        title="Reports"
        path="/admin/reports"
        items={[
          {
            title: "Raw Data",
            path: "/admin/reports/raw_data"
          },
          {
            title: "Bookings",
            path: "/admin/reports/bookings"
          },
          {
            title: "Services",
            path: "/admin/reports/services"
          },
          { title: "FEC", path: "/admin/reports/fec" },
          { title: "Technicians", path: "/admin/reports/technician" },
          { title: "Supervisors", path: "/admin/reports/supervisor" },
          { title: "Municipals", path: "/admin/reports/municipality" }
        ]}
      />
      <Divider />

      <DrawerMenuItem
        title="Finance"
        path="/admin/finance"
        items={[
          {
            title: "Place Holder",
            path: "#"
          }
        ]}
      />
      <Divider />
      <div style={{ display: "flex", flex: 1 }} />
      <Button
        variant="outlined"
        onClick={() => {
          Router.push("/admin/my_account");
        }}
      >
        My Account
      </Button>
      <LogoutButton
        onLogout={() => {
          Router.push("/login");
        }}
      />
    </div>
  );
};

export default withRouter(withStyles(styles)(DrawerMenu));
