import { useState } from "react";
import { connect } from "react-redux";
import { Button, Grid, TextField } from "@material-ui/core";
import ReportActions from "../../../../../actions/ReportActions";
import { exportToCsv } from "../../../../../api/utility";
import { DateTime } from "luxon";

const component = ({ getRawReport }) => {
  const [startDate, setStartDate] = useState(
    DateTime.local().toFormat("yyyy-MM-dd")
  );
  const [endDate, setEndDate] = useState(
    DateTime.local().toFormat("yyyy-MM-dd")
  );

  const getReport = (report, { startDate, endDate }) => {
    getRawReport({ report, query: { startDate, endDate } }, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }
      exportToCsv(
        `rawdata_${report}_${DateTime.local().toFormat("ddMMyy")}.csv`,
        res
      );
    });
  };
  return (
    <div>
      <Grid container spacing={16}>
        <Grid item xs={6}>
          <TextField
            label="Start Date"
            variant="outlined"
            value={startDate}
            type="date"
            fullWidth
            onChange={evt => {
              setStartDate(evt.target.value);
            }}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            label="End Date"
            variant="outlined"
            value={endDate}
            type="date"
            fullWidth
            onChange={evt => {
              setEndDate(evt.target.value);
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            onClick={getReport.bind(this, "equipment_maintenance", {
              startDate,
              endDate
            })}
            variant="outlined"
          >
            Generate Equipment Maintenance Schedule
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};

const mapStateToProps = function(state) {
  return {};
};

export default connect(
  mapStateToProps,
  ReportActions
)(component);
