import { Grid, Button, TextField, Typography } from "@material-ui/core";
import { useState } from "react";

export default ({ getReport }) => {
  const [clusterId, setClusterId] = useState("");
  return (
    <Grid container spacing={16} alignItems="center">
      <Grid item xs={12}>
        <Typography variant="h6">Individual Cluster Profile</Typography>
      </Grid>
      <Grid item xs={6}>
        <TextField
          variant="outlined"
          value={clusterId}
          fullWidth
          onChange={evt => {
            setClusterId(evt.target.value);
          }}
        />
      </Grid>
      <Grid item xs={6}>
        <Button
          onClick={getReport.bind(this, "cluster", { clusterId })}
          fullWidth
          variant="outlined"
        >
          Export Cluster Profile
        </Button>
      </Grid>
    </Grid>
  );
};
