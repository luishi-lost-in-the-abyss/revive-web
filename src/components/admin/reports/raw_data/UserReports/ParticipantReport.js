import { Grid, Button, TextField, Typography } from "@material-ui/core";
import { useState } from "react";
import { DOWNLOAD_SERVER } from "../../../../../api/EndPoints";

export default ({ getReport }) => {
  console.log(DOWNLOAD_SERVER);
  const [participantId, setParticipantId] = useState("");
  return (
    <Grid container spacing={16} alignItems="center">
      <Grid item xs={12}>
        <Typography variant="h6">Individual Participant Profile</Typography>
      </Grid>
      <Grid item xs={6}>
        <TextField
          variant="outlined"
          value={participantId}
          fullWidth
          onChange={evt => {
            setParticipantId(evt.target.value);
          }}
        />
      </Grid>
      <Grid item xs={4}>
        <Button
          onClick={getReport.bind(this, "participant", { participantId })}
          fullWidth
          variant="outlined"
        >
          Export Participant Profile
        </Button>
      </Grid>
      <Grid item xs={2}>
        <Button
          target="_blank"
          href={`${DOWNLOAD_SERVER}/farmer/${participantId}.jpg`}
          fullWidth
          variant="outlined"
        >
          Open ID
        </Button>
      </Grid>
    </Grid>
  );
};
