import { connect } from "react-redux";
import { Button, Grid } from "@material-ui/core";
import ReportActions from "../../../../../actions/ReportActions";
import { exportToCsv } from "../../../../../api/utility";
import { DateTime } from "luxon";
import ParticipantReport from "./ParticipantReport";
import ClusterReport from "./ClusterReport";

const component = ({ getRawReport, getRawReportPaginate }) => {
  const getReport = (report, query) => {
    const cb = (err, res) => {
      if (err) {
        console.log(err);
        return;
      }
      exportToCsv(
        `rawdata_${report}_${DateTime.local().toFormat("ddMMyy")}.csv`,
        res
      );
    };
    if (
      report === "participants" ||
      report === "farm_plots" ||
      report == "formal_clusters"
    ) {
      getRawReportPaginate({ report, query }, cb);
    } else {
      getRawReport({ report, query }, cb);
    }
  };
  return (
    <div>
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <ParticipantReport getReport={getReport} />
        </Grid>
        <Grid item xs={12}>
          <Button
            onClick={getReport.bind(this, "participants", {})}
            variant="outlined"
          >
            Export ALL Participants
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Button
            onClick={getReport.bind(this, "formal_clusters", {})}
            variant="outlined"
          >
            Export ALL Formal Clusters
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Button
            onClick={getReport.bind(this, "farm_plots", {})}
            variant="outlined"
          >
            Export ALL Farm Plots
          </Button>
        </Grid>
        <Grid item xs={12}>
          <ClusterReport getReport={getReport} />
        </Grid>
      </Grid>
    </div>
  );
};

const mapStateToProps = function(state) {
  return {};
};

export default connect(
  mapStateToProps,
  ReportActions
)(component);
