import { connect } from "react-redux";
import { Button, Grid } from "@material-ui/core";
import ReportActions from "../../../../../actions/ReportActions";
import { exportToCsv } from "../../../../../api/utility";
import { DateTime } from "luxon";

const component = ({ getRawReport }) => {
  const getReport = report => {
    getRawReport({ report }, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }
      exportToCsv(
        `rawdata_${report}_${DateTime.local().toFormat("ddMMyy")}.csv`,
        res
      );
    });
  };
  return (
    <div>
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Button onClick={getReport.bind(this, "services")} variant="outlined">
            Export ALL Services
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Button
            onClick={getReport.bind(this, "equipment")}
            variant="outlined"
          >
            Export ALL Equipment
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Button
            onClick={getReport.bind(this, "equipment_assets")}
            variant="outlined"
          >
            Export ALL Equipment Assets
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};

const mapStateToProps = function(state) {
  return {};
};

export default connect(
  mapStateToProps,
  ReportActions
)(component);
