import React from "react";
import Link from "next/link";
import { Grid, Button, TextField, withStyles } from "@material-ui/core";
import SelectInput from "../../../common/SelectInput";
import { styles } from "../../../../theme";
import FormalClusterForm from "./FormalClusterForm";

class CreateClusterForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { code: "", formal: false, errors: {}, errorType: "none" };
  }
  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";

    if (errorType === "duplicateCode") return "Duplicated cluster code.";
    return null;
  }

  static getDerivedStateFromProps(props, prevState) {
    const { submitError, submitErrorType } = props;
    if (submitError && submitErrorType === "duplicateCode") {
      return { errors: { code: true }, errorType: "duplicateCode" };
    }
    return {};
  }

  onSubmit() {
    const { code } = this.state;
    if (!code) {
      this.setState({
        errors: { code: !code },
        errorType: "blank"
      });
      return;
    }
    this.setState({ errors: {} });
    this.props.onSubmit({ code });
  }

  onFormalSubmit(details) {
    const { code } = this.state;
    if (!code) {
      this.setState({
        errors: { code: !code },
        errorType: "blank"
      });
      return;
    }

    this.props.onFormalSubmit(Object.assign({}, details, { code }));
  }

  render() {
    const {
      classes,
      submitError,
      submitErrorType,
      formalError,
      formalErrorType,
      userRole
    } = this.props;
    const { formal, errors } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <form>
            <TextField
              label="Cluster Code"
              {...Object.assign(
                {},
                commonTextFieldProps("code"),
                submitError.code
                  ? { error: true, helperText: "Cluster code already exists" }
                  : null
              )}
            />

            <SelectInput
              {...{
                id: "newcluster-formal",
                name: "formal",
                onInputChange: this.onInputChange.bind(this),
                value: formal,
                menuArr: [
                  { label: "Yes", value: true },
                  { label: "No", value: false }
                ],
                label: "Formal",
                error: errors["formal"],
                helperText: this.getHelperText("formal")
              }}
            />
            {formal ? (
              <FormalClusterForm
                submitError={formalError}
                submitErrorType={formalErrorType}
                onSubmit={this.onFormalSubmit.bind(this)}
              />
            ) : (
              <Grid container justify="space-between">
                <Grid item>
                  <Link
                    href={`/${userRole ? userRole : "admin"}/users/clusters`}
                  >
                    <Button color="secondary" variant="outlined">
                      Cancel
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={this.onSubmit.bind(this)}
                  >
                    Create
                  </Button>
                </Grid>
              </Grid>
            )}
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(CreateClusterForm);
