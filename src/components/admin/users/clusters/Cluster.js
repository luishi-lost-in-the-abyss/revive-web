import React from "react";
import {} from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
export default ({ cluster, clusterTypes }) => (
  <React.Fragment>
    <VerticalTable
      data={cluster}
      tableValues={[
        { label: "Code", key: "code" },
        {
          label: "Type",
          key: "formal_cluster",
          render: formal => (formal ? "Formal" : "Informal")
        },
        cluster.formal_cluster
          ? null
          : {
              label: "Cluster Leader",
              key: "Leader",
              render: leader => (leader ? leader.name : "None")
            },
        {
          label: "No. of Members",
          key: null,
          render: obj => {
            if (!obj.formal_cluster) {
              const arr = obj.farm_plots;
              const unique = arr.map(({ participantId }) => participantId);
              return [...new Set(unique)].length;
            } else {
              return obj.formal_cluster.participants.length;
            }
          }
        }
      ].filter(a => !!a)}
    />
  </React.Fragment>
);
