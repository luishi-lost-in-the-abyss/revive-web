import React from "react";
import Link from "next/link";
import { Grid, Button, TextField, withStyles } from "@material-ui/core";
import SelectInput from "../../../common/SelectInput";
import { styles } from "../../../../theme";
import FormalClusterForm from "./FormalClusterForm";

class EditClusterForm extends React.Component {
  constructor(props) {
    super(props);
    const { Leader, formal_cluster, farm_plots } = props.cluster;
    const participants = {};
    farm_plots.map(({ participant }) => {
      participants[participant.id] = participant;
    });
    this.state = {
      ...props.cluster,
      formal: !!formal_cluster,
      clusterLeader: Leader ? Leader.id : "",
      participants: Object.values(participants),
      errors: {},
      errorType: "none"
    };
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";

    if (errorType === "duplicateCode") return "Duplicated cluster code.";
    return null;
  }

  static getDerivedStateFromProps(props, prevState) {
    const { submitError, submitErrorType } = props;
    if (submitError && submitErrorType === "duplicateCode") {
      return { errors: { code: true }, errorType: "duplicateCode" };
    }
    return {};
  }

  onSubmit() {
    const { code, clusterLeader } = this.state;
    const errors = { code: !code };
    let hasErrors = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasErrors) {
      this.setState({
        errors,
        errorType: "blank"
      });
      return;
    }

    this.setState({ errors: {} });
    this.props.onSubmit({ code, clusterLeader });
  }

  onFormalSubmit(details) {
    const { code, clusterLeader, id } = this.state;
    if (!code) {
      this.setState({
        errors: { code: !code },
        errorType: "blank"
      });
      return;
    }

    this.props.onFormalSubmit(
      Object.assign({ formalCluster: details }, { code, clusterLeader, id })
    );
  }

  render() {
    const {
      classes,
      cluster,
      formalError,
      formalErrorType,
      userRole
    } = this.props;
    const { participants, formal, errors } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <form>
            <TextField
              label="Cluster Code"
              {...Object.assign(
                {},
                commonTextFieldProps("code"),
                formalError.code
                  ? { error: true, helperText: "Cluster code already exists" }
                  : null
              )}
            />
            {cluster.formal_cluster ? null : (
              <SelectInput
                {...{
                  id: "editcluster-clusterLeader",
                  className: classes.formField,
                  name: "clusterLeader",
                  onInputChange: this.onInputChange.bind(this),
                  value: this.state.clusterLeader,
                  menuArr: participants.map(participant => ({
                    label: `${participant.firstName} ${
                      participant.middleName
                    } ${participant.lastName}`,
                    value: participant.id
                  })),
                  label: "Cluster Leader",
                  error: errors["clusterLeader"],
                  helperText: this.getHelperText("clusterLeader")
                }}
              />
            )}
            <SelectInput
              {...{
                id: "editcluster-formal",
                className: classes.formField,
                name: "formal",
                onInputChange: this.onInputChange.bind(this),
                value: formal,
                disabled: true,
                menuArr: [
                  { label: "Yes", value: true },
                  { label: "No", value: false }
                ],
                label: "Formal",
                error: errors["formal"],
                helperText: this.getHelperText("formal")
              }}
            />
            {formal ? (
              <FormalClusterForm
                editCluster={cluster}
                submitError={formalError}
                submitErrorType={formalErrorType}
                onSubmit={this.onFormalSubmit.bind(this)}
              />
            ) : (
              <Grid container justify="space-between">
                <Grid item>
                  <Link
                    href={`/${
                      userRole ? userRole : "admin"
                    }/users/clusters/cluster?id=${cluster.id}`}
                  >
                    <Button color="secondary" variant="outlined">
                      Cancel
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={this.onSubmit.bind(this)}
                  >
                    Edit
                  </Button>
                </Grid>
              </Grid>
            )}
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(EditClusterForm);
