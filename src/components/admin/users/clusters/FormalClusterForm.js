import React from "react";
import dynamic from "next/dynamic";
import Link from "next/link";
import RegisterLandStep from "../../../Register/RegisterLandStep";
import FeathersClient from "../../../../api/FeathersClient";
import {
  Grid,
  TextField,
  withStyles,
  Typography,
  Button
} from "@material-ui/core";
import { styles } from "../../../../theme";
import SelectInput from "../../../common/SelectInput";
import { extractLocationDetails } from "../../../../api/utility";
const MuiPhoneInput = dynamic(import("material-ui-phone-number"), {
  ssr: false
});

class FormalClusterForm extends React.Component {
  constructor(props) {
    super(props);
    const { editCluster } = props;
    this.state = Object.assign(
      {
        type: "lmf",
        firstName: "",
        middleName: "",
        lastName: "",
        operator: "",
        contact: "",
        gender: "",
        bankName: "",
        bankAccType: "",
        bankAccName: "",
        bankTinNo: "",
        plots: [],
        fecs: [],
        technicians: { 0: "" },
        inputFec: { 0: "" },
        inputTechnician: { 0: "" },
        errorsTechnician: { 0: { fec: false, technician: false } },
        username: "",
        email: "",
        password: "",
        confirmPassword: "",
        registrationParams: {},
        errors: {},
        errorType: "none",
        loading: true
      },
      editCluster ? editCluster.formal_cluster : null
    );

    if (editCluster) {
      // set the technicians:
      editCluster.formal_cluster.farm_plots.map(({ barangay, ...plot }, i) => {
        this.state.technicians[i] = [plot.technician];
        this.state.inputTechnician[i] = plot.technicianId;
        this.state.inputFec[i] = "";
        this.state.errorsTechnician[i] = { fec: false, technician: false };
        plot.cropInsuranceDetails = plot.cropInsurance
          ? plot.cropInsurance
          : "";
        plot.cropInsurance = !!plot.cropInsurance;
        const {
          country,
          region,
          province,
          municipality
        } = extractLocationDetails(barangay);
        this.state.plots.push(
          Object.assign({}, plot, {
            barangay: barangay.id,
            country: country.id,
            region: region.id,
            province: province.id,
            municipality: municipality.id
          })
        );
      });
    }

    this.onAddPlot = this.onAddPlot.bind(this);
    this.onRemovePlot = this.onRemovePlot.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onPlotInputChange = this.onPlotInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onEditSubmit = this.onEditSubmit.bind(this);
  }

  async componentDidMount() {
    const client = FeathersClient.getClient();
    const registrationParams = await client
      .service("public-methods")
      .create({ method: "get_registration_params" });
    this.setState(
      Object.assign(
        {
          registrationParams
        },
        this.props.editCluster
          ? {}
          : {
              plots: [
                Object.assign({}, registrationParams.plotDetail, {
                  type: "lmf"
                })
              ]
            }
      ),
      async () => {
        let fecs;
        try {
          fecs = await client.service("fec").find({ query: { $limit: "-1" } });
          this.setState({
            fecs: fecs.map(fec => ({ label: fec.name, value: fec.id }))
          });
        } catch (err) {
          console.log(err);
        } finally {
          this.setState({ loading: false });
        }
      }
    );
  }

  onPlotInputChange(i, evt, cb = null) {
    const { name, value } = evt.target;
    const { plots } = this.state;

    const newPlots = plots.map((plot, index) => {
      if (index !== i) return plot;
      return Object.assign({}, plot, { [name]: value });
    });
    if (typeof cb === "function") {
      this.setState({ plots: newPlots }, cb);
    } else {
      this.setState({ plots: newPlots });
    }
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  onAddPlot() {
    const { plotDetail } = this.state.registrationParams;
    const { inputFec, inputTechnician, errorsTechnician } = this.state;
    const plots = [
      ...this.state.plots,
      Object.assign({}, plotDetail, { type: this.state.type })
    ];
    this.setState({
      plots,
      inputFec: Object.assign({}, inputFec, { [plots.length - 1]: "" }),
      inputTechnician: Object.assign({}, inputTechnician, {
        [plots.length - 1]: ""
      }),
      errorsTechnician: Object.assign({}, errorsTechnician, {
        [plots.length - 1]: { fec: false, technician: false }
      })
    });
  }

  onRemovePlot(i) {
    const { plots } = this.state;
    const { inputFec, inputTechnician, errorsTechnician } = this.state;
    for (let j = i; j < plots.length - 1; j++) {
      inputFec[j] = inputFec[j + 1];
      inputTechnician[j] = inputTechnician[j + 1];
      errorsTechnician[j] = errorsTechnician[j + 1];
    }
    this.setState({ plots: [...plots.slice(0, i), ...plots.slice(i + 1)] });
  }

  getHelperText(name, i) {
    const { errors, errorsTechnician, errorType } = this.state;
    if (!errors[name] || !errorsTechnician[i]["technician"]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";
    if (errorType === "matchPassword") return "Passwords do not match.";
    if (errorType === "sameBarangay")
      return "Technician is in the same barangay as the plot location!";
    return null;
  }

  validateLandInfo() {
    const { plotDetail } = this.state.registrationParams;
    const errorsTechnician = {};
    const { plots, inputFec, inputTechnician, technicians } = this.state;
    let errors = {};
    const keys = Object.keys(plotDetail);
    plots.map((plot, i) => {
      errorsTechnician[i] = {
        technician: !inputTechnician[i]
      };
      errors[i] = {};
      keys.map(key => {
        if (key === "cropInsuranceDetails") {
          //only need to fill up insurance details if have insurance
          errors[i][key] = plot["cropInsurance"] && !plot[key];
          return;
        }
        if (key === "commodityOthers") {
          //only need to fill up other commodity if select other
          errors[i][key] = plot["commodity"] === "others" && !plot[key];
          return;
        }
        if (typeof plot[key] === "boolean") return;
        if (key === "clusterId") return;
        errors[i][key] = !plot[key];
      });
    });
    let hasError = Object.values(errors)
      .map(errObj => {
        return Object.values(errObj).reduce((a, b) => a || b, false);
      })
      .reduce((a, b) => a || b, false);
    hasError = hasError
      ? hasError
      : Object.values(errorsTechnician)
          .map(errObj => {
            return Object.values(errObj).reduce(
              (a, b) => a || b.technician,
              false
            );
          })
          .reduce((a, b) => a || b, false);

    if (hasError) {
      this.setState({ errors, errorsTechnician, errorType: "blank" });
      return false;
    }

    plots.map(({ plantableArea, totalArea }, i) => {
      try {
        plantableArea = parseFloat(plantableArea);
        totalArea = parseFloat(totalArea);
      } catch (err) {
        plantableArea = 0;
        totalArea = 0;
      }
      errors[i]["totalArea"] = totalArea < plantableArea || totalArea == 0;
      errors[i]["plantableArea"] = plantableArea > totalArea;
    });
    hasError = Object.values(errors)
      .map(errObj => {
        return Object.values(errObj).reduce((a, b) => a || b, false);
      })
      .reduce((a, b) => a || b, false);
    if (hasError) {
      this.setState({ errors, errorType: "area" });
      return false;
    }

    plots.map(({ barangay }, i) => {
      errorsTechnician[i]["technician"] =
        technicians[i].filter(({ id }) => id === inputTechnician[i])[0]
          .barangayId === barangay;
    });
    hasError = Object.values(errorsTechnician)
      .map(errObj => {
        return Object.values(errObj).reduce((a, b) => a || b, false);
      })
      .reduce((a, b) => a || b, false);
    if (hasError) {
      this.setState({ errorsTechnician, errorType: "sameBarangay" });
      return false;
    }

    this.setState({ errors: {}, errorType: "none" });
    return true;
  }

  onSubmit() {
    const {
      type,
      firstName,
      middleName,
      lastName,
      contact,
      operator,
      bankName,
      bankAccType,
      bankAccName,
      bankTinNo,
      username,
      password,
      confirmPassword
    } = this.state;
    let errors = {
      type: !type,
      firstName: !firstName,
      middleName: !middleName,
      lastName: !lastName,
      contact: !contact,
      operator: !operator,
      bankName: !bankName,
      bankAccType: !bankAccType,
      bankAccName: !bankAccName,
      bankTinNo: !bankTinNo,
      username: !username,
      password: !password
    };
    let hasError = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasError) {
      this.setState({ errors, errorType: "blank" });
      return;
    }
    if (!this.validateLandInfo()) return;

    errors.password = password !== confirmPassword;
    errors.confirmPassword = errors.password;
    hasError = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasError) {
      this.setState({ errors, errorType: "matchPassword" });
      return;
    }
    this.setState({ errors: {}, errorType: "none" }, () => {
      this.props.onSubmit(this.state);
    });
  }

  onEditSubmit() {
    const {
      type,
      firstName,
      middleName,
      lastName,
      contact,
      operator,
      bankName,
      bankAccType,
      bankAccName,
      bankTinNo
    } = this.state;
    let errors = {
      type: !type,
      firstName: !firstName,
      middleName: !middleName,
      lastName: !lastName,
      contact: !contact,
      operator: !operator,
      bankName: !bankName,
      bankAccType: !bankAccType,
      bankAccName: !bankAccName,
      bankTinNo: !bankTinNo
    };
    let hasError = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasError) {
      this.setState({ errors, errorType: "blank" });
      return;
    }
    if (!this.validateLandInfo()) return;
    this.setState({ errors: {}, errorType: "none" }, () => {
      this.props.onSubmit(this.state);
    });
  }

  onFecInputChange(i, evt) {
    const { inputFec, inputTechnician } = this.state;
    const { value } = evt.target;
    if (value === inputFec[i]) return;
    this.setState(
      {
        inputFec: Object.assign({}, inputFec, { [i]: value }),
        inputTechnician: Object.assign({}, inputTechnician, { [i]: "" })
      },
      async () => {
        const { technicians } = this.state;
        const client = FeathersClient.getClient();
        const fecTech = await client
          .service("technicians")
          .find({ query: { $limit: "-1", fecId: value } });
        this.setState({
          technicians: Object.assign({}, technicians, { [i]: fecTech })
        });
      }
    );
  }

  onTechnicianInputChange(i, evt) {
    const { inputTechnician } = this.state;
    const { value } = evt.target;
    this.setState({
      inputTechnician: Object.assign({}, inputTechnician, { [i]: value })
    });
  }

  render() {
    const { classes, submitError } = this.props;
    const {
      plots,
      fecs,
      technicians,
      inputFec,
      inputTechnician,
      errorsTechnician,
      loading,
      registrationParams,
      errors,
      errorType
    } = this.state;
    if (loading) return "Loading...";

    const editMode = !!this.props.editCluster;

    const noVerticalPadding = { paddingBottom: 0, paddingTop: 0 };
    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange,
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    const accountDetails = (
      <React.Fragment>
        <Typography variant="h4" gutterBottom>
          User Account Details
        </Typography>
        <TextField
          label="Username"
          {...Object.assign(
            {},
            commonTextFieldProps("username"),
            submitError.username
              ? { error: true, helperText: "Username already exists" }
              : null
          )}
        />
        <TextField
          label="Email"
          helperText="Optional"
          {...commonTextFieldProps("email")}
        />
        <TextField
          label="Password"
          type="password"
          helperText="Minimum length 8 characters"
          {...commonTextFieldProps("password")}
        />
        <TextField
          label="Confirm Password"
          type="password"
          helperText="Please re-enter your password above"
          {...commonTextFieldProps("confirmPassword")}
        />
      </React.Fragment>
    );

    return (
      <React.Fragment>
        <Typography variant="h4" gutterBottom>
          Participant Details
        </Typography>
        <SelectInput
          {...{
            id: "register-type",
            name: "type",
            onInputChange: this.onInputChange,
            value: this.state.type,
            menuArr: [
              { label: "FSS", value: "fss" },
              { label: "LMF", value: "lmf" }
            ],
            error: errors["type"],
            helperText: this.getHelperText("type"),
            label: "Main Participation Type"
          }}
        />
        <Grid container justify="space-evenly" spacing={16}>
          <Grid item xs={4}>
            <TextField
              label="First Name"
              {...commonTextFieldProps("firstName")}
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              label="Middle Name"
              {...commonTextFieldProps("middleName")}
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              label="Last Name"
              {...commonTextFieldProps("lastName")}
            />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={12}>
            <Grid container spacing={16}>
              {/*
          <Grid item xs={2} style={noVerticalPadding}>
            <TextField
              disabled={true}
              {...Object.assign({}, commonTextFieldProps("prefix"), {
                value: "+63"
              })}
            />
          </Grid> */}
              <Grid item xs={12} md={4} style={noVerticalPadding}>
                <MuiPhoneInput
                  variant="outlined"
                  enableLongNumbers={true}
                  onlyCountries={["ph"]}
                  countryCodeEditable={false}
                  defaultCountry="ph"
                  label="Contact Number"
                  value={this.state.contact}
                  {...Object.assign({}, commonTextFieldProps("contact"), {
                    onChange: value => {
                      this.onInputChange({
                        target: { name: "contact", value }
                      });
                    }
                  })}
                />
              </Grid>
              <Grid item xs={12} md={4} style={noVerticalPadding}>
                <SelectInput
                  {...{
                    id: "register-operator",
                    name: "operator",
                    onInputChange: this.onInputChange,
                    value: this.state.operator,
                    menuArr: registrationParams.mobileOperators,
                    error: errors["operator"],
                    helperText: this.getHelperText("operator"),
                    label: "Operator"
                  }}
                />
              </Grid>
              <Grid item xs={12} md={4} style={noVerticalPadding}>
                <SelectInput
                  {...{
                    id: "register-gender",
                    name: "gender",
                    onInputChange: this.onInputChange,
                    value: this.state.gender,
                    menuArr: [
                      { label: "Male", value: "m" },
                      { label: "Female", value: "f" }
                    ],
                    error: errors["gender"],
                    helperText: this.getHelperText("gender"),
                    label: "Gender"
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid container justify="space-evenly" spacing={16}>
            <Grid item xs={12} style={noVerticalPadding}>
              <TextField
                label="Bank Name"
                {...commonTextFieldProps("bankName")}
              />
            </Grid>

            <Grid item xs={12} style={noVerticalPadding}>
              <TextField
                label="TIN Number"
                {...commonTextFieldProps("bankTinNo")}
              />
            </Grid>

            <Grid item xs={12} sm={6} style={noVerticalPadding}>
              <TextField
                label="Account Type"
                {...commonTextFieldProps("bankAccType")}
              />
            </Grid>
            <Grid item xs={12} sm={6} style={noVerticalPadding}>
              <TextField
                label="Account Name"
                {...commonTextFieldProps("bankAccName")}
              />
            </Grid>
          </Grid>
        </Grid>
        <Typography variant="h4" gutterBottom>
          Cluster Plot Details
        </Typography>
        <RegisterLandStep
          {...{
            registrationParams,
            onAddPlot: this.onAddPlot,
            onRemovePlot: this.onRemovePlot,
            onInputChange: this.onPlotInputChange,
            rootState: this.state,
            errors: errors,
            errorType: errorType,
            fullWidth: true,
            editMode,
            disableInformalCluster: true
          }}
        />
        <Typography variant="h4" gutterBottom>
          Cluster Plot Assignment
        </Typography>
        {plots.map((plot, i) => (
          <div key={`plot-${i}`}>
            <Typography variant="h6">
              Plot #{i + 1} ({plot.code})
            </Typography>
            <SelectInput
              label="Fec"
              id={`plot-fec-${i}`}
              name={`fec`}
              value={inputFec[i]}
              menuArr={fecs}
              onInputChange={this.onFecInputChange.bind(this, i)}
              error={errorsTechnician[i].fec}
            />

            <SelectInput
              label="Technician"
              id={`plot-technician-${i}`}
              name={`technician`}
              value={inputTechnician[i]}
              menuArr={
                technicians[i]
                  ? technicians[i].map(tech => ({
                      label: tech.name + ` (${tech.user.username})`,
                      value: tech.id
                    }))
                  : []
              }
              onInputChange={this.onTechnicianInputChange.bind(this, i)}
              error={errorsTechnician[i].technician}
              helperText={
                errorsTechnician[i].technician
                  ? errorType === "sameBarangay"
                    ? "Technician is in the same barangay as the plot location!"
                    : "This field cannot be blank!"
                  : null
              }
            />
          </div>
        ))}

        {editMode ? null : accountDetails}

        <Grid
          container
          justify="space-between"
          style={{ marginTop: editMode ? 20 : 0 }}
        >
          <Grid item>
            <Link passHref href="/admin/users/clusters">
              <Button color="secondary" variant="outlined">
                Cancel
              </Button>
            </Link>
          </Grid>
          <Grid item>
            <Button
              color="primary"
              variant="contained"
              onClick={editMode ? this.onEditSubmit : this.onSubmit}
            >
              {editMode ? "Edit" : "Create"}
            </Button>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(FormalClusterForm);
