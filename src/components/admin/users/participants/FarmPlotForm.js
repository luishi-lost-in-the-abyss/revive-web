import { styles } from "../../../../theme";
import { Grid, TextField, withStyles, Button } from "@material-ui/core";
import SelectInput from "../../../common/SelectInput";

/*
errors
onInputChange
onLocationChange
registrationParams
*/

const component = ({
  classes,
  errors,
  registrationParams,
  onInputChange,
  onLocationChange,
  clusters,
  ...state
}) => {
  const getHelperText = name => {
    const { errorType } = state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";
    return null;
  };

  const commonTextFieldProps = name => ({
    className: classes.formField,
    fullWidth: true,
    variant: "outlined",
    onChange: onInputChange,
    name,
    value: state[name],
    error: errors[name],
    helperText: getHelperText(name)
  });

  const commonSelectFieldProps = name => ({
    id: `plot-${name}`,
    name,
    value: state[name],
    labelWidth: 100,
    onInputChange: onInputChange,
    value: state[name],
    helperText: getHelperText(name)
  });

  const commonLocationFieldProps = name =>
    Object.assign({}, commonSelectFieldProps(name), {
      onInputChange: onLocationChange.bind(this)
    });

  return (
    <React.Fragment>
      <TextField label="Code" {...commonTextFieldProps("code")} />
      <TextField label="Description" {...commonTextFieldProps("description")} />
      <Grid container spacing={16}>
        <Grid item xs={9}>
          <SelectInput
            label="Informal Cluster (Optional)"
            menuArr={clusters.map(cluster => ({
              label: cluster.code,
              value: cluster.id
            }))}
            {...commonSelectFieldProps("clusterId")}
          />
        </Grid>
        <Grid item xs={3}>
          <div>
            <Button
              onClick={() => {
                onInputChange({
                  target: { value: "", name: "clusterId" }
                });
              }}
            >
              REMOVE
            </Button>
          </div>
        </Grid>
      </Grid>
      <SelectInput
        {...Object.assign(
          {
            menuArr: [
              { label: "FSS", value: "fss" },
              { label: "LMF", value: "lmf" }
            ],
            label: "Type"
          },
          commonLocationFieldProps("type")
        )}
      />
      <SelectInput
        {...Object.assign(
          {
            menuArr:
              state["type"] === "fss"
                ? [
                    { label: "Financing", value: "financing" },
                    { label: "Growership", value: "growership" },
                    { label: "Direct Buying", value: "directBuying" }
                  ]
                : [
                    { label: "In-house", value: "inhouse" },
                    { label: "Lease", value: "lease" }
                  ],
            label: "Engagement Scheme"
          },
          commonLocationFieldProps("engagementScheme")
        )}
      />

      <SelectInput
        {...Object.assign(
          {
            menuArr: state.plotLocation.countryArr,
            label: "Country"
          },
          commonLocationFieldProps("country")
        )}
      />

      <SelectInput
        {...Object.assign(
          {
            menuArr: state.plotLocation.regionArr,
            label: "Region"
          },
          commonLocationFieldProps("region")
        )}
      />

      <SelectInput
        {...Object.assign(
          {
            menuArr: state.plotLocation.provinceArr,
            label: "Province"
          },
          commonLocationFieldProps("province")
        )}
      />

      <SelectInput
        {...Object.assign(
          {
            menuArr: state.plotLocation.municipalityArr,
            label: "Municipality"
          },
          commonLocationFieldProps("municipality")
        )}
      />

      <SelectInput
        {...Object.assign(
          {
            menuArr: state.plotLocation.barangayArr,
            label: "Barangay"
          },
          commonLocationFieldProps("barangay")
        )}
      />

      <TextField label="Purok" {...commonTextFieldProps("purok")} />

      <SelectInput
        {...Object.assign(
          {
            menuArr: registrationParams.tenureStatus,
            label: "Tenure Status"
          },
          commonSelectFieldProps("tenure")
        )}
      />

      <SelectInput
        {...Object.assign(
          {
            menuArr: registrationParams.cropType,
            label: "Commodity"
          },
          commonSelectFieldProps("commodity")
        )}
      />
      {state.commodity === "others" && (
        <TextField
          label="Specify Commodity"
          {...commonTextFieldProps("commodityOthers")}
        />
      )}

      <TextField
        label="Total Area"
        step={0.01}
        type="number"
        {...commonTextFieldProps("totalArea")}
      />

      <TextField
        label="Plantable Area"
        step={0.01}
        type="number"
        {...commonTextFieldProps("plantableArea")}
      />

      <SelectInput
        {...Object.assign(
          {
            menuArr: registrationParams.waterSystem,
            label: "Water System"
          },
          commonSelectFieldProps("waterSystem")
        )}
      />

      <TextField label="Coordinates" {...commonTextFieldProps("coordinates")} />
      <Grid container justify="space-evenly" spacing={16}>
        <Grid item xs={12} sm={4}>
          <TextField
            label="Previous Buyer"
            {...commonTextFieldProps("prevBuyer")}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <TextField
            label="Previous Price"
            type="number"
            {...commonTextFieldProps("prevPrice")}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <SelectInput
            {...Object.assign(
              {
                menuArr: registrationParams.modeOfPayment,
                label: "Payment Mode"
              },
              commonSelectFieldProps("prevPaymentMode")
            )}
          />
        </Grid>
      </Grid>

      <SelectInput
        {...Object.assign(
          {
            menuArr: registrationParams.financialSource,
            label: "Source of Financing"
          },
          commonSelectFieldProps("sourceFinance")
        )}
      />

      <SelectInput
        {...Object.assign(
          {
            menuArr: registrationParams.cropInsurance,
            label: "Crop Insurance"
          },
          commonSelectFieldProps("cropInsurance")
        )}
      />
      {state.cropInsurance && (
        <TextField
          label="Crop Insurance Details"
          {...commonTextFieldProps("cropInsuranceDetails")}
        />
      )}
    </React.Fragment>
  );
};

export default withStyles(styles)(component);
