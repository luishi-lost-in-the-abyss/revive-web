import {
  withStyles,
  Grid,
  TextField,
  Button,
  Typography
} from "@material-ui/core";
import Link from "next/link";
import { styles } from "../../../../theme";
import RegisterAccountStep from "../../../Register/RegisterAccountStep";
import RegisterFarmerStep from "../../../Register/RegisterFarmerStep";
import Pica from "pica/dist/pica";
const pica = Pica();

class EditParticipantForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = Object.assign(
      {
        file: "",
        password: "",
        confirmPassword: "",
        errors: {},
        errorType: "none"
      },
      props.participant
    );
    //need to massage the data to allow for editing.
    this.state.type = "fss";
    this.state.isCluster = !!this.state.formalClusterId;
    this.state.cluster = this.state.formalClusterId
      ? this.state.formalClusterId
      : "";
    this.state.email = this.state.user.email;

    this.onInputChange = this.onInputChange.bind(this);
    this.onFileChange = this.onFileChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  onFileChange(evt) {
    const file = evt.target.files[0];
    evt.target.value = "";
    if (file.size > 5000000) {
      alert("Image size must be below 5mb!");
      return;
    }

    let img = new Image();
    img.src = window.URL.createObjectURL(file);
    img.onload = () => {
      let src = document.createElement("canvas");
      src.width = img.width;
      src.height = img.height;
      let ctx = src.getContext("2d");
      ctx.drawImage(img, 0, 0);
      let dest = document.createElement("canvas");
      dest.width = img.width;
      dest.height = img.height;

      pica.resize(src, dest).then(result => {
        this.setState({ file: result });
      });
    };
  }

  onClickUpload() {
    const input = document.getElementById("upload-profile");
    input.click();
  }

  validateData() {
    const {
      name,
      birthdate,
      occupation,
      civil,
      contact,
      operator,
      gender,
      residentAddress,
      bankName,
      bankAccType,
      bankAccName,
      bankTinNo,
      healthConditions,
      healthOthers,
      type,
      isCluster,
      cluster,
      password,
      confirmPassword
    } = this.state;

    let errors = {
      name: !name,
      birthdate: !birthdate,
      occupation: !occupation,
      civil: !civil,
      contact: !contact,
      operator: !operator,
      gender: !gender,
      residentAddress: !residentAddress,
      bankName: !bankName,
      bankAccType: !bankAccType,
      bankAccName: !bankAccName,
      bankTinNo: !bankTinNo,
      healthOthers: healthConditions.indexOf("others") > -1 && !healthOthers,
      type: !type,
      cluster: isCluster && !cluster,
      password: password && password !== confirmPassword,
      confirmPassword: password && password !== confirmPassword
    };

    let hasErrors = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasErrors) {
      this.setState({ errors, errorType: "blank" });
      return false;
    }

    this.setState({ errors: {}, errorType: "none" });
    return true;
  }

  onSubmit() {
    if (!this.validateData()) {
      return;
    }
    this.props.onSubmit(
      Object.assign({}, this.state, {
        formalClusterId: this.state.isCluster ? this.state.cluster : null
      })
    );
  }

  render() {
    const { classes, userRole } = this.props;
    const { errors, errorType } = this.state;

    const commonStepProps = {
      registrationParams: this.props.registrationParams,
      onInputChange: this.onInputChange,
      rootState: this.state,
      errors: errors,
      errorType: errorType
    };
    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange,
      value: this.state[name],
      error: errors[name]
    });

    return (
      <React.Fragment>
        <Typography variant="h5" gutterBottom>
          <b>Participation Details</b>
        </Typography>
        <RegisterAccountStep {...commonStepProps} />
        <Typography variant="h5" gutterBottom>
          <b>Participant Details</b>
        </Typography>
        <RegisterFarmerStep
          {...commonStepProps}
          onClickUpload={this.onClickUpload.bind(this)}
        />
        <Typography variant="h5" gutterBottom>
          <b>User Account Details</b>
        </Typography>
        <Grid container>
          <Grid item xs={12} sm={10} md={8}>
            <Grid container justify="space-between">
              <Grid item xs={12}>
                <TextField
                  label="Email"
                  name="email"
                  helperText="Optional"
                  {...commonTextFieldProps("email")}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="Password"
                  name="password"
                  helperText="Leave blank if there are no changes"
                  {...commonTextFieldProps("password")}
                  type="password"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="Confirm Password"
                  name="confirmPassword"
                  {...commonTextFieldProps("confirmPassword")}
                  type="password"
                />
              </Grid>
              <Grid item>
                <Link
                  href={`/${
                    userRole ? userRole : "admin"
                  }/users/participants/participant?id=${
                    this.props.participant.id
                  }`}
                >
                  <Button color="secondary" variant="outlined">
                    Cancel
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={this.onSubmit}
                >
                  Edit
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <input
          style={{ display: "none" }}
          id="upload-profile"
          accept="image/x-png,image/gif,image/jpeg"
          type="file"
          onChange={this.onFileChange}
        />
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(EditParticipantForm);
