import React from "react";
import Router from "next/router";
import { Button, Typography } from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
import DataTable from "../../../common/DataTable";
import { DOWNLOAD_SERVER } from "../../../../api/EndPoints";
import { extractLocationDetails } from "../../../../api/utility";
export default ({ plot, userRole = "admin" }) => {
  const locationObj = extractLocationDetails(plot.barangay);

  return (
    <React.Fragment>
      <VerticalTable
        data={plot}
        tableValues={[
          {
            label: "Code",
            key: "code"
          },
          {
            label: "Description",
            key: "description"
          },
          {
            label: "Participation Type",
            key: "type"
          },
          {
            label: "Engagement Scheme",
            key: "engagementScheme"
          },
          {
            label: "Location",
            key: null,
            render: data => {
              const {
                barangay,
                country,
                province,
                region,
                municipality
              } = locationObj;
              return `${country.name} ${province.name} ${region.name} ${
                municipality.name
              } ${barangay.name} ${data.purok}`;
            }
          },
          {
            label: "Tenure Status",
            key: "tenure"
          },
          {
            label: "Commodity",
            key: null,
            render: ({ commodity, commodityOthers }) =>
              commodity === "others" ? `Others - ${commodityOthers}` : commodity
          },
          {
            label: "Water System",
            key: "waterSystem"
          },
          { label: "Coordinates", key: "coordinates" },
          { label: "Previous Buyer", key: "prevBuyer" },
          { label: "Previous Price", key: "prevPrice" },
          { label: "Previous Payment Mode", key: "prevPaymentMode" },
          {
            label: "Source of Financing",
            key: "sourceFinance"
          },
          {
            label: "Crop Insurance",
            key: null,
            render: ({ cropInsurance, cropInsuranceDetails }) => {
              return cropInsurance && cropInsurance === true
                ? cropInsuranceDetails
                : cropInsurance
                ? cropInsurance
                : "No";
            }
          }
        ]}
      />
    </React.Fragment>
  );
};
