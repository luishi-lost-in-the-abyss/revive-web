import React from "react";
import Router from "next/router";
import { Button, Typography } from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
import DataTable from "../../../common/DataTable";
import { DOWNLOAD_SERVER } from "../../../../api/EndPoints";
import { extractLocationDetails } from "../../../../api/utility";
export default ({ participant, userRole = "admin" }) => {
  return (
    <React.Fragment>
      <VerticalTable
        data={participant}
        tableValues={[
          {
            label: "Formal Cluster",
            key: null,
            render: ({ formal_cluster }) => {
              return formal_cluster ? formal_cluster.cluster.code : "-";
            }
          },
          { label: "Name", key: "name" },
          { label: "Birth Date", key: "birthdate" },
          { label: "Occupation", key: "occupation" },
          { label: "Civil Status", key: "civil" },
          { label: "Contact Number", key: "contact" },
          { label: "Mobile Operator", key: "operator" },
          { label: "Gender", key: "gender" },
          { label: "Resident Address", key: "residentAddress" },
          { label: "Bank Name", key: "bankName" },
          { label: "Bank Account Type", key: "bankAccType" },
          { label: "Bank Account Name", key: "bankAccName" },
          { label: "Bank TIN Number", key: "bankTinNo" },
          {
            label: "Health Condition",
            key: null,
            render: ({ healthConditions, healthOthers }) => {
              return (
                <div>
                  {healthConditions.length === 0
                    ? "None"
                    : healthConditions.join(", ")}
                  {healthConditions.indexOf("others") > -1 ? (
                    <React.Fragment>
                      <br />
                      Others: {healthOthers}
                    </React.Fragment>
                  ) : null}
                </div>
              );
            }
          },
          {
            label: "ID Image",
            key: null,
            render: ({ id }) => (
              <img
                id="profile-image"
                src={`${DOWNLOAD_SERVER}/farmer/${
                  id ? id : 0
                }.jpg?timestamp=${new Date().getTime()}`}
                onError={e => {
                  e.target.onerror = null;
                  e.target.src = "/static/images/id-card.png";
                }}
                style={{ width: "100%" }}
              />
            )
          }
        ]}
      />

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          marginTop: 40,
          marginBottom: 20
        }}
      >
        <Typography variant="h5" gutterBottom>
          Farm Plots
        </Typography>
        <div>
          {userRole !== "technician" && (
            <Button
              variant="outlined"
              color="primary"
              onClick={() => {
                Router.push(
                  `/${userRole}/users/participants/add_plot?id=${
                    participant.id
                  }`
                );
              }}
            >
              Add New Plot
            </Button>
          )}
        </div>
      </div>
      <DataTable
        offline={true}
        dataArr={participant.farm_plots}
        service="Services"
        disableSearch={true}
        reloadData={participant.id}
        dataCols={[
          { content: "ID", data: "id" },
          { content: "Code", data: "code" },
          {
            content: "Address",
            data: null,
            render: ({ barangay, purok }) => {
              const {
                municipality,
                province,
                region,
                country
              } = extractLocationDetails(barangay);
              return `${country.name} ${region.name} ${province.name} ${
                municipality.name
              } ${barangay.name} ${purok}`;
            }
          },
          { content: "Total Area", data: "totalArea" },
          { content: "Plantable Area", data: "plantableArea" },
          {
            content: "Assigned Tech.",
            data: "technician",
            render: technician => (technician ? technician.name : "-")
          },
          {
            content: "Actions",
            data: null,
            render: data => (
              <React.Fragment>
                <Button
                  variant="outlined"
                  onClick={() => {
                    Router.push(
                      `/${userRole}/users/participants/plot?id=${data.id}`
                    );
                  }}
                >
                  View
                </Button>
                {userRole !== "technician" && (
                  <Button
                    variant="outlined"
                    onClick={() => {
                      Router.push(
                        `/${userRole}/users/participants/edit_plot?id=${
                          data.id
                        }`
                      );
                    }}
                  >
                    Edit
                  </Button>
                )}
              </React.Fragment>
            )
          }
        ]}
      />
    </React.Fragment>
  );
};
