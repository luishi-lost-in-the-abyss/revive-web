import React from "react";
import Link from "next/link";
import {
  Grid,
  Button,
  TextField,
  withStyles,
  Typography
} from "@material-ui/core";
import SelectInput from "../../../common/SelectInput";
import { styles } from "../../../../theme";
import FeathersClient from "../../../../api/FeathersClient";
import FarmPlotForm from "./FarmPlotForm";

class AddPlot extends React.Component {
  constructor(props) {
    super(props);
    const { registrationParams } = props;
    this.state = {
      ...registrationParams.plotDetail,
      inputTechnician: "",
      inputFec: "",
      errors: {},
      errorType: "none",
      clusters: [],
      plotLocation: {
        countryArr: [],
        regionArr: [],
        provinceArr: [],
        municipalityArr: [],
        barangayArr: []
      }
    };
    this.onInputChange = this.onInputChange.bind(this);
    this.onFecInputChange = this.onFecInputChange.bind(this);
    this.onTechnicianInputChange = this.onTechnicianInputChange.bind(this);
    this.onLocationChange = this.onLocationChange.bind(this);
  }

  async componentDidMount() {
    this.getLocation("country", null);
    const client = FeathersClient.getClient();
    const clusters = await client
      .service("clusters")
      .find({ query: { $limit: "-1" } });
    this.setState({
      clusters: clusters.filter(cluster => !cluster.formal_cluster)
    });
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  onFecInputChange(evt) {
    const { inputFec, inputTechnician } = this.state;
    const { value } = evt.target;
    if (value === inputFec) return;
    this.setState({ inputFec: value, inputTechnician: "" }, async () => {
      const client = FeathersClient.getClient();
      const fecTech = await client
        .service("technicians")
        .find({ query: { $limit: "-1", fecId: value } });
      this.setState({
        technicians: fecTech
      });
    });
  }

  onTechnicianInputChange(evt) {
    const { value } = evt.target;
    this.setState({ inputTechnician: value });
  }

  async getLocation(type, value = null, parent = null) {
    const client = FeathersClient.getClient();
    const service = client.service(`location-${type}`);
    const results =
      !value && !parent
        ? await service.find({ query: { $limit: "-1" } }) // this will always be the country search
        : await service.find({
            query: {
              $limit: "-1",
              [`${parent}Id`]: value,
              $sort: { name: 1 }
            }
          });
    const plotLocation = this.state.plotLocation;

    this.setState({
      plotLocation: Object.assign({}, plotLocation, {
        [`${type}Arr`]: results.map(({ id, name }) => ({
          label: name,
          value: id
        }))
      })
    });
  }

  onLocationChange(evt) {
    const { name, value } = evt.target;
    switch (name) {
      case "country":
        this.getLocation("region", value, "Country");
        break;
      case "region":
        this.getLocation("province", value, "Region");
        break;
      case "province":
        this.getLocation("municipality", value, "Province");
        break;
      case "municipality":
        this.getLocation("barangay", value, "Municipality");
        break;
    }

    this.setState({ [name]: value }, () => {
      switch (name) {
        case "country":
          this.onInputChange({
            target: { name: "region", value: "" }
          });
        case "region":
          this.onInputChange({
            target: { name: "province", value: "" }
          });
        case "province":
          this.onInputChange({
            target: { name: "municipality", value: "" }
          });
        case "municipality":
          this.onInputChange({
            target: { name: "barangay", value: "" }
          });
          break;
        default:
          break;
      }
    });
  }

  getHelperText(name) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";
    if (errorType === "sameBarangay")
      return "Technician is in the same barangay as the plot location!";
    return null;
  }

  static getDerivedStateFromProps(props, prevState) {
    const { submitError, submitErrorType } = props;
    if (submitError && submitErrorType === "duplicateCode") {
      return { errors: { code: true }, errorType: "duplicateCode" };
    }
    return {};
  }

  validateLandInfo() {
    const { plotDetail } = this.props.registrationParams;
    let errors = {};
    const keys = Object.keys(plotDetail);

    keys.map(key => {
      if (key === "cropInsuranceDetails") {
        //only need to fill up insurance details if have insurance
        errors[key] = this.state["cropInsurance"] && !this.state[key];
        return;
      }
      if (key === "commodityOthers") {
        //only need to fill up other commodity if select other
        errors[key] = this.state["commodity"] === "others" && !this.state[key];
        return;
      }
      if (typeof this.state[key] === "boolean") return;
      if (key === "clusterId") return;
      errors[key] = !this.state[key];
    });

    let hasError = Object.values(errors).reduce((a, b) => a || b, false);

    if (hasError) {
      this.setState({ errors, errorType: "blank" });
      return false;
    }

    let { plantableArea, totalArea } = this.state;
    try {
      plantableArea = parseFloat(plantableArea);
      totalArea = parseFloat(totalArea);
    } catch (err) {
      plantableArea = 0;
      totalArea = 0;
    }
    errors["totalArea"] = totalArea < plantableArea || totalArea == 0;
    errors["plantableArea"] = plantableArea > totalArea;

    hasError = Object.values(errors).reduce((a, b) => a || b, false);

    if (hasError) {
      this.setState({ errors, errorType: "area" });
      return false;
    }

    this.setState({ errors: {}, errorType: "none" });
    return true;
  }

  onSubmit() {
    if (!this.validateLandInfo()) return;
    const errors = {};
    const { inputTechnician } = this.state;
    errors["inputTechnician"] = !inputTechnician;
    let hasErrors = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasErrors) {
      this.setState({
        errors,
        errorType: "blank"
      });
      return;
    }

    if (
      this.state.technicians.filter(tech => tech.id === inputTechnician)[0]
        .barangayId === this.state.barangay
    ) {
      this.setState({
        errors: { inputTechnician: true },
        errorType: "sameBarangay"
      });
      return;
    }

    this.setState({ errors: {} });
    this.props.onSubmit(this.state);
  }

  render() {
    const { classes, fecArr: fecs, registrationParams } = this.props;
    const { errors, technicians, inputFec, inputTechnician } = this.state;

    const commonSelectFieldProps = name => ({
      id: `plot-${name}`,
      name,
      value: this.state[name],
      labelWidth: 100,
      onInputChange: this.onInputChange,
      value: this.state[name],
      helperText: this.getHelperText(name)
    });

    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <form>
            <Typography variant="h5" gutterBottom>
              Technician Assignment
            </Typography>
            <SelectInput
              label="Fec"
              name={`inputFec`}
              value={inputFec}
              menuArr={fecs.map(fec => ({ label: fec.name, value: fec.id }))}
              onInputChange={this.onFecInputChange}
              error={errors.inputFec}
            />

            <SelectInput
              label="Technician"
              name={`inputTechnician`}
              value={inputTechnician}
              menuArr={
                technicians
                  ? technicians.map(tech => ({
                      label: tech.name + ` (${tech.user.username})`,
                      value: tech.id
                    }))
                  : []
              }
              onInputChange={this.onTechnicianInputChange}
              error={errors["inputTechnician"]}
              helperText={this.getHelperText(name)}
            />
            <Typography variant="h6" gutterBottom>
              Plot Details
            </Typography>
            <FarmPlotForm
              {...this.state}
              onInputChange={this.onInputChange}
              onLocationChange={this.onLocationChange}
              registrationParams={registrationParams}
            />

            <Grid container justify="space-between">
              <Grid item>
                <Link
                  href={
                    "/admin/users/participants/participant?id=" +
                    this.props.participantId
                  }
                >
                  <Button color="secondary" variant="outlined">
                    Cancel
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={this.onSubmit.bind(this)}
                >
                  Create
                </Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(AddPlot);
