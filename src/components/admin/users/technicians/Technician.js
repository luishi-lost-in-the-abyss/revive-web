import React from "react";
import {} from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
export default ({ technician }) => (
  <React.Fragment>
    <VerticalTable
      data={technician}
      tableValues={[
        { label: "Name", key: "name" },
        {
          label: "Barangay",
          key: "barangay",
          render: barangay => barangay.name
        },
        { label: "Contact", key: "contact" },
        {
          label: "FEC",
          key: "supervisor",
          render: supervisor => supervisor.fec.name
        },
        {
          label: "Supervisor",
          key: "supervisor",
          render: supervisor => supervisor.name
        }
      ]}
    />
  </React.Fragment>
);
