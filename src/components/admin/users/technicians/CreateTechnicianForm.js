import React from "react";
import Link from "next/link";
import {
  Grid,
  Button,
  TextField,
  withStyles,
  Typography
} from "@material-ui/core";
import { styles } from "../../../../theme";
import SelectInput from "../../../common/SelectInput";
import { validateEmail, extractLocationDetails } from "../../../../api/utility";
import FeathersClient from "../../../../api/FeathersClient";
import dynamic from "next/dynamic";
const MuiPhoneInput = dynamic(import("material-ui-phone-number"), {
  ssr: false
});

class CreateTechnicianForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      username: "",
      email: "",
      password: "",
      confirmPassword: "",
      contact: "",
      fec: "",
      supervisor: "",
      errors: {},
      fecs: [],
      country: "",
      region: "",
      province: "",
      municipality: "",
      barangayId: "",
      supervisors: [],
      technicianLocation: {
        countryArr: [],
        regionArr: [],
        provinceArr: [],
        municipalityArr: [],
        barangayArr: []
      },
      errorType: "none"
    };

    if (props.editMode) {
      const { technician } = props;
      this.state.username = technician.user.username;
      this.state.email = technician.user.email;
      this.state.name = technician.name;
      this.state.barangayId = technician.barangayId;
      this.state.supervisor = technician.supervisor.id;
      this.state.supervisors = [
        { label: technician.supervisor.name, value: technician.supervisor.id }
      ];
      const {
        country,
        region,
        province,
        municipality
      } = extractLocationDetails(technician.barangay);
      this.state = Object.assign({}, this.state, {
        country: country.id,
        region: region.id,
        province: province.id,
        municipality: municipality.id
      });
    }
    this.onLocationChange = this.onLocationChange.bind(this);
  }
  componentDidMount() {
    const { technician, editMode } = this.props;
    this.getLocation("country", null);
    if (editMode) {
      const {
        country,
        region,
        province,
        municipality
      } = extractLocationDetails(technician.barangay);
      this.getLocation("region", region.id, "country");
      this.getLocation("province", country.id, "region");
      this.getLocation("municipality", province.id, "province");
      this.getLocation("barangay", municipality.id, "municipality");
    }
  }

  static getDerivedStateFromProps(props, prevState) {
    return Object.assign(
      {},
      {
        fecs: props.fec.map(fec => ({
          label: fec.name,
          value: fec.id
        }))
      },
      props.submitError
        ? { errors: { username: true }, errorType: "submitError" }
        : null
    );
  }

  async getSupervisors(fec) {
    const client = FeathersClient.getClient();
    const supervisors = await client
      .service("supervisors")
      .find({ query: { $limit: "-1", fecId: fec } });
    this.setState({
      supervisors: supervisors.map(supervisor => ({
        label: supervisor.name,
        value: supervisor.id
      }))
    });
  }

  async getLocation(type, value = null, parent = null) {
    const client = FeathersClient.getClient();
    const service = client.service(`location-${type}`);
    const results =
      !value && !parent
        ? await service.find({ query: { $limit: "-1" } }) // this will always be the country search
        : await service.find({
            query: { $limit: "-1", [`${parent}Id`]: value, $sort: { name: 1 } }
          });
    const technicianLocation = this.state.technicianLocation;

    this.setState({
      technicianLocation: Object.assign({}, technicianLocation, {
        [`${type}Arr`]: results.map(({ id, name }) => ({
          label: name,
          value: id
        }))
      })
    });
  }

  onLocationChange(evt) {
    const { name, value } = evt.target;
    switch (name) {
      case "country":
        this.getLocation("region", value, "country");
        break;
      case "region":
        this.getLocation("province", value, "region");
        break;
      case "province":
        this.getLocation("municipality", value, "province");
        break;
      case "municipality":
        this.getLocation("barangay", value, "municipality");
        break;
    }

    this.setState({ [name]: value }, () => {
      switch (name) {
        case "country":
          this.onInputChange({
            target: { name: "region", value: "" }
          });
        case "region":
          this.onInputChange({
            target: { name: "province", value: "" }
          });
        case "province":
          this.onInputChange({
            target: { name: "municipality", value: "" }
          });
        case "municipality":
          this.onInputChange({
            target: { name: "barangayId", value: "" }
          });
          break;
        default:
          break;
      }
    });
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    const newState = { [name]: value };
    if (name === "fec") {
      newState.supervisor = "";
      this.getSupervisors(value);
    }
    this.setState(newState);
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";

    switch (name) {
      case "username":
        return "Username already taken";
      case "email":
        return "Invalid email address";
      case "password":
      case "confirmPassword":
        return "Passwords do not match!";
    }

    return null;
  }

  onSubmit() {
    const { editMode } = this.props;
    const {
      name,
      username,
      email,
      contact,
      password,
      confirmPassword,
      supervisor,
      barangayId
    } = this.state;

    let errors = {
      name: !name,
      username: !username,
      email: !email,
      password: editMode ? false : !password,
      confirmPassword: editMode ? false : !confirmPassword,
      supervisor: !supervisor,
      barangayId: !barangayId
    };

    let hasError = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasError) {
      this.setState({ errors, errorType: "blank" });
      return;
    }

    errors = {
      email: !validateEmail(email),
      password:
        editMode && (password || confirmPassword)
          ? password !== confirmPassword
          : false,
      confirmPassword:
        editMode && (password || confirmPassword)
          ? password !== confirmPassword
          : false
    };
    hasError = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasError) {
      this.setState({ errors, errorType: "validation" });
      return;
    }

    this.setState({ errors: {} });
    this.props.onSubmit({
      name,
      username,
      email,
      contact,
      password,
      supervisor,
      barangayId
    });
  }

  render() {
    const { classes, editMode } = this.props;
    const { fecs, supervisors, errors } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });
    const commonLocationFieldProps = name =>
      Object.assign({}, commonTextFieldProps(name), {
        onInputChange: this.onLocationChange
      });

    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <form>
            <Typography variant="h6" gutterBottom>
              User Details
            </Typography>
            <TextField
              label="Username"
              {...commonTextFieldProps("username")}
              disabled={editMode}
            />
            <Grid container spacing={8}>
              <Grid item xs={12} sm={8}>
                <TextField label="Email" {...commonTextFieldProps("email")} />
              </Grid>
              <Grid item xs={12} sm={4}>
                <MuiPhoneInput
                  variant="outlined"
                  enableLongNumbers={true}
                  onlyCountries={["ph"]}
                  countryCodeEditable={false}
                  defaultCountry="ph"
                  label="Contact Number"
                  value={this.state.contact}
                  {...Object.assign({}, commonTextFieldProps("contact"), {
                    onChange: value => {
                      this.onInputChange({
                        target: { name: "contact", value }
                      });
                    }
                  })}
                />
              </Grid>
            </Grid>
            <SelectInput
              {...Object.assign(
                {
                  menuArr: this.state.technicianLocation.countryArr,
                  label: "Country"
                },
                commonLocationFieldProps("country")
              )}
            />

            <SelectInput
              {...Object.assign(
                {
                  menuArr: this.state.technicianLocation.regionArr,
                  label: "Region"
                },
                commonLocationFieldProps("region")
              )}
            />

            <SelectInput
              {...Object.assign(
                {
                  menuArr: this.state.technicianLocation.provinceArr,
                  label: "Province"
                },
                commonLocationFieldProps("province")
              )}
            />

            <SelectInput
              {...Object.assign(
                {
                  menuArr: this.state.technicianLocation.municipalityArr,
                  label: "Municipality"
                },
                commonLocationFieldProps("municipality")
              )}
            />

            <SelectInput
              {...Object.assign(
                {
                  menuArr: this.state.technicianLocation.barangayArr,
                  label: "Barangay"
                },
                commonLocationFieldProps("barangayId")
              )}
            />
            <TextField
              label={`Password ${
                editMode ? "(Leave blank to keep password)" : ""
              }`}
              type="password"
              {...commonTextFieldProps("password")}
            />
            <TextField
              label="Confirm Password"
              type="password"
              {...commonTextFieldProps("confirmPassword")}
            />

            <Typography variant="h6" gutterBottom>
              Technician Details
            </Typography>
            <TextField
              label="Technician Name (Surname, First Name, Middle Name, Ext)"
              {...commonTextFieldProps("name")}
            />
            <SelectInput
              {...commonTextFieldProps("fec")}
              disabled={fecs.length === 0}
              label="FEC"
              onInputChange={this.onInputChange.bind(this)}
              menuArr={fecs}
            />
            <SelectInput
              {...commonTextFieldProps("supervisor")}
              disabled={supervisors.length === 0}
              label="Supervisor"
              onInputChange={this.onInputChange.bind(this)}
              menuArr={supervisors}
            />
            <Grid container justify="space-between">
              <Grid item>
                <Link
                  href={`/admin/users/technicians${
                    editMode ? `/technician?id=${this.props.technician.id}` : ""
                  }`}
                >
                  <Button color="secondary" variant="outlined">
                    Cancel
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={this.onSubmit.bind(this)}
                >
                  {editMode ? "Edit" : "Create"}
                </Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(CreateTechnicianForm);
