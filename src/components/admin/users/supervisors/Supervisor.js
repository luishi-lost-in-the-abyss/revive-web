import React from "react";
import {} from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
export default ({ supervisor }) => (
  <React.Fragment>
    <VerticalTable
      data={supervisor}
      tableValues={[
        { label: "Name", key: "name" },
        { label: "Contact", key: "contact" },
        { label: "FEC", key: "fec", render: fec => fec.name },
        {
          label: "No. of Technicians",
          key: "technicians",
          render: arr => arr.length
        }
      ]}
    />
  </React.Fragment>
);
