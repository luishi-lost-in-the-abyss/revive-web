import React from "react";
import Link from "next/link";
import {
  Grid,
  Button,
  TextField,
  withStyles,
  Typography
} from "@material-ui/core";
import { styles } from "../../../../theme";
import SelectInput from "../../../common/SelectInput";
import { validateEmail } from "../../../../api/utility";
import dynamic from "next/dynamic";
const MuiPhoneInput = dynamic(import("material-ui-phone-number"), {
  ssr: false
});

class CreateSupervisorForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      username: "",
      email: "",
      contact: "",
      password: "",
      confirmPassword: "",
      fec: "",
      errors: {},
      fecs: [],
      errorType: "none"
    };

    if (props.editMode) {
      const { supervisor } = props;
      this.state.username = supervisor.user.username;
      this.state.email = supervisor.user.email;
      this.state.name = supervisor.name;
      this.state.fec = supervisor.fecId;
    }
  }

  static getDerivedStateFromProps(props, prevState) {
    return Object.assign(
      {},
      {
        fecs: props.fec.map(fec => ({ label: fec.name, value: fec.id }))
      },
      props.submitError
        ? { errors: { username: true }, errorType: "submitError" }
        : null
    );
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";

    switch (name) {
      case "username":
        return "Username already taken";
      case "email":
        return "Invalid email address";
      case "password":
      case "confirmPassword":
        return "Passwords do not match!";
    }

    return null;
  }

  onSubmit() {
    const { editMode } = this.props;
    const {
      name,
      username,
      contact,
      email,
      password,
      confirmPassword,
      fec
    } = this.state;

    let errors = {
      name: !name,
      username: !username,
      email: !email,
      password: editMode ? false : !password,
      confirmPassword: editMode ? false : !confirmPassword,
      fec: !fec
    };

    let hasError = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasError) {
      this.setState({ errors, errorType: "blank" });
      return;
    }

    errors = {
      email: !validateEmail(email),
      password:
        editMode && (password || confirmPassword)
          ? password !== confirmPassword
          : false,
      confirmPassword:
        editMode && (password || confirmPassword)
          ? password !== confirmPassword
          : false
    };
    hasError = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasError) {
      this.setState({ errors, errorType: "validation" });
      return;
    }

    this.setState({ errors: {} });
    this.props.onSubmit({
      name,
      username,
      contact,
      email,
      password,
      fec
    });
  }

  render() {
    const { editMode, classes } = this.props;
    const { fecs, errors } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <form>
            <Typography variant="h6" gutterBottom>
              User Details
            </Typography>
            <TextField
              label="Username"
              {...commonTextFieldProps("username")}
              disabled={editMode}
            />
            <Grid container spacing={8}>
              <Grid item xs={12} sm={8}>
                <TextField label="Email" {...commonTextFieldProps("email")} />
              </Grid>
              <Grid item xs={12} sm={4}>
                <MuiPhoneInput
                  variant="outlined"
                  enableLongNumbers={true}
                  onlyCountries={["ph"]}
                  countryCodeEditable={false}
                  defaultCountry="ph"
                  label="Contact Number"
                  value={this.state.contact}
                  {...Object.assign({}, commonTextFieldProps("contact"), {
                    onChange: value => {
                      this.onInputChange({
                        target: { name: "contact", value }
                      });
                    }
                  })}
                />
              </Grid>
            </Grid>
            <TextField
              label={`Password ${
                editMode ? "(Leave blank to keep password)" : ""
              }`}
              type="password"
              {...commonTextFieldProps("password")}
            />
            <TextField
              label="Confirm Password"
              type="password"
              {...commonTextFieldProps("confirmPassword")}
            />

            <Typography variant="h6" gutterBottom>
              Supervisor Details
            </Typography>
            <TextField
              label="Supervisor Name"
              {...commonTextFieldProps("name")}
            />
            <SelectInput
              {...commonTextFieldProps("fec")}
              disabled={fecs.length === 0}
              label="FEC"
              onInputChange={this.onInputChange.bind(this)}
              menuArr={fecs}
            />
            <Grid container justify="space-between">
              <Grid item>
                <Link
                  href={`/admin/users/supervisors${
                    editMode ? `/supervisor?id=${this.props.supervisor.id}` : ""
                  }`}
                >
                  <Button color="secondary" variant="outlined">
                    Cancel
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={this.onSubmit.bind(this)}
                >
                  {editMode ? "Edit" : "Create"}
                </Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(CreateSupervisorForm);
