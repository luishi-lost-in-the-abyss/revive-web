import React from "react";
import {} from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
export default ({ fec }) => (
  <React.Fragment>
    <VerticalTable
      data={fec}
      tableValues={[
        { label: "Name", key: "name" },
        {
          label: "No. of Supervisors",
          key: "supervisors",
          render: arr => arr.length
        }
      ]}
    />
  </React.Fragment>
);
