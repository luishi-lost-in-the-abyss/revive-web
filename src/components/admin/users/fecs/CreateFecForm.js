import React from "react";
import Link from "next/link";
import { Grid, Button, TextField, withStyles } from "@material-ui/core";
import { styles } from "../../../../theme";

class CreateFecForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: "", errors: {}, errorType: "none" };
    if (props.editMode) {
      this.state.name = props.fec.name;
    }
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  getHelperText(name, i) {
    const { errors, errorType } = this.state;
    if (!errors[name]) return null;
    if (errorType === "blank") return "This field cannot be left blank.";

    return null;
  }

  onSubmit() {
    const { name } = this.state;
    if (!name) {
      this.setState({ errors: { name: !name }, errorType: "blank" });
      return;
    }
    this.setState({ errors: {} });
    this.props.onSubmit({ name });
  }

  render() {
    const { editMode, classes } = this.props;
    const { errors } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name],
      error: errors[name],
      helperText: this.getHelperText(name)
    });

    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <form>
            <TextField label="Name" {...commonTextFieldProps("name")} />
            <Grid container justify="space-between">
              <Grid item>
                <Link
                  href={`/admin/users/fecs${
                    editMode ? `/fec?id=${this.props.fec.id}` : ""
                  }`}
                >
                  <Button color="secondary" variant="outlined">
                    Cancel
                  </Button>
                </Link>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={this.onSubmit.bind(this)}
                >
                  {editMode ? "Edit" : "Create"}
                </Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(CreateFecForm);
