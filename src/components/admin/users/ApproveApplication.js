import React from "react";
import FeathersClient from "../../../api/FeathersClient";
import SelectInput from "../../common/SelectInput";
import { Typography, Button } from "@material-ui/core";

export default class AdminApproveApplication extends React.Component {
  constructor(props) {
    super(props);
    const inputTechnician = {},
      inputFec = {},
      errors = {};
    props.plots.map((plot, i) => {
      inputTechnician[i] = "";
      inputFec[i] = "";
      errors[i] = { fec: false, technician: false };
    });
    this.state = {
      fecs: [],
      technicians: {},
      inputFec,
      inputTechnician,
      errors,
      loading: true
    };
  }
  async componentDidMount() {
    const client = FeathersClient.getClient();
    let fecs;
    try {
      fecs = await client.service("fec").find({ query: { $limit: "-1" } });
      this.setState({
        fecs: fecs.map(fec => ({ label: fec.name, value: fec.id }))
      });
    } catch (err) {
      console.log(err);
    } finally {
      this.setState({ loading: false });
    }
  }

  approveApplication() {
    const { approveApplication, plots } = this.props;
    const { inputFec, inputTechnician, errors } = this.state;
    let hasError = false;
    const errorObj = Object.assign({}, errors);
    Object.values(inputFec).map((val, i) => {
      if (!val) {
        hasError = true;
        errorObj[i].fec = true;
      } else {
        errorObj[i].fec = false;
      }
    });
    Object.values(inputTechnician).map((val, i) => {
      if (!val) {
        hasError = true;
        errorObj[i].technician = true;
      } else {
        errorObj[i].technician = false;
      }
    });
    this.setState({ errors: errorObj });
    if (hasError) {
      return;
    }
    approveApplication(
      plots.map((plot, i) => ({
        plotId: plot.id,
        technicianId: inputTechnician[i]
      }))
    );
    return;
  }

  onFecInputChange(i, evt) {
    const { inputFec, inputTechnician } = this.state;
    const { value } = evt.target;
    if (value === inputFec[i]) return;
    this.setState(
      {
        inputFec: Object.assign({}, inputFec, { [i]: value }),
        inputTechnician: Object.assign({}, inputTechnician, { [i]: "" })
      },
      async () => {
        const { technicians } = this.state;
        const client = FeathersClient.getClient();
        const fecTech = await client.service("technicians").find({
          query: {
            fecId: value,
            //prevent assignment of same technician with their barangays
            barangayId: { $ne: this.props.plots[i].barangay },
            $limit: "-1"
          }
        });
        this.setState({
          technicians: Object.assign({}, technicians, { [i]: fecTech })
        });
      }
    );
  }

  onTechnicianInputChange(i, evt) {
    const { inputTechnician } = this.state;
    const { value } = evt.target;
    this.setState({
      inputTechnician: Object.assign({}, inputTechnician, { [i]: value })
    });
  }

  render() {
    const { plots } = this.props;
    const {
      fecs,
      technicians,
      inputFec,
      inputTechnician,
      errors,
      loading
    } = this.state;
    if (loading) return "loading...";
    return (
      <React.Fragment>
        <Typography variant="h5">Assign Technicians</Typography>
        <Typography>
          Note: Technicians that are from the same barangay as the plot location
          will not appear.
        </Typography>
        <form>
          {plots.map((plot, i) => (
            <div key={`plot-${i}`}>
              <Typography variant="h6">Plot #{i + 1}</Typography>
              <SelectInput
                label="Fec"
                id={`plot-fec-${i}`}
                name={`fec`}
                value={inputFec[i]}
                menuArr={fecs}
                onInputChange={this.onFecInputChange.bind(this, i)}
                error={errors[i].fec}
              />

              <SelectInput
                label="Technician"
                id={`plot-technician-${i}`}
                name={`technician`}
                value={inputTechnician[i]}
                menuArr={
                  technicians[i]
                    ? technicians[i].map(tech => ({
                        label: tech.name + ` (${tech.user.username})`,
                        value: tech.id
                      }))
                    : []
                }
                onInputChange={this.onTechnicianInputChange.bind(this, i)}
                error={errors[i].technician}
              />
            </div>
          ))}
        </form>
        <div
          style={{
            marginTop: 20,
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          <Button
            variant="outlined"
            color="secondary"
            onClick={this.props.goBack}
          >
            Back
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={this.approveApplication.bind(this)}
          >
            Approve Application
          </Button>
        </div>
      </React.Fragment>
    );
  }
}
