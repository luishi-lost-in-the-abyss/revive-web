import React from "react";
import {} from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
export default ({ dispatcher }) => (
  <React.Fragment>
    <VerticalTable
      data={dispatcher}
      tableValues={[
        { label: "Name", key: "name" },
        { label: "Contact", key: "contact" },
        { label: "FEC", key: "fec", render: fec => (fec ? fec.name : "") }
      ]}
    />
  </React.Fragment>
);
