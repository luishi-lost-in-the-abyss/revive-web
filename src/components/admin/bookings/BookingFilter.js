import {
  Grid,
  Typography,
  TextField,
  withStyles,
  Button,
  InputAdornment,
  IconButton,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails
} from "@material-ui/core";
import { styles } from "../../../theme";
import CancelIcon from "@material-ui/icons/Cancel";
import { DateTime } from "luxon";
import SelectInput from "../../common/SelectInput";
import FeathersClient from "../../../api/FeathersClient";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const initialState = {
  startDate: "",
  endDate: "",
  serviceCode: "",
  status: "",
  participantName: "",
  participantNameType: "participant",
  technicianName: "",
  showOnlyType: "all",
  fecId: "",
  filtersApplied: false
};

class BookingFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = Object.assign(
      { forceRefresh: new Date().getTime() },
      initialState
    );
    this.onInputChange = this.onInputChange.bind(this);
    this.resetAll = this.resetAll.bind(this);
    this.applyFilters = this.applyFilters.bind(this);
    this.fecArr = [];
  }
  async componentDidMount() {
    const client = FeathersClient.getClient();
    this.fecArr = await client.service("fec").find({ query: { $limit: "-1" } });
    this.setState({ forceRefresh: new Date().getTime() });
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  resetField(name) {
    this.setState({ [name]: "" });
  }

  resetAll() {
    this.setState(Object.assign({}, initialState));
    this.props.applyFilters(null);
  }

  applyFilters() {
    const findQuery = {};
    const {
      startDate,
      endDate,
      serviceCode,
      status,
      participantName,
      participantNameType,
      technicianName,
      showOnlyType,
      fecId
    } = this.state;
    if (startDate) {
      findQuery.startDate = {
        $gte: DateTime.fromJSDate(new Date(startDate))
          .startOf("day")
          .toJSDate()
      };
    }
    if (endDate) {
      findQuery.endDate = {
        $lte: DateTime.fromJSDate(new Date(endDate))
          .endOf("day")
          .toJSDate()
      };
    }
    if (serviceCode) {
      findQuery.serviceCode = { $like: `%${serviceCode}%` };
    }

    if (status) {
      findQuery.status = status;
    }
    if (participantName) {
      findQuery[
        participantNameType === "formal"
          ? "formalParticipantName"
          : "participantName"
      ] = { $like: `%${participantName}%` };
    }
    if (technicianName) {
      findQuery.technicianName = { $like: `%${technicianName}%` };
    }
    if (showOnlyType) {
      findQuery.showOnlyType = showOnlyType;
    }
    if (fecId) {
      findQuery.fecId = fecId;
    }
    this.setState({ filtersApplied: true });
    this.props.applyFilters(findQuery);
  }

  render() {
    const { classes } = this.props;
    const { filtersApplied, isExpanded } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange,
      name,
      value: this.state[name],
      InputLabelProps: { shrink: true },
      InputProps: {
        endAdornment: (
          <InputAdornment position="end">
            <IconButton onClick={this.resetField.bind(this, name)}>
              <CancelIcon />
            </IconButton>
          </InputAdornment>
        )
      }
    });
    return (
      <React.Fragment>
        <Typography variant="h5" gutterBottom>
          Filters
        </Typography>
        <ExpansionPanel expanded={filtersApplied || isExpanded}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Expand Filters</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Grid container spacing={8}>
              <Grid item xs={12}>
                <Typography variant="h6" gutterBottom>
                  Filter for bookings between these dates:
                </Typography>
                <Grid container spacing={8}>
                  <Grid item xs={6}>
                    <TextField
                      label="Start Date"
                      type="date"
                      {...commonTextFieldProps("startDate")}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <TextField
                      label="End Date"
                      type="date"
                      {...commonTextFieldProps("endDate")}
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={6}>
                <TextField
                  label="Service Code"
                  {...commonTextFieldProps("serviceCode")}
                />
              </Grid>
              <Grid item xs={6}>
                <SelectInput
                  {...commonTextFieldProps("status")}
                  label="Status"
                  onInputChange={this.onInputChange.bind(this)}
                  menuArr={[
                    { label: "Completed", value: "completed" },
                    { label: "Pending", value: "pending" },
                    { label: "Cancel", value: "cancel" }
                  ]}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  label="Participant's Name"
                  {...commonTextFieldProps("participantName")}
                />
              </Grid>
              <Grid item xs={6}>
                <SelectInput
                  {...commonTextFieldProps("participantNameType")}
                  label="Type for Input Name"
                  onInputChange={this.onInputChange.bind(this)}
                  menuArr={[
                    {
                      label: "Individual / Informal Cluster",
                      value: "participant"
                    },
                    { label: "Formal Cluster", value: "formal" }
                  ]}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  label="Technician's Name"
                  {...commonTextFieldProps("technicianName")}
                />
              </Grid>
              <Grid item xs={6}>
                <SelectInput
                  {...commonTextFieldProps("showOnlyType")}
                  label="Bookings by"
                  onInputChange={this.onInputChange.bind(this)}
                  menuArr={[
                    { label: "All", value: "all" },
                    { label: "Technicians", value: "technician" },
                    { label: "System Generated", value: "system" }
                  ]}
                />
              </Grid>
              <Grid item xs={6}>
                <SelectInput
                  {...Object.assign({}, commonTextFieldProps("fecId"), {
                    id: "bookingfilter-fecId",
                    label: "FEC",
                    onInputChange: this.onInputChange.bind(this),
                    menuArr: this.fecArr.map(fec => ({
                      label: fec.name,
                      value: fec.id
                    }))
                  })}
                />
              </Grid>
            </Grid>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <div>
            <Button onClick={this.applyFilters}>Apply Filters</Button>
            <Button onClick={this.resetAll}>Reset All</Button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(BookingFilter);
