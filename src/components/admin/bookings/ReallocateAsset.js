import Link from "next/link";
import { Grid, Button } from "@material-ui/core";
import SelectInput from "../../common/SelectInput";

export default class ReallocateAsset extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    props.booking.equipment_assets.map(({ equipmentId, serial }) => {
      this.state[equipmentId] = serial;
    });
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    const newState = { [name]: value };
    this.setState(newState);
  }

  render() {
    const { assets, booking, userRole } = this.props;
    const commonTextFieldProps = name => ({
      fullWidth: true,
      variant: "outlined",
      onInputChange: this.onInputChange,
      name,
      value: this.state[name]
    });
    return (
      <Grid container>
        <Grid item xs={12} sm={10} md={8}>
          <Grid container>
            {booking.equipment_assets.map(({ equipmentId, equipment }) => {
              return (
                <React.Fragment key={`selection-${equipmentId}`}>
                  <Grid item xs={6}>
                    {equipment.name}
                  </Grid>
                  <Grid item xs={6}>
                    <SelectInput
                      {...commonTextFieldProps("" + equipmentId)}
                      label={equipment.name}
                      menuArr={assets[equipmentId].map(serial => ({
                        label: serial,
                        value: serial
                      }))}
                    />
                  </Grid>
                </React.Fragment>
              );
            })}
          </Grid>
          <Grid container justify="space-between">
            <Grid item>
              <Link
                href={`/${userRole ? userRole : "admin"}/bookings/booking?id=${
                  booking.id
                }`}
              >
                <Button color="secondary" variant="outlined">
                  Cancel
                </Button>
              </Link>
            </Grid>
            <Grid item>
              <Button
                color="primary"
                variant="contained"
                onClick={this.props.onSubmit.bind(null, this.state)}
              >
                Allocate
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}
