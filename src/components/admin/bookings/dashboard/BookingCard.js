import Link from "next/link";
import {
  Card,
  CardContent,
  Typography,
  CardActions,
  Button
} from "@material-ui/core";
import { DateTime } from "luxon";

export default ({
  userRole = "admin",
  id,
  service,
  farm_plot,
  startDate,
  endDate,
  showEnd
}) => (
  <Card style={{ marginBottom: 10 }}>
    <CardContent>
      <Typography color="textSecondary" gutterBottom>
        Booking ID: {id}
      </Typography>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "flex-end"
        }}
      >
        <Typography variant="h5" component="h2">
          {showEnd ? "End" : "Start"}:{" "}
          {DateTime.fromISO(showEnd ? endDate : startDate).toFormat(
            "EEEE dd-MM-yyyy"
          )}
        </Typography>
      </div>
      <Typography color="textSecondary">
        {service.code} {service.name}
      </Typography>
      <Typography component="p">
        Plot {farm_plot.code} (ID: {farm_plot.id})
      </Typography>
    </CardContent>
    <CardActions>
      <Link passHref href={`/${userRole}/bookings/booking?id=${id}`}>
        <Button>View Details</Button>
      </Link>
    </CardActions>
  </Card>
);
