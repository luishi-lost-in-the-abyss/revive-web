import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../theme";
import {
  Grid,
  Button,
  TextField,
  Typography,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction
} from "@material-ui/core";
import UIActions from "../../../../actions/UIActions";
import SelectInput from "../../../common/SelectInput";
import FeathersClient from "../../../../api/FeathersClient";
import DataTable from "../../../common/DataTable";

class SelectEquipmentStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fecs: [],
      fecId: props.fecId,
      equipmentCount: {},
      equipment: props.equipment,
      submitError: false,
      errors: {}
    };
  }
  static getDerivedStateFromProps(props, prevState) {
    return Object.assign(
      {},
      {
        fecs: props.fec.map(fec => ({
          label: fec.name,
          value: fec.id
        }))
      }
    );
  }

  componentDidMount() {
    if (this.state.fecId) {
      this.getAvailableEquipment(this.state.fecId);
    }
  }

  async getAvailableEquipment(fecId) {
    const client = FeathersClient.getClient();
    //this.props.showLoadingModal(true);
    const { fromHour, toHour } = this.props.selectedTimes;
    const res = await client.service("booking-methods").create({
      method: "get_all_available_equipment",
      query: {
        fecId,
        startDate: fromHour.toJSDate(),
        endDate: toHour.toJSDate()
      }
    });
    this.setState({ equipmentCount: res.equipment }, () => {
      this.props.showLoadingModal(false);
    });
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    const newState = { [name]: value };
    if (name === "fecId") {
      //get unavailable equipment list
      this.getAvailableEquipment(value);
      this.props.onSelectFec(value);
    }
    this.setState(newState);
  }

  selectEquipment(equipObj) {
    const { equipment } = this.state;
    if (equipment[equipObj.id]) {
      const { [equipObj.id]: toRemove, ...finalObj } = equipment;
      this.setState({ equipment: finalObj }, () => {
        this.props.onSelectEquipment(this.state.equipment);
      });
    } else {
      this.setState(
        {
          equipment: Object.assign({}, equipment, { [equipObj.id]: equipObj })
        },
        () => {
          this.props.onSelectEquipment(this.state.equipment);
        }
      );
    }
  }

  render() {
    const { classes } = this.props;
    const { fecs, submitError, errors, equipment, equipmentCount } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name]
    });

    return (
      <React.Fragment>
        <Grid container spacing={16} justify="center">
          <Grid item xs={12} />
          <SelectInput
            {...commonTextFieldProps("fecId")}
            disabled={fecs.length === 0}
            label="FEC"
            onInputChange={this.onInputChange.bind(this)}
            menuArr={fecs}
          />
        </Grid>
        {this.state.fecId && (
          <div>
            {!this.props.disableSelectEquipment && (
              <React.Fragment>
                <Typography variant="h6" gutterBottom>
                  Selected Equipment
                </Typography>
                <List>
                  {Object.values(equipment).map(equip => (
                    <ListItem key={`selectedequipment-${equip.id}`}>
                      <ListItemText primary={equip.name} />
                      <ListItemSecondaryAction>
                        <Button
                          variant="outlined"
                          onClick={this.selectEquipment.bind(this, equip)}
                        >
                          Remove
                        </Button>
                      </ListItemSecondaryAction>
                    </ListItem>
                  ))}
                </List>
              </React.Fragment>
            )}
            <Typography variant="h6" gutterBottom>
              Equipment Browser
            </Typography>
            <DataTable
              colWidths={[400, null, 150]}
              service="equipment"
              dataCols={[
                { content: "Equipment Name", data: "name" },
                { content: "Description", data: "description" },
                {
                  content: "Actions",
                  data: null,
                  render: equip => {
                    return equipmentCount[equip.id] &&
                      equipmentCount[equip.id] > 0 &&
                      !this.props.disableSelectEquipment ? (
                      <Button
                        variant="outlined"
                        onClick={this.selectEquipment.bind(this, equip)}
                      >
                        {equipment[equip.id] ? "Remove" : "Add"}
                      </Button>
                    ) : equipmentCount[equip.id] &&
                      equipmentCount[equip.id] > 0 &&
                      this.props.disableSelectEquipment ? (
                      "AVAILABLE"
                    ) : (
                      "NO ASSETS AVAILABLE"
                    );
                  }
                }
              ]}
              searchCategory={[{ label: "Name", value: "name" }]}
              defaultSearchCategory="name"
            />
          </div>
        )}
      </React.Fragment>
    );
  }
}

const component = withStyles(styles)(SelectEquipmentStep);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  Object.assign({}, UIActions)
)(component);
