import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../theme";
import {
  Grid,
  TextField,
  MenuItem,
  Typography,
  Button
} from "@material-ui/core";
import AdminActions from "../../../../actions/AdminActions";

class SelectTechnicianStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      technicianId: props.technicianId
    };
  }

  onInputChange(evt) {
    const { value, name } = evt.target;
    const newState = { [name]: value };
    if (name === "technicianId") {
      this.props.onSelectTechnician(value);
    }
    this.setState(newState);
  }

  render() {
    if (this.state.loading) return "";
    const { technicians, classes } = this.props;
    const { technicianId } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name]
    });

    return (
      <React.Fragment>
        <Grid container spacing={16} alignItems="center">
          <Grid item xs={10}>
            <Typography variant="h5">Select Technician</Typography>
            <TextField
              {...commonTextFieldProps("technicianId")}
              select
              value={technicianId}
            >
              {technicians.map(tech => {
                return (
                  <MenuItem key={`tech-${tech.id}`} value={tech.id}>
                    {tech.name} (ID: {tech.id})
                  </MenuItem>
                );
              })}
            </TextField>
          </Grid>
          <Grid item xs={2}>
            <Button
              fullWidth
              onClick={this.onInputChange.bind(this, {
                target: {
                  value: "",
                  name: "technicianId"
                }
              })}
            >
              Clear
            </Button>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

const component = withStyles(styles)(SelectTechnicianStep);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  Object.assign({}, AdminActions)
)(component);
