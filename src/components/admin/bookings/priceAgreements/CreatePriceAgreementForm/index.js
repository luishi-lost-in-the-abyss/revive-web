import React from "react";
import Link from "next/link";
import { styles } from "../../../../../theme";
import {
  withStyles,
  Grid,
  TextField,
  Button,
  Typography
} from "@material-ui/core";
import SelectBookingModal from "./SelectBookingModal";
import { extractLocationDetails } from "../../../../../api/utility";
import SelectInput from "../../../../common/SelectInput";

class CreatePriceAgreementForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deliveryMode: "",
      bookingId: "",
      booking: {},
      deliveredPrice: "",
      truckingPrice: "",
      openSelectBooking: false,
      errors: {}
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.selectBooking = this.selectBooking.bind(this);
    this.toggleOpenSelect = this.toggleOpenSelect.bind(this);
  }

  onSubmit() {
    const {
      deliveryMode,
      deliveredPrice,
      bookingId,
      truckingPrice
    } = this.state;
    let errors = {
      deliveryMode: !deliveryMode,
      bookingId: !bookingId,
      deliveredPrice:
        !deliveredPrice ||
        parseFloat(deliveredPrice) < parseFloat(truckingPrice),
      truckingPrice:
        !truckingPrice || parseFloat(deliveredPrice) < parseFloat(truckingPrice)
    };
    console.log(errors);
    console.log(deliveredPrice < truckingPrice);
    let hasErrors = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasErrors) {
      this.setState({ errors });
      return;
    }
    this.setState({ errors: {} });
    this.props.onSubmit({
      deliveryMode,
      deliveredPrice,
      bookingId,
      truckingPrice
    });
  }

  selectBooking(booking) {
    this.setState({ booking, bookingId: booking.id }, this.toggleOpenSelect);
    console.log(booking);
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    const newState = { [name]: value };
    this.setState(newState);
  }

  toggleOpenSelect() {
    this.setState({ openSelectBooking: !this.state.openSelectBooking });
  }

  render() {
    const { classes, userRole } = this.props;
    const {
      booking,
      deliveredPrice,
      truckingPrice,
      openSelectBooking,
      errors
    } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange,
      name,
      value: this.state[name],
      error: errors[name]
    });

    const getBuyingPrice = () => {
      let parsedDelivered, parsedTrucking;
      try {
        parsedDelivered = parseFloat(deliveredPrice);
        parsedDelivered = parsedDelivered ? parsedDelivered : 0;
      } catch (err) {
        parsedDelivered = 0;
      }
      try {
        parsedTrucking = parseFloat(truckingPrice);
        parsedTrucking = parsedTrucking ? parsedTrucking : 0;
      } catch (err) {
        parsedTrucking = 0;
      }

      return parsedDelivered - parsedTrucking;
    };

    const renderBookingDetails = () => {
      if (Object.keys(booking).length === 0)
        return <Typography variant="h6">None selected</Typography>;
      const participant = booking.dataSnapshot.participants[0];
      const {
        barangay,
        municipality,
        province,
        region,
        country
      } = extractLocationDetails(
        booking.dataSnapshot.booking.farm_plot.barangay
      );
      return (
        <div>
          <Typography variant="h6">Booking ID: {booking.id}</Typography>
          <Typography variant="h6">
            Farmer Name: {participant.firstName} {participant.middleName}{" "}
            {participant.lastName}
          </Typography>
          <Typography variant="h6">
            Plot Address: {country.name} {region.name} {province.name}{" "}
            {municipality.name} {barangay.name}{" "}
            {booking.dataSnapshot.booking.farm_plot.purok}
          </Typography>
        </div>
      );
    };

    return (
      <React.Fragment>
        <SelectBookingModal
          open={openSelectBooking}
          onClose={this.toggleOpenSelect}
          selectBooking={this.selectBooking}
        />
        <Grid container>
          <Grid item xs={12} sm={10} md={8}>
            <form>
              <Grid container style={{ marginBottom: 20 }}>
                <Grid item xs={8}>
                  {renderBookingDetails()}
                </Grid>
                <Grid item xs={4}>
                  <Button
                    fullWidth
                    className={classes.formField}
                    variant="outlined"
                    onClick={this.toggleOpenSelect}
                  >
                    Select Booking
                  </Button>
                </Grid>
              </Grid>
              <SelectInput
                {...{
                  id: "priceagreement-deliveryMode",
                  name: "deliveryMode",
                  onInputChange: this.onInputChange.bind(this),
                  value: this.state.deliveryMode,
                  menuArr: [
                    { label: "Pick-up", value: "pickup" },
                    { label: "Delivered", value: "delivered" }
                  ],
                  label: "Delivery Mode",
                  error: errors["deliveryMode"]
                }}
              />
              <TextField
                label="Delivered Price (Php/Kg)"
                type="number"
                step="0.01"
                {...commonTextFieldProps("deliveredPrice")}
              />
              <TextField
                label="Trucking Price (Php/Kg)"
                type="number"
                step="0.01"
                {...commonTextFieldProps("truckingPrice")}
              />
              <Typography variant="h6">
                Buying Price: {getBuyingPrice()} Php/Kg
              </Typography>
              <Grid container justify="space-between">
                <Grid item>
                  <Link
                    href={`/${
                      userRole ? userRole : "admin"
                    }/bookings/price_agreements`}
                  >
                    <Button color="secondary" variant="outlined">
                      Cancel
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={this.onSubmit}
                  >
                    Create
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}
export default withStyles(styles)(CreatePriceAgreementForm);
