import React from "react";
import { styles } from "../../../../../theme";
import {
  withStyles,
  Grid,
  TextField,
  Button,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent
} from "@material-ui/core";
import DataTable from "../../../../common/DataTable";
import { DateTime } from "luxon";

class CreatePriceAgreementForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bookingId: "",
      errors: {}
    };
  }

  onSubmit() {}

  render() {
    const { classes } = this.props;

    return (
      <Dialog
        fullWidth
        maxWidth="lg"
        open={this.props.open}
        onClose={this.props.onClose}
      >
        <DialogTitle>Select Booking</DialogTitle>
        <DialogContent style={{ paddingTop: 10 }}>
          <DataTable
            service="bookings"
            dataCols={[
              { content: "ID", data: "id" },
              { content: "Status", data: "status" },
              {
                content: "Service Code",
                data: null,
                render: ({ dataSnapshot }) => {
                  if (dataSnapshot && dataSnapshot.booking.service) {
                    return dataSnapshot.booking.service.code;
                  }
                  return "-";
                }
              },
              {
                content: "Participant",
                data: null,
                render: data => {
                  const plot = data["farm_plot"];
                  if (plot.formalClusterId) {
                    return plot["formal_cluster"]["cluster"]["code"];
                  }
                  return plot.participant.name;
                }
              },
              {
                content: "Start Date",
                data: "startDate",
                render: date =>
                  DateTime.fromJSDate(new Date(date)).toFormat("dd-MM-yy H:mm")
              },
              {
                content: "End Date",
                data: "endDate",
                render: date =>
                  DateTime.fromJSDate(new Date(date)).toFormat("dd-MM-yy H:mm")
              },
              {
                content: "Technician",
                data: "technician",
                render: tech =>
                  tech
                    ? tech.name + ` (${tech.user.username})`
                    : "SYSTEM GENERATED"
              },
              {
                content: "Actions",
                data: null,
                render: booking => {
                  return (
                    <Button
                      variant="outlined"
                      onClick={this.props.selectBooking.bind(null, booking)}
                    >
                      Select
                    </Button>
                  );
                }
              }
            ]}
            searchCategory={[{ label: "ID", value: "id" }]}
            defaultSearchCategory="id"
          />
        </DialogContent>
      </Dialog>
    );
  }
}
export default withStyles(styles)(CreatePriceAgreementForm);
