import React from "react";
import {} from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";
import { extractLocationDetails } from "../../../../api/utility";
export default ({ agreement }) => (
  <React.Fragment>
    <VerticalTable
      data={agreement}
      tableValues={[
        { label: "ID", key: "id" },
        { label: "Booking ID", key: "bookingId" },
        {
          label: "Farmer Name",
          key: "booking",
          render: booking => {
            const participant = booking.dataSnapshot.participants[0];

            return `${participant.firstName} ${participant.middleName} ${
              participant.lastName
            }`;
          }
        },
        {
          label: "Delivered Price",
          key: "deliveredPrice"
        },
        {
          label: "Trucking Price",
          key: "truckingPrice"
        },
        {
          label: "Commodity",
          key: "booking",
          render: booking => booking.dataSnapshot.booking.farm_plot.commodity
        },
        {
          label: "Farm Plot Address",
          key: "booking",
          render: booking => {
            const {
              barangay,
              municipality,
              province,
              region,
              country
            } = extractLocationDetails(
              booking.dataSnapshot.booking.farm_plot.barangay
            );
            return `${country.name} ${region.name} ${province.name} ${
              municipality.name
            } ${barangay.name} ${booking.dataSnapshot.booking.farm_plot.purok}`;
          }
        }
      ]}
    />
  </React.Fragment>
);
