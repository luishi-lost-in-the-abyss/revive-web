import {
  Paper,
  withStyles,
  Typography,
  TextField,
  DialogContent,
  Dialog,
  DialogTitle,
  DialogActions,
  Grid,
  Button
} from "@material-ui/core";
import { styles } from "../../../theme";
import SelectInput from "../../common/SelectInput";
import { DateTime, Duration } from "luxon";

const statusMenuArr = [
  { label: "Pending", value: "pending" },
  { label: "Cancel", value: "cancel" }
];

const statusMenuCompleteArr = [
  { label: "Pending", value: "pending" },
  { label: "Completed", value: "completed" },
  { label: "Cancel", value: "cancel" }
];

const copmleteMenuArr = [
  { label: "Bad Weather", value: "Bad Weather" },
  { label: "Unit Breakdown", value: "Unit Breakdown" },
  { label: "Others", value: "others" }
];

const cancelMenuArr = [
  { label: "Bad Weather", value: "Bad Weather" },
  { label: "Under Repair", value: "Under Repair" },
  {
    label: "Permits and Licensed Concern",
    value: "Permits and Licensed Concern"
  },
  { label: "Reschedule", value: "Reschedule" },
  { label: "Others", value: "others" }
];

class EditStatusPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: props.initialStatus,
      actualStartDate: DateTime.fromJSDate(new Date(props.startDate))
        .setZone("local")
        .toString()
        .slice(0, 19),
      actualEndDate: DateTime.fromJSDate(new Date(props.endDate))
        .setZone("local")
        .toString()
        .slice(0, 19),
      statusReason: "",
      statusReasonOthers: "",
      completedArea: props.serviceArea,
      errors: {}
    };
    this.onInputChange = this.onInputChange.bind(this);
  }
  onInputChange(evt) {
    const { name, value } = evt.target;
    const newState = { [name]: value };
    if (name === "status") {
      newState.statusReason = "";
      newState.statusReasonOthers = "";
    }
    if (name === "completedArea") {
      if (parseFloat(value) > parseFloat(this.props.serviceArea)) {
        newState[name] = this.props.serviceArea;
      }
    }
    this.setState(newState);
  }

  onSubmit() {
    const {
      status,
      actualStartDate,
      actualEndDate,
      statusReason,
      completedArea,
      statusReasonOthers
    } = this.state;
    if (status === "cancel") {
      if (!statusReason) {
        this.setState({ errors: { statusReason: true } });
        return;
      } else if (statusReason === "others" && !statusReasonOthers) {
        this.setState({ errors: { statusReasonOthers: true } });
        return;
      }
    }

    this.setState({ errors: {} });
    this.props.editStatus({
      status,
      completedArea: status === "completed" ? completedArea : null,
      actualStartDate: new Date(actualStartDate),
      actualEndDate: new Date(actualEndDate),
      statusReason:
        status === "cancel" || status === "completed"
          ? statusReason === "others"
            ? statusReasonOthers
            : statusReason
          : ""
    });
  }
  render() {
    const { open, onCancel, classes, startDate, serviceArea } = this.props;
    const { status, statusReason, errors } = this.state;

    const showCompleted =
      DateTime.fromJSDate(new Date()) >=
      DateTime.fromJSDate(new Date(startDate));

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange,
      name,
      value: this.state[name],
      error: errors[name]
    });

    return (
      <Dialog open={open} onClose={onCancel} fullWidth>
        <DialogTitle>Update Status</DialogTitle>
        <DialogContent style={{ paddingTop: 5 }}>
          <SelectInput
            {...commonTextFieldProps("status")}
            onInputChange={this.onInputChange}
            label="Status"
            menuArr={showCompleted ? statusMenuCompleteArr : statusMenuArr}
          />
          {status === "completed" && (
            <React.Fragment>
              <TextField
                type="number"
                inputProps={{ max: serviceArea, step: 0.01 }}
                label={`Actual Area (Booked: ${serviceArea})`}
                {...commonTextFieldProps("completedArea")}
              />
              <TextField
                inputProps={{ step: 1 }}
                type="datetime-local"
                label={`Start Date and Time`}
                {...commonTextFieldProps("actualStartDate")}
              />
              <TextField
                inputProps={{ step: 1 }}
                type="datetime-local"
                label={`End Date and Time`}
                {...commonTextFieldProps("actualEndDate")}
              />
            </React.Fragment>
          )}
          {(status === "cancel" ||
            (status === "completed" &&
              parseFloat(this.state.completedArea) <
                parseFloat(serviceArea))) && (
            <React.Fragment>
              <SelectInput
                {...commonTextFieldProps("statusReason")}
                onInputChange={this.onInputChange}
                label="Reason"
                menuArr={status === "cancel" ? cancelMenuArr : copmleteMenuArr}
              />
              {statusReason === "others" && (
                <TextField
                  {...commonTextFieldProps("statusReasonOthers")}
                  label="Others (please specify)"
                />
              )}
            </React.Fragment>
          )}
        </DialogContent>
        <DialogActions>
          <Grid container justify="space-between">
            <Grid item>
              <Button color="secondary" variant="outlined" onClick={onCancel}>
                Cancel
              </Button>
            </Grid>
            <Grid item>
              <Button
                color="primary"
                variant="contained"
                onClick={this.onSubmit.bind(this)}
              >
                Edit
              </Button>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(styles)(EditStatusPopup);
