import {
  withStyles,
  TextField,
  MenuItem,
  Typography,
  Grid
} from "@material-ui/core";
import { styles } from "../../theme";
import SelectInput from "../common/SelectInput";

const CustomSelectField = ({
  multiple = false,
  menuArr,
  style,
  InputProps,
  ...props
}) => (
  <TextField
    InputLabelProps={{
      shrink: true,
      style: {
        transform: "translate(0, -50px) ",
        fontSize: 16,
        color: "black"
      }
    }}
    SelectProps={{
      multiple
    }}
    InputProps={Object.assign({}, { notched: false }, InputProps)}
    {...props}
    style={Object.assign({ marginTop: 50 }, style)}
    select
  >
    {menuArr.map(item => (
      <MenuItem key={`${name}-${item.value}`} value={item.value}>
        {item.label}
      </MenuItem>
    ))}
  </TextField>
);
const CustomTextField = ({ menuArr, style, InputProps, ...props }) => (
  <TextField
    InputLabelProps={{
      shrink: true,
      style: {
        transform: "translate(0, -50px) ",
        fontSize: 16,
        color: "black"
      }
    }}
    InputProps={Object.assign({}, { notched: false }, InputProps)}
    {...props}
    style={Object.assign({ marginTop: 50 }, style)}
  />
);

const component = ({ state, classes, errors, onInputChange }) => {
  const commonTextFieldProps = name => ({
    className: classes.formField,
    fullWidth: true,
    variant: "outlined",
    onChange: onInputChange,
    name,
    value: state[name],
    error: errors[name]
  });

  return (
    <div>
      <CustomSelectField
        {...Object.assign(
          {
            menuArr: [
              { label: "Married", value: "Married" },
              { label: "Single (never married)", value: "Single" },
              { label: "Separated", value: "Separated" },
              { label: "Divorced", value: "Divorced" },
              { label: "Widowed", value: "Widowed" }
            ],
            label: "1. What is your current marital situation"
          },
          commonTextFieldProps("q1")
        )}
      />
      {state.q1 === "Married" && (
        <div>
          <TextField
            label="Name of Spouse"
            {...commonTextFieldProps("spouseName")}
          />
          <TextField
            label="Spouse's Occupation"
            {...commonTextFieldProps("spouseOccupation")}
          />
        </div>
      )}
      <CustomSelectField
        {...Object.assign(
          {
            menuArr: [
              { label: "Yes", value: "Yes" },
              { label: "No", value: "No" }
            ],
            label:
              "2.	Is anybody else other than your spouse living with you (i.e. sharing the same house)?"
          },
          commonTextFieldProps("q2")
        )}
      />
      {state["q2"] === "Yes" && (
        <div style={{ marginTop: 15 }}>
          <Typography style={{ fontSize: 16 }}>
            3. Please indicate the number of persons living with you? Not
            including myself or my spouse there are:
          </Typography>
          <Grid container spacing={8}>
            <Grid item xs={4}>
              <CustomTextField
                {...commonTextFieldProps("q3a")}
                label="Persons age 18 or younger"
              />
            </Grid>
            <Grid item xs={4}>
              <CustomTextField
                {...commonTextFieldProps("q3b")}
                label="Persons age 19 to 49"
              />
            </Grid>
            <Grid item xs={4}>
              <CustomTextField
                {...commonTextFieldProps("q3c")}
                label="Persons age 50 to 59"
              />
            </Grid>
            <Grid item xs={4}>
              <CustomTextField
                {...commonTextFieldProps("q3d")}
                label="Persons age 60 to 69"
              />
            </Grid>
            <Grid item xs={4}>
              <CustomTextField
                {...commonTextFieldProps("q3e")}
                label="Persons age 70 or older"
              />
            </Grid>
          </Grid>
        </div>
      )}

      <CustomSelectField
        {...Object.assign(
          {
            menuArr: [
              { label: "Very satisfied", value: "Very satisfied" },
              { label: "Satisfied", value: "Satisfied" },
              {
                label: "Neither satisfied nor dissatisfied",
                value: "Neither"
              },
              { label: "Dissatisfied", value: "Dissatisfied" },
              { label: "Very dissatisfied", value: "Very dissatisfied" }
            ],
            label:
              "4.	Now, we have some general questions about your life. Taking all things together, how satisfied are you with your life as a whole these days?"
          },
          commonTextFieldProps("q4")
        )}
      />
      <CustomSelectField
        {...Object.assign(
          {
            menuArr: [
              { label: "Very satisfied", value: "Very satisfied" },
              { label: "Satisfied", value: "Satisfied" },
              {
                label: "Neither satisfied nor dissatisfied",
                value: "Neither"
              },
              { label: "Dissatisfied", value: "Dissatisfied" },
              { label: "Very dissatisfied", value: "Very dissatisfied" }
            ],
            label:
              "5.	How satisfied are you with your overall economic situation?"
          },
          commonTextFieldProps("q5")
        )}
      />
      <CustomSelectField
        {...Object.assign(
          {
            menuArr: [
              { label: "Very satisfied", value: "Very satisfied" },
              { label: "Satisfied", value: "Satisfied" },
              {
                label: "Neither satisfied nor dissatisfied",
                value: "Neither"
              },
              { label: "Dissatisfied", value: "Dissatisfied" },
              { label: "Very dissatisfied", value: "Very dissatisfied" }
            ],
            label: "6.	How satisfied are you with your health?"
          },
          commonTextFieldProps("q6")
        )}
      />
      <CustomSelectField
        {...Object.assign(
          {
            menuArr: [
              { label: "Excellent", value: "Excellent" },
              { label: "Very good", value: "Very good" },
              { label: "Good", value: "Good" },
              { label: "Fair", value: "Fair" },
              { label: "Poor", value: "poor" }
            ],
            label:
              "7.	Would you say your health is excellent, very good, good, fair, or poor?"
          },
          commonTextFieldProps("q7")
        )}
      />

      <CustomSelectField
        {...Object.assign(
          {
            menuArr: [
              { label: "Yes", value: "Yes" },
              { label: "No", value: "No" }
            ],
            label:
              "8.	In the last calendar month, were you seen by a medical doctor?"
          },
          commonTextFieldProps("q8")
        )}
      />

      <CustomTextField
        type="number"
        {...commonTextFieldProps("q9")}
        label="9.	How much was your total income from work in the previous month(s), before taxes and other deductions?"
      />

      <CustomTextField
        type="number"
        {...commonTextFieldProps("q10")}
        label="10. How many hours do you usually work per week?"
      />

      <div style={{ marginTop: 15 }}>
        <Typography style={{ fontSize: 16 }}>
          11. Household Operations. Please provide your best estimate of how
          much in total you and your family spent in January in the following
          categories. (Leave 0 to indicate no money spend)
        </Typography>
        <Grid container spacing={8} style={{ marginTop: 15 }}>
          <Grid item xs={4}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q11a")}
              label="Mortgage"
              placeholder="Interest and Principal"
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q11b")}
              label="Property tax"
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q11c")}
              label="Rent"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q11d")}
              label="Utilities and other fuels"
              placeholder="water supply, electricity, gas, other fuels, refuse disposal "
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q11e")}
              label="Communication"
              placeholder="internet, telephone, hand phone, cable TV subscription"
            />
          </Grid>
        </Grid>
      </div>
      <div style={{ marginTop: 15 }}>
        <Typography style={{ fontSize: 16 }}>
          12. Health spending. Please provide your best estimate of how much in
          total you and your family spent in the following categories. (Leave 0
          to indicate no money spend)
        </Typography>
        <Grid container spacing={8} style={{ marginTop: 15 }}>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q12a")}
              label="Health insurance"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q12b")}
              label="Prescription medication"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q12c")}
              label="Other medications"
              placeholder="including other medical products (e.g. wheelchair, crutches) and therapeutic equipment"
            />
          </Grid>
        </Grid>
      </div>
      <div style={{ marginTop: 15 }}>
        <Typography style={{ fontSize: 16 }}>
          13. Outpatient services: out-of-pocket cost of visits to doctors,
          traditional physicians (eg. traditional Chinese physicians),
          physiotherapists, and psychologists; eye care and dental service fees;
          lab tests. (Leave 0 to indicate no money spend)
        </Typography>
        <Grid container spacing={8} style={{ marginTop: 15 }}>
          <Grid item xs={12}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q13")}
              label=""
            />
          </Grid>
        </Grid>
      </div>
      <div style={{ marginTop: 15 }}>
        <Typography style={{ fontSize: 16 }}>
          14. Hospital services: out-of-pocket cost for hospital and nursing
          home care (Leave 0 to indicate no money spend)
        </Typography>
        <Grid container spacing={8} style={{ marginTop: 15 }}>
          <Grid item xs={12}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q14")}
              label=""
            />
          </Grid>
        </Grid>
      </div>
      <div style={{ marginTop: 15 }}>
        <Typography style={{ fontSize: 16 }}>
          15. Home nursing: hiring costs of a helper due to health problems (do
          not include domestic services by a maid here) (Leave 0 to indicate no
          money spend)
        </Typography>
        <Grid container spacing={8} style={{ marginTop: 15 }}>
          <Grid item xs={12}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q15")}
              label=""
            />
          </Grid>
        </Grid>
      </div>
      <div style={{ marginTop: 15 }}>
        <Typography style={{ fontSize: 16 }}>
          16. Leisure, sports, vacations. Please provide your best estimate of
          how much in total youyou’re your family spent in the following
          categories. (Leave 0 to indicate no money spend)
        </Typography>
        <Grid container spacing={8} style={{ marginTop: 15 }}>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q16a")}
              label="Entertainment"
              placeholder="tickets to movies, sporting events, concerts, museums, etc"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q16b")}
              label="Sports"
              placeholder="gym, exercise equipment such as bicycles, boats, etc"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q16c")}
              label="Hobbies and leisure equipment"
              placeholder="photography, reading materials (newspapers, magazines, books), pets, electronic entertainment "
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q16d")}
              label="Package tours and vacations"
              placeholder="transportation, accommodation, and recreational expenses on tours and trips "
            />
          </Grid>
        </Grid>
      </div>
      <div style={{ marginTop: 15 }}>
        <Typography style={{ fontSize: 16 }}>
          17. Transportation. Please provide your best estimate of how much in
          total you and your family spent following categories. (Leave 0 to
          indicate no money spend)
        </Typography>
        <Grid container spacing={8} style={{ marginTop: 15 }}>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q17a")}
              label="Vehicle payments"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q17b")}
              label="Vehicle insurance"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q17c")}
              label="Vehicle repair and maintenance"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q17d")}
              label="Public transportation"
              placeholder="car rental, taxi, bus"
            />
          </Grid>
        </Grid>
      </div>
      <CustomSelectField
        multiple
        {...Object.assign(
          {
            menuArr: [
              { label: "Television", value: "Television" },
              {
                label: "DVD/BLURAY player and recorder",
                value: "DVD/BLURAY player and recorder"
              },
              { label: "Refrigerator", value: "Refrigerator" },
              { label: "Microwave oven", value: "Microwave oven" },
              { label: "Vacuum cleaner", value: "Vacuum cleaner" },
              { label: "Washing machine", value: "Washing machine" },
              { label: "Clothes dryer", value: "Clothes dryer" },
              { label: "Air-conditioner", value: "Air-conditioner" }
            ],
            label:
              "18.	Did you and your spouse purchase any of the following household appliances during the period under review? (You may select multiple)"
          },
          commonTextFieldProps("q18")
        )}
      />
      <div style={{ marginTop: 15 }}>
        <Typography style={{ fontSize: 16 }}>
          29. Education, insurance, contributions, gifts, other. Please provide
          your best estimate of how much in total you and your family spent
          during the review period in the following categories
        </Typography>
        <Grid container spacing={8} style={{ marginTop: 15 }}>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q19a")}
              label="Education"
              placeholder="school fees, private tuition fees, books and supplies, assessment papers, study guides"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q19b")}
              label="Life insurance"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q19c")}
              label="Contributions to religious or charitable organisations"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              InputLabelProps={{ shrink: true }}
              type="number"
              {...commonTextFieldProps("q19d")}
              label="Any other spending, not yet reported"
              placeholder="such as toys, gear, equipment etc"
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default withStyles(styles)(component);

/*

{label: "", value: ""}

*/
