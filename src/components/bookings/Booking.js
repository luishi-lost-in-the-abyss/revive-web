import { useState } from "react";
import Link from "next/link";
import VerticalTable from "../common/VerticalTable";
import { Typography, Button, CircularProgress } from "@material-ui/core";
import { DateTime } from "luxon";
import BookingAssetCard from "./BookingAssetCard";
import BookingParticipantCard from "./BookingParticipantCard";

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import FeathersClient from "../../api/FeathersClient";
import { extractLocationDetails } from "../../api/utility";

export default ({ booking, informalClusterParticipants, userRole }) => {
  const client = FeathersClient.getClient();
  const [loadingPurchase, setLoadingPurchase] = useState(false);
  const [loadingInvoice, setLoadingInvoice] = useState(false);
  const [loadingDelivery, setLoadingDelivery] = useState(false);

  const downloadPurchase = async () => {
    setLoadingPurchase(true);
    const { dd: definition } = await client
      .service("generate-document-methods")
      .create({
        method: "generate_purchase",
        query: {
          bookingId: booking.id
        }
      });
    pdfMake.createPdf(definition).download(`RE-SO-${booking.id}.pdf`);
    setLoadingPurchase(false);
  };
  const downloadInvoice = async () => {
    setLoadingInvoice(true);
    const { dd: definition } = await client
      .service("generate-document-methods")
      .create({
        method: "generate_invoice",
        query: {
          bookingId: booking.id
        }
      });
    pdfMake.createPdf(definition).download(`RE-INV-${booking.id}.pdf`);
    setLoadingInvoice(false);
  };
  const downloadDelivery = async () => {
    setLoadingDelivery(true);
    const { dd: definition } = await client
      .service("generate-document-methods")
      .create({
        method: "generate_delivery",
        query: {
          bookingId: booking.id
        }
      });
    pdfMake.createPdf(definition).download(`RE-SR-${booking.id}.pdf`);
    setLoadingDelivery(false);
  };

  return (
    <React.Fragment>
      {!informalClusterParticipants && (
        <div style={{ textAlign: "right" }}>
          {booking.status !== "cancel" && (
            <Button
              disabled={loadingPurchase}
              color="primary"
              variant="outlined"
              style={{ marginRight: 10 }}
              onClick={downloadPurchase}
            >
              {loadingPurchase ? <CircularProgress size={12} /> : "SO"}
            </Button>
          )}
          {booking.status === "completed" && (
            <Button
              disabled={loadingInvoice}
              color="primary"
              variant="outlined"
              style={{ marginRight: 10 }}
              onClick={downloadInvoice}
            >
              {loadingInvoice ? <CircularProgress size={12} /> : "INV"}
            </Button>
          )}
          {booking.status !== "cancel" && (
            <Button
              disabled={loadingDelivery}
              color="primary"
              variant="outlined"
              onClick={downloadDelivery}
            >
              {loadingDelivery ? <CircularProgress size={12} /> : "SR"}
            </Button>
          )}
        </div>
      )}
      <Typography variant="h6">Booking Details</Typography>
      <VerticalTable
        data={booking}
        tableValues={[
          {
            label: "Service Code (Name)",
            key: "service",
            render: ({ code, name }) => `${code} (${name})`
          },
          {
            label: "Booked Dates",
            key: null,
            render: ({ startDate, endDate }) =>
              DateTime.fromJSDate(new Date(startDate)).toFormat(
                "dd-MM-yy H:mm"
              ) +
              " - " +
              DateTime.fromJSDate(new Date(endDate)).toFormat("dd-MM-yy H:mm")
          },
          {
            label: "Actual Service Dates",
            key: null,
            render: ({
              status,
              actualStartDate,
              actualEndDate,
              startDate,
              endDate
            }) =>
              status === "completed"
                ? DateTime.fromJSDate(
                    new Date(actualStartDate ? actualStartDate : startDate)
                  ).toFormat("dd-MM-yy H:mm") +
                  " - " +
                  DateTime.fromJSDate(
                    new Date(actualEndDate ? actualEndDate : endDate)
                  ).toFormat("dd-MM-yy H:mm")
                : "-"
          },
          {
            label: "Status",
            key: "status",
            render: status => {
              return status === "completed"
                ? `completed - ${
                    booking.actualCompletedArea
                      ? booking.actualCompletedArea
                      : booking.serviceArea
                  } ${booking.statusReason ? `(${booking.statusReason})` : ""}`
                : status === "cancel"
                ? `cancel - ${booking.statusReason}`
                : status;
            }
          },
          {
            label: "Participant",
            data: null,
            render: data => {
              const plot = data["farm_plot"];
              if (plot.formalClusterId) {
                return plot["formal_cluster"]["cluster"]["code"];
              }
              return plot.participant.name;
            }
          },
          {
            label: "Plot Code",
            data: null,
            render: data => {
              const plot = data["farm_plot"];
              return plot.code;
            }
          },
          {
            label: "Commodity",
            data: null,
            render: data => data["farm_plot"].commodity
          },
          {
            label: "Plot Address",
            data: null,
            render: data => {
              const plot = data["farm_plot"];
              const {
                country,
                region,
                municipality,
                province,
                barangay
              } = extractLocationDetails(plot.barangay);
              const { purok } = plot;
              return (
                <React.Fragment>
                  <Typography>{country.name}</Typography>
                  <Typography>{region.name}</Typography>
                  <Typography>{municipality.name}</Typography>
                  <Typography>{province.name}</Typography>
                  <Typography>{barangay.name}</Typography>
                  <Typography>{purok}</Typography>
                </React.Fragment>
              );
            }
          }
        ]}
      />
      <div
        style={{
          marginTop: 20,
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <Typography variant="h6">Equipment Assets Allocation</Typography>
        {(userRole === "dispatcher" || userRole === "admin") &&
          booking.status === "pending" && (
            <div>
              <Link
                href={`/${userRole}/bookings/reallocate_asset?id=${booking.id}`}
              >
                <Button variant="outlined">Reallocate Assets</Button>
              </Link>
            </div>
          )}
      </div>
      <div style={{ display: "flex" }}>
        {booking["equipment_assets"].map(asset => (
          <div key={`asset-${asset.serial}`} style={{ marginRight: 20 }}>
            <BookingAssetCard asset={asset} userRole={userRole} />
          </div>
        ))}
      </div>
      {informalClusterParticipants && (
        <React.Fragment>
          <Typography style={{ marginTop: 20 }} variant="h6">
            Informal Cluster Plot Participants
          </Typography>
          <div style={{ display: "flex" }}>
            {informalClusterParticipants.map(participant => (
              <div
                key={`participant-${participant.id}`}
                style={{ marginRight: 20 }}
              >
                <BookingParticipantCard {...participant} booking={booking} />
              </div>
            ))}
          </div>
        </React.Fragment>
      )}
      <Link passHref href={`/${userRole}/bookings/view`}>
        <Button color="secondary" variant="outlined" style={{ marginTop: 20 }}>
          Go Back
        </Button>
      </Link>
    </React.Fragment>
  );
};
