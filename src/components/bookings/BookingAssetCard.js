import Link from "next/link";
import {
  Card,
  CardContent,
  Typography,
  CardActions,
  Button
} from "@material-ui/core";

export default ({ userRole, asset }) => {
  const { serial, equipment } = asset;
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="h2">
          {serial}
        </Typography>
        <Typography component="p">{equipment.name}</Typography>
      </CardContent>
    </Card>
  );
};
