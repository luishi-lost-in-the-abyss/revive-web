import { useState } from "react";
import {
  Card,
  CardContent,
  Typography,
  CardActions,
  Button,
  CircularProgress
} from "@material-ui/core";

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import FeathersClient from "../../api/FeathersClient";

export default ({ farm_plots, booking, ...participant }) => {
  const client = FeathersClient.getClient();
  const [loadingPurchase, setLoadingPurchase] = useState(false);
  const [loadingInvoice, setLoadingInvoice] = useState(false);
  const [loadingDelivery, setLoadingDelivery] = useState(false);

  const downloadPurchase = async () => {
    setLoadingPurchase(true);
    const { dd: definition } = await client
      .service("generate-document-methods")
      .create({
        method: "generate_purchase_informal",
        query: {
          bookingId: booking.id,
          participantId: participant.id
        }
      });
    pdfMake
      .createPdf(definition)
      .download(`RE-SO-${booking.id}-${participant.id}.pdf`);
    setLoadingPurchase(false);
  };
  const downloadInvoice = async () => {
    setLoadingInvoice(true);
    const { dd: definition } = await client
      .service("generate-document-methods")
      .create({
        method: "generate_invoice_informal",
        query: {
          bookingId: booking.id,
          participantId: participant.id
        }
      });
    pdfMake
      .createPdf(definition)
      .download(`RE-INV-${booking.id}-${participant.id}.pdf`);
    setLoadingInvoice(false);
  };
  const downloadDelivery = async () => {
    setLoadingDelivery(true);
    const { dd: definition } = await client
      .service("generate-document-methods")
      .create({
        method: "generate_delivery_informal",
        query: {
          bookingId: booking.id,
          participantId: participant.id
        }
      });
    pdfMake
      .createPdf(definition)
      .download(`RE-SR-${booking.id}-${participant.id}.pdf`);
    setLoadingDelivery(false);
  };

  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="h2">
          {participant.name}
        </Typography>
        <Typography component="p">
          Total Plantable Area:{" "}
          {farm_plots.reduce((a, b) => a + b.plantableArea, 0)}
        </Typography>
      </CardContent>
      <CardActions>
        {booking.status !== "cancel" && (
          <Button
            disabled={loadingPurchase}
            onClick={downloadPurchase}
            variant="outlined"
            color="primary"
          >
            {loadingPurchase ? <CircularProgress size={12} /> : "SO"}
          </Button>
        )}
        {booking.status === "completed" && (
          <Button
            disabled={loadingInvoice}
            variant="outlined"
            color="primary"
            onClick={downloadInvoice}
          >
            {loadingInvoice ? <CircularProgress size={12} /> : "INV"}
          </Button>
        )}
        {booking.status !== "cancel" && (
          <Button
            disabled={loadingDelivery}
            variant="outlined"
            color="primary"
            onClick={downloadDelivery}
          >
            {loadingDelivery ? <CircularProgress size={12} /> : "SR"}
          </Button>
        )}
      </CardActions>
    </Card>
  );
};
