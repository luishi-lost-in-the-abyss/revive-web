import React from "react";
import Link from "next/link";
import Router, { withRouter } from "next/router";
import { Divider, withStyles, Button } from "@material-ui/core";
import { styles } from "../../theme";
import LogoutButton from "../common/LogoutButton";
import DrawerMenuItem from "../common/DrawerMenuItem";

const DrawerMenu = ({ classes, router }) => {
  return (
    <div style={{ display: "flex", flexDirection: "column", height: "100%" }}>
      <Link passHref href="/dispatcher">
        <a>
          <img
            src="/static/images/LOGO_2.jpg"
            style={{ width: "100%", padding: 10 }}
          />
        </a>
      </Link>
      <Divider />
      <DrawerMenuItem
        title="Bookings"
        path="/dispatcher/bookings"
        otherPaths={[]}
        items={[
          {
            title: "Dashboard",
            path: "/dispatcher/bookings/dashboard"
          },
          { title: "View Bookings", path: "/dispatcher/bookings/view" },
          {
            title: "Create Bookings",
            path: "/dispatcher/bookings/create_booking"
          },
          {
            title: "Asset Availability",
            path: "/dispatcher/bookings/check_asset_availability"
          }
        ]}
      />
      <Divider />
      <DrawerMenuItem
        title="Resources"
        path="/dispatcher/resources"
        otherPaths={[]}
        items={[
          {
            title: "Service Types",
            path: "/dispatcher/resources/service_category"
          },
          {
            title: "Services",
            path: "/dispatcher/resources/services"
          },
          { title: "Equipment", path: "/dispatcher/resources/equipment" },
          {
            title: "Assets Dashboard",
            path: "/dispatcher/resources/asset_dashboard"
          }
        ]}
      />
      <Divider />
      <DrawerMenuItem
        title="Reports"
        path="/dispatcher/reports"
        otherPaths={[]}
        items={[
          {
            title: "Raw Data",
            path: "/dispatcher/reports/raw_data"
          },
          {
            title: "Bookings",
            path: "/dispatcher/reports/bookings"
          },
          {
            title: "Services",
            path: "/dispatcher/reports/services"
          }
        ]}
      />
      <Divider />
      <div style={{ display: "flex", flex: 1 }} />
      <Button
        variant="outlined"
        onClick={() => {
          Router.push("/dispatcher/my_account");
        }}
      >
        My Account
      </Button>
      <LogoutButton
        onLogout={() => {
          Router.push("/login");
        }}
      />
    </div>
  );
};

export default withRouter(withStyles(styles)(DrawerMenu));
