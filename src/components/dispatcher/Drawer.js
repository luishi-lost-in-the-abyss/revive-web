import React from "react";
import { Drawer, Hidden, withStyles } from "@material-ui/core";
import DrawerMenu from "./DrawerMenu";
import { styles } from "../../theme";

const SupervisorDrawer = ({ openMobile, handleToggle, classes }) => (
  <nav className={classes.drawer}>
    <Hidden smUp implementation="css">
      <Drawer
        variant="temporary"
        anchor="left"
        open={openMobile}
        onClose={handleToggle}
        classes={{
          paper: classes.drawerPaper
        }}
        ModalProps={{
          keepMounted: true
        }}
      >
        <DrawerMenu />
      </Drawer>
    </Hidden>

    <Hidden xsDown implementation="css">
      <Drawer
        classes={{
          paper: classes.drawerPaper
        }}
        variant="permanent"
        open
      >
        <DrawerMenu />
      </Drawer>
    </Hidden>
  </nav>
);

export default withStyles(styles)(SupervisorDrawer);
