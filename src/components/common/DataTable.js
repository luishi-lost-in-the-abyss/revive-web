import React from "react";
import PropTypes from "prop-types";
import { styles } from "../../theme";
import {
  withStyles,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Grid,
  TextField,
  Button,
  IconButton,
  TableFooter,
  TablePagination
} from "@material-ui/core";
import CancelIcon from "@material-ui/icons/Cancel";
import FeathersClient from "../../api/FeathersClient";
import SelectInput from "./SelectInput";

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

class DataTable extends React.Component {
  constructor(props) {
    super(props);
    const { offline, dataArr, defaultSearchCategory } = props;
    this.state = {
      initialLoading: !offline,
      loading: false,
      dataArr: dataArr ? dataArr : [],
      activePage: 1,
      paginate: { limit: 10, skip: 0, total: 0 },
      search: "",
      searchCategory: defaultSearchCategory ? defaultSearchCategory : "",
      enableSearch: false
    };

    this.onInputChange = this.onInputChange.bind(this);
    this.loadData = this.loadData.bind(this);
    this.enableSearch = this.enableSearch.bind(this);
    this.resetSearch = this.resetSearch.bind(this);
    this.onChangePage = this.onChangePage.bind(this);
  }

  componentDidMount() {
    if (this.state.initialLoading && !this.props.offline) {
      //handle initial loading of data
      this.loadData();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.reloadData !== this.props.reloadData) {
      this.setState({ activePage: 1 }, () => {
        this.loadData();
      });
    }
  }

  async loadData() {
    if (this.props.offline) {
      this.setState({ dataArr: this.props.dataArr });
      return;
    }
    const client = FeathersClient.getClient();
    const {
      paginate,
      activePage,
      enableSearch,
      search,
      searchCategory
    } = this.state;
    const { findQuery, loadData, service: dataService } = this.props;
    const skipQuery = paginate.limit * (activePage - 1);
    if (loadData) {
      loadData(
        {
          query: Object.assign(
            {},
            { $skip: skipQuery },
            findQuery,
            enableSearch && searchCategory
              ? { [searchCategory]: { $like: `%${search}%` } }
              : null
          )
        },
        res => {
          this.setState({
            dataArr: res.data,
            loading: false,
            initialLoading: false,
            paginate: { limit: res.limit, skip: res.skip, total: res.total }
          });
        }
      );
      return;
    }

    const res = await client.service(dataService).find({
      query: Object.assign(
        { $skip: skipQuery },
        findQuery ? findQuery : {},
        enableSearch && searchCategory
          ? { [searchCategory]: { $like: `%${search}%` } }
          : null
      )
    });
    const { data, limit, skip, total } = res;
    this.setState({
      dataArr: data,
      initialLoading: false,
      paginate: { limit, skip, total }
    });
  }

  getDataRows() {
    const { initialLoading, loading, dataArr } = this.state;
    const { dataCols, service, rowLink } = this.props;

    if (initialLoading || loading) {
      return (
        <TableRow>
          <TableCell
            colSpan={dataCols.length}
            align="center"
            key={"data-loading"}
          >
            Fetching data...
          </TableCell>
        </TableRow>
      );
    }

    if (dataArr.length === 0) {
      return (
        <TableRow>
          <TableCell
            colSpan={dataCols.length}
            align="center"
            key={"data-loading"}
          >
            Empty
          </TableCell>
        </TableRow>
      );
    }

    return dataArr.map((data, dataIndex) => {
      return (
        <TableRow key={`${service}-${data.id}-${dataIndex}`}>
          {dataCols.map((dat, i) => (
            <TableCell key={`data-${service}-${i}`}>
              {dat.render
                ? dat.data
                  ? dat.render(data[dat.data])
                  : dat.render(data, dataIndex)
                : data[dat.data]}
            </TableCell>
          ))}
        </TableRow>
      );
    });
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  enableSearch() {
    this.setState({ enableSearch: true, activePage: 1 }, this.loadData);
  }

  resetSearch() {
    const { defaultSearchCategory } = this.props;
    this.setState(
      {
        search: "",
        searchCategory: defaultSearchCategory ? defaultSearchCategory : "",
        activePage: 1,
        enableSearch: false
      },
      this.loadData
    );
  }

  renderSearch() {
    const {
      searchCategory: searchCategories,
      hideSearchCategories
    } = this.props;
    const { search, searchCategory, enableSearch } = this.state;
    return (
      <Grid
        container
        style={{ marginBottom: 20 }}
        alignItems="center"
        spacing={16}
      >
        <Grid item xs={12} md={hideSearchCategories ? 10 : 7}>
          <TextField
            variant="outlined"
            name="search"
            label="Search"
            fullWidth
            value={search}
            onChange={this.onInputChange}
          />
        </Grid>
        {!hideSearchCategories && (
          <Grid item xs={6} md={3} style={{ alignItems: "center" }}>
            <SelectInput
              id="datatable-search"
              label="Category"
              name="searchCategory"
              value={searchCategory}
              menuArr={searchCategories ? searchCategories : []}
              onInputChange={this.onInputChange}
              containerStyle={{ margin: 0 }}
            />
          </Grid>
        )}
        <Grid item xs={6} md={2} style={{ justifyContent: "center" }}>
          <Button
            variant="outlined"
            color="primary"
            onClick={this.enableSearch}
          >
            Search
          </Button>
          <IconButton onClick={this.resetSearch}>
            <CancelIcon />
          </IconButton>
        </Grid>
      </Grid>
    );
  }

  onChangePage(evt, page) {
    this.setState({ activePage: page + 1 }, this.loadData);
  }

  render() {
    const { dataCols, colWidths, disableSearch } = this.props;
    const { paginate, activePage } = this.state;
    return (
      <React.Fragment>
        {disableSearch ? null : this.renderSearch()}
        <Table>
          {colWidths ? (
            <colgroup>
              {colWidths.map((width, i) =>
                width ? (
                  <col key={`colwidth-${i}`} width={width} />
                ) : (
                  <col key={`colwidth-${i}`} />
                )
              )}
            </colgroup>
          ) : null}
          <TableHead>
            <TableRow>
              {dataCols.map((data, i) => (
                <CustomTableCell key={`tableheader-${i}`}>
                  {data.content}
                </CustomTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>{this.getDataRows()}</TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                count={paginate.total}
                page={activePage - 1}
                rowsPerPage={paginate.limit}
                onChangePage={this.onChangePage}
                rowsPerPageOptions={[paginate.limit]}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </React.Fragment>
    );
  }
}

export default DataTable;

DataTable.propTypes = {
  offline: PropTypes.bool, //set true for no loading of data
  dataArr: PropTypes.array, // for offline data
  service: PropTypes.string.isRequired,
  colWidths: PropTypes.array, // [200, null, 200]
  dataCols: PropTypes.array.isRequired, // {content, data, render}
  findQuery: PropTypes.object,
  loadData: PropTypes.func,
  reloadData: PropTypes.number,
  rowLink: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  searchCategory: PropTypes.array, // [{label: "", value: ""}]
  defaultSearchCategory: PropTypes.string,
  disableSearch: PropTypes.bool,
  hideSearchCategories: PropTypes.bool
};
