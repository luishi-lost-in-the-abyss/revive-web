import React from "react";
import Router, { withRouter } from "next/router";
import Link from "next/link";
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Typography,
  List,
  ListItem,
  withStyles
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { styles } from "../../theme";

const DrawerMenuItem = ({
  title,
  path,
  otherPaths = [],
  items,
  classes,
  router
}) => {
  const isCurrentPathExpanded = function(path) {
    const routerPath = router.pathname;
    return (
      routerPath.indexOf(path) === 0 ||
      otherPaths.filter(a => routerPath.indexOf(a) === 0).length > 0
    );
  };
  const isCurrentPath = function(path) {
    return router.pathname.indexOf(path) === 0;
  };

  const LinkItem = ({ path, title }) => (
    <Link passHref href={path}>
      <ListItem
        button
        selected={isCurrentPath(path)}
        className={classes.sideMenuItem}
      >
        {title}
      </ListItem>
    </Link>
  );

  return (
    <ExpansionPanel
      elevation={0}
      color="green"
      defaultExpanded={isCurrentPathExpanded(path)}
    >
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography align="center" variant="overline">
          {title}
        </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <List style={{ width: "100%" }}>
          {items.map(({ path, title }, i) => (
            <LinkItem key={`menu-${title}-${i}`} path={path} title={title} />
          ))}
        </List>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

export default withRouter(withStyles(styles)(DrawerMenuItem));
