import React from "react";
import {
  Popper,
  withStyles,
  Paper,
  ClickAwayListener,
  Button
} from "@material-ui/core";
import { styles } from "../../theme";

const ConfirmationPopup = ({
  open,
  anchorEl,
  onClickAway,
  classes,
  title,
  onConfirm,
  onCancel,
  dialogMode
}) => (
  <Popper
    open={open}
    anchorEl={anchorEl}
    style={dialogMode ? { zIndex: 1301 } : {}}
  >
    <ClickAwayListener onClickAway={onClickAway}>
      <Paper className={classes.rootPaper}>
        <p style={{ textAlign: "center", maxWidth: 400 }}>{title}</p>
        <div style={{ textAlign: "center" }}>
          <Button
            color="secondary"
            variant="outlined"
            onClick={onCancel}
            style={{ marginRight: 10 }}
          >
            Cancel
          </Button>
          <Button variant="contained" color="primary" onClick={onConfirm}>
            Confirm
          </Button>
        </div>
      </Paper>
    </ClickAwayListener>
  </Popper>
);

export default withStyles(styles)(ConfirmationPopup);

/*
open (boolean) - if popper is opened
anchorEl (element) - where to show the popper
onClickAway (function) - what hapepns when clicked outside of popup
title (string) - title
onCancel (function) - what happens when user presses cancel
onConfirm (function) - what happens when user presses
dialogMode (boolean) - if being used with dialog
*/
