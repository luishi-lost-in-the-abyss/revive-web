import React from "react";
import { Table, TableBody, TableRow, TableCell } from "@material-ui/core";
export default ({ id = new Date().getTime(), data, tableValues, colWidth }) => (
  <Table>
    {colWidth ? (
      <colgroup>
        {colWidth.map((width, i) =>
          width ? (
            <col key={`${id}-col-width-${i}`} width={width} />
          ) : (
            <col key={`${id}-col-width-${i}`} />
          )
        )}
      </colgroup>
    ) : null}
    <TableBody>
      {tableValues.map((val, i) => (
        <TableRow key={`${id}-table-${val.key}-${i}`}>
          <TableCell variant="head">{val.label}</TableCell>
          <TableCell>
            {val.render
              ? val.render(val.key ? data[val.key] : data)
              : data[val.key]}
          </TableCell>
        </TableRow>
      ))}
    </TableBody>
  </Table>
);

/*
data: Object
tableValues: {label, key, render?}
colWidth: array of Integer
*/
