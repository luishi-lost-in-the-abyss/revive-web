import React from "react";
import {
  Popper,
  withStyles,
  Paper,
  ClickAwayListener
} from "@material-ui/core";
import { styles } from "../../theme";

const Popup = ({ open, anchorEl, onClickAway, classes, children }) => (
  <Popper open={open} anchorEl={anchorEl}>
    <ClickAwayListener onClickAway={onClickAway}>
      <Paper className={classes.rootPaper}>{children}</Paper>
    </ClickAwayListener>
  </Popper>
);

export default withStyles(styles)(Popup);
