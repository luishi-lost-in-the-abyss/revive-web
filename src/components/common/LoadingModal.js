import React from "react";
import { connect } from "react-redux";
import { UIActions } from "../../actions";
import { Modal, CircularProgress } from "@material-ui/core";

class LoadingModal extends React.Component {
  render() {
    return (
      <Modal open={this.props.open}>
        <div
          style={{
            display: "flex",
            height: "100vh",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <CircularProgress />
        </div>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    open: state.ui.loadingModal
  };
}

export default connect(
  mapStateToProps,
  UIActions
)(LoadingModal);
