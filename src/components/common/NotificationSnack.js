import React from "react";
import { connect } from "react-redux";
import { UIActions } from "../../actions";
import { Snackbar, IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

class NotificationSnack extends React.Component {
  handleClose() {
    this.props.showNotificationSnack({ open: false });
  }
  render() {
    const { open, notification, type } = this.props;
    return (
      <Snackbar
        open={open}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        message={<span>{notification}</span>}
        onClose={this.handleClose.bind(this)}
        action={[
          <IconButton
            key="notificationSnack-closeIcon"
            onClick={this.handleClose.bind(this)}
          >
            <CloseIcon />
          </IconButton>
        ]}
      />
    );
  }
}

function mapStateToProps(state) {
  const { open, notification, type } = state.ui.notificationSnack;
  return { open, notification, type };
}

export default connect(
  mapStateToProps,
  UIActions
)(NotificationSnack);
