import { withStyles } from "@material-ui/core";
import { styles } from "../../theme";
import {
  Select,
  FormControl,
  FormHelperText,
  InputLabel,
  OutlinedInput,
  MenuItem
} from "@material-ui/core";

const SelectInput = ({
  classes,
  id,
  name,
  label,
  value,
  onInputChange,
  menuArr,
  error,
  multiple = false,
  disabled = false,
  helperText,
  containerStyle = {}
}) => (
  <FormControl
    variant="outlined"
    fullWidth
    className={classes.formField}
    style={containerStyle}
  >
    <InputLabel id={`${id}-label`} htmlFor={id}>
      {label}
    </InputLabel>
    <Select
      onChange={onInputChange}
      value={value}
      error={error}
      multiple={multiple}
      disabled={disabled}
      input={
        <OutlinedInput labelWidth={label.length * 5 + 40} name={name} id={id} />
      }
    >
      {menuArr.map(item => (
        <MenuItem key={`${name}-${item.value}`} value={item.value}>
          {item.label}
        </MenuItem>
      ))}
    </Select>
    {helperText ? <FormHelperText error>{helperText}</FormHelperText> : null}
  </FormControl>
);

export default withStyles(styles)(SelectInput);
