import React from "react";
import { connect } from "react-redux";
import { Button } from "@material-ui/core";
import AccountActions from "../../actions/AccountActions";

const LogoutButton = ({ logoutUser, onLogout }) => (
  <Button onClick={logoutUser.bind(this, {}, onLogout)}>Logout</Button>
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  AccountActions
)(LogoutButton);
