import React from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  withStyles,
  IconButton,
  CssBaseline
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import AdminDrawer from "../admin/Drawer";
import TechnicianDrawer from "../technician/Drawer";
import SupervisorDrawer from "../supervisor/Drawer";
import DispatcherDrawer from "../dispatcher/Drawer";
import { styles } from "../../theme";

class AppContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileDrawer: false
    };
  }

  handleDrawerToggle() {
    this.setState({ mobileDrawer: !this.state.mobileDrawer });
  }

  render() {
    const { mobileDrawer } = this.state;
    const {
      children,
      title,
      appType,
      endComponent,
      classes,
      ...otherProps
    } = this.props;
    return (
      <div style={{ display: "flex", flexDirection: "column" }}>
        <AppBar position="static" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle.bind(this)}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit">
              {title}
            </Typography>
            <div style={{ flex: 1 }} />
            {endComponent ? endComponent : null}
          </Toolbar>
        </AppBar>
        {appType === "admin" ? (
          <AdminDrawer
            handleToggle={this.handleDrawerToggle.bind(this)}
            openMobile={mobileDrawer}
          />
        ) : appType === "technician" ? (
          <TechnicianDrawer
            handleToggle={this.handleDrawerToggle.bind(this)}
            openMobile={mobileDrawer}
          />
        ) : appType === "supervisor" ? (
          <SupervisorDrawer
            handleToggle={this.handleDrawerToggle.bind(this)}
            openMobile={mobileDrawer}
          />
        ) : appType === "dispatcher" ? (
          <DispatcherDrawer
            handleToggle={this.handleDrawerToggle.bind(this)}
            openMobile={mobileDrawer}
          />
        ) : null}
        <main className={classes.containerContent}>{children}</main>
      </div>
    );
  }
}

export default withStyles(styles)(AppContainer);
