import { connect } from "react-redux";
import { Typography, withStyles, TextField, Button } from "@material-ui/core";
import { styles } from "../../theme";
import AccountActions from "../../actions/AccountActions";

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = { password: "", confirmPassword: "", errors: {} };
    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  onSubmit() {
    const { password, confirmPassword } = this.state;
    let errors = {
      password: !password || password !== confirmPassword,
      confirmPassword: !confirmPassword || password !== confirmPassword
    };
    let hasErrors = Object.values(errors).reduce((a, b) => a || b, false);
    if (hasErrors) {
      this.setState({ errors });
      return;
    }
    this.setState({ errors: {}, password: "", confirmPassword: "" });
    this.props.changePassword({ password });
  }
  render() {
    const { classes } = this.props;
    const commonTextFieldProps = name => ({
      className: classes.formField,
      name,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange,
      value: this.state[name],
      error: this.state.errors[name]
    });

    return (
      <React.Fragment>
        <Typography variant="h4">Change Password</Typography>
        <TextField
          label="Password"
          type="password"
          {...commonTextFieldProps("password")}
        />
        <TextField
          label="Confirm Password"
          type="password"
          helperText="Please re-enter your password above"
          {...commonTextFieldProps("confirmPassword")}
        />
        <Button onClick={this.onSubmit} variant="outlined">
          Change Password
        </Button>
      </React.Fragment>
    );
  }
}

const component = withStyles(styles)(ChangePassword);
const mapStateToProps = function(state) {
  return {};
};
export default connect(
  mapStateToProps,
  Object.assign({}, AccountActions)
)(component);
