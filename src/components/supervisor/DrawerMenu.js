import React from "react";
import Link from "next/link";
import Router, { withRouter } from "next/router";
import { Divider, withStyles, Button } from "@material-ui/core";
import { styles } from "../../theme";
import LogoutButton from "../common/LogoutButton";
import DrawerMenuItem from "../common/DrawerMenuItem";

const DrawerMenu = ({ classes, router }) => {
  return (
    <div style={{ display: "flex", flexDirection: "column", height: "100%" }}>
      <Link passHref href="/supervisor">
        <a>
          <img
            src="/static/images/LOGO_2.jpg"
            style={{ width: "100%", padding: 10 }}
          />
        </a>
      </Link>
      <Divider />
      <DrawerMenuItem
        title="Users"
        path="/supervisor/registration"
        otherPaths={["/supervisor/users"]}
        items={[
          {
            title: "Pending Appli.",
            path: "/supervisor/registration/applications"
          },
          {
            title: "Participants",
            path: "/supervisor/users/participants"
          },
          {
            title: "Technicians",
            path: "/supervisor/users/technicians"
          },
          {
            title: "Clusters",
            path: "/supervisor/users/clusters"
          }
        ]}
      />
      <Divider />
      <DrawerMenuItem
        title="Bookings"
        path="/supervisor/bookings"
        otherPaths={[]}
        items={[{ title: "View Bookings", path: "/supervisor/bookings/view" }]}
      />
      <Divider />
      <DrawerMenuItem
        title="Reports"
        path="/supervisor/reports"
        otherPaths={[]}
        items={[
          { title: "Raw Data", path: "/supervisor/reports/raw_data" },
          { title: "Municipality", path: "/supervisor/reports/municipality" }
        ]}
      />
      <Divider />
      <div style={{ display: "flex", flex: 1 }} />
      <Button
        variant="outlined"
        onClick={() => {
          Router.push("/supervisor/my_account");
        }}
      >
        My Account
      </Button>
      <LogoutButton
        onLogout={() => {
          Router.push("/login");
        }}
      />
    </div>
  );
};

export default withRouter(withStyles(styles)(DrawerMenu));
