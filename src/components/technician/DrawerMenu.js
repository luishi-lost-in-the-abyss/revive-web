import React from "react";
import Link from "next/link";
import Router, { withRouter } from "next/router";
import { Divider, withStyles, Button } from "@material-ui/core";
import { styles } from "../../theme";
import LogoutButton from "../common/LogoutButton";
import DrawerMenuItem from "../common/DrawerMenuItem";

const DrawerMenu = ({ classes, router }) => {
  return (
    <div style={{ display: "flex", flexDirection: "column", height: "100%" }}>
      <Link passHref href="/technician">
        <a>
          <img
            src="/static/images/LOGO_2.jpg"
            style={{ width: "100%", padding: 10 }}
          />
        </a>
      </Link>
      <Divider />
      <DrawerMenuItem
        title="Users"
        path="/technician/registration"
        otherPaths={["/technician/users"]}
        items={[
          {
            title: "New Participant",
            path: "/technician/registration/create_application"
          },
          {
            title: "Participants",
            path: "/technician/users/participants"
          },
          {
            title: "Clusters",
            path: "/technician/users/clusters"
          }
        ]}
      />
      <Divider />
      <DrawerMenuItem
        title="Bookings"
        path="/technician/bookings"
        otherPaths={[]}
        items={[
          { title: "View Bookings", path: "/technician/bookings/view" },
          {
            title: "New Booking",
            path: "/technician/bookings/create_booking"
          },
          {
            title: "Price Agreements",
            path: "/technician/bookings/price_agreements"
          }
        ]}
      />
      <Divider />
      <DrawerMenuItem
        title="Reports"
        path="/technician/reports"
        otherPaths={[]}
        items={[
          { title: "Raw Data", path: "/technician/reports/raw_data" },
          { title: "Municipality", path: "/technician/reports/municipality" }
        ]}
      />
      <Divider />
      <div style={{ display: "flex", flex: 1 }} />
      <Button
        variant="outlined"
        onClick={() => {
          Router.push("/technician/my_account");
        }}
      >
        My Account
      </Button>
      <LogoutButton
        onLogout={() => {
          Router.push("/login");
        }}
      />
    </div>
  );
};

export default withRouter(withStyles(styles)(DrawerMenu));
