import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../theme";
import ServiceCategoryBrowser from "../../../admin/resources/service_category/ServiceCategoryBrowser";
import { Grid } from "@material-ui/core";
import ServicesBrowser from "./ServicesBrowser";

class SelectServiceStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      category: null,
      service: props.selectedService,
      submitError: false
    };

    this.onCategoryChange = this.onCategoryChange.bind(this);
  }

  onCategoryChange(id) {
    this.setState({ category: id });
  }

  render() {
    const { classes } = this.props;
    const { category, submitError } = this.state;

    return (
      <Grid container spacing={16}>
        <Grid item xs={12} md={4}>
          <ServiceCategoryBrowser onCategoryChange={this.onCategoryChange} />
        </Grid>
        <Grid item xs={12} md={8}>
          <ServicesBrowser
            selectedService={this.props.selectedService}
            onServiceChange={this.props.onSelectedService}
            categoryId={category}
          />
        </Grid>
      </Grid>
    );
  }
}

const component = withStyles(styles)(SelectServiceStep);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  {}
)(component);
