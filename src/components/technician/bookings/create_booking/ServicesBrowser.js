import React from "react";
import Link from "next/link";
import {
  withStyles,
  Typography,
  List,
  ListItem,
  ListItemText,
  Paper,
  ListItemIcon,
  Button
} from "@material-ui/core";
import { styles } from "../../../../theme";
import FeathersClient from "../../../../api/FeathersClient";

class ServicesBrowser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      service: props.selectedService,
      services: [],
      errors: {},
      errorType: "none"
    };
    this.onRemoveService = this.onRemoveService.bind(this);
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.categoryId !== this.props.categoryId) {
      this.getServices();
    }
  }
  async componentDidMount() {
    this.getServices();
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  onSelectService(service) {
    this.props.onServiceChange(service);
    this.setState({ service });
  }
  onRemoveService() {
    this.props.onServiceChange({});
    this.setState({ service: {} });
  }

  async getServices() {
    const { categoryId } = this.props;
    const client = FeathersClient.getClient();
    const res = await client
      .service("services")
      .find({ query: { $limit: "-1", serviceCategoryId: categoryId } });
    this.setState({
      services: res
    });
  }

  render() {
    const { classes } = this.props;
    const { service: selectedService, services } = this.state;

    return (
      <React.Fragment>
        <Typography variant="h6" gutterBottom>
          Services:
        </Typography>
        <Paper>
          <List>
            {services.length === 0 ? (
              <ListItem>
                <ListItemText primary={`No Services Found`} />
              </ListItem>
            ) : (
              services.map(service => (
                <ListItem
                  selected={selectedService.code === service.code}
                  key={`service-${service.code}`}
                >
                  <ListItemText
                    primary={`${service.code} (${service.name})`}
                    secondary={service.description}
                  />
                  <ListItemIcon>
                    {selectedService.code === service.code ? (
                      <Button
                        color="primary"
                        variant="outlined"
                        onClick={this.onRemoveService}
                      >
                        Remove
                      </Button>
                    ) : (
                      <Button
                        color="secondary"
                        variant="outlined"
                        onClick={this.onSelectService.bind(this, service)}
                      >
                        Select
                      </Button>
                    )}
                  </ListItemIcon>
                </ListItem>
              ))
            )}
          </List>
        </Paper>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(ServicesBrowser);
