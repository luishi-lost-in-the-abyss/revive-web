import React from "react";
import Head from "next/head";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../theme";
import { Grid, Typography } from "@material-ui/core";
import DayPicker, { DateUtils } from "react-day-picker";
import FeathersClient from "../../../../api/FeathersClient";
import { DateTime } from "luxon";
import UIActions from "../../../../actions/UIActions";
import TimeTablePicker from "./TimeTablePicker";
import { START_HOUR, END_HOUR } from "../../../../api/utility";

class SelectDatesStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      from: props.selectedDates.startDate,
      to: props.selectedDates.endDate,
      fromHour: props.selectedTimes.fromHour,
      toHour: props.selectedTimes.toHour,
      disabledDates: {},
      disabledHours: {},
      submitError: false
    };
    this.onSelect = this.onSelect.bind(this);
    this.todayDate = new Date();
    this.startingBookDate = DateTime.fromJSDate(this.todayDate)
      .plus({ days: 1 })
      .toJSDate();
    this.onMonthChange = this.onMonthChange.bind(this);
  }

  componentDidMount() {
    this.getUnavailableDates(this.todayDate);
  }

  async getUnavailableDates(startDate = new Date()) {
    const { selectedService } = this.props;
    const client = FeathersClient.getClient();
    this.props.showLoadingModal(true);
    const res = await client.service("booking-methods").create({
      method: "get_available_dates",
      query: {
        serviceId: selectedService.id,
        startDate,
        endDate: DateTime.fromJSDate(startDate)
          .plus({ months: 2 })
          .endOf("month")
          .toJSDate()
      }
    });
    this.setState(
      {
        disabledDates: res.datesUnavailable,
        disabledHours: res.hoursUnavailable
      },
      () => {
        this.props.showLoadingModal(false);
      }
    );
  }

  onMonthChange(date) {
    this.getUnavailableDates(date);
  }

  onSelect(day, { disabled }) {
    if (disabled) return;
    const { from, to, disabledDates } = this.state;
    let range, invalid;
    if (to) {
      range = DateUtils.addDayToRange(day, { from: null, to: null });
    } else {
      range = DateUtils.addDayToRange(day, { from, to });
      // check if disabled days are part of the range
      Object.keys(disabledDates).map(disabledDate => {
        const invalidRange = DateUtils.isDayInRange(
          new Date(disabledDate),
          range
        );
        if (invalidRange) {
          invalid = true;
        }
      });
    }

    if (invalid) {
      range = { from: null, to: null };
    }
    this.props.onSelectedDate(
      Object.assign(range, { fromHour: {}, toHour: {} })
    );
    this.setState(Object.assign(range, { fromHour: {}, toHour: {} }));
  }

  onSelectTime(from, dateTimeObj) {
    const disabledHours = Object.keys(this.state.disabledHours);
    let newState;
    newState = Object.assign(
      { [from ? "fromHour" : "toHour"]: dateTimeObj },
      from ? { toHour: {} } : null
    );
    let error = false;
    if (!from) {
      //need to check if inbetween any dates and hours are not available
      const toHour = dateTimeObj;
      const fromHour = this.state.fromHour;
      if (fromHour.toFormat) {
        const interval = fromHour.until(toHour);
        const noOfDays = interval.count("day");
        for (let day = 0; day < noOfDays; day++) {
          const currentDate = fromHour.plus({ days: day });
          const startHour = day === 0 ? fromHour.hour : START_HOUR;
          const endHour = day + 1 === noOfDays ? toHour.hour : END_HOUR;
          for (let hour = startHour; hour < endHour; hour++) {
            const currentDateHour = currentDate.set({ hour });
            const dateKey = currentDateHour.toFormat("yyyy-MM-dd H:mm");
            if (disabledHours.indexOf(dateKey) >= 0) {
              error = true;
            }
          }
        }
      }
    }
    newState = error ? { fromHour: {}, toHour: {} } : newState;
    this.props.onSelectedTime(newState);
    this.setState(newState);
  }

  render() {
    const { classes } = this.props;
    const {
      from,
      fromHour,
      to,
      toHour,
      disabledDates,
      disabledHours,
      submitError
    } = this.state;

    return (
      <React.Fragment>
        <Head>
          <link rel="stylesheet" href="/static/daypicker-style.css" />
        </Head>
        <Grid container spacing={16} justify="center">
          <Grid
            item
            xs={12}
            style={{ display: "flex", justifyContent: "center" }}
          >
            <DayPicker
              numberOfMonths={3}
              onDayClick={this.onSelect}
              selectedDays={[from, { from, to }]}
              fromMonth={this.todayDate}
              disabledDays={[
                { before: this.startingBookDate },
                ...Object.keys(disabledDates).map(date => new Date(date))
              ]}
              onMonthChange={this.onMonthChange}
            />
          </Grid>
          <Grid xs={4}>
            <Typography variant="h6">
              Start date:{" "}
              {from
                ? DateTime.fromJSDate(from).toFormat("dd-MM-yyyy")
                : "Not Selected"}
            </Typography>
            <Typography variant="h6">
              Start time:{" "}
              {fromHour.toFormat ? fromHour.toFormat("H:mm") : "Not Selected"}
            </Typography>

            {from && to && (
              <TimeTablePicker
                selectedDate={from}
                selectedHour={fromHour}
                disabledHours={disabledHours}
                onSelectTime={this.onSelectTime.bind(this, true)}
              />
            )}
          </Grid>
          <Grid xs={4}>
            <Typography variant="h6">
              End date:{" "}
              {to
                ? DateTime.fromJSDate(to).toFormat("dd-MM-yyyy")
                : "Not Selected"}
            </Typography>
            <Typography variant="h6">
              End time:{" "}
              {toHour.toFormat ? toHour.toFormat("H:mm") : "Not Selected"}
            </Typography>

            {from && to && (
              <TimeTablePicker
                isEnd={true}
                selectedDate={to}
                selectedHour={toHour}
                selectedFromDate={from}
                selectedFromHour={fromHour}
                disabledHours={disabledHours}
                onSelectTime={this.onSelectTime.bind(this, false)}
              />
            )}
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

const component = withStyles(styles)(SelectDatesStep);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  Object.assign({}, UIActions)
)(component);
