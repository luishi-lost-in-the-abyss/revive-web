import React from "react";
import Head from "next/head";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../theme";
import { Grid, TextField, Typography } from "@material-ui/core";
import TechnicianActions from "../../../../actions/TechnicianActions";
import VerticalTable from "../../../common/VerticalTable";
import { DateTime } from "luxon";
import AdditionalCostsTable from "./AdditionalCostsTable";

const colWidth = [300, null];

class ConfirmationStep extends React.Component {
  constructor(props) {
    super(props);
    const { participant, plot, individualInformal } = props;
    const participantType = participant.key.split("-")[0];
    this.state = {
      participantType,
      plotObj: participant.plots.filter(plt => plt.id === plot)[0],
      discount: 0
    };

    if (participantType === "informal_cluster" && !individualInformal) {
      this.state.plotObj = {
        plantableArea: participant.plots.reduce(
          (a, b) => a + parseInt(b.plantableArea),
          0
        )
      };
    }

    this.onInputChange = this.onInputChange.bind(this);
  }

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {
    const { discount } = this.state;
    if (discount > 100) {
      this.setState({ discount: 100 });
    } else if (discount < 0) {
      this.setState({ discount: 0 });
    }

    if (discount !== prevState.discount) {
      this.props.onDiscountChange(discount);
    }
  }

  onInputChange(evt) {
    const { value, name } = evt.target;
    const newState = { [name]: value };
    this.setState(newState);
  }

  render() {
    const {
      service,
      participant,
      plot,
      serviceArea,
      fromHour,
      toHour,
      individualInformal,
      classes
    } = this.props;
    const { participantType, plotObj, discount } = this.state;

    const isInformal =
      participantType === "informal_cluster" && !individualInformal;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange,
      name,
      value: this.state[name]
    });
    return (
      <React.Fragment>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Typography gutterBottom variant="h4">
              Review Booking Details
            </Typography>
            <Typography gutterBottom variant="h5">
              Dates
            </Typography>
            <VerticalTable
              data={{ fromHour, toHour }}
              tableValues={[
                {
                  label: "Start Date",
                  key: "fromHour",
                  render: date => date.toFormat("d LLLL yyyy H:mm")
                },
                {
                  label: "End Date",
                  key: "toHour",
                  render: date => date.toFormat("d LLLL yyyy H:mm")
                }
              ]}
              colWidth={colWidth}
            />
            <Typography gutterBottom variant="h5">
              Service
            </Typography>
            <VerticalTable
              data={service}
              tableValues={[
                { label: "Code", key: "code" },
                { label: "Name", key: "name" },
                { label: "Description", key: "description" },
                { label: "Price", key: "price" }
              ]}
              colWidth={colWidth}
            />
            <Typography gutterBottom variant="h5">
              Participant
            </Typography>
            <VerticalTable
              data={
                individualInformal
                  ? plotObj.participant
                  : participant[isInformal ? "leader" : participantType]
              }
              tableValues={[
                {
                  label: "Name" + (isInformal ? " (Leader)" : ""),
                  key: "name"
                },
                {
                  label: "Participation Type",
                  key: null,
                  render: () =>
                    individualInformal ? "informal_cluster" : participantType
                }
              ]}
              colWidth={colWidth}
            />
            <Typography gutterBottom variant="h5">
              Plot
            </Typography>
            {isInformal ? (
              participant.plots.map(plt => (
                <VerticalTable
                  key={`informal_cluster-plot-${plt.id}`}
                  data={plt}
                  tableValues={[
                    { label: "ID", key: "id" },
                    { label: "Plantable Area (ha)", key: "plantableArea" }
                  ]}
                  colWidth={colWidth}
                />
              ))
            ) : (
              <VerticalTable
                data={plotObj}
                tableValues={[
                  { label: "ID", key: "id" },
                  { label: "Plantable Area (ha)", key: "plantableArea" }
                ]}
                colWidth={colWidth}
              />
            )}
            <Typography gutterBottom variant="h5">
              Service Payment
            </Typography>
            <VerticalTable
              data={{ id: "calculation table" }}
              tableValues={[
                {
                  label: "Price per ha",
                  key: "price-per-ha",
                  render: () => service.price
                },
                {
                  label: "Service Area (ha)",
                  key: "plantable-area",
                  render: () =>
                    isInformal ? plotObj.plantableArea : serviceArea
                },
                {
                  label: "Total Price",
                  key: "total-price",
                  render: () =>
                    parseFloat(service.price) *
                    parseFloat(isInformal ? plotObj.plantableArea : serviceArea)
                }
              ]}
              colWidth={colWidth}
            />
            <div style={{ marginBottom: 20 }}>
              <AdditionalCostsTable
                additionalCosts={this.props.additionalCosts}
                updateAdditionalCosts={this.props.updateAdditionalCosts}
              />
            </div>
            <TextField
              InputProps={{ max: 100, min: 0 }}
              type="number"
              label="Discount (%)"
              {...commonTextFieldProps("discount")}
            />
            <Typography align="right" variant="h6">
              Final Payable Amount:{" "}
              {(parseFloat(
                this.props.additionalCosts.reduce(
                  (a, { amount }) => a + parseFloat(amount),
                  0
                )
              ) +
                parseFloat(service.price) *
                  parseFloat(
                    isInformal ? plotObj.plantableArea : serviceArea
                  )) *
                (1 - parseFloat(discount ? discount : 0) / 100)}
            </Typography>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

const component = withStyles(styles)(ConfirmationStep);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  Object.assign({}, TechnicianActions)
)(component);
