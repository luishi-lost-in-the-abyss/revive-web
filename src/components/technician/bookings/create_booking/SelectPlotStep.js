import React from "react";
import Head from "next/head";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../theme";
import {
  Grid,
  TextField,
  MenuItem,
  Typography,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";
import TechnicianActions from "../../../../actions/TechnicianActions";

class SelectPlotStep extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      plot: {},
      selectedPlot:
        props.selectedPlot && props.selectedPlot !== "informal"
          ? props.selectedPlot
          : "",
      selectedParticipant: props.selectedParticipant
        ? props.selectedParticipant.key
        : "",
      plots: {},
      individualInformal: props.individualInformal,
      selectedInformalParticipant: props.selectedInformalParticipant
        ? props.selectedInformalParticipant.key
        : "",
      serviceArea: props.serviceArea ? props.serviceArea : 0,
      submitError: false,
      filterParticipant: "",
      loading: true
    };

    this.onFilterParticipantChange = this.onFilterParticipantChange.bind(this);
  }

  componentDidMount() {
    this.props.getFarmerPlots((err, res) => {
      if (err) {
        return;
      }
      this.setState({ plots: res, loading: false });
    });
  }

  onInputChange(evt) {
    const { selectedParticipant, plots } = this.state;
    const { value, name } = evt.target;
    const newState = { [name]: value };
    if (name === "selectedParticipant" && value !== selectedParticipant) {
      newState.selectedPlot = "";
      if (value.split("-")[0] === "informal_cluster") {
        this.props.onSelectedPlot({
          plot: "informal",
          participant: Object.assign({ key: value }, plots[value])
        });
      } else {
        this.props.onSelectedPlot({ plot: "", participant: null });
      }
    }
    if (name === "selectedPlot") {
      this.props.onSelectedPlot({
        plot: value,
        participant: Object.assign(
          { key: selectedParticipant },
          plots[selectedParticipant]
        )
      });
    }
    if (name === "serviceArea") {
      this.props.onServiceAreaChange(value);
    }
    this.setState(newState);
  }

  onFilterParticipantChange(evt) {
    this.setState({ filterParticipant: evt.target.value });
  }

  onCheckChange(evt) {
    this.setState({ individualInformal: evt.target.checked });
    this.props.onCheckInformalIndividual(evt.target.checked);
  }

  render() {
    if (this.state.loading) return "";
    const { classes } = this.props;
    const {
      plots,
      selectedParticipant,
      selectedInformalParticipant,
      selectedPlot,
      submitError,
      filterParticipant
    } = this.state;

    const commonTextFieldProps = name => ({
      className: classes.formField,
      fullWidth: true,
      variant: "outlined",
      onChange: this.onInputChange.bind(this),
      name,
      value: this.state[name]
    });

    return (
      <React.Fragment>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Typography variant="h5">Select Participant</Typography>
            <TextField
              label="Filter Participant Name/Code"
              variant="outlined"
              fullWidth
              style={{ marginTop: 10, marginBottom: 10 }}
              onChange={this.onFilterParticipantChange}
            />
            <TextField
              {...commonTextFieldProps("selectedParticipant")}
              select
              value={selectedParticipant}
            >
              {Object.keys(plots)
                .map(key => {
                  const data = plots[key];
                  let type = key.split("-")[0];
                  let name = "";
                  switch (type) {
                    case "cluster":
                      name = data.cluster.cluster.code;
                      break;
                    case "informal_cluster":
                      name = data.leader
                        ? data.leader.Leader.code
                        : "NO LEADER";
                      break;
                    case "participant":
                      name = data.participant.name;
                      break;
                  }
                  type = type === "informal";
                  if (
                    filterParticipant &&
                    name
                      .toLowerCase()
                      .indexOf(filterParticipant.toLowerCase()) < 0
                  )
                    return null;
                  return (
                    <MenuItem key={key} value={key}>
                      {name}
                    </MenuItem>
                  );
                })
                .filter(obj => !!obj)}
            </TextField>

            {selectedParticipant &&
              selectedParticipant.split("-")[0] === "informal_cluster" && (
                <React.Fragment>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.individualInformal}
                        onChange={this.onCheckChange.bind(this)}
                      />
                    }
                    label="Book for individual participant plot"
                  />
                  {this.state.individualInformal && (
                    <React.Fragment>
                      <Typography variant="h5">Select Plot</Typography>
                      <TextField
                        {...commonTextFieldProps("selectedPlot")}
                        select
                        value={selectedPlot}
                      >
                        {plots[selectedParticipant].plots.map(plot => {
                          return (
                            <MenuItem key={`plot-${plot.id}`} value={plot.id}>
                              {plot.participant.name} - {plot.code} (ID:{" "}
                              {plot.id})
                            </MenuItem>
                          );
                        })}
                      </TextField>
                    </React.Fragment>
                  )}
                </React.Fragment>
              )}

            {selectedParticipant &&
            selectedParticipant.split("-")[0] !== "informal_cluster" ? (
              <React.Fragment>
                <Typography variant="h5">Select Plot</Typography>
                <TextField
                  {...commonTextFieldProps("selectedPlot")}
                  select
                  value={selectedPlot}
                >
                  {plots[selectedParticipant].plots.map(plot => {
                    return (
                      <MenuItem key={`plot-${plot.id}`} value={plot.id}>
                        {plot.code} (ID: {plot.id})
                      </MenuItem>
                    );
                  })}
                </TextField>
              </React.Fragment>
            ) : null}
            {selectedParticipant && selectedPlot && (
              <TextField
                label={`Service Area (Max Plantable Area: ${
                  this.props.plantableArea
                }`}
                type="number"
                step={0.1}
                min={0}
                max={this.props.plantableArea}
                {...Object.assign({}, commonTextFieldProps("serviceArea"), {
                  value: this.props.serviceArea
                })}
              />
            )}
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

const component = withStyles(styles)(SelectPlotStep);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  Object.assign({}, TechnicianActions)
)(component);
