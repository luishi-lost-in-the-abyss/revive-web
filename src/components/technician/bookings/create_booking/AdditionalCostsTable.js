import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../theme";
import { TextField, Typography, Button, Grid } from "@material-ui/core";
import VerticalTable from "../../../common/VerticalTable";

class AdditionalCostsTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      additionalCosts: [],
      manualUpdate: 0
    };
    this.addAdditionalCost = this.addAdditionalCost.bind(this);
  }

  componentDidMount() {
    this.setState({ additionalCosts: this.props.additionalCosts });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.manualUpdate !== this.state.manualUpdate) {
      this.props.updateAdditionalCosts(this.state.additionalCosts);
    }
  }

  onAdditionalCostInput(i, evt) {
    const { value, name } = evt.target;
    //we do unorthodox way of updating, shouldnt be done like that
    //this edits the object directly
    this.state.additionalCosts[i][name] = value;
    this.setState({ manualUpdate: this.state.manualUpdate + 1 });
  }
  addAdditionalCost() {
    const { additionalCosts } = this.state;
    const newAdditionalCost = [...additionalCosts, { type: "", amount: 0 }];
    this.setState({
      additionalCosts: newAdditionalCost
    });
    this.props.updateAdditionalCosts(newAdditionalCost);
  }
  removeAdditionalCost(i) {
    const { additionalCosts } = this.state;
    const newAdditionalCost = [
      ...additionalCosts.slice(0, i),
      ...additionalCosts.slice(i + 1)
    ];
    this.setState({
      additionalCosts: newAdditionalCost
    });
    this.props.updateAdditionalCosts(newAdditionalCost);
  }

  render() {
    const { classes } = this.props;
    const { additionalCosts } = this.state;

    const commonTextFieldProps = (name, i) => ({
      fullWidth: true,
      variant: "outlined",
      onChange: this.onAdditionalCostInput.bind(this, i),
      name,
      value: this.state.additionalCosts[i][name]
    });

    return (
      <React.Fragment>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <Typography gutterBottom variant="h5">
            Additional Costs
          </Typography>
          <Button onClick={this.addAdditionalCost}>Add</Button>
        </div>
        {additionalCosts.map((arr, i) => {
          return (
            <Grid
              justify="center"
              container
              key={`additional-costs-${i}`}
              spacing={16}
            >
              <Grid item xs={4}>
                <TextField
                  key={`additional-cost-label-${i}`}
                  label="Type"
                  {...commonTextFieldProps("type", i)}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  key={`additional-cost-amount-${i}`}
                  InputProps={{ max: 100, min: 0 }}
                  type="number"
                  label="Amount"
                  {...commonTextFieldProps("amount", i)}
                />
              </Grid>
              <Grid item xs={2}>
                <Button
                  variant="outlined"
                  onClick={this.removeAdditionalCost.bind(this, i)}
                >
                  Remove
                </Button>
              </Grid>
            </Grid>
          );
        })}
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(AdditionalCostsTable);
