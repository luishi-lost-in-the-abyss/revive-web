import { END_HOUR, START_END_HOURS, START_HOUR } from "../../../../api/utility";
import { DateTime } from "luxon";

import {
  List,
  ListItem,
  ListItemText,
  withStyles,
  Paper
} from "@material-ui/core";
import { styles } from "../../../../theme";

const component = ({
  isEnd = false,
  selectedDate,
  disabledHours = {},
  selectedHour,
  selectedFromHour,
  selectedFromDate,
  onSelectTime,
  classes
}) => {
  const sameDate = DateTime.fromJSDate(selectedFromDate).equals(
    DateTime.fromJSDate(selectedDate)
  );
  const disabledArr = Object.keys(disabledHours);
  const listItems = [];
  let startingHour = START_HOUR + (isEnd && sameDate ? 1 : 0);
  if (isEnd && sameDate) {
    startingHour = selectedFromHour.plus
      ? selectedFromHour.plus({ hour: 1 }).hour
      : startingHour;
  }
  for (let hour = startingHour; hour < END_HOUR + (isEnd ? 1 : 0); hour++) {
    const currentDate = DateTime.fromJSDate(selectedDate);
    const currentDateHour = currentDate.set({ hour });
    const dateKey = currentDateHour.toFormat("yyyy-MM-dd H:mm");
    const disableKey = (isEnd
      ? currentDateHour.minus({ hour: 1 })
      : currentDateHour
    ).toFormat("yyyy-MM-dd H:mm");
    listItems.push(
      <ListItem
        button
        key={dateKey}
        selected={
          selectedHour.toFormat &&
          selectedHour.toFormat("yyyy-MM-dd H:mm") === dateKey
        }
        disabled={disabledArr.indexOf(disableKey) >= 0}
        onClick={onSelectTime.bind(this, currentDateHour)}
      >
        <ListItemText primary={currentDateHour.toFormat("H:mm")} />
      </ListItem>
    );
  }
  return (
    <Paper>
      <List style={{ height: 200, overflowX: "auto" }}>{listItems}</List>
    </Paper>
  );
};

export default withStyles(styles)(component);
