import { UI_LOADING_MODAL, UI_NOTIFICATION_SNACK } from "../api/ActionTypes";
import FeathersClient from "../api/FeathersClient";
import Pica from "pica/dist/pica";
const pica = Pica();

function approveApplication({ userId, plotTechnicians, approve = true }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("admin-methods").create({
        method: approve ? "approve_application" : "reject_application",
        query: {
          userId,
          plotTechnicians
        }
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully ${
            approve ? "approved" : "rejected"
          } application`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    }
    dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
  };
}

function createFec({ name }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("fec").create({ name });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created ${name} FEC`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editFec({ fecId, name }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("fec").patch(fecId, { name });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited ${name} FEC`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createTechnician(
  { name, username, email, contact, password, supervisor, barangayId },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("admin-methods").create({
        method: "create_technician",
        query: {
          name,
          username,
          contact,
          email,
          password,
          supervisor,
          barangayId
        }
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created ${name} technician`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editTechnician(
  { technicianId, name, email, contact, password, supervisor, barangayId },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("admin-methods").create({
        method: "edit_technician",
        query: {
          technicianId,
          name,
          email,
          contact,
          password,
          supervisor,
          barangayId
        }
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited ${name} technician`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createSupervisor(
  { name, username, email, contact, password, fec },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("admin-methods").create({
        method: "create_supervisor",
        query: { name, username, email, contact, password, fec }
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created ${name} supervisor`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editSupervisor(
  { supervisorId, name, email, contact, password, fec },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("admin-methods").create({
        method: "edit_supervisor",
        query: { supervisorId, name, email, contact, password, fec }
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited ${name} supervisor`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createCluster({ code }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("clusters").create({ code });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created Cluster ${code}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editCluster({ clusterId, code, clusterLeader }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("clusters").patch(clusterId, {
        code,
        clusterLeaderId: clusterLeader
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited Cluster ${code}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createFormalCluster(details, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("admin-methods")
        .create({ method: "create_formal_cluster", query: details });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created formal cluster`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editFormalCluster(details, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("admin-methods").create({
        method: "edit_formal_cluster",
        query: details
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited Cluster ${details.code}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createServiceCategory({ name, parentCategoryId }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("service-category")
        .create({ name, parentCategoryId });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created service category ${name}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editServiceCategory(
  { serviceCategoryId, name, parentCategoryId },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("service-category")
        .patch(serviceCategoryId, { name, parentCategoryId });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited service category ${name}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createEquipment({ name, description }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment")
        .create({ name, description });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created equipment: ${name}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editEquipment({ equipmentId, name, description }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment")
        .patch(equipmentId, { name, description });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited equipment: ${name}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createEquipmentAsset({ serial, fecId, equipmentId }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment-assets")
        .create({ serial, fecId, equipmentId });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created asset: ${serial}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editEquipmentAsset({ serial, fecId }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment-assets")
        .patch(serial, { fecId });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited asset: ${serial}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createService(
  { name, code, description, price, equipment, categoryId },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("services").create({
        name,
        code,
        description,
        price,
        equipment,
        serviceCategoryId: categoryId
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created service: ${code}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editService(
  { serviceId, name, code, description, price, equipment, categoryId },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("services").patch(serviceId, {
        name,
        code,
        description,
        price,
        equipment,
        serviceCategoryId: categoryId
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited service: ${code}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function removeObject({ id, type }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service(type).remove(id);

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully deleted`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}
function toggleAssetStatus({ assetId, enable }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment-assets")
        .patch(assetId, { condition: enable ? "working" : "retired" });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully set asset status`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}
function createMaintenance({ assetId, startDate, endDate }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment-asset-maintenance")
        .create({ startDate, endDate, equipmentAssetSerial: assetId });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created maintenance schedule`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}
function deleteMaintenance({ id }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment-asset-maintenance")
        .remove(id);

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully deleted schedule`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editFarmPlot({ plotId, ...plot }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("farm-plots").patch(plotId, plot);

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited farm plot ${plot.code}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}
function addFarmPlot({ ...plot }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("farm-plots").create(plot);

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully added farm plot ${res.code}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editParticipant({ participant, participantId }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("participants").patch(
        participantId,
        Object.assign({}, participant, {
          file: !!participant.file,
          patchIndividualParticipant: true
        })
      );
      if (res.fileUploadToken) {
        dispatch({
          type: UI_NOTIFICATION_SNACK,
          payload: {
            open: true,
            notification: `Uploading new ID Image for ${participant.name}`,
            type: "success"
          }
        });
      } else {
        dispatch({
          type: UI_NOTIFICATION_SNACK,
          payload: {
            open: true,
            notification: `Successfully edited participant ${participant.name}`,
            type: "success"
          }
        });
      }

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    } finally {
      if (!participant.file) {
        dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
      }
    }
  };
}

function editBookingStatus(
  {
    bookingId,
    status,
    statusReason,
    completedArea,
    actualStartDate,
    actualEndDate
  },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("bookings").patch(
        bookingId,
        Object.assign(
          {
            status,
            statusReason: statusReason ? statusReason : ""
          },
          status === "completed"
            ? {
                actualCompletedArea: completedArea,
                actualStartDate,
                actualEndDate
              }
            : {}
        )
      );

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited booking status`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createPriceAgreement(
  { bookingId, deliveredPrice, truckingPrice, deliveryMode },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("price-agreements")
        .create({ bookingId, deliveredPrice, truckingPrice, deliveryMode });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created price agreement`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createBooking(
  {
    serviceId,
    startDate,
    endDate,
    plotId,
    clusterId,
    fecId,
    equipment,
    serviceArea,
    discount,
    additionalCosts,
    individualInformal,
    technicianId
  },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("booking-methods").create({
        method: "create_booking_manual",
        query: {
          serviceId,
          startDate,
          endDate,
          plotId,
          clusterId,
          fecId,
          equipment,
          discount,
          serviceArea,
          additionalCosts,
          individualInformal,
          technicianId
        }
      });
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "Successfully created booking",
          type: "success"
        }
      });
      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createDispatcher(
  { name, username, fec, email, contact, password },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("admin-methods").create({
        method: "create_dispatcher",
        query: { name, username, fecId: fec, email, contact, password }
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created ${name} dispatcher`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editDispatcher(
  { dispatcherId, name, email, fec, contact, password },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("admin-methods").create({
        method: "edit_dispatcher",
        query: { dispatcherId, name, fecId: fec, email, contact, password }
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited ${name} dispatcher`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editBookingAssets({ bookingId, assets }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("booking-methods").create({
        method: "edit_booking_asset",
        query: { assets, bookingId }
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully reallocated booking assets`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

export default {
  approveApplication,
  createFec,
  editFec,
  createSupervisor,
  editSupervisor,
  createTechnician,
  editTechnician,
  createCluster,
  editCluster,
  createFormalCluster,
  editFormalCluster,
  createServiceCategory,
  editServiceCategory,
  createEquipment,
  editEquipment,
  createEquipmentAsset,
  editEquipmentAsset,
  createService,
  editService,
  removeObject,
  toggleAssetStatus,
  createMaintenance,
  deleteMaintenance,
  editFarmPlot,
  addFarmPlot,
  editParticipant,
  editBookingStatus,
  createPriceAgreement,
  createBooking,
  createDispatcher,
  editDispatcher,
  editBookingAssets
};
