import { UI_LOADING_MODAL, UI_NOTIFICATION_SNACK } from "../api/ActionTypes";
import AdminActions from "./AdminActions";
const {
  createCluster,
  editCluster,
  editFormalCluster,
  createFormalCluster
} = AdminActions;
import FeathersClient from "../api/FeathersClient";

function approveApplication({ userId, plotTechnicians, approve = true }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("supervisor-methods").create({
        method: approve ? "approve_application" : "reject_application",
        query: {
          userId,
          plotTechnicians
        }
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully ${
            approve ? "approved" : "rejected"
          } application`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    }
    dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
  };
}

function removeObject({ id, type }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service(type).remove(id);

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully deleted`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editFarmPlot({ plotId, ...plot }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("farm-plots").patch(plotId, plot);

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited farm plot ${plot.code}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}
function addFarmPlot({ ...plot }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("farm-plots").create(plot);

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully added farm plot ${res.code}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

export default {
  approveApplication,
  editParticipant: AdminActions.editParticipant,
  removeObject,
  addFarmPlot,
  editFarmPlot,
  createCluster,
  editCluster,
  editFormalCluster,
  createFormalCluster
};
