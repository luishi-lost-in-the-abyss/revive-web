import { UI_LOADING_MODAL, UI_NOTIFICATION_SNACK } from "../api/ActionTypes";
import FeathersClient from "../api/FeathersClient";
import AdminActions from "./AdminActions";

function editService(
  { serviceId, name, code, description, equipment, categoryId },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("services").patch(serviceId, {
        name,
        code,
        description,
        equipment,
        serviceCategoryId: categoryId
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited service: ${code}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createEquipment({ name, description }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment")
        .create({ name, description });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created equipment: ${name}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editEquipment({ equipmentId, name, description }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment")
        .patch(equipmentId, { name, description });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited equipment: ${name}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createEquipmentAsset({ serial, fecId, equipmentId }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment-assets")
        .create({ serial, fecId, equipmentId });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created asset: ${serial}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editEquipmentAsset({ serial, fecId }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment-assets")
        .patch(serial, { fecId });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully edited asset: ${serial}`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createMaintenance({ assetId, startDate, endDate }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment-asset-maintenance")
        .create({ startDate, endDate, equipmentAssetSerial: assetId });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created maintenance schedule`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}
function deleteMaintenance({ id }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("equipment-asset-maintenance")
        .remove(id);

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully deleted schedule`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function removeObject({ id, type }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service(type).remove(id);

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully deleted`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function editBookingAssets({ bookingId, assets }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("booking-methods").create({
        method: "edit_booking_asset",
        query: { assets, bookingId }
      });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully reallocated booking assets`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

export default {
  editBookingStatus: AdminActions.editBookingStatus,
  createBooking: AdminActions.createBooking,
  editService,
  createEquipment,
  editEquipment,
  createEquipmentAsset,
  editEquipmentAsset,
  createMaintenance,
  deleteMaintenance,
  removeObject,
  editBookingAssets
};
