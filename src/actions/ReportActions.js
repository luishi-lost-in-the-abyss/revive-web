import { UI_LOADING_MODAL, UI_NOTIFICATION_SNACK } from "../api/ActionTypes";
import FeathersClient from "../api/FeathersClient";

function getRawReport({ report, query = {} }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("report-api").create({
        report: "rawdata_" + report,
        query
      });
      cb && cb(null, res);
    } catch (err) {
      let notification;
      switch (err.name) {
        case "NotFound":
          notification = "ID not found";
          break;
        default:
          notification = "An error has occured. Please try again";
      }
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification,
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function getRawReportPaginate({ report, query = {} }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      let paginate = { $skip: 0, $limit: 50 };
      let allData = [];
      let res = await client.service("report-api").create({
        report: "rawdata_" + report,
        query,
        paginate
      });
      allData = res.header.concat(res.values);
      let totalCalls = Math.ceil(res.paginate.total / res.paginate.limit);
      let allPromises = [];
      for (let i = 1; i < totalCalls; i++) {
        const newPaginate = Object.assign({}, paginate, {
          $skip: res.paginate.limit * i
        });
        allPromises.push(
          client.service("report-api").create({
            report: "rawdata_" + report,
            query,
            paginate: newPaginate
          })
        );
      }
      allPromises = await Promise.all(allPromises);
      allPromises.map(({ values }) => {
        allData = allData.concat(values);
      });

      cb && cb(null, allData);
    } catch (err) {
      let notification;
      switch (err.name) {
        case "NotFound":
          notification = "ID not found";
          break;
        default:
          notification = "An error has occured. Please try again";
      }
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification,
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function getBookingReport({ report, query = {} }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("report-api").create({
        report: "booking_" + report,
        query
      });
      cb && cb(null, res);
    } catch (err) {
      let notification;
      switch (err.name) {
        case "NotFound":
          notification = "ID not found";
          break;
        default:
          notification = "An error has occured. Please try again";
      }
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification,
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function getServiceReport({ report, query = {} }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("report-api").create({
        report: "service_" + report,
        query
      });
      cb && cb(null, res);
    } catch (err) {
      let notification;
      switch (err.name) {
        case "NotFound":
          notification = "ID not found";
          break;
        default:
          notification = "An error has occured. Please try again";
      }
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification,
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

export default {
  getRawReport,
  getRawReportPaginate,
  getBookingReport,
  getServiceReport
};
