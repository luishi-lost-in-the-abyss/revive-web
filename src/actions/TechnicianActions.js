import { UI_LOADING_MODAL, UI_NOTIFICATION_SNACK } from "../api/ActionTypes";
import FeathersClient from "../api/FeathersClient";

function getFarmerPlots(cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("booking-methods")
        .create({ method: "get_bookable_plots" });
      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createBooking(
  {
    serviceId,
    startDate,
    endDate,
    plotId,
    clusterId,
    serviceArea,
    discount,
    additionalCosts,
    individualInformal
  },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client.service("booking-methods").create({
        method: "create_booking",
        query: {
          serviceId,
          startDate,
          endDate,
          plotId,
          clusterId,
          discount,
          serviceArea,
          additionalCosts,
          individualInformal
        }
      });
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "Successfully created booking",
          type: "success"
        }
      });
      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

function createPriceAgreement(
  { bookingId, deliveredPrice, truckingPrice, deliveryMode },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    try {
      const client = FeathersClient.getClient();
      const res = await client
        .service("price-agreements")
        .create({ bookingId, deliveredPrice, truckingPrice, deliveryMode });

      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully created price agreement`,
          type: "success"
        }
      });

      cb && cb(null, res);
    } catch (err) {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured. Please try again",
          type: "error"
        }
      });
      cb && cb(err, null);
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

export default { getFarmerPlots, createBooking, createPriceAgreement };
