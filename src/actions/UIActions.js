import { UI_LOADING_MODAL, UI_NOTIFICATION_SNACK } from "../api/ActionTypes";

function showLoadingModal(open) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open } });
  };
}

function showNotificationSnack({ open, notification, type }) {
  return async dispatch => {
    dispatch({
      type: UI_NOTIFICATION_SNACK,
      payload: open ? { open, notification, type } : { open }
    });
  };
}

export default { showLoadingModal, showNotificationSnack };
