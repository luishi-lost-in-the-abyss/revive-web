import {
  UI_LOADING_MODAL,
  UI_NOTIFICATION_SNACK,
  ACCOUNT_LOGIN,
  ACCOUNT_LOGOUT
} from "../api/ActionTypes";
import FeathersClient from "../api/FeathersClient";
import { initializeAuthClient } from "../api/utility";
import { API_AUTHENTICATION } from "../api/EndPoints";

async function loginWeb(token) {
  //this is a workaround to force cookies to be set as websocket to support cookies
  return await fetch(API_AUTHENTICATION, {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ accessToken: token, strategy: "jwt" })
  });
}

function registerUser(
  {
    firstName,
    middleName,
    lastName,
    username,
    email,
    password,
    birthdate,
    occupation,
    civil,
    operator,
    contact,
    residentAddress,
    gender,
    bankName,
    bankAccType,
    bankAccName,
    bankTinNo,
    healthConditions,
    healthOthers,
    socio,
    beneficiary,
    plots,
    isCluster,
    cluster,
    file
  },
  cb
) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    const client = FeathersClient.getClient();
    try {
      const res = await client.service("users").create({
        firstName,
        middleName,
        lastName,
        username,
        email,
        password,
        birthdate,
        occupation,
        civil,
        contact,
        operator,
        gender,
        residentAddress,
        bankName,
        bankAccType,
        bankAccName,
        bankTinNo,
        healthConditions,
        healthOthers,
        socio,
        beneficiary,
        plots,
        cluster: isCluster ? cluster : null,
        file: !!file
      });
      if (res.profile.fileUploadToken) {
        dispatch({
          type: UI_NOTIFICATION_SNACK,
          payload: {
            open: true,
            notification: `Uploading ID Image`,
            type: "success"
          }
        });
      }
      cb && cb(null, res);
    } catch (err) {
      cb && cb(err, null);
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    } finally {
      if (!file) {
        dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
      }
    }
  };
}

function loginUser({ username, password }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    const client = FeathersClient.getClient();
    initializeAuthClient(client, dispatch);
    let successLogin;
    try {
      await client.logout();
      successLogin = await client.authenticate({
        strategy: "local",
        username,
        password
      });
    } catch (e) {
      cb && cb(e, null);
      return;
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
    const { accessToken, role: tokenRole } = successLogin;
    await loginWeb(accessToken);

    window.localStorage.setItem("token", accessToken);

    dispatch({
      type: ACCOUNT_LOGIN,
      payload: {
        token: accessToken,
        role: tokenRole
      }
    });

    cb && cb(null, tokenRole);
  };
}

function logoutUser({}, cb) {
  return async dispatch => {
    window.localStorage.removeItem("token");
    dispatch({ type: ACCOUNT_LOGOUT });
    cb && cb(null, true);
  };
}

function changePassword({ password }, cb) {
  return async dispatch => {
    dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
    const client = FeathersClient.getClient();
    try {
      await client.service("users").patch(0, { password });
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: `Successfully changed password`,
          type: "success"
        }
      });
      cb && cb(null, true);
    } catch (e) {
      cb && cb(e, null);
      return;
    } finally {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
    }
  };
}

export default { registerUser, loginUser, logoutUser, changePassword };
