import feathersClient from "../api/FeathersClient";
import {
  FILEUPLOAD_NEW,
  FILEUPLOAD_UPDATE,
  FILEUPLOAD_END,
  UI_NOTIFICATION_SNACK
} from "../api/ActionTypes";

function uploadSlice({ socket, file, sliceIndex, sliceSize, fileId }) {
  const fileReader = new FileReader();
  fileReader.onload = evt => {
    socket.emit("file upload slice", {
      fileId,
      data: fileReader.result
    });
  };
  const place = sliceIndex * sliceSize,
    slice = file.slice(place, place + Math.min(100000, file.size - place));
  fileReader.readAsArrayBuffer(slice);
}

function uploadFile({ token, type, typeId, file, uploadToken }, cb) {
  return async dispatch => {
    const socket = feathersClient.getNewSocket();

    socket.on("file upload start", data => {
      dispatch({ type: FILEUPLOAD_NEW, fileId: data.fileId });
      uploadSlice({
        socket,
        file,
        sliceIndex: 0,
        sliceSize: data.sliceSize,
        fileId: data.fileId
      });

      cb && cb(data.fileId);
    });
    socket.on("file upload continue", data => {
      const noOfSlices = file.size % data.sliceSize;
      dispatch({
        type: FILEUPLOAD_UPDATE,
        fileId: data.fileId,
        progress: (data.slice / noOfSlices) * 100
      });
      uploadSlice({
        socket,
        file,
        sliceIndex: data.slice,
        sliceSize: data.sliceSize,
        fileId: data.fileId
      });
    });
    socket.on("file upload end", data => {
      dispatch({ type: FILEUPLOAD_END, fileId: data.fileId });
      socket.close();
    });
    socket.on("file error", err => {
      dispatch({
        type: UI_NOTIFICATION_SNACK,
        payload: {
          open: true,
          notification: "An error has occured uploading the file.",
          type: "error"
        }
      });
      console.log(err);
      socket.close();
    });

    socket.emit("file upload new", {
      token,
      uploadType: type,
      uploadTypeId: typeId,
      uploadToken: uploadToken,
      file: {
        name: file.name,
        type: file.type,
        size: file.size
      }
    });
  };
}

export default { uploadFile };
