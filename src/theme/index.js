import { createMuiTheme } from "@material-ui/core/styles";
import { brown, green } from "@material-ui/core/colors";

export default createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: brown,
    secondary: {
      main: green[900]
    }
  }
});

const drawerWidth = 240;

export const styles = theme => ({
  "@global": {
    body: { fontFamily: '"Roboto", "Helvetica", "Arial", "sans-serif"' }
  },
  rootPaper: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  },
  formField: {
    marginBottom: theme.spacing.unit * 3
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0
    }
  },
  drawerPaper: {
    width: drawerWidth
  },
  appBar: {
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth
    }
  },
  toolbar: theme.mixins.toolbar,
  containerContent: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    [theme.breakpoints.up("sm")]: {
      marginLeft: drawerWidth
    },
    overflowY: "auto"
  },

  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  },
  sideMenuItem: {
    ...theme.typography.overline,
    paddingTop: 2,
    paddingBottom: 2
  },
  endComponentButton: {
    backgroundColor: "black",
    color: "white"
  },
  paperList: {
    backgroundColor: theme.palette.background.paper
  }
});
