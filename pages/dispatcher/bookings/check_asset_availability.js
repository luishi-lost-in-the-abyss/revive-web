import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import AppContainer from "../../../src/components/common/AppContainer";
import { Button } from "@material-ui/core";
import SelectDatesStep from "../../../src/components/admin/bookings/create_booking/SelectDatesStep";
import DispatcherActions from "../../../src/actions/DispatcherActions";
import SelectEquipmentStep from "../../../src/components/admin/bookings/create_booking/SelectEquipmentStep";
import FeathersClient from "../../../src/api/FeathersClient";

class DispatcherCheckAssetAvailabilityPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fec: [],
      startDate: null,
      endDate: null,
      fromHour: {},
      toHour: {},
      fecId: "",
      equipment: {},
      submitError: false
    };

    this.onSelectedDate = this.onSelectedDate.bind(this);
    this.onSelectedTime = this.onSelectedTime.bind(this);
    this.onSelectFec = this.onSelectFec.bind(this);
    this.onSelectEquipment = this.onSelectEquipment.bind(this);
  }

  async componentDidMount() {
    //fetch the supervisors
    const client = FeathersClient.getClient();
    const fec = await client
      .service("fec")
      .find({ query: { $limit: "-1", getOwnFec: true } });
    this.setState({ fec });
  }

  onSelectedDate({ from, to, fromHour, toHour }) {
    this.setState({ startDate: from, endDate: to, fromHour, toHour });
  }

  onSelectedTime({ fromHour, toHour }) {
    this.setState(
      Object.assign(
        {},
        fromHour ? { fromHour } : null,
        toHour ? { toHour } : null
      )
    );
  }

  onSelectEquipment(equipment) {
    this.setState({ equipment });
  }
  onSelectFec(fecId) {
    this.setState({ fecId });
  }

  render() {
    const { classes } = this.props;
    const {
      startDate,
      endDate,
      activeStep,
      fromHour,
      toHour,
      submitError
    } = this.state;

    const finalStep = activeStep === 4;

    return (
      <AppContainer title="Check Asset Availability" appType="dispatcher">
        <SelectDatesStep
          selectedDates={{ startDate, endDate }}
          selectedTimes={{ fromHour, toHour }}
          onSelectedDate={this.onSelectedDate}
          onSelectedTime={this.onSelectedTime}
        />
        {Object.keys(fromHour).length > 0 && Object.keys(toHour).length > 0 && (
          <SelectEquipmentStep
            fec={this.state.fec}
            fecId={this.state.fecId}
            selectedTimes={{ fromHour, toHour }}
            equipment={this.state.equipment}
            onSelectFec={this.onSelectFec}
            disableSelectEquipment={true}
            onSelectEquipment={this.onSelectEquipment}
          />
        )}
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(DispatcherCheckAssetAvailabilityPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  Object.assign({}, DispatcherActions)
)(component);
