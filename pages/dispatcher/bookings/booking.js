import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import { Typography, Button } from "@material-ui/core";
import AppContainer from "../../../src/components/common/AppContainer";
import FeathersClient from "../../../src/api/FeathersClient";
import VerticalTable from "../../../src/components/common/VerticalTable";
import { DateTime } from "luxon";
import Booking from "../../../src/components/bookings/Booking";
import Popup from "../../../src/components/common/Popup";
import EditStatusPopp from "../../../src/components/admin/bookings/EditStatusPopp";
import DispatcherActions from "../../../src/actions/DispatcherActions";

class DispatcherViewBookingPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      booking: null,
      informalClusterParticipants: null,
      editStatus: false
    };

    this.toggleEditStatus = this.toggleEditStatus.bind(this);
    this.editStatus = this.editStatus.bind(this);
  }

  async getBooking() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("bookings").get(this.props.bookingId);

      const {
        dataSnapshot,
        status,
        statusReason,
        actualCompletedArea,
        actualStartDate,
        actualEndDate
      } = results;
      const { booking, participants } = dataSnapshot;
      let informalClusterParticipants = null;
      if (
        // check if informal cluster, if so retrieve the members of the informal cluster
        booking.farm_plot.clusterId &&
        !booking.individualInformal
      ) {
        informalClusterParticipants = participants;
      }
      this.setState({
        booking: Object.assign({}, booking, {
          status,
          statusReason,
          actualCompletedArea,
          actualStartDate,
          actualEndDate
        }),
        informalClusterParticipants
      });
    } catch (err) {
      this.setState({ error: true });
    }
  }
  async componentDidMount() {
    await this.getBooking();
  }

  static async getInitialProps({ query }) {
    return { bookingId: query.id };
  }

  toggleEditStatus() {
    this.setState({
      editStatus: !this.state.editStatus
    });
  }

  editStatus({
    status,
    statusReason,
    completedArea,
    actualStartDate,
    actualEndDate
  }) {
    this.props.editBookingStatus(
      {
        bookingId: this.props.bookingId,
        status,
        completedArea,
        statusReason,
        actualStartDate,
        actualEndDate
      },
      () => {
        this.getBooking();
        this.toggleEditStatus();
      }
    );
  }

  render() {
    const { classes, bookingId } = this.props;
    const {
      error,
      booking,
      informalClusterParticipants,
      editStatus
    } = this.state;

    return (
      <AppContainer
        title={`Booking ${booking ? booking.id : ""}`}
        appType="dispatcher"
        endComponent={
          booking &&
          booking.status === "pending" && (
            <Button
              onClick={this.toggleEditStatus}
              variant="contained"
              style={{ backgroundColor: "black", color: "white" }}
            >
              UPDATE STATUS
            </Button>
          )
        }
      >
        {error ? (
          "No such booking"
        ) : booking ? (
          <React.Fragment>
            <EditStatusPopp
              initialStatus={booking.status}
              startDate={booking.startDate}
              endDate={booking.endDate}
              serviceArea={booking.serviceArea}
              open={editStatus}
              onCancel={this.toggleEditStatus}
              editStatus={this.editStatus}
            />
            <Booking
              isAdmin={true}
              booking={booking}
              informalClusterParticipants={informalClusterParticipants}
              userRole="dispatcher"
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const DispatcherViewBookingPageWithStyles = withStyles(styles)(
  DispatcherViewBookingPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  Object.assign({ editBookingStatus: DispatcherActions.editBookingStatus })
)(DispatcherViewBookingPageWithStyles);
