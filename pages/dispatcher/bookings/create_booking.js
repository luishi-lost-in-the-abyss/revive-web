import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import AppContainer from "../../../src/components/common/AppContainer";
import {
  Typography,
  Step,
  Stepper,
  StepConnector,
  StepLabel,
  Button
} from "@material-ui/core";
import SelectServiceStep from "../../../src/components/technician/bookings/create_booking/SelectServiceStep";
import SelectDatesStep from "../../../src/components/admin/bookings/create_booking/SelectDatesStep";
import SelectPlotStep from "../../../src/components/technician/bookings/create_booking/SelectPlotStep";
import ConfirmationStep from "../../../src/components/technician/bookings/create_booking/ConfirmationStep";
import DispatcherActions from "../../../src/actions/DispatcherActions";
import SelectEquipmentStep from "../../../src/components/admin/bookings/create_booking/SelectEquipmentStep";
import FeathersClient from "../../../src/api/FeathersClient";
import SelectTechnicianStep from "../../../src/components/admin/bookings/create_booking/SelectTechnicianStep";

const steps = [
  "Service Code",
  "Booking Dates",
  "Equipment",
  "Farmer Plot",
  "Confirmation"
];
function getStepContent(step) {
  switch (step) {
    case 0:
      return "Select service to book.";
    case 1:
      return "Select start and end dates.";
    case 2:
      return "Select FEC and equipment.";
    case 3:
      return "Select the plot booking is for.";
    case 4:
      return "Review booking details.";
    default:
      return "Unknown step";
  }
}

class DispatcherCreateBookingPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      fec: [],
      service: {},
      startDate: null,
      endDate: null,
      fromHour: {},
      toHour: {},
      fecId: "",
      equipment: {},
      plot: "",
      individualInformal: false,
      additionalCosts: [],
      discount: 0,
      serviceArea: 0,
      plantableArea: 0,
      participant: null,
      technicianId: "",
      technicians: [],
      submitError: false
    };

    this.onSelectedService = this.onSelectedService.bind(this);
    this.onSelectedDate = this.onSelectedDate.bind(this);
    this.onSelectedTime = this.onSelectedTime.bind(this);
    this.onSelectFec = this.onSelectFec.bind(this);
    this.onSelectEquipment = this.onSelectEquipment.bind(this);
    this.onSelectedPlot = this.onSelectedPlot.bind(this);
    this.onCheckInformalIndividual = this.onCheckInformalIndividual.bind(this);
    this.onSelectTechnician = this.onSelectTechnician.bind(this);
    this.onServiceAreaChange = this.onServiceAreaChange.bind(this);
    this.updateAdditionalCosts = this.updateAdditionalCosts.bind(this);
    this.onDiscountChange = this.onDiscountChange.bind(this);
    this.createBooking = this.createBooking.bind(this);
  }

  async componentDidMount() {
    //fetch the supervisors
    const client = FeathersClient.getClient();
    const fec = await client
      .service("fec")
      .find({ query: { $limit: "-1", getOwnFec: true } });
    const technicians = await client
      .service("technicians")
      .find({ query: { $limit: "-1" } });
    this.setState({ fec, fecId: fec[0].id, technicians });
  }

  onSelectedService(service) {
    this.setState({ service });
  }

  onSelectedDate({ from, to, fromHour, toHour }) {
    this.setState({ startDate: from, endDate: to, fromHour, toHour });
  }

  onSelectedTime({ fromHour, toHour }) {
    this.setState(
      Object.assign(
        {},
        fromHour ? { fromHour } : null,
        toHour ? { toHour } : null
      )
    );
  }

  onSelectedPlot({ plot, participant }) {
    const newState = { plot, participant };
    if (participant && plot !== "informal") {
      newState.serviceArea = participant.plots.filter(
        ({ id }) => id === plot
      )[0].plantableArea;
      try {
        newState.serviceArea = parseFloat(newState.serviceArea).toFixed(2);
        newState.plantableArea = newState.serviceArea;
      } catch (err) {
        newState.serviceArea = 0;
      }
    }
    this.setState(newState);
  }

  onCheckInformalIndividual(check) {
    const newState = { individualInformal: check, plot: "" };
    if (!check) {
      newState.plot = "informal";
    }
    this.setState(newState);
  }

  updateAdditionalCosts(additionalCosts) {
    this.setState({ additionalCosts: [...additionalCosts] });
  }

  onDiscountChange(discount) {
    this.setState({ discount });
  }

  onServiceAreaChange(serviceArea) {
    try {
      this.setState({ serviceArea: parseFloat(serviceArea) });
    } catch (err) {}
  }

  onSelectEquipment(equipment) {
    this.setState({ equipment });
  }
  onSelectFec(fecId) {
    this.setState({ fecId });
  }
  onSelectTechnician(technicianId) {
    this.setState({ technicianId });
  }

  createBooking() {
    const {
      service,
      serviceArea,
      fromHour,
      toHour,
      fecId,
      equipment,
      plot,
      participant,
      discount,
      additionalCosts,
      individualInformal,
      technicianId
    } = this.state;
    this.props.createBooking(
      {
        serviceId: service.id,
        startDate: fromHour.toJSDate(),
        endDate: toHour.toJSDate(),
        fecId,
        equipment: Object.keys(equipment),
        serviceArea,
        plotId: participant.leader && !individualInformal ? null : plot,
        individualInformal,
        technicianId,
        clusterId:
          participant.leader && !individualInformal
            ? participant.leader.clusterLeaderId
            : null,
        discount,
        additionalCosts
      },
      (err, res) => {
        if (err) {
          console.log(err);
          return;
        }
        Router.push("/dispatcher/bookings/booking?id=" + res.id);
      }
    );
  }

  changeStep(add) {
    const { activeStep } = this.state;
    this.setState({ activeStep: activeStep + (add ? 1 : -1) });
  }

  isNextDisabled() {
    const {
      activeStep,
      service,
      startDate,
      fromHour,
      toHour,
      endDate,
      participant,
      serviceArea,
      plantableArea,
      plot
    } = this.state;
    let disabled = false;
    switch (activeStep) {
      case 0:
        disabled = service && !service.id;
        break;
      case 1:
        disabled = !fromHour.toFormat || !toHour.toFormat;
        break;
      case 3:
        disabled =
          !(plot && participant) ||
          (plot !== "informal" && serviceArea <= 0) ||
          (plot !== "informal" && serviceArea > plantableArea);
        break;
      default:
        break;
    }
    return disabled;
  }

  render() {
    const { classes } = this.props;
    const {
      startDate,
      endDate,
      participant,
      plot,
      serviceArea,
      service,
      discount,
      activeStep,
      fromHour,
      toHour,
      submitError,
      individualInformal,
      additionalCosts
    } = this.state;

    const finalStep = activeStep === 4;

    return (
      <AppContainer title="Create New Booking" appType="dispatcher">
        <Stepper activeStep={activeStep} connector={<StepConnector />}>
          {steps.map((label, index) => {
            return (
              <Step key={`step-${index}`}>
                <StepLabel
                  optional={
                    <Typography variant="caption">
                      {getStepContent(index)}
                    </Typography>
                  }
                >
                  {label}
                </StepLabel>
              </Step>
            );
          })}
        </Stepper>
        {activeStep === 0 ? (
          <SelectServiceStep
            selectedService={service}
            onSelectedService={this.onSelectedService}
          />
        ) : activeStep === 1 ? (
          <SelectDatesStep
            selectedService={service}
            selectedDates={{ startDate, endDate }}
            selectedTimes={{ fromHour, toHour }}
            onSelectedDate={this.onSelectedDate}
            onSelectedTime={this.onSelectedTime}
          />
        ) : activeStep === 2 ? (
          <SelectEquipmentStep
            fec={this.state.fec}
            fecId={this.state.fecId}
            selectedTimes={{ fromHour, toHour }}
            equipment={this.state.equipment}
            onSelectFec={this.onSelectFec}
            onSelectEquipment={this.onSelectEquipment}
          />
        ) : activeStep === 3 ? (
          <React.Fragment>
            <SelectTechnicianStep
              technicians={this.state.technicians}
              technicianId={this.state.technicianId}
              onSelectTechnician={this.onSelectTechnician}
            />
            <SelectPlotStep
              onSelectedPlot={this.onSelectedPlot}
              selectedParticipant={participant}
              selectedPlot={plot}
              serviceArea={serviceArea}
              onServiceAreaChange={this.onServiceAreaChange}
              plantableArea={this.state.plantableArea}
              individualInformal={individualInformal}
              onCheckInformalIndividual={this.onCheckInformalIndividual}
            />
          </React.Fragment>
        ) : finalStep ? (
          <ConfirmationStep
            additionalCosts={additionalCosts}
            updateAdditionalCosts={this.updateAdditionalCosts}
            onDiscountChange={this.onDiscountChange}
            individualInformal={individualInformal}
            {...{
              participant,
              plot,
              fromHour,
              toHour,
              plot,
              service,
              serviceArea,
              discount,
              additionalCosts
            }}
          />
        ) : null}

        <div
          style={{
            marginTop: 20,
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          <Button
            variant="outlined"
            color="secondary"
            style={{ marginRight: 50 }}
            disabled={activeStep === 0}
            onClick={this.changeStep.bind(this, false)}
          >
            Back
          </Button>
          <Button
            variant="contained"
            color="primary"
            disabled={this.isNextDisabled()}
            onClick={
              finalStep ? this.createBooking : this.changeStep.bind(this, true)
            }
          >
            {finalStep ? "Submit" : "Next"}
          </Button>
        </div>
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(DispatcherCreateBookingPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  Object.assign({}, DispatcherActions)
)(component);
