import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import { Grid, Typography } from "@material-ui/core";
import AppContainer from "../../../src/components/common/AppContainer";
import FeathersClient from "../../../src/api/FeathersClient";
import BookingCard from "../../../src/components/admin/bookings/dashboard/BookingCard";

class DispatcherBookingsDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = { upcomingBookings: null, incomingBookings: null };
  }

  async componentDidMount() {
    const currentDate = new Date();
    const client = FeathersClient.getClient();
    const upcomingBookings = await client.service("bookings").find({
      query: {
        $limit: 5,
        status: "pending",
        startDate: { $gte: currentDate },
        $sort: { startDate: 1 }
      }
    });
    const incomingBookings = await client.service("bookings").find({
      query: {
        $limit: 5,
        status: "pending",
        startDate: { $lte: currentDate },
        endDate: { $gte: currentDate },
        $sort: { endDate: 1 }
      }
    });
    this.setState({
      upcomingBookings: upcomingBookings.data,
      incomingBookings: incomingBookings.data
    });
  }

  render() {
    const { classes } = this.props;
    const { upcomingBookings, incomingBookings } = this.state;
    return (
      <AppContainer title="Bookings Dashboard" appType="dispatcher">
        <Grid container spacing={16}>
          <Grid item md={4}>
            <Typography variant="h6">Upcoming Bookings</Typography>
            {upcomingBookings &&
              upcomingBookings.map(booking => (
                <BookingCard
                  userRole="dispatcher"
                  key={`upcoming-booking-${booking.id}`}
                  {...booking}
                />
              ))}
          </Grid>
          <Grid item md={4}>
            <Typography variant="h6">Ending Bookings</Typography>
            {incomingBookings &&
              incomingBookings.map(booking => (
                <BookingCard
                  userRole="dispatcher"
                  showEnd={true}
                  key={`incoming-booking-${booking.id}`}
                  {...booking}
                />
              ))}
          </Grid>
        </Grid>
      </AppContainer>
    );
  }
}

export default withStyles(styles)(DispatcherBookingsDashboard);
