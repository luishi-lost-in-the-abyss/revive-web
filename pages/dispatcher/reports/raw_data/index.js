import {
  withStyles,
  Grid,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import { useState } from "react";
import ResourceReports from "../../../../src/components/admin/reports/raw_data/ResourceReports";

const reports = [{ title: "Resources", key: "resources" }];

const component = ({ classes }) => {
  const [nav, setNav] = useState("");

  const renderReportSection = () => {
    switch (nav) {
      case "resources":
        return <ResourceReports />;
      default:
        return "Choose a category";
    }
  };
  return (
    <AppContainer title="Raw Data Reports" appType="dispatcher">
      <Grid container spacing={16}>
        <Grid item xs={4}>
          <List className={classes.paperList}>
            {reports.map(({ title, key }) => (
              <ListItem
                button
                selected={key === nav}
                key={`listitem-${key}`}
                onClick={() => setNav(key)}
              >
                <ListItemText primary={title} />
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid item xs={8}>
          {renderReportSection()}
        </Grid>
      </Grid>
    </AppContainer>
  );
};

export default withStyles(styles)(component);
