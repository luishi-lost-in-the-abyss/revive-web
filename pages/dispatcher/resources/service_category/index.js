import React from "react";
import { connect } from "react-redux";
import Link from "next/link";
import { withStyles, Grid, Button, Typography } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import { styles } from "../../../../src/theme";
import DataTable from "../../../../src/components/common/DataTable";
import DispatcherActions from "../../../../src/actions/DispatcherActions";
import FeathersClient from "../../../../src/api/FeathersClient";

class DispatcherServiceCategoriesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false,
      currentCategory: null,
      editCategory: null,
      currentCategoryObj: null,
      path: [null],
      reloadData: new Date().getTime()
    };
  }

  static async getInitialProps({ query }) {
    if (query.id) {
      return { goToCategory: query.id };
    }
  }

  async goToCategory() {
    //navigate to the right category
    const { goToCategory } = this.props;
    const client = FeathersClient.getClient();
    const category = await client.service("service-category").get(goToCategory);
    this.navigate(category);
  }

  componentDidMount() {
    if (this.props.goToCategory) {
      this.goToCategory();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.goToCategory !== this.props.goToCategory) {
      this.goToCategory();
    }
  }

  navigate(category) {
    const { path } = this.state;
    this.setState({
      currentCategory: category.id,
      currentCategoryObj: category,
      path: [...path, category],
      reloadData: new Date().getTime()
    });
  }

  goBackCategory(index) {
    const { path } = this.state;
    const cat = path[index];
    this.setState({
      path: [...path.slice(0, index), cat],
      currentCategory: cat ? cat.id : null,
      currentCategoryObj: cat,
      reloadData: new Date().getTime()
    });
  }

  render() {
    const {
      currentCategory,
      currentCategoryObj,
      reloadData,
      path,
      openForm,
      editCategory
    } = this.state;
    return (
      <React.Fragment>
        <AppContainer title="Service Categories" appType="dispatcher">
          <Typography>Currently browsing:</Typography>
          <div
            style={{ marginBottom: 20, display: "flex", alignItems: "center" }}
          >
            {path.map((p, i) => {
              return (
                <React.Fragment key={`path-${p ? p.id : "root"}`}>
                  <div
                    {...(p && p.id === currentCategory
                      ? {}
                      : {
                          style: { cursor: "pointer" },
                          onClick: this.goBackCategory.bind(this, i)
                        })}
                  >
                    <Typography variant="h5">{p ? p.name : "Home"}</Typography>
                  </div>
                  {i !== path.length - 1 ? (
                    <div style={{ marginLeft: 5, marginRight: 5 }}>
                      <Typography variant="h5">></Typography>
                    </div>
                  ) : null}
                </React.Fragment>
              );
            })}
          </div>

          <DataTable
            service="service-category"
            colWidths={[null, 400]}
            findQuery={{ parentCategoryId: currentCategory }}
            reloadData={reloadData}
            dataCols={[
              { content: "Name", data: "name" },
              {
                content: "Actions",
                data: null,
                render: cat => {
                  return (
                    <React.Fragment>
                      {path.length === 1 ? (
                        <Button
                          style={{ marginLeft: 10 }}
                          variant="outlined"
                          onClick={this.navigate.bind(this, cat)}
                        >
                          View Phases
                        </Button>
                      ) : (
                        <Link
                          href={`/dispatcher/resources/service_category/category?id=${
                            cat.id
                          }`}
                        >
                          <Button variant="outlined">View Services</Button>
                        </Link>
                      )}
                    </React.Fragment>
                  );
                }
              }
            ]}
            disableSearch={true}
          />
        </AppContainer>
      </React.Fragment>
    );
  }
}

const component = withStyles(styles)(DispatcherServiceCategoriesPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  DispatcherActions
)(component);
