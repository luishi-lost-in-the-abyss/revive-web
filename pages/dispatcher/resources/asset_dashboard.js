import React from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../src/components/common/AppContainer";
import DataTable from "../../../src/components/common/DataTable";
import { Interval, DateTime } from "luxon";
import AssetDashboardFilter from "../../../src/components/admin/resources/equipment/AssetDashboardFilter";

const initialFindQuery = {};
class DispatcherAssetDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      findQuery: initialFindQuery,
      reloadData: new Date().getTime()
    };
    this.applyFilters = this.applyFilters.bind(this);
  }

  applyFilters(filterQuery) {
    if (!filterQuery) {
      this.setState({
        findQuery: initialFindQuery,
        reloadData: new Date().getTime()
      });
    }
    this.setState({
      findQuery: Object.assign({}, initialFindQuery, filterQuery),
      reloadData: new Date().getTime()
    });
  }

  render() {
    const { classes } = this.props;
    const { reloadData, findQuery } = this.state;

    const mainContent = (
      <DataTable
        colWidths={[200, 150, null, 150, 150, 100]}
        reloadData={reloadData}
        service="equipment-assets"
        findQuery={findQuery}
        dataCols={[
          { content: "Serial", data: "serial" },
          { content: "FEC", data: "fec", render: fec => fec.name },
          {
            content: "Equipment",
            data: "equipment",
            render: equip => equip.name
          },
          {
            content: "Booking",
            data: "bookings",
            render: bookings => {
              if (bookings.length === 0) return "-";
              return DateTime.fromJSDate(
                new Date(bookings[0].startDate)
              ).toFormat("dd-MM-yyyy");
            }
          },
          {
            content: "Condition",
            data: "conditionText"
          },
          {
            content: "Actions",
            data: "serial",
            render: id => {
              return (
                <React.Fragment>
                  <Link
                    passHref
                    href={`/dispatcher/resources/equipment/asset?id=${id}`}
                  >
                    <Button variant="outlined">View</Button>
                  </Link>
                </React.Fragment>
              );
            }
          }
        ]}
        searchCategory={[{ label: "Serial", value: "serial" }]}
        defaultSearchCategory="serial"
      />
    );

    return (
      <AppContainer title="Assets" appType="dispatcher">
        <div style={{ marginBottom: 20 }}>
          <AssetDashboardFilter applyFilters={this.applyFilters} />
        </div>
        {mainContent}
      </AppContainer>
    );
  }
}

export default withStyles(styles)(DispatcherAssetDashboard);
