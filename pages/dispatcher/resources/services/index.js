import React from "react";
import Link from "next/link";
import { withStyles, Grid, Button, Typography } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import { styles } from "../../../../src/theme";
import DataTable from "../../../../src/components/common/DataTable";

class DispatcherServicesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <AppContainer title="Services" appType="dispatcher">
        <DataTable
          service="services"
          colWidths={[150, 350, null, 100]}
          dataCols={[
            { content: "Code", data: "code" },
            { content: "Name", data: "name" },
            {
              content: "Equipment Usage",
              data: "equipment",
              render: equipment => equipment.map(({ name }) => name).join(" | ")
            },
            {
              content: "Actions",
              data: "id",
              render: id => {
                return (
                  <Link
                    href={`/dispatcher/resources/services/service?id=${id}`}
                  >
                    <Button variant="outlined">View</Button>
                  </Link>
                );
              }
            }
          ]}
          searchCategory={[
            { label: "Name", value: "name" },
            { label: "Code", value: "code" }
          ]}
          defaultSearchCategory="code"
        />
      </AppContainer>
    );
  }
}

export default withStyles(styles)(DispatcherServicesPage);
