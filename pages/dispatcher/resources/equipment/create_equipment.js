import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateEquipmentForm from "../../../../src/components/admin/resources/equipment/CreateEquipmentForm";
import DispatcherActions from "../../../../src/actions/DispatcherActions";

class DispatcherCreateEquipmentPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onSubmit({ name, description }) {
    this.props.createEquipment({ name, description }, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }
      Router.push("/dispatcher/resources/equipment");
    });
  }

  render() {
    const { classes } = this.props;

    return (
      <AppContainer title="Create New Equipment" appType="dispatcher">
        <CreateEquipmentForm
          userRole="dispatcher"
          onSubmit={this.onSubmit.bind(this)}
        />
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(DispatcherCreateEquipmentPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  DispatcherActions
)(component);
