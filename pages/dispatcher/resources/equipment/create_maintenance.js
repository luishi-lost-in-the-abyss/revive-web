import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import DispatcherActions from "../../../../src/actions/DispatcherActions";
import CreateAssetMaintenanceForm from "../../../../src/components/admin/resources/equipment/CreateAssetMaintenanceForm";

class DispatcherAssetMaintenancePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      asset: null
    };
    this.onSubmit = this.onSubmit.bind(this);
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("equipment-assets")
        .get(this.props.assetId);
      this.setState({ asset: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { assetId: query.id };
  }

  onSubmit({ endDate, startDate }) {
    const { assetId } = this.props;
    this.props.createMaintenance(
      { startDate, endDate, assetId },
      (err, res) => {
        if (err) {
          console.log(err);
          this.setState({ error: true });
          return;
        }
        Router.push(`/dispatcher/resources/equipment/asset?id=${assetId}`);
      }
    );
  }

  render() {
    const { classes, assetId } = this.props;
    const { error, asset } = this.state;

    return (
      <AppContainer
        title={`Scheulde Maintenance for Asset ${asset ? asset.serial : ""}`}
        appType="dispatcher"
      >
        {error ? (
          "No such Asset"
        ) : asset ? (
          <React.Fragment>
            <CreateAssetMaintenanceForm
              userRole="dispatcher"
              asset={asset}
              assetId={assetId}
              onSubmit={this.onSubmit}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const DispatcherAssetMaintenancePageWithStyles = withStyles(styles)(
  DispatcherAssetMaintenancePage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  DispatcherActions
)(DispatcherAssetMaintenancePageWithStyles);
