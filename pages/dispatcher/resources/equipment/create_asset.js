import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateAssetForm from "../../../../src/components/admin/resources/equipment/CreateAssetForm";
import DispatcherActions from "../../../../src/actions/DispatcherActions";
import FeathersClient from "../../../../src/api/FeathersClient";

class DispatcherCreateAssetPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false };
  }

  static async getInitialProps({ query }) {
    return { equipmentId: query.id };
  }

  onSubmit({ serial, fecId, equipmentId }) {
    this.props.createEquipmentAsset(
      { serial, fecId, equipmentId },
      (err, res) => {
        if (err) {
          console.log(err);
          this.setState({ error: true });
          return;
        }
        Router.push(
          `/dispatcher/resources/equipment/equipment${
            this.props.equipmentId ? `?id=${this.props.equipmentId}` : ""
          }`
        );
      }
    );
  }

  render() {
    const { classes, equipmentId } = this.props;
    const { error } = this.state;

    return (
      <AppContainer title="Create New Asset" appType="dispatcher">
        <CreateAssetForm
          userRole="dispatcher"
          equipmentId={equipmentId}
          onSubmit={this.onSubmit.bind(this)}
          submitError={error}
        />
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(DispatcherCreateAssetPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  DispatcherActions
)(component);
