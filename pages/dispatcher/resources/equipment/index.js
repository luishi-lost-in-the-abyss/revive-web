import React from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import DataTable from "../../../../src/components/common/DataTable";

class DispatcherEquipmentPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    const mainContent = (
      <DataTable
        service="equipment"
        colWidths={[400, null, 100, 100]}
        dataCols={[
          { content: "Name", data: "name" },
          { content: "Description", data: "description" },
          {
            content: "No. of Assets",
            data: "equipment_assets",
            render: arr => arr.length
          },
          {
            content: "Actions",
            data: "id",
            render: id => {
              return (
                <React.Fragment>
                  <Link
                    href={`/dispatcher/resources/equipment/equipment?id=${id}`}
                  >
                    <Button variant="outlined">View</Button>
                  </Link>
                </React.Fragment>
              );
            }
          }
        ]}
        searchCategory={[{ label: "Name", value: "name" }]}
        defaultSearchCategory="name"
      />
    );

    return (
      <AppContainer title="Equipment" appType="dispatcher">
        <Grid container style={{ marginBottom: 20 }} justify="flex-end">
          <Grid item xs={8} md={3}>
            <Link
              passHref
              href={`/dispatcher/resources/equipment/create_equipment`}
            >
              <Button color="primary" variant="outlined" fullWidth>
                Create New Equipment
              </Button>
            </Link>
          </Grid>
        </Grid>

        {mainContent}
      </AppContainer>
    );
  }
}

export default withStyles(styles)(DispatcherEquipmentPage);
