import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import DispatcherActions from "../../../../src/actions/DispatcherActions";
import Asset from "../../../../src/components/admin/resources/equipment/Asset";
import DataTable from "../../../../src/components/common/DataTable";
import { DateTime } from "luxon";

class DispatcherAssetPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reloadData: new Date().getTime(),
      error: false,
      asset: null
    };
    this.toggleBooking = this.toggleBooking.bind(this);
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("equipment-assets")
        .get(this.props.assetId);
      this.setState({ asset: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { assetId: query.id };
  }

  toggleBooking() {
    this.props.toggleAssetStatus(
      {
        assetId: this.props.assetId,
        enable: this.state.asset.condition !== "working"
      },
      (err, res) => {
        if (err) {
          console.log(err);
          return;
        }
        this.setState({
          asset: Object.assign({}, this.state.asset, {
            condition: res.condition
          })
        });
      }
    );
  }

  deleteMaintenance(id) {
    this.props.deleteMaintenance({ id }, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }
      this.setState({
        reloadData: new Date().getTime()
      });
    });
  }

  render() {
    const { classes, assetId } = this.props;
    const { reloadData, error, asset } = this.state;

    return (
      <AppContainer
        title={`Asset ${asset ? asset.serial : ""}`}
        appType="dispatcher"
        endComponent={
          <Button
            variant="contained"
            onClick={this.toggleBooking}
            className={classes.endComponentButton}
          >
            {asset && asset.condition === "working" ? "Disable" : "Enable"} for
            Booking
          </Button>
        }
      >
        {error ? (
          "No such Asset"
        ) : asset ? (
          <React.Fragment>
            <div style={{ textAlign: "right" }}>
              <Link
                href={`/dispatcher/resources/equipment/edit_asset?id=${
                  asset.serial
                }`}
              >
                <Button
                  color="secondary"
                  variant="outlined"
                  style={{ marginTop: 20 }}
                >
                  Edit Asset
                </Button>
              </Link>
            </div>
            <Typography variant="h6">Details</Typography>
            <Asset asset={asset} />

            <Typography variant="h6" style={{ marginTop: 20 }}>
              Maintenance Schedule
            </Typography>
            <div style={{ textAlign: "right" }}>
              <Link
                href={`/dispatcher/resources/equipment/create_maintenance?id=${
                  asset.serial
                }`}
              >
                <Button
                  color="secondary"
                  variant="outlined"
                  style={{ marginBottom: 20 }}
                >
                  Create New Schedule
                </Button>
              </Link>
            </div>
            <DataTable
              reloadData={reloadData}
              disableSearch={true}
              service="equipment-asset-maintenance"
              dataCols={[
                {
                  content: "Start Date",
                  data: "startDate",
                  render: date =>
                    DateTime.fromJSDate(new Date(date)).toFormat("d LLLL yyyy")
                },
                {
                  content: "End Date",
                  data: "endDate",
                  render: date =>
                    DateTime.fromJSDate(new Date(date)).toFormat("d LLLL yyyy")
                },
                {
                  content: "Actions",
                  data: "id",
                  render: id => {
                    return (
                      <Button
                        variant="outlined"
                        onClick={this.deleteMaintenance.bind(this, id)}
                      >
                        Delete
                      </Button>
                    );
                  }
                }
              ]}
              findQuery={{
                equipmentAssetSerial: asset.serial,
                endDate: { $gte: new Date() }
              }}
            />

            <Link
              href={
                "/dispatcher/resources/equipment/equipment?id=" +
                asset.equipmentId
              }
            >
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const DispatcherAssetPageWithStyles = withStyles(styles)(DispatcherAssetPage);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  DispatcherActions
)(DispatcherAssetPageWithStyles);
