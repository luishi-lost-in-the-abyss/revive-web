import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
import FeathersClient from "../../../../src/api/FeathersClient";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateEquipmentForm from "../../../../src/components/admin/resources/equipment/CreateEquipmentForm";
import DispatcherActions from "../../../../src/actions/DispatcherActions";
import ConfirmationPopup from "../../../../src/components/common/ConfirmationPopup";

class DispatcherEditEquipmentPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      equipment: null,
      error: false,
      submitError: false,
      openConfirmation: false,
      popperPosition: null
    };

    this.toggleConfirmation = this.toggleConfirmation.bind(this);
    this.removeEquipment = props.removeObject.bind(
      this,
      {
        id: props.equipmentId,
        type: "equipment"
      },
      (err, res) => {
        Router.push("/dispatcher/resources/equipment");
      }
    );
  }

  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("equipment")
        .get(this.props.equipmentId);
      this.setState({ equipment: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { equipmentId: query.id };
  }

  onSubmit({ name, description }) {
    this.props.editEquipment(
      { equipmentId: this.props.equipmentId, name, description },
      (err, res) => {
        if (err) {
          console.log(err);
          return;
        }
        Router.push(
          "/dispatcher/resources/equipment/equipment?id=" +
            this.props.equipmentId
        );
      }
    );
  }

  toggleConfirmation(evt) {
    this.setState({
      openConfirmation: !this.state.openConfirmation,
      popperPosition: evt.target
    });
  }

  render() {
    const { classes, equipmentId } = this.props;
    const {
      equipment,
      error,
      submitError,
      openConfirmation,
      popperPosition
    } = this.state;

    return (
      <AppContainer
        title={`Editing Equipment ${equipment ? equipment.name : ""}`}
        appType="dispatcher"
        endComponent={
          <Button
            variant="outlined"
            className={classes.endComponentButton}
            onClick={this.toggleConfirmation}
          >
            Delete Equipment
          </Button>
        }
      >
        {error ? (
          "No such Equipment"
        ) : equipment ? (
          <React.Fragment>
            <ConfirmationPopup
              open={openConfirmation}
              anchorEl={popperPosition}
              onClickAway={this.toggleConfirmation}
              title="Delete Equipment? All assets under this equipment will be removed!"
              onCancel={this.toggleConfirmation}
              onConfirm={this.removeEquipment}
            />
            <CreateEquipmentForm
              userRole="dispatcher"
              onSubmit={this.onSubmit.bind(this)}
              submitError={submitError}
              editMode={true}
              equipment={equipment}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(DispatcherEditEquipmentPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  DispatcherActions
)(component);
