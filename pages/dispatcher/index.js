import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../src/theme";
import { Grid, Typography } from "@material-ui/core";
import AppContainer from "../../src/components/common/AppContainer";
import FeathersClient from "../../src/api/FeathersClient";
import { DateTime } from "luxon";
import {
  VictoryChart,
  VictoryLine,
  VictoryTheme,
  VictoryZoomContainer,
  VictoryLegend,
  VictoryBar,
  VictoryGroup,
  VictoryTooltip,
  VictoryLabel
} from "victory";

class DispatcherHomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bookingCounts: null,
      cancelReasonCounts: null,
      completedCancelChart: null,
      cancelReasonChart: null,
      minMax: { min: 0, max: 5 }
    };
  }

  async loadData() {
    const client = FeathersClient.getClient();
    let bookings = [],
      temp;
    const currentDate = DateTime.local();

    do {
      temp = await client.service("bookings").find({
        query: {
          $skip: bookings.length,
          $or: [
            {
              $and: [
                { actualEndDate: null },
                { endDate: { $gte: currentDate.startOf("year").toJSDate() } },
                { endDate: { $lte: currentDate.endOf("year").toJSDate() } }
              ]
            },
            {
              $and: [
                {
                  actualEndDate: {
                    $gte: currentDate.startOf("year").toJSDate()
                  }
                },
                {
                  actualEndDate: { $lte: currentDate.endOf("year").toJSDate() }
                }
              ]
            }
          ],
          status: { $or: ["completed", "cancel", "pending"] }
        }
      });
      bookings = [...bookings, ...temp.data];
    } while (bookings.length !== temp.total);

    const minMax = { min: 99, max: 0 };
    const completedCancelChart = { completed: {}, cancel: {} };
    const cancelReasonChart = {
      "Bad Weather": {},
      "Under Repair": {},
      "Permits and Licensed Concern": {},
      Reschedule: {},
      Others: {}
    };
    const bookingCounts = { completed: 0, cancel: 0, pending: 0 };
    const cancelReasonCounts = {
      "Bad Weather": 0,
      "Under Repair": 0,
      "Permits and Licensed Concern": 0,
      Reschedule: 0,
      Others: 0
    };
    bookings.map(booking => {
      const { actualEndDate, endDate, status, statusReason } = booking;
      bookingCounts[status] += 1;
      if (status === "pending") return;

      const eDate = DateTime.fromJSDate(
        new Date(actualEndDate ? actualEndDate : endDate)
      );
      const week = eDate.get("weekNumber");
      if (completedCancelChart[status][week]) {
        completedCancelChart[status][week] += 1;
      } else {
        completedCancelChart[status][week] = 1;
        const otherStatus = status === "completed" ? "cancel" : "completed";
        if (!completedCancelChart[otherStatus][week]) {
          completedCancelChart[otherStatus][week] = 0;
        }
      }

      if (status === "cancel") {
        let reason;
        switch (statusReason) {
          case "Bad Weather":
          case "Under Repair":
          case "Permits and Licensed Concern":
          case "Reschedule":
            reason = statusReason;
            break;
          default:
            reason = "Others";
            break;
        }
        cancelReasonCounts[reason] += 1;
        if (!cancelReasonChart[reason][week]) {
          cancelReasonChart[reason][week] = 1;
        } else {
          cancelReasonChart[reason][week] += 1;
        }
        Object.keys(cancelReasonChart).map(otherReason => {
          if (otherReason === reason) return;
          if (!cancelReasonChart[otherReason][week]) {
            cancelReasonChart[otherReason][week] = 0;
          }
        });

        if (week > minMax.max) {
          minMax.max = week;
        }
        if (week < minMax.min) {
          minMax.min = week;
        }
      }
    });

    Object.keys(cancelReasonChart).map(reason => {
      const obj = cancelReasonChart[reason];
      for (let i = minMax.min; i <= minMax.max; i++) {
        if (!obj[i]) {
          obj[i] = 0;
        }
      }
    });
    Object.keys(completedCancelChart).map(status => {
      const obj = completedCancelChart[status];
      for (let i = minMax.min; i <= minMax.max; i++) {
        if (!obj[i]) {
          obj[i] = 0;
        }
      }
    });

    this.setState({
      bookingCounts,
      completedCancelChart,
      cancelReasonChart,
      cancelReasonCounts,
      minMax
    });
  }

  async componentDidMount() {
    this.loadData();
  }

  render() {
    const { classes } = this.props;
    const {
      completedCancelChart,
      cancelReasonChart,
      bookingCounts,
      cancelReasonCounts,
      minMax
    } = this.state;

    const noOfData = minMax.max - minMax.min + 2;
    return (
      <AppContainer title="Dashboard" appType="dispatcher">
        {!bookingCounts && "Fetching Data..."}
        <Grid container spacing={32} alignItems="flex-end">
          <Grid item xs={12} md={6}>
            {bookingCounts && (
              <div style={{ display: "flex", marginBottom: 50 }}>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Completed: {bookingCounts.completed}
                </Typography>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Cancel: {bookingCounts.cancel}
                </Typography>
                <Typography variant="h6">
                  Pending: {bookingCounts.pending}
                </Typography>
              </div>
            )}
            {completedCancelChart && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      minMax.max - minMax.min > 5
                        ? { x: [noOfData - 5, noOfData] }
                        : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  x={180}
                  title="COMPLETED VS CANCEL"
                  centerTitle
                  orientation="horizontal"
                  style={{
                    title: { fontSize: 12 }
                  }}
                  data={[
                    {
                      name: "Completed",
                      symbol: { fill: "#1b5e20", type: "square" }
                    },
                    {
                      name: "Cancel",
                      symbol: { fill: "#795548", type: "square" }
                    }
                  ]}
                />
                <VictoryLine
                  style={{ data: { stroke: "#1b5e20" } }}
                  labelComponent={<VictoryLabel />}
                  data={Object.keys(completedCancelChart.completed).map(
                    week => ({
                      x: "Week " + week,
                      y: completedCancelChart.completed[week],
                      label: completedCancelChart.completed[week]
                    })
                  )}
                />
                <VictoryLine
                  labelComponent={<VictoryLabel />}
                  style={{ data: { stroke: "#795548" } }}
                  data={Object.keys(completedCancelChart.cancel).map(week => ({
                    x: "Week " + week,
                    y: completedCancelChart.cancel[week],
                    label: completedCancelChart.cancel[week]
                  }))}
                />
              </VictoryChart>
            )}
          </Grid>
          <Grid item xs={12} md={6}>
            {cancelReasonCounts && (
              <div
                style={{
                  display: "flex",
                  marginBottom: 50,
                  justifyContent: "space-between",
                  flexWrap: "wrap"
                }}
              >
                {Object.keys(cancelReasonCounts).map(reason => {
                  return (
                    <Typography
                      variant="h6"
                      style={{ marginRight: 20 }}
                      key={`reasoncount-${reason}`}
                    >
                      {reason}: {cancelReasonCounts[reason]}
                    </Typography>
                  );
                })}
              </div>
            )}
            {cancelReasonChart && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      minMax.max - minMax.min > 5
                        ? { x: [noOfData - 5, noOfData] }
                        : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  title="REASON FOR CANCEL STATUS"
                  x={100}
                  orientation="horizontal"
                  itemsPerRow={3}
                  centerTitle
                  style={{ title: { fontSize: 12 }, labels: { fontSize: 8 } }}
                  colorScale="heatmap"
                  data={Object.keys(cancelReasonChart).map(reason => ({
                    name: reason
                  }))}
                />
                <VictoryGroup offset={15} colorScale={"heatmap"}>
                  {Object.keys(cancelReasonChart).map(reason => {
                    const data = cancelReasonChart[reason];
                    return (
                      <VictoryBar
                        barWidth={15}
                        labelComponent={<VictoryTooltip />}
                        key={`cancelreasonchart-${reason}`}
                        data={Object.keys(data).map(week => ({
                          x: "Week " + week,
                          y: data[week],
                          label: reason + ": " + data[week]
                        }))}
                      />
                    );
                  })}
                </VictoryGroup>
              </VictoryChart>
            )}
          </Grid>
        </Grid>
      </AppContainer>
    );
  }
}

export default withStyles(styles)(DispatcherHomePage);
