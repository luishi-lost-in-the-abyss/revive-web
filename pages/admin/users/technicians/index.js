import React from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import DataTable from "../../../../src/components/common/DataTable";
import TechnicianFilter from "../../../../src/components/admin/users/technicians/TechnicianFilter";

const initialFindQuery = {};

class AdminTechniciansPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reloadData: new Date().getTime()
    };
    this.applyFilters = this.applyFilters.bind(this);
  }

  applyFilters(filterQuery) {
    if (!filterQuery) {
      this.setState({
        findQuery: initialFindQuery,
        reloadData: new Date().getTime()
      });
    }
    this.setState({
      findQuery: Object.assign({}, initialFindQuery, filterQuery),
      reloadData: new Date().getTime()
    });
  }

  render() {
    const { classes } = this.props;
    const { findQuery, reloadData } = this.state;

    const mainContent = (
      <DataTable
        service="technicians"
        reloadData={reloadData}
        findQuery={findQuery}
        dataCols={[
          { content: "ID", data: "id" },
          {
            content: "Username",
            data: null,
            render: data => data.user.username
          },
          { content: "Name", data: "name" },
          { content: "Contact", data: "contact" },
          { content: "Email", data: null, render: data => data.user.email },
          {
            content: "FEC",
            data: "supervisor",
            render: supervisor => supervisor.fec.name
          },
          {
            content: "Actions",
            data: "id",
            render: id => {
              return (
                <React.Fragment>
                  <Link
                    passHref
                    href={`/admin/users/technicians/technician?id=${id}`}
                  >
                    <Button variant="outlined">View</Button>
                  </Link>
                </React.Fragment>
              );
            }
          }
        ]}
        searchCategory={[
          { label: "Name", value: "name" },
          { label: "Username", value: "username" }
        ]}
        defaultSearchCategory="name"
      />
    );

    return (
      <AppContainer title="Technicians" appType="admin">
        <Grid container style={{ marginBottom: 20 }} justify="flex-end">
          <Grid item xs={8} md={3}>
            <Link passHref href={`/admin/users/technicians/create_technician`}>
              <Button color="primary" variant="outlined" fullWidth>
                Create New Technician
              </Button>
            </Link>
          </Grid>
        </Grid>
        <div style={{ marginBottom: 20 }}>
          <TechnicianFilter applyFilters={this.applyFilters} />
        </div>

        {mainContent}
      </AppContainer>
    );
  }
}

export default withStyles(styles)(AdminTechniciansPage);
