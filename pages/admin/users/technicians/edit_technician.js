import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
import FeathersClient from "../../../../src/api/FeathersClient";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateTechnicianForm from "../../../../src/components/admin/users/technicians/CreateTechnicianForm";
import AdminActions from "../../../../src/actions/AdminActions";
import ConfirmationPopup from "../../../../src/components/common/ConfirmationPopup";

class AdminEditTechnicianPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fec: [],
      technician: null,
      submitError: false,
      openConfirmation: false,
      popperPosition: null
    };

    this.toggleConfirmation = this.toggleConfirmation.bind(this);
    this.removeTechnician = props.removeObject.bind(
      this,
      {
        id: props.technicianId,
        type: "technicians"
      },
      (err, res) => {
        Router.push("/admin/users/technicians");
      }
    );
  }

  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("technicians")
        .get(this.props.technicianId);

      const fec = await client.service("fec").find({ query: { $limit: "-1" } });
      this.setState({ technician: results, fec });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { technicianId: query.id };
  }

  onSubmit({ name, email, password, supervisor, contact, barangayId }) {
    this.setState({ submitError: false });
    this.props.editTechnician(
      {
        name,
        email,
        contact,
        password,
        supervisor,
        barangayId,
        technicianId: this.props.technicianId
      },
      (err, res) => {
        if (err) {
          console.log(err);
          this.setState({ submitError: true });
          return;
        }
        Router.push(
          "/admin/users/technicians/technician?id=" + this.props.technicianId
        );
      }
    );
  }

  toggleConfirmation(evt) {
    this.setState({
      openConfirmation: !this.state.openConfirmation,
      popperPosition: evt.target
    });
  }

  render() {
    const { classes, technicianId } = this.props;
    const {
      fec,
      error,
      technician,
      submitError,
      openConfirmation,
      popperPosition
    } = this.state;
    return (
      <AppContainer
        title={`Editing Technician ${technician ? technician.name : ""}`}
        appType="admin"
        endComponent={
          <Button
            variant="outlined"
            className={classes.endComponentButton}
            onClick={this.toggleConfirmation}
          >
            Delete Technician
          </Button>
        }
      >
        {error ? (
          "No such technician"
        ) : technician ? (
          <React.Fragment>
            <ConfirmationPopup
              open={openConfirmation}
              anchorEl={popperPosition}
              onClickAway={this.toggleConfirmation}
              title="Delete Technician?"
              onCancel={this.toggleConfirmation}
              onConfirm={this.removeTechnician}
            />
            <CreateTechnicianForm
              fec={fec}
              onSubmit={this.onSubmit.bind(this)}
              submitError={submitError}
              editMode={true}
              technician={technician}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminEditTechnicianPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
