import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import AdminActions from "../../../../src/actions/AdminActions";
import Cluster from "../../../../src/components/admin/users/clusters/Cluster";
import DataTable from "../../../../src/components/common/DataTable";
import { extractLocationDetails } from "../../../../src/api/utility";

class AdminClusterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, cluster: null, isFormal: false };
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("clusters").get(this.props.clusterId);
      this.setState({ cluster: results, isFormal: !!results.formal_cluster });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { clusterId: query.id };
  }

  render() {
    const { classes, clusterId } = this.props;
    const { error, cluster, isFormal } = this.state;

    return (
      <AppContainer
        title={`Cluster ${cluster ? cluster.code : ""}`}
        appType="admin"
      >
        {error ? (
          "No such cluster"
        ) : cluster ? (
          <React.Fragment>
            <div style={{ textAlign: "right" }}>
              <Link
                passHref
                href={`/admin/users/clusters/edit_cluster?id=${clusterId}`}
              >
                <Button
                  color="secondary"
                  variant="outlined"
                  style={{ marginTop: 20 }}
                >
                  Edit Cluster
                </Button>
              </Link>
            </div>
            <Typography variant="h6">Cluster Details</Typography>
            <Cluster cluster={cluster} />
            <Typography variant="h6">Cluster Members</Typography>
            <DataTable
              service="participants"
              dataCols={[
                { content: "ID", data: "id" },
                { content: "Name", data: "name" }
              ]}
              findQuery={Object.assign(
                { userStatus: "activated" },
                isFormal ? { formalCLusterId: clusterId } : { clusterId }
              )}
              searchCategory={[{ label: "Name", value: "name" }]}
              defaultSearchCategory="name"
            />
            <Typography variant="h6">Farm Plots</Typography>
            <DataTable
              offline={true}
              dataArr={
                cluster
                  ? cluster.formal_cluster
                    ? cluster.formal_cluster.farm_plots
                    : cluster.farm_plots
                  : []
              }
              service="Farm Plots"
              disableSearch={true}
              dataCols={[
                { content: "ID", data: "id" },
                { content: "Code", data: "code" },
                {
                  content: "Address",
                  data: null,
                  render: ({ barangay, purok }) => {
                    const {
                      municipality,
                      province,
                      region,
                      country
                    } = extractLocationDetails(barangay);
                    return `${country.name} ${region.name} ${province.name} ${
                      municipality.name
                    } ${barangay.name} ${purok}`;
                  }
                },
                { content: "Total Area", data: "totalArea" },
                { content: "Plantable Area", data: "plantableArea" },
                {
                  content: "Assigned Tech.",
                  data: "technician",
                  render: technician => (technician ? technician.name : "-")
                },
                {
                  content: "Actions",
                  data: null,
                  render: data => (
                    <React.Fragment>
                      <Button
                        variant="outlined"
                        onClick={() => {
                          Router.push(
                            `/admin/users/participants/plot?id=${data.id}`
                          );
                        }}
                      >
                        View
                      </Button>
                      <Button
                        variant="outlined"
                        onClick={() => {
                          Router.push(
                            `/admin/users/participants/edit_plot?id=${data.id}`
                          );
                        }}
                      >
                        Edit
                      </Button>
                    </React.Fragment>
                  )
                }
              ]}
            />
            <Link href="/admin/users/clusters">
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const AdminClusterPageWithStyles = withStyles(styles)(AdminClusterPage);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  AdminActions
)(AdminClusterPageWithStyles);
