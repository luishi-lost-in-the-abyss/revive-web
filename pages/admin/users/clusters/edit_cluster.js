import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
import FeathersClient from "../../../../src/api/FeathersClient";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import EditClusterForm from "../../../../src/components/admin/users/clusters/EditClusterForm";
import AdminActions from "../../../../src/actions/AdminActions";

class AdminEditClusterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submitError: false,
      submitErrorType: "none",
      formalError: {},
      formalErrorType: "none"
    };
  }

  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("clusters").get(this.props.clusterId);
      this.setState({ cluster: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { clusterId: query.id };
  }

  onSubmit({ code, clusterLeader }) {
    this.setState({ submitError: false, submitErrorType: "none" });
    this.props.editCluster(
      {
        clusterId: this.props.clusterId,
        code,
        clusterLeader
      },
      (err, res) => {
        if (err) {
          console.log(err);
          this.setState({
            submitError: true,
            submitErrorType: "duplicateCode"
          });
          return;
        }
        Router.push(`/admin/users/clusters/cluster?id=${this.props.clusterId}`);
      }
    );
    return;
  }

  onFormalSubmit(details) {
    this.setState({ submitError: false, submitErrorType: "none" });
    this.props.editFormalCluster(details, (err, res) => {
      if (err) {
        console.log(err);
        if (err.errors && err.errors[0].path === "code") {
          this.setState({
            formalError: { code: true },
            formalErrorType: "duplicateCode"
          });
          window.scrollTo(0, 0);
        }
        return;
      }
      Router.push(`/admin/users/clusters/cluster?id=${this.props.clusterId}`);
    });
    return;
  }

  render() {
    const { classes, clusterId } = this.props;
    const {
      error,
      cluster,
      submitError,
      submitErrorType,
      formalError,
      formalErrorType
    } = this.state;

    return (
      <AppContainer
        title={`Editing Cluster ${cluster ? cluster.code : ""}`}
        appType="admin"
      >
        {error ? (
          "No such cluster"
        ) : cluster ? (
          <React.Fragment>
            <EditClusterForm
              cluster={cluster}
              submitError={submitError}
              submitErrorType={submitErrorType}
              formalError={formalError}
              formalErrorType={formalErrorType}
              onSubmit={this.onSubmit.bind(this)}
              onFormalSubmit={this.onFormalSubmit.bind(this)}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminEditClusterPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
