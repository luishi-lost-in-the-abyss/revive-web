import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import FeathersClient from "../../../../src/api/FeathersClient";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateClusterForm from "../../../../src/components/admin/users/clusters/CreateClusterForm";
import AdminActions from "../../../../src/actions/AdminActions";

class AdminCreateClusterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submitError: false,
      submitErrorType: "none",
      formalError: {},
      formalErrorType: "none"
    };
  }

  onSubmit({ code }) {
    this.setState({ submitError: false, submitErrorType: "none" });
    this.props.createCluster({ code }, (err, res) => {
      if (err) {
        console.log(err);
        this.setState({ submitError: true, submitErrorType: "duplicateCode" });
        return;
      }
      Router.push("/admin/users/clusters");
    });
  }

  onFormalSubmit(details) {
    this.setState({ formalError: {}, formalErrorType: "none" });
    this.props.createFormalCluster(details, (err, res) => {
      if (err) {
        if (err.errors && err.errors[0].path === "username") {
          this.setState({
            formalError: { username: true },
            submitErrorType: "duplicateUsername"
          });
        } else if (err.errors && err.errors[0].path === "code") {
          window.scrollTo(0, 0);
          this.setState({
            submitError: true,
            submitErrorType: "duplicateCode"
          });
        }
        return;
      }
      Router.push("/admin/users/clusters");
    });
  }

  onSubmit({ code, type }) {
    this.setState({ submitError: false, submitErrorType: "none" });
    this.props.createCluster({ code, type }, (err, res) => {
      if (err) {
        console.log(err);
        this.setState({ submitError: true, submitErrorType: "duplicateCode" });
        return;
      }
      Router.push("/admin/users/clusters");
    });
  }

  render() {
    const { classes } = this.props;
    const {
      submitErrorType,
      submitError,
      formalError,
      formalErrorType
    } = this.state;

    return (
      <AppContainer title="Create New Cluster" appType="admin">
        <CreateClusterForm
          submitError={submitError}
          submitErrorType={submitErrorType}
          onSubmit={this.onSubmit.bind(this)}
          onFormalSubmit={this.onFormalSubmit.bind(this)}
          formalError={formalError}
          formalErrorType={formalErrorType}
        />
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminCreateClusterPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
