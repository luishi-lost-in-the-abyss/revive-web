import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateFecForm from "../../../../src/components/admin/users/fecs/CreateFecForm";
import AdminActions from "../../../../src/actions/AdminActions";

class AdminCreateFecPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onSubmit({ name }) {
    this.props.createFec({ name }, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }
      Router.push("/admin/users/fecs");
    });
  }

  render() {
    const { classes } = this.props;

    return (
      <AppContainer title="Create New FEC" appType="admin">
        <CreateFecForm onSubmit={this.onSubmit.bind(this)} />
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminCreateFecPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
