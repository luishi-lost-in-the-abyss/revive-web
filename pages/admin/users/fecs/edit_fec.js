import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
import FeathersClient from "../../../../src/api/FeathersClient";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateFecForm from "../../../../src/components/admin/users/fecs/CreateFecForm";
import AdminActions from "../../../../src/actions/AdminActions";

class AdminEditFecPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fec: null,
      error: false,
      submitError: false
    };
  }

  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("fec").get(this.props.fecId);
      this.setState({ fec: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { fecId: query.id };
  }

  onSubmit({ name }) {
    this.setState({ submitError: false });
    this.props.editFec({ name, fecId: this.props.fecId }, (err, res) => {
      if (err) {
        console.log(err);
        this.setState({ submitError: true });
        return;
      }
      Router.push("/admin/users/fecs/fec?id=" + this.props.fecId);
    });
  }

  render() {
    const { classes, fecId } = this.props;
    const { fec, error, submitError } = this.state;

    return (
      <AppContainer
        title={`Editing FEC ${fec ? fec.name : ""}`}
        appType="admin"
      >
        {error ? (
          "No such FEC"
        ) : fec ? (
          <React.Fragment>
            <CreateFecForm
              onSubmit={this.onSubmit.bind(this)}
              submitError={submitError}
              editMode={true}
              fec={fec}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminEditFecPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
