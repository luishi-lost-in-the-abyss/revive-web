import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import AdminActions from "../../../../src/actions/AdminActions";
import Dispatcher from "../../../../src/components/admin/users/dispatchers/Dispatcher";

class AdminDispatcherPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, dispatcher: null };
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("dispatchers")
        .get(this.props.dispatcherId);
      this.setState({ dispatcher: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { dispatcherId: query.id };
  }

  render() {
    const { classes, dispatcherId } = this.props;
    const { error, dispatcher } = this.state;

    return (
      <AppContainer
        title={`Dispatcher ${dispatcher ? dispatcher.name : ""}`}
        appType="admin"
      >
        {error ? (
          "No such Dispatcher"
        ) : dispatcher ? (
          <React.Fragment>
            <div style={{ textAlign: "right" }}>
              <Link
                href={`/admin/users/dispatchers/edit_dispatcher?id=${dispatcherId}`}
              >
                <Button
                  color="secondary"
                  variant="outlined"
                  style={{ marginTop: 20 }}
                >
                  Edit Dispatcher
                </Button>
              </Link>
            </div>
            <Typography variant="h6">Details</Typography>
            <Dispatcher dispatcher={dispatcher} />
            <Link passHref href="/admin/users/dispatchers">
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const AdminDispatcherPageWithStyles = withStyles(styles)(AdminDispatcherPage);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  AdminActions
)(AdminDispatcherPageWithStyles);
