import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
import FeathersClient from "../../../../src/api/FeathersClient";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateDispatcherForm from "../../../../src/components/admin/users/dispatchers/CreateDispatcherForm";
import AdminActions from "../../../../src/actions/AdminActions";
import ConfirmationPopup from "../../../../src/components/common/ConfirmationPopup";

class AdminEditDispatcherPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fec: [],
      dispatcher: null,
      submitError: false,
      openConfirmation: false,
      popperPosition: null
    };

    this.toggleConfirmation = this.toggleConfirmation.bind(this);
    this.removeDispatcher = props.removeObject.bind(
      this,
      {
        id: props.dispatcherId,
        type: "dispatchers"
      },
      (err, res) => {
        Router.push("/admin/users/dispatchers");
      }
    );
  }

  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("dispatchers")
        .get(this.props.dispatcherId);

      const fec = await client.service("fec").find({ query: { $limit: "-1" } });
      this.setState({ dispatcher: results, fec });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { dispatcherId: query.id };
  }

  onSubmit({ name, fec, email, contact, password }) {
    this.setState({ submitError: false });
    this.props.editDispatcher(
      {
        name,
        email,
        fec,
        password,
        contact,
        dispatcherId: this.props.dispatcherId
      },
      (err, res) => {
        if (err) {
          console.log(err);
          this.setState({ submitError: true });
          return;
        }
        Router.push(
          "/admin/users/dispatchers/dispatcher?id=" + this.props.dispatcherId
        );
      }
    );
  }

  toggleConfirmation(evt) {
    this.setState({
      openConfirmation: !this.state.openConfirmation,
      popperPosition: evt.target
    });
  }

  render() {
    const { classes, dispatcherId } = this.props;
    const {
      fec,
      error,
      dispatcher,
      submitError,
      openConfirmation,
      popperPosition
    } = this.state;

    return (
      <AppContainer
        title={`Editing Dispatcher ${dispatcher ? dispatcher.name : ""}`}
        appType="admin"
        endComponent={
          <Button
            variant="outlined"
            className={classes.endComponentButton}
            onClick={this.toggleConfirmation}
          >
            Delete Dispatcher
          </Button>
        }
      >
        {error ? (
          "No such dispatcher"
        ) : dispatcher ? (
          <React.Fragment>
            <ConfirmationPopup
              open={openConfirmation}
              anchorEl={popperPosition}
              onClickAway={this.toggleConfirmation}
              title="Delete Dispatcher?"
              onCancel={this.toggleConfirmation}
              onConfirm={this.removeDispatcher}
            />
            <CreateDispatcherForm
              fec={fec}
              onSubmit={this.onSubmit.bind(this)}
              submitError={submitError}
              editMode={true}
              dispatcher={dispatcher}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminEditDispatcherPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
