import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
import FeathersClient from "../../../../src/api/FeathersClient";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import EditParticipantForm from "../../../../src/components/admin/users/participants/EditParticipantForm";
import AdminActions from "../../../../src/actions/AdminActions";
import FileUploadActions from "../../../../src/actions/FileUploadActions";
import ConfirmationPopup from "../../../../src/components/common/ConfirmationPopup";
import UIActions from "../../../../src/actions/UIActions";
import Pica from "pica/dist/pica";
const pica = Pica();

class AdminEditParticipantPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      participant: null,
      submitError: false,
      openConfirmation: false,
      popperPosition: null
    };

    this.toggleConfirmation = this.toggleConfirmation.bind(this);
    this.removeParticipant = props.removeObject.bind(
      this,
      {
        id: props.participantId,
        type: "participants"
      },
      (err, res) => {
        Router.push("/admin/users/participants");
      }
    );
  }

  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("participants")
        .get(this.props.participantId);
      this.setState({ participant: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    if (
      this.state.uploadingFile &&
      Object.keys(prevProps.uploading).length > 0 &&
      Object.keys(this.props.uploading).length === 0
    ) {
      //finished uploading
      this.props.showLoadingModal(false);
      Router.push(
        "/admin/users/participants/participant?id=" + this.props.participantId
      );
    }
  }

  static async getInitialProps({ query }) {
    const client = FeathersClient.getClient();
    const registrationParams = await client
      .service("public-methods")
      .create({ method: "get_registration_params" });
    return { participantId: query.id, registrationParams };
  }

  onSubmit(participant) {
    this.setState({ submitError: false });
    // need to modify the data a bit
    participant.clusterId = participant.isCluster ? participant.cluster : null;
    this.props.editParticipant(
      {
        participant,
        participantId: this.props.participantId
      },
      async (err, res) => {
        if (err) {
          console.log(err);
          this.setState({ submitError: true });
          return;
        }
        if (res.fileUploadToken) {
          // need to upload file
          const uploadFile = await pica.toBlob(
            participant.file,
            "image/jpeg",
            0.75
          );
          this.props.uploadFile(
            {
              token: this.props.token,
              type: "farmerId",
              typeId: res.id, //this doesn't matter, will be replaced
              file: uploadFile,
              uploadToken: res.fileUploadToken
            },
            res => {
              this.setState({ uploadingFile: true });
              this.props.showLoadingModal(true);
            }
          );
        } else {
          Router.push(
            "/admin/users/participants/participant?id=" +
              this.props.participantId
          );
        }
      }
    );
  }

  toggleConfirmation(evt) {
    this.setState({
      openConfirmation: !this.state.openConfirmation,
      popperPosition: evt.target
    });
  }

  render() {
    const { classes, participantId } = this.props;
    const {
      error,
      participant,
      submitError,
      openConfirmation,
      popperPosition
    } = this.state;
    return (
      <AppContainer
        title={`Editing Participant ${participant ? participant.name : ""}`}
        appType="admin"
        endComponent={
          <Button
            variant="outlined"
            className={classes.endComponentButton}
            onClick={this.toggleConfirmation}
          >
            Delete Participant
          </Button>
        }
      >
        {error ? (
          "No such participant"
        ) : participant ? (
          <React.Fragment>
            <ConfirmationPopup
              open={openConfirmation}
              anchorEl={popperPosition}
              onClickAway={this.toggleConfirmation}
              title="Delete Participant?"
              onCancel={this.toggleConfirmation}
              onConfirm={this.removeParticipant}
            />
            <EditParticipantForm
              onSubmit={this.onSubmit.bind(this)}
              submitError={submitError}
              registrationParams={this.props.registrationParams}
              participant={participant}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminEditParticipantPage);

function mapStateToProps(state) {
  const { account, fileupload } = state;
  return {
    token: account ? account.token : "",
    uploading: fileupload.uploading
  };
}

export default connect(
  mapStateToProps,
  Object.assign({}, AdminActions, FileUploadActions, UIActions)
)(component);
