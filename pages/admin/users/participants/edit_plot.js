import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import AdminActions from "../../../../src/actions/AdminActions";
import EditPlot from "../../../../src/components/admin/users/participants/EditPlot";
import ConfirmationPopup from "../../../../src/components/common/ConfirmationPopup";

class AdminEditFarmPlotPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      plot: null,
      fecArr: [],
      technicianArr: [],
      openConfirmation: false,
      popperPosition: null
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.toggleConfirmation = this.toggleConfirmation.bind(this);
    this.removePlot = this.removePlot.bind(this);
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("farm-plots").get(this.props.plotId);
      const fecArr = await client
        .service("fec")
        .find({ query: { $limit: "-1" } });
      const technicianArr = results.technician
        ? await client.service("technicians").find({
            query: { $limit: "-1", fecId: results.technician.supervisor.fecId }
          })
        : [];
      this.setState({ plot: results, fecArr, technicianArr });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    const client = FeathersClient.getClient();
    const registrationParams = await client
      .service("public-methods")
      .create({ method: "get_registration_params" });
    return { plotId: query.id, registrationParams };
  }

  removePlot() {
    this.props.removeObject(
      {
        id: this.props.plotId,
        type: "farm-plots"
      },
      (err, res) => {
        Router.push(
          `/admin/users/participants/participant?id=${
            this.state.plot.participantId
          }`
        );
      }
    );
  }

  toggleConfirmation(evt) {
    this.setState({
      openConfirmation: !this.state.openConfirmation,
      popperPosition: evt.target
    });
  }

  onSubmit(state) {
    const {
      id,
      barangay,
      inputTechnician,
      cropInsurance,
      cropInsuranceDetails,
      ...plot
    } = state;
    this.props.editFarmPlot(
      Object.assign(plot, {
        cropInsurance: cropInsurance ? cropInsuranceDetails : null,
        technicianId: inputTechnician,
        barangayId: barangay,
        plotId: id
      }),
      (err, res) => {
        if (err) {
          console.log(err);
          return;
        }
        Router.push(
          `/admin/users/participants/participant?id=${res.participantId}`
        );
      }
    );
  }

  render() {
    const { classes, plotId, registrationParams } = this.props;
    const {
      error,
      plot,
      fecArr,
      technicianArr,
      submitError,
      openConfirmation,
      popperPosition
    } = this.state;

    return (
      <AppContainer
        title={`Plot ${plot ? `${plot.code} (ID: ${plot.id})` : ""}`}
        appType="admin"
        endComponent={
          !error && (
            <Button
              variant="outlined"
              className={classes.endComponentButton}
              onClick={this.toggleConfirmation}
            >
              Delete Plot
            </Button>
          )
        }
      >
        {error ? (
          "No such Plot"
        ) : plot ? (
          <React.Fragment>
            <ConfirmationPopup
              open={openConfirmation}
              anchorEl={popperPosition}
              onClickAway={this.toggleConfirmation}
              title="Delete Plot?"
              onCancel={this.toggleConfirmation}
              onConfirm={this.removePlot}
            />
            <EditPlot
              plot={plot}
              onSubmit={this.onSubmit}
              registrationParams={registrationParams}
              fecArr={fecArr}
              technicianArr={technicianArr}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const AdminFarmPlotPageWithStyles = withStyles(styles)(AdminEditFarmPlotPage);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  AdminActions
)(AdminFarmPlotPageWithStyles);
