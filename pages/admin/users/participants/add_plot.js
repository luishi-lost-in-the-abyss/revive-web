import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid, CircularProgress } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import AdminActions from "../../../../src/actions/AdminActions";
import EditPlot from "../../../../src/components/admin/users/participants/EditPlot";
import AddPlot from "../../../../src/components/admin/users/participants/AddPlot";

class AdminAddFarmPlotPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, fecArr: [], technicianArr: [] };
    this.onSubmit = this.onSubmit.bind(this);
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      const fecArr = await client
        .service("fec")
        .find({ query: { $limit: "-1" } });

      this.setState({ fecArr });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    const client = FeathersClient.getClient();
    const registrationParams = await client
      .service("public-methods")
      .create({ method: "get_registration_params" });
    return { participantId: query.id, registrationParams };
  }

  onSubmit(state) {
    const {
      barangay,
      inputTechnician,
      cropInsurance,
      cropInsuranceDetails,
      ...plot
    } = state;
    this.props.addFarmPlot(
      Object.assign(plot, {
        cropInsurance: cropInsurance ? cropInsuranceDetails : null,
        technicianId: inputTechnician,
        participantId: this.props.participantId,
        barangayId: barangay
      }),
      (err, res) => {
        if (err) {
          console.log(err);
          return;
        }
        Router.push(
          `/admin/users/participants/participant?id=${this.props.participantId}`
        );
      }
    );
  }

  render() {
    const { classes, participantId, registrationParams } = this.props;
    const { error, fecArr, technicianArr } = this.state;

    return (
      <AppContainer title={`Add new Plot`} appType="admin">
        {fecArr.length === 0 ? (
          <CircularProgress />
        ) : (
          <AddPlot
            participantId={participantId}
            onSubmit={this.onSubmit}
            registrationParams={registrationParams}
            fecArr={fecArr}
            technicianArr={technicianArr}
          />
        )}
      </AppContainer>
    );
  }
}

const AdminFarmPlotPageWithStyles = withStyles(styles)(AdminAddFarmPlotPage);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  AdminActions
)(AdminFarmPlotPageWithStyles);
