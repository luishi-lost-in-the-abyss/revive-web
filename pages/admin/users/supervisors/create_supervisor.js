import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateSupervisorForm from "../../../../src/components/admin/users/supervisors/CreateSupervisorForm";
import AdminActions from "../../../../src/actions/AdminActions";
import FeathersClient from "../../../../src/api/FeathersClient";

class AdminCreateSupervisorPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fec: [],
      submitError: false
    };
  }

  async componentDidMount() {
    //fetch the fec
    const client = FeathersClient.getClient();
    const fec = await client.service("fec").find({ query: { $limit: "-1" } });
    this.setState({ fec });
  }

  onSubmit({ name, username, email, contact, password, fec }) {
    this.setState({ submitError: false });
    this.props.createSupervisor(
      { name, username, email, contact, password, fec },
      (err, res) => {
        if (err) {
          console.log(err);
          this.setState({ submitError: true });
          return;
        }
        Router.push("/admin/users/supervisors");
      }
    );
  }

  render() {
    const { classes } = this.props;
    const { fec, submitError } = this.state;

    return (
      <AppContainer title="Create New Supervisor" appType="admin">
        <CreateSupervisorForm
          fec={fec}
          onSubmit={this.onSubmit.bind(this)}
          submitError={submitError}
        />
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminCreateSupervisorPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
