import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import AdminActions from "../../../../src/actions/AdminActions";
import Supervisor from "../../../../src/components/admin/users/supervisors/Supervisor";

class AdminSupervisorPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, supervisor: null };
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("supervisors")
        .get(this.props.supervisorId);
      this.setState({ supervisor: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { supervisorId: query.id };
  }

  render() {
    const { classes, supervisorId } = this.props;
    const { error, supervisor } = this.state;

    return (
      <AppContainer
        title={`Supervisor ${supervisor ? supervisor.name : ""}`}
        appType="admin"
      >
        {error ? (
          "No such Supervisor"
        ) : supervisor ? (
          <React.Fragment>
            <div style={{ textAlign: "right" }}>
              <Link
                href={`/admin/users/supervisors/edit_supervisor?id=${supervisorId}`}
              >
                <Button
                  color="secondary"
                  variant="outlined"
                  style={{ marginTop: 20 }}
                >
                  Edit Supervisor
                </Button>
              </Link>
            </div>
            <Typography variant="h6">Details</Typography>
            <Supervisor supervisor={supervisor} />
            <Link passHref href="/admin/users/supervisors">
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const AdminSupervisorPageWithStyles = withStyles(styles)(AdminSupervisorPage);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  AdminActions
)(AdminSupervisorPageWithStyles);
