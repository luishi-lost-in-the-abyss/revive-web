import React from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import DataTable from "../../../../src/components/common/DataTable";

class AdminSupervisorsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    const mainContent = (
      <DataTable
        service="supervisors"
        dataCols={[
          { content: "ID", data: "id" },
          {
            content: "Username",
            data: null,
            render: data => data.user.username
          },
          { content: "Name", data: "name" },
          { content: "Contact", data: "contact" },
          { content: "Email", data: null, render: data => data.user.email },
          {
            content: "Actions",
            data: "id",
            render: id => {
              return (
                <React.Fragment>
                  <Link
                    passHref
                    href={`/admin/users/supervisors/supervisor?id=${id}`}
                  >
                    <Button variant="outlined">View</Button>
                  </Link>
                </React.Fragment>
              );
            }
          }
        ]}
        searchCategory={[
          { label: "Username", value: "username" },
          { label: "Name", value: "name" }
        ]}
        defaultSearchCategory="username"
      />
    );

    return (
      <AppContainer title="Supervisors" appType="admin">
        <Grid container style={{ marginBottom: 20 }} justify="flex-end">
          <Grid item xs={8} md={3}>
            <Link passHref href={`/admin/users/supervisors/create_supervisor`}>
              <Button color="primary" variant="outlined" fullWidth>
                Create New Supervisor
              </Button>
            </Link>
          </Grid>
        </Grid>

        {mainContent}
      </AppContainer>
    );
  }
}

export default withStyles(styles)(AdminSupervisorsPage);
