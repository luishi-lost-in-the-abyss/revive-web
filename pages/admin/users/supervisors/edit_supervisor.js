import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
import FeathersClient from "../../../../src/api/FeathersClient";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateSupervisorForm from "../../../../src/components/admin/users/supervisors/CreateSupervisorForm";
import AdminActions from "../../../../src/actions/AdminActions";
import ConfirmationPopup from "../../../../src/components/common/ConfirmationPopup";

class AdminEditSupervisorPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fec: [],
      supervisor: null,
      submitError: false,
      openConfirmation: false,
      popperPosition: null
    };

    this.toggleConfirmation = this.toggleConfirmation.bind(this);
    this.removeSupervisor = props.removeObject.bind(
      this,
      {
        id: props.supervisorId,
        type: "supervisors"
      },
      (err, res) => {
        Router.push("/admin/users/supervisors");
      }
    );
  }

  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("supervisors")
        .get(this.props.supervisorId);

      const fec = await client.service("fec").find({ query: { $limit: "-1" } });
      this.setState({ supervisor: results, fec });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { supervisorId: query.id };
  }

  onSubmit({ name, email, contact, password, fec }) {
    this.setState({ submitError: false });
    this.props.editSupervisor(
      {
        name,
        email,
        password,
        contact,
        fec,
        supervisorId: this.props.supervisorId
      },
      (err, res) => {
        if (err) {
          console.log(err);
          this.setState({ submitError: true });
          return;
        }
        Router.push(
          "/admin/users/supervisors/supervisor?id=" + this.props.supervisorId
        );
      }
    );
  }

  toggleConfirmation(evt) {
    this.setState({
      openConfirmation: !this.state.openConfirmation,
      popperPosition: evt.target
    });
  }

  render() {
    const { classes, supervisorId } = this.props;
    const {
      fec,
      error,
      supervisor,
      submitError,
      openConfirmation,
      popperPosition
    } = this.state;

    return (
      <AppContainer
        title={`Editing Supervisor ${supervisor ? supervisor.name : ""}`}
        appType="admin"
        endComponent={
          <Button
            variant="outlined"
            className={classes.endComponentButton}
            onClick={this.toggleConfirmation}
          >
            Delete Supervisor
          </Button>
        }
      >
        {error ? (
          "No such supervisor"
        ) : supervisor ? (
          <React.Fragment>
            <ConfirmationPopup
              open={openConfirmation}
              anchorEl={popperPosition}
              onClickAway={this.toggleConfirmation}
              title="Delete Supervisor? (All technicians under supervisor will be shifted to another supervisor under the same FEC. In the event there is no other supervisor, you cannot delete the supervisor)"
              onCancel={this.toggleConfirmation}
              onConfirm={this.removeSupervisor}
            />
            <CreateSupervisorForm
              fec={fec}
              onSubmit={this.onSubmit.bind(this)}
              submitError={submitError}
              editMode={true}
              supervisor={supervisor}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminEditSupervisorPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
