import {
  withStyles,
  Grid,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import { useState } from "react";
import ScheduleReport from "../../../../src/components/admin/reports/bookings/ScheduleReport";
import EquipmentAssetReport from "../../../../src/components/admin/reports/bookings/EquipmentAssetReport";

const reports = [
  { title: "Schedule (RAW)", key: "schedule" },
  { title: "Equipment Asset Bookings", key: "equipment-asset" }
];

const component = ({ classes }) => {
  const [nav, setNav] = useState("");

  const renderReportSection = () => {
    switch (nav) {
      case "schedule":
        return <ScheduleReport />;
      case "equipment-asset":
        return <EquipmentAssetReport />;
      default:
        return "Choose a category";
    }
  };
  return (
    <AppContainer title="Booking Reports" appType="admin">
      <Grid container spacing={16}>
        <Grid item xs={4}>
          <List className={classes.paperList}>
            {reports.map(({ title, key }) => (
              <ListItem
                button
                selected={key === nav}
                key={`listitem-${key}`}
                onClick={() => setNav(key)}
              >
                <ListItemText primary={title} />
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid item xs={8}>
          {renderReportSection()}
        </Grid>
      </Grid>
    </AppContainer>
  );
};

export default withStyles(styles)(component);
