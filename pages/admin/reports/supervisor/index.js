import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Grid, Typography } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import {
  VictoryChart,
  VictoryLine,
  VictoryTheme,
  VictoryZoomContainer,
  VictoryLegend,
  VictoryBar,
  VictoryGroup,
  VictoryTooltip,
  VictoryLabel
} from "victory";
import FeathersClient from "../../../../src/api/FeathersClient";

class AdminSupervisorReportPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData: null,
      numOfPlots: 0
    };
  }

  async loadData() {
    const client = FeathersClient.getClient();
    let supervisors = [],
      technicians = [],
      plots = [],
      temp;

    do {
      temp = await client.service("technicians").find({
        query: {
          $skip: technicians.length
        }
      });
      technicians = [...technicians, ...temp.data];
    } while (technicians.length !== temp.total);
    do {
      temp = await client.service("supervisors").find({
        query: {
          $skip: supervisors.length
        }
      });
      supervisors = [...supervisors, ...temp.data];
    } while (supervisors.length !== temp.total);
    do {
      temp = await client.service("farm-plots").find({
        query: {
          $skip: plots.length
        }
      });
      plots = [...plots, ...temp.data];
    } while (plots.length !== temp.total);

    const chartData = {};
    const techSupervisorMap = {};
    supervisors.map(sup => {
      chartData[sup.id] = {
        name: sup.name,
        technicians: 0,
        area: 0
      };
    });
    technicians.map(tech => {
      techSupervisorMap[tech.id] = tech.supervisorId;
      chartData[tech.supervisorId].technicians += 1;
    });
    plots.map(plot => {
      const { technicianId, plantableArea } = plot;
      if (!technicianId) return;
      const supervisorId = techSupervisorMap[technicianId];
      chartData[supervisorId].area += parseFloat(plantableArea);
    });
    this.setState({ chartData });
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    const { classes } = this.props;
    const { chartData } = this.state;

    const noOfSup = chartData ? Object.keys(chartData).length : 0;
    return (
      <AppContainer title="Dashboard" appType="admin">
        {!chartData && "Fetching Data..."}
        <Grid container>
          <Grid item xs={12} md={6}>
            {chartData && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      noOfSup > 5 ? { x: [noOfSup - 3, noOfSup + 2] } : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  x={150}
                  centerTitle
                  title="AREA (HA) PER SUPERVISOR"
                  data={[]}
                />
                <VictoryBar
                  barWidth={25}
                  labelComponent={<VictoryTooltip />}
                  data={Object.keys(chartData).map(id => {
                    const supervisor = chartData[id];
                    return {
                      x: `${supervisor.name}`,
                      y: supervisor.area,
                      label: supervisor.area.toFixed(0)
                    };
                  })}
                />
              </VictoryChart>
            )}
          </Grid>
          <Grid item xs={12} md={6}>
            {chartData && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      noOfSup > 5 ? { x: [noOfSup - 3, noOfSup + 2] } : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  x={150}
                  centerTitle
                  orientation="horizontal"
                  title="TECHNICIANS PER SUPERVISOR"
                  data={[]}
                />
                <VictoryBar
                  labelComponent={<VictoryTooltip />}
                  data={Object.keys(chartData).map(id => {
                    const supervisor = chartData[id];
                    return {
                      x: `${supervisor.name}`,
                      y: supervisor.technicians,
                      label: supervisor.technicians
                    };
                  })}
                />
              </VictoryChart>
            )}
          </Grid>
        </Grid>
      </AppContainer>
    );
  }
}

export default withStyles(styles)(AdminSupervisorReportPage);
