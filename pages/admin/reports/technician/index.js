import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Grid, Typography } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import {
  VictoryChart,
  VictoryLine,
  VictoryTheme,
  VictoryZoomContainer,
  VictoryLegend,
  VictoryBar,
  VictoryGroup,
  VictoryTooltip,
  VictoryLabel
} from "victory";
import FeathersClient from "../../../../src/api/FeathersClient";

class AdminTechnicianReportPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData: null,
      numOfPlots: 0
    };
  }

  async loadData() {
    const client = FeathersClient.getClient();
    let technicians = [],
      plots = [],
      temp;

    do {
      temp = await client.service("technicians").find({
        query: {
          $skip: technicians.length
        }
      });
      technicians = [...technicians, ...temp.data];
    } while (technicians.length !== temp.total);
    do {
      temp = await client.service("farm-plots").find({
        query: {
          $skip: plots.length
        }
      });
      plots = [...plots, ...temp.data];
    } while (plots.length !== temp.total);

    const chartData = {};
    technicians.map(tech => {
      chartData[tech.id] = {
        id: tech.id,
        name: tech.name,
        area: 0,
        plots: 0,
        farmers: new Set()
      };
    });
    plots.map(plot => {
      const { participantId, technicianId, plantableArea } = plot;
      if (!technicianId) return;
      chartData[technicianId].area += parseFloat(plantableArea);
      chartData[technicianId].plots += 1;
      chartData[technicianId].farmers.add(participantId);
    });
    this.setState({ chartData });
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    const { classes } = this.props;
    const { chartData } = this.state;

    const noOfTech = chartData ? Object.keys(chartData).length : 0;
    return (
      <AppContainer title="Dashboard" appType="admin">
        {!chartData && "Fetching Data..."}
        <Grid container>
          <Grid item xs={12} md={6}>
            {chartData && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      noOfTech > 5 ? { x: [noOfTech - 3, noOfTech + 2] } : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  x={150}
                  centerTitle
                  title="AREA (HA) PER TECHNICIAN"
                  data={[]}
                />
                <VictoryBar
                  barWidth={25}
                  labelComponent={<VictoryTooltip />}
                  data={Object.keys(chartData).map(id => {
                    const tech = chartData[id];
                    return {
                      x: `${tech.name}`,
                      y: tech.area,
                      label: tech.area.toFixed(0)
                    };
                  })}
                />
              </VictoryChart>
            )}
          </Grid>
          <Grid item xs={12} md={6}>
            {chartData && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      noOfTech > 5 ? { x: [noOfTech - 3, noOfTech + 2] } : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  x={150}
                  centerTitle
                  orientation="horizontal"
                  title="FARMER-PLOT PER TECHNICIAN"
                  data={[
                    {
                      name: "Plots",
                      symbol: { fill: "#1b5e20", type: "square" }
                    },
                    {
                      name: "Farmers",
                      symbol: { fill: "#795548", type: "square" }
                    }
                  ]}
                />
                <VictoryGroup offset={25}>
                  <VictoryBar
                    labelComponent={<VictoryTooltip />}
                    style={{ data: { fill: "#1b5e20" } }}
                    data={Object.keys(chartData).map(id => {
                      const tech = chartData[id];
                      return {
                        x: `${tech.name}`,
                        y: tech.plots,
                        label: tech.plots
                      };
                    })}
                  />
                  <VictoryBar
                    labelComponent={<VictoryTooltip />}
                    style={{ data: { fill: "#795548" } }}
                    data={Object.keys(chartData).map(id => {
                      const tech = chartData[id];
                      return {
                        x: `${tech.name}`,
                        y: tech.farmers.size,
                        label: tech.farmers.size
                      };
                    })}
                  />
                </VictoryGroup>
              </VictoryChart>
            )}
          </Grid>
        </Grid>
      </AppContainer>
    );
  }
}

export default withStyles(styles)(AdminTechnicianReportPage);
