import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import { TextField, MenuItem, Grid, Typography } from "@material-ui/core";
import {
  VictoryChart,
  VictoryLine,
  VictoryTheme,
  VictoryZoomContainer,
  VictoryLegend,
  VictoryBar,
  VictoryTooltip,
  VictoryAxis,
  VictoryGroup
} from "victory";
import { blue, red, teal, green, yellow } from "@material-ui/core/colors";
import { DateTime } from "luxon";
import { engagementObj } from "../../../../src/api/utility";
/*
{ label: "Corn", value: "corn" },
{ label: "Corn Silage", value: "corn_silage" },
{ label: "Rice", value: "rice" },
{ label: "Napier", value: "napier" },
{ label: "Others", value: "others" }
*/

const cancelReasonCounts = {
  "Bad Weather": 0,
  "Under Repair": 0,
  "Permits and Licensed Concern": 0,
  Reschedule: 0,
  Others: 0
};

class AdminViewFecReportPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fec: null,
      fecs: {},
      colors: {
        corn: blue[400],
        corn_silage: red[400],
        rice: teal[400],
        napier: green[400],
        others: yellow[400]
      },
      booking: null,
      bookings: {},
      minMax: { min: 0, max: 5 },
      error: false
    };
  }

  async loadAllFecData() {
    const client = FeathersClient.getClient();
    let plots = [],
      fecs = [],
      fecObj = {},
      bookings = [],
      bookingsObj = {},
      temp;
    do {
      temp = await client.service("fec").find({});
      fecs = [...fecs, ...temp.data];
    } while (fecs.length !== temp.total);
    fecs.map(({ id, name }) => {
      const cropType = {
        corn: 0,
        corn_silage: 0,
        rice: 0,
        napier: 0,
        others: 0
      };
      fecObj[id] = Object.assign(
        {
          name,
          id,
          participantType: {
            lmf: Object.assign({}, cropType),
            fss: Object.assign({}, cropType)
          },
          engagementScheme: {
            directBuying: Object.assign({}, cropType),
            growership: Object.assign({}, cropType),
            financing: Object.assign({}, cropType),
            inhouse: Object.assign({}, cropType),
            lease: Object.assign({}, cropType)
          }
        },
        cropType
      );
      bookingsObj[id] = { id, name, cancel: {} };
    });
    do {
      temp = await client.service("farm-plots").find({
        query: {
          $skip: plots.length
        }
      });
      plots = [...plots, ...temp.data];
    } while (plots.length !== temp.total);

    plots.map(plot => {
      const { engagementScheme, plantableArea, commodity, technician } = plot;
      const fecId =
        technician && technician.supervisor
          ? technician.supervisor.fecId
          : null;
      if (!fecId) return;
      const area = parseFloat(plantableArea);
      const commodity_ = fecObj[fecId][commodity] >= 0 ? commodity : "others";
      fecObj[fecId][commodity_] += area;
      fecObj[fecId].participantType[engagementObj[engagementScheme].type][
        commodity_
      ] += area;
      fecObj[fecId].engagementScheme[engagementScheme][commodity_] += area;
    });

    const currentDate = DateTime.local();
    do {
      temp = await client.service("bookings").find({
        query: {
          $skip: bookings.length,
          $or: [
            {
              $and: [
                { actualEndDate: null },
                { endDate: { $gte: currentDate.startOf("year").toJSDate() } },
                { endDate: { $lte: currentDate.endOf("year").toJSDate() } }
              ]
            },
            {
              $and: [
                {
                  actualEndDate: {
                    $gte: currentDate.startOf("year").toJSDate()
                  }
                },
                {
                  actualEndDate: { $lte: currentDate.endOf("year").toJSDate() }
                }
              ]
            }
          ],
          status: { $or: ["cancel"] }
        }
      });
      bookings = [...bookings, ...temp.data];
    } while (bookings.length !== temp.total);

    const minMax = { min: 99, max: 0 };

    bookings.map(({ dataSnapshot, statusReason, endDate }) => {
      const { booking } = dataSnapshot;
      if (!booking.equipment_assets || booking.equipment_assets.length === 0)
        return;
      const fecId = booking.equipment_assets[0].fecId;
      const eDate = DateTime.fromJSDate(new Date(endDate));
      let reason;
      switch (statusReason) {
        case "Bad Weather":
        case "Under Repair":
        case "Permits and Licensed Concern":
        case "Reschedule":
          reason = statusReason;
          break;
        default:
          reason = "Others";
          break;
      }
      const month = eDate.month;
      if (!bookingsObj[fecId].cancel[month]) {
        bookingsObj[fecId].cancel[month] = Object.assign(
          {},
          cancelReasonCounts
        );
      }
      bookingsObj[fecId].cancel[month][reason] += 1;

      if (month > minMax.max) {
        minMax.max = month;
      }
      if (month < minMax.min) {
        minMax.min = month;
      }
    });

    Object.keys(bookingsObj).map(fec => {
      const obj = bookingsObj[fec].cancel;
      for (let i = minMax.min; i <= minMax.max; i++) {
        if (!obj[i]) {
          obj[i] = Object.assign({}, cancelReasonCounts);
        }
      }
    });

    const state = { fecs: fecObj, bookings: bookingsObj, minMax };
    if (this.props.fecId) {
      state.fec = fecObj[this.props.fecId];
      state.booking = bookingsObj[this.props.fecId];
    }

    this.setState(state);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.fecId && prevProps.fecId !== this.props.fecId) {
      if (this.state.fecs[this.props.fecId]) {
        this.setState({
          fec: this.state.fecs[this.props.fecId],
          booking: this.state.bookings[this.props.fecId]
        });
      } else {
        this.setState({ error: true });
      }
    }
  }

  async componentDidMount() {
    this.loadAllFecData();
  }

  static async getInitialProps({ query }) {
    return { fecId: query.id };
  }

  render() {
    const { classes } = this.props;
    const { error, fec, fecs, booking, bookings, colors, minMax } = this.state;

    let cropOrder = fec
      ? Object.keys(fec.engagementScheme.directBuying)
          .sort()
          .filter(crop => crop !== "corn_silage" && crop !== "others")
      : [];
    const noOfData = minMax.max - minMax.min + 2;

    return (
      <AppContainer title={`FEC Report ${fec ? fec.name : ""}`} appType="admin">
        <TextField
          fullWidth
          label="Select a FEC"
          variant="outlined"
          select
          value={fec ? fec.id : ""}
          onChange={evt => {
            this.setState({
              fec: fecs[evt.target.value],
              booking: bookings[evt.target.value]
            });
          }}
        >
          {Object.keys(fecs).map(id => {
            const fec = fecs[id];
            return (
              <MenuItem key={`select-${id}`} value={id}>
                {fec.name}
              </MenuItem>
            );
          })}
        </TextField>
        {fec && (
          <div>
            <Grid container>
              <Grid item xs={12}>
                <div style={{ display: "flex", marginBottom: 50 }}>
                  <Typography variant="h6" style={{ marginRight: 20 }}>
                    Corn: {parseFloat(fec.corn).toFixed(0)}
                  </Typography>
                  {/* 
                  <Typography variant="h6" style={{ marginRight: 20 }}>
                    Corn Silage: {fec.corn_silage}
                  </Typography>
                  */}
                  <Typography variant="h6" style={{ marginRight: 20 }}>
                    Rice: {parseFloat(fec.rice).toFixed(0)}
                  </Typography>
                  <Typography variant="h6" style={{ marginRight: 20 }}>
                    Napier: {parseFloat(fec.napier).toFixed(0)}
                  </Typography>
                  {/* 
                  <Typography variant="h6">Others: {fec.others}</Typography>
                  */}
                </div>
              </Grid>
              <Grid item xs={12} md={6}>
                <VictoryChart
                  domainPadding={{ x: 40, y: 20 }}
                  width={500}
                  theme={VictoryTheme.material}
                  containerComponent={
                    <VictoryZoomContainer zoomDimension="x" allowZoom={false} />
                  }
                >
                  <VictoryLegend
                    title={`FEC ${fec.name}: PARTICIPANT TYPE`}
                    x={150}
                    colorScale="heatmap"
                    orientation="horizontal"
                    itemsPerRow={5}
                    centerTitle
                    style={{
                      title: { fontSize: 12 },
                      labels: { fontSize: 12 }
                    }}
                    data={cropOrder.map(crop => ({
                      name: crop,
                      symbol: { fill: colors[crop] }
                    }))}
                  />
                  <VictoryGroup offset={30} colorScale="heatmap">
                    {cropOrder.map(crop => {
                      const lmfCrop = fec.participantType.lmf[crop];
                      const fssCrop = fec.participantType.fss[crop];
                      return (
                        <VictoryBar
                          key={`participantType-${crop}`}
                          barWidth={30}
                          labelComponent={<VictoryTooltip />}
                          style={{ data: { fill: colors[crop] } }}
                          data={[
                            {
                              x: "LMF",
                              y: lmfCrop,
                              label: parseFloat(lmfCrop).toFixed(0)
                            },
                            {
                              x: "FSS",
                              y: fssCrop,
                              label: parseFloat(fssCrop).toFixed(0)
                            }
                          ]}
                        />
                      );
                    })}
                  </VictoryGroup>
                </VictoryChart>
              </Grid>
              <Grid item xs={12} md={6}>
                <VictoryChart
                  domainPadding={{ x: 40, y: 20 }}
                  width={500}
                  theme={VictoryTheme.material}
                  containerComponent={
                    <VictoryZoomContainer zoomDimension="x" allowZoom={false} />
                  }
                >
                  <VictoryLegend
                    title={`FEC ${fec.name}: ENGAGEMENT SCHEME`}
                    x={150}
                    orientation="horizontal"
                    itemsPerRow={5}
                    centerTitle
                    colorScale="heatmap"
                    style={{
                      title: { fontSize: 12 },
                      labels: { fontSize: 12 }
                    }}
                    data={cropOrder.map(crop => ({
                      name: crop,
                      symbol: { fill: colors[crop] }
                    }))}
                  />
                  <VictoryGroup offset={15} colorScale="heatmap">
                    {cropOrder.map(crop => {
                      const directBuying =
                        fec.engagementScheme.directBuying[crop];
                      const growership = fec.engagementScheme.growership[crop];
                      const financing = fec.engagementScheme.financing[crop];
                      const inhouse = fec.engagementScheme.inhouse[crop];
                      const lease = fec.engagementScheme.lease[crop];
                      return (
                        <VictoryBar
                          key={`engagementScheme-${crop}`}
                          barWidth={15}
                          labelComponent={<VictoryTooltip />}
                          style={{ data: { fill: colors[crop] } }}
                          data={[
                            {
                              x: "Direct Buying",
                              y: directBuying,
                              label: parseFloat(directBuying).toFixed(0)
                            },
                            {
                              x: "Growership",
                              y: growership,
                              label: parseFloat(growership).toFixed(0)
                            },
                            {
                              x: "Financing",
                              y: financing,
                              label: parseFloat(financing).toFixed(0)
                            },
                            {
                              x: "In-House",
                              y: inhouse,
                              label: parseFloat(inhouse).toFixed(0)
                            },
                            {
                              x: "Lease",
                              y: lease,
                              label: parseFloat(lease).toFixed(0)
                            }
                          ]}
                        />
                      );
                    })}
                  </VictoryGroup>
                </VictoryChart>
              </Grid>
              <Grid item xs={12} md={6}>
                <VictoryChart
                  domainPadding={{ x: 40, y: 30 }}
                  width={500}
                  theme={VictoryTheme.material}
                  containerComponent={
                    <VictoryZoomContainer
                      zoomDimension="x"
                      allowZoom={false}
                      zoomDomain={
                        minMax.max - minMax.min > 5
                          ? { x: [noOfData - 5, noOfData] }
                          : {}
                      }
                    />
                  }
                >
                  <VictoryLegend
                    title={`FEC ${fec.name}: CANCEL TRANSACTION`}
                    colorScale="heatmap"
                    x={50}
                    orientation="horizontal"
                    itemsPerRow={3}
                    centerTitle
                    style={{
                      title: { fontSize: 12 },
                      labels: { fontSize: 12 }
                    }}
                    data={Object.keys(cancelReasonCounts).map(reason => ({
                      name: reason
                    }))}
                  />
                  <VictoryGroup offset={15} colorScale="heatmap">
                    {Object.keys(cancelReasonCounts).map(reason => {
                      return (
                        <VictoryBar
                          key={`canceltransaction-${reason}`}
                          barWidth={15}
                          labelComponent={<VictoryTooltip />}
                          data={Object.keys(booking.cancel).map(month => ({
                            x: DateTime.fromObject({ month }).monthShort,
                            y: booking.cancel[month][reason],
                            label: booking.cancel[month][reason]
                          }))}
                        />
                      );
                    })}
                  </VictoryGroup>
                </VictoryChart>
              </Grid>
            </Grid>
          </div>
        )}
      </AppContainer>
    );
  }
}

const AdminViewFecReportPageWithStyles = withStyles(styles)(
  AdminViewFecReportPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  {}
)(AdminViewFecReportPageWithStyles);
