import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import { TextField, MenuItem, Grid, Typography } from "@material-ui/core";
import {
  VictoryChart,
  VictoryLine,
  VictoryTheme,
  VictoryZoomContainer,
  VictoryLegend,
  VictoryBar,
  VictoryTooltip,
  VictoryAxis,
  VictoryGroup
} from "victory";
import { engagementObj } from "../../../../src/api/utility";
import { blue, red, teal, green, yellow } from "@material-ui/core/colors";

const cropType = {
  corn: 0,
  corn_silage: 0,
  rice: 0,
  napier: 0,
  others: 0
};

class AdminViewMunicipalityReportPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      provinces: {},
      provinceObj: {},
      province: null,
      colors: {
        corn: blue[400],
        corn_silage: red[400],
        rice: teal[400],
        napier: green[400],
        others: yellow[400]
      },
      error: false
    };
  }

  async loadAllPlotsData() {
    const client = FeathersClient.getClient();
    let plots = [],
      temp;
    do {
      temp = await client.service("farm-plots").find({
        query: {
          $skip: plots.length
        }
      });
      plots = [...plots, ...temp.data];
    } while (plots.length !== temp.total);

    const provinces = {};
    const provinceObj = {};
    plots.map(plot => {
      const { engagementScheme, commodity, plantableArea, barangay } = plot;
      const municipalId = barangay.municipality.id;
      const provinceId = barangay.municipality.province.id;

      let municipals = provinces[provinceId];
      if (!municipals) {
        provinceObj[provinceId] = barangay.municipality.province.name;
        provinces[provinceId] = {};
        municipals = provinces[provinceId];
      }
      let obj = municipals[municipalId];
      if (!obj) {
        municipals[municipalId] = Object.assign(
          { name: barangay.municipality.name },
          cropType
        );
        Object.keys(engagementObj).map(scheme => {
          municipals[municipalId][scheme] = 0;
        });
        obj = municipals[municipalId];
      }
      const area = parseFloat(plantableArea);
      const commodity_ = obj[commodity] >= 0 ? commodity : "others";
      obj[engagementScheme] += area;
      obj[commodity_] += area;
    });
    const state = { provinces, provinceObj };
    this.setState(state);
  }

  async componentDidMount() {
    this.loadAllPlotsData();
  }

  static async getInitialProps({ query }) {
    return { municipalityId: query.id };
  }

  render() {
    const { classes } = this.props;
    const { error, colors, provinces, province, provinceObj } = this.state;
    const municipals = province ? provinces[province] : {};
    let noOfMunicipals = Object.keys(municipals).length;

    return (
      <AppContainer title={`Municipality Report`} appType="admin">
        <TextField
          fullWidth
          label="Select a Province"
          variant="outlined"
          select
          value={province ? province : ""}
          onChange={evt => {
            //we need to set to null first to reset the entire chart else chart won't render properly
            this.setState({ province: null }, () => {
              this.setState({ province: evt.target.value });
            });
          }}
        >
          {Object.keys(provinceObj).map(id => {
            return (
              <MenuItem key={`select-${id}`} value={id}>
                {provinceObj[id]}
              </MenuItem>
            );
          })}
        </TextField>
        {province && (
          <Grid container>
            <Grid item xs={12} md={6}>
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      noOfMunicipals > 5
                        ? {
                            x: [noOfMunicipals - 3, noOfMunicipals + 2]
                          }
                        : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  colorScale="heatmap"
                  title={`ENGAGEMENT SCHEME PER LOCATION`}
                  x={100}
                  orientation="horizontal"
                  itemsPerRow={3}
                  centerTitle
                  style={{ title: { fontSize: 12 }, labels: { fontSize: 12 } }}
                  colorScale="heatmap"
                  data={Object.keys(engagementObj).map(scheme => {
                    return {
                      name: engagementObj[scheme].label,
                      symbol: { fill: engagementObj[scheme].color }
                    };
                  })}
                />
                <VictoryGroup offset={15} colorScale="heatmap">
                  {Object.keys(engagementObj).map(scheme => {
                    return (
                      <VictoryBar
                        barWidth={15}
                        style={{ data: { fill: engagementObj[scheme].color } }}
                        labelComponent={<VictoryTooltip />}
                        data={Object.keys(municipals).map(id => {
                          const municipal = municipals[id];
                          return {
                            x: municipal.name,
                            y: municipal[scheme],
                            label: parseFloat(municipal[scheme]).toFixed(0)
                          };
                        })}
                      />
                    );
                  })}
                </VictoryGroup>
              </VictoryChart>
            </Grid>
            <Grid item xs={12} md={6}>
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      noOfMunicipals > 5
                        ? {
                            x: [noOfMunicipals - 3, noOfMunicipals + 2]
                          }
                        : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  title={`COMMODITY PER LOCATION`}
                  x={175}
                  orientation="horizontal"
                  itemsPerRow={3}
                  centerTitle
                  style={{ title: { fontSize: 12 }, labels: { fontSize: 12 } }}
                  colorScale="heatmap"
                  data={Object.keys(cropType)
                    .filter(crop => crop !== "corn_silage" && crop !== "others")
                    .map(crop => {
                      return { name: crop, symbol: { fill: colors[crop] } };
                    })}
                />
                <VictoryGroup offset={15} colorScale="heatmap">
                  {Object.keys(cropType)
                    .filter(crop => crop !== "corn_silage" && crop !== "others")
                    .map(crop => {
                      return (
                        <VictoryBar
                          barWidth={15}
                          style={{ data: { fill: colors[crop] } }}
                          labelComponent={<VictoryTooltip />}
                          data={Object.keys(municipals).map(id => {
                            const municipal = municipals[id];
                            return {
                              x: municipal.name,
                              y: municipal[crop],
                              label: parseFloat(municipal[crop]).toFixed(0)
                            };
                          })}
                        />
                      );
                    })}
                </VictoryGroup>
              </VictoryChart>
            </Grid>
          </Grid>
        )}
      </AppContainer>
    );
  }
}

const AdminViewMunicipalityReportPageWithStyles = withStyles(styles)(
  AdminViewMunicipalityReportPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  {}
)(AdminViewMunicipalityReportPageWithStyles);
