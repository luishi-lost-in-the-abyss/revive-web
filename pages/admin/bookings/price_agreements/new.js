import React from "react";
import Router from "next/router";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import AdminActions from "../../../../src/actions/AdminActions";
import CreatePriceAgreementForm from "../../../../src/components/admin/bookings/priceAgreements/CreatePriceAgreementForm";

class AdminCreatePriceAgreement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submitError: false
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  async componentDidMount() {}

  onSubmit({ bookingId, deliveredPrice, truckingPrice, deliveryMode }) {
    this.props.createPriceAgreement(
      {
        bookingId,
        deliveredPrice,
        truckingPrice,
        deliveryMode
      },
      (err, res) => {
        if (err) return;
        Router.push("/admin/bookings/price_agreements");
      }
    );
    this.setState({ submitError: false });
  }

  render() {
    const { classes } = this.props;

    return (
      <AppContainer title="Create New Price Agreement" appType="admin">
        <CreatePriceAgreementForm onSubmit={this.onSubmit} />
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminCreatePriceAgreement);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
