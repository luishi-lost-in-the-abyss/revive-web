import React from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import PriceAgreement from "../../../../src/components/admin/bookings/priceAgreements/PriceAgreement";
import { Button, CircularProgress } from "@material-ui/core";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

class AdminViewPriceAgreementPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      agreement: null,
      downloading: false
    };

    this.download = this.download.bind(this);
  }

  async getPriceAgreement() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("price-agreements")
        .get(this.props.agreementId);

      this.setState({
        agreement: Object.assign({}, results)
      });
    } catch (err) {
      this.setState({ error: true });
    }
  }
  async componentDidMount() {
    await this.getPriceAgreement();
  }

  static async getInitialProps({ query }) {
    return { agreementId: query.id };
  }

  async download() {
    this.setState({ downloading: true });
    const client = FeathersClient.getClient();
    const { dd: definition } = await client
      .service("generate-document-methods")
      .create({
        method: "generate_price_agreement",
        query: { agreementId: this.props.agreementId }
      });
    pdfMake
      .createPdf(definition)
      .download(`PRICE-AGREEMENT-${this.props.agreementId}.pdf`);
    this.setState({ downloading: false });
  }

  render() {
    const { classes, agreementId } = this.props;
    const { agreement, downloading, error } = this.state;

    return (
      <AppContainer
        title={`Price Agreement ${agreement ? agreement.id : ""}`}
        appType="admin"
      >
        {error ? (
          "No such price agreement"
        ) : agreement ? (
          <React.Fragment>
            <div style={{ textAlign: "right" }}>
              <Button
                disabled={downloading}
                color="primary"
                variant="outlined"
                style={{ marginRight: 10 }}
                onClick={this.download}
              >
                {downloading ? <CircularProgress size={12} /> : "Download"}
              </Button>
            </div>
            <PriceAgreement agreement={agreement} />
            <Link passHref href="/admin/bookings/price_agreements">
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const AdminViewPriceAgreementPageWithStyles = withStyles(styles)(
  AdminViewPriceAgreementPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  {}
)(AdminViewPriceAgreementPageWithStyles);
