import React from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import DataTable from "../../../../src/components/common/DataTable";
import { extractLocationDetails } from "../../../../src/api/utility";

class AdminPriceAgreements extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;
    return (
      <AppContainer title="Price Agreements" appType="admin">
        <Grid container style={{ marginBottom: 20 }} justify="flex-end">
          <Grid item xs={8} md={3}>
            <Link passHref href={`/admin/bookings/price_agreements/new`}>
              <Button color="primary" variant="outlined" fullWidth>
                Create New Agreement
              </Button>
            </Link>
          </Grid>
        </Grid>
        <DataTable
          service="price-agreements"
          dataCols={[
            { content: "ID", data: "id" },
            { content: "Booking ID", data: "bookingId" },
            {
              content: "Farmer Name",
              data: "booking",
              render: booking => {
                const participant = booking.dataSnapshot.participants[0];

                return `${participant.firstName} ${participant.middleName} ${
                  participant.lastName
                }`;
              }
            },
            {
              content: "Delivered Price",
              data: "deliveredPrice"
            },
            {
              content: "Trucking Price",
              data: "truckingPrice"
            },
            {
              content: "Commodity",
              data: "booking",
              render: booking =>
                booking.dataSnapshot.booking.farm_plot.commodity
            },
            {
              content: "Farm Plot Address",
              data: "booking",
              render: booking => {
                const {
                  barangay,
                  municipality,
                  province,
                  region,
                  country
                } = extractLocationDetails(
                  booking.dataSnapshot.booking.farm_plot.barangay
                );
                return `${country.name} ${region.name} ${province.name} ${
                  municipality.name
                } ${barangay.name} ${
                  booking.dataSnapshot.booking.farm_plot.purok
                }`;
              }
            },
            {
              content: "Actions",
              data: "id",
              render: id => {
                return (
                  <Link
                    passHref
                    href={`/admin/bookings/price_agreements/view?id=${id}`}
                  >
                    <Button variant="outlined">View</Button>
                  </Link>
                );
              }
            }
          ]}
          searchCategory={[{ label: "ID", value: "id" }]}
          defaultSearchCategory="id"
        />
      </AppContainer>
    );
  }
}

export default withStyles(styles)(AdminPriceAgreements);
