import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import { Typography, Button } from "@material-ui/core";
import AppContainer from "../../../src/components/common/AppContainer";
import FeathersClient from "../../../src/api/FeathersClient";
import VerticalTable from "../../../src/components/common/VerticalTable";
import { DateTime } from "luxon";
import AdminActions from "../../../src/actions/AdminActions";
import ReallocateAsset from "../../../src/components/admin/bookings/ReallocateAsset";

class AdminReallocateBookingPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      assets: {},
      booking: null
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  async getBooking() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("bookings").get(this.props.bookingId);
      const {
        dataSnapshot,
        status,
        statusReason,
        equipment_assets,
        startDate,
        endDate
      } = results;
      const { booking } = dataSnapshot;
      let fecId;
      const equipmentIds = equipment_assets.map(
        ({ equipmentId }) => equipmentId
      );
      if (equipment_assets[0]) {
        fecId = equipment_assets[0].fecId;
      }
      let assets = await client.service("booking-methods").create({
        method: "get_available_assets",
        query: { equipmentIds, fecId, startDate, endDate }
      });
      equipment_assets.map(({ equipmentId, serial }) => {
        assets[equipmentId].push(serial);
      });
      this.setState({
        booking: Object.assign({}, booking, { status, statusReason }),
        assets
      });
    } catch (err) {
      this.setState({ error: true });
    }
  }
  async componentDidMount() {
    await this.getBooking();
  }

  onSubmit(assets) {
    const { booking } = this.state;
    const bookingAssets = [];
    booking.equipment_assets.map(({ equipmentId }) => {
      bookingAssets.push(assets[equipmentId]);
    });
    this.props.editBookingAssets(
      {
        bookingId: booking.id,
        assets: bookingAssets
      },
      (err, res) => {
        if (err) return;
        Router.push(`/admin/bookings/booking?id=${booking.id}`);
      }
    );
  }

  static async getInitialProps({ query }) {
    return { bookingId: query.id };
  }

  render() {
    const { classes, bookingId } = this.props;
    const { error, booking, assets } = this.state;

    return (
      <AppContainer
        title={`Reallocate Assets for Booking ${booking ? booking.id : ""}`}
        appType="admin"
      >
        {error ? (
          "No such booking"
        ) : booking ? (
          <React.Fragment>
            <ReallocateAsset
              role="admin"
              booking={booking}
              assets={assets}
              onSubmit={this.onSubmit}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const AdminReallocateBookingPageWithStyles = withStyles(styles)(
  AdminReallocateBookingPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  Object.assign({}, AdminActions)
)(AdminReallocateBookingPageWithStyles);
