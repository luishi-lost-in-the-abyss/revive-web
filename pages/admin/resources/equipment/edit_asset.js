import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import AdminActions from "../../../../src/actions/AdminActions";
import CreateAssetForm from "../../../../src/components/admin/resources/equipment/CreateAssetForm";
import ConfirmationPopup from "../../../../src/components/common/ConfirmationPopup";

class AdminEditAssetPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      asset: null,
      openConfirmation: false,
      popperPosition: null
    };

    this.toggleConfirmation = this.toggleConfirmation.bind(this);
    this.removeAsset = this.removeAsset.bind(this);
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("equipment-assets")
        .get(this.props.assetId);
      this.setState({ asset: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { assetId: query.id };
  }

  onSubmit({ serial, fecId }) {
    this.props.editEquipmentAsset({ serial, fecId }, (err, res) => {
      if (err) {
        console.log(err);
        this.setState({ error: true });
        return;
      }
      Router.push(`/admin/resources/equipment/asset?id=${this.props.assetId}`);
    });
  }

  removeAsset() {
    this.props.removeObject(
      {
        id: this.props.assetId,
        type: "equipment-assets"
      },
      (err, res) => {
        Router.push(
          "/admin/resources/equipment/equipment?id=" +
            this.state.asset.equipmentId
        );
      }
    );
  }

  toggleConfirmation(evt) {
    this.setState({
      openConfirmation: !this.state.openConfirmation,
      popperPosition: evt.target
    });
  }

  render() {
    const { classes, assetId } = this.props;
    const { error, asset, openConfirmation, popperPosition } = this.state;

    return (
      <AppContainer
        title={`Editing Asset ${asset ? asset.serial : ""}`}
        appType="admin"
        endComponent={
          <Button
            variant="outlined"
            className={classes.endComponentButton}
            onClick={this.toggleConfirmation}
          >
            Delete Asset
          </Button>
        }
      >
        {error ? (
          "No such Asset"
        ) : asset ? (
          <React.Fragment>
            <ConfirmationPopup
              open={openConfirmation}
              anchorEl={popperPosition}
              onClickAway={this.toggleConfirmation}
              title="Delete Asset?"
              onCancel={this.toggleConfirmation}
              onConfirm={this.removeAsset}
            />
            <CreateAssetForm
              equipmentId={asset.equipmentId}
              onSubmit={this.onSubmit.bind(this)}
              submitError={error}
              editMode={true}
              asset={asset}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const AdminAssetPageWithStyles = withStyles(styles)(AdminEditAssetPage);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  AdminActions
)(AdminAssetPageWithStyles);
