import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateAssetForm from "../../../../src/components/admin/resources/equipment/CreateAssetForm";
import AdminActions from "../../../../src/actions/AdminActions";
import FeathersClient from "../../../../src/api/FeathersClient";

class AdminCreateAssetPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false };
  }

  static async getInitialProps({ query }) {
    return { equipmentId: query.id };
  }

  onSubmit({ serial, fecId, equipmentId }) {
    this.props.createEquipmentAsset(
      { serial, fecId, equipmentId },
      (err, res) => {
        if (err) {
          console.log(err);
          this.setState({ error: true });
          return;
        }
        Router.push(
          `/admin/resources/equipment/equipment${
            this.props.equipmentId ? `?id=${this.props.equipmentId}` : ""
          }`
        );
      }
    );
  }

  render() {
    const { classes, equipmentId } = this.props;
    const { error } = this.state;

    return (
      <AppContainer title="Create New Asset" appType="admin">
        <CreateAssetForm
          equipmentId={equipmentId}
          onSubmit={this.onSubmit.bind(this)}
          submitError={error}
        />
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminCreateAssetPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
