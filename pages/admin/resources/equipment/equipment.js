import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import AdminActions from "../../../../src/actions/AdminActions";
import Equipment from "../../../../src/components/admin/resources/equipment/Equipment";
import DataTable from "../../../../src/components/common/DataTable";
import { Interval, DateTime } from "luxon";

class AdminViewEquipmentPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, equipment: null };
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("equipment")
        .get(this.props.equipmentId);
      this.setState({ equipment: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { equipmentId: query.id };
  }

  render() {
    const { classes, equipmentId } = this.props;
    const { error, equipment } = this.state;

    return (
      <AppContainer
        title={`Equipment ${equipment ? equipment.name : ""}`}
        appType="admin"
      >
        {error ? (
          "No such equipment"
        ) : equipment ? (
          <React.Fragment>
            <div style={{ textAlign: "right" }}>
              <Link
                href={`/admin/resources/equipment/edit_equipment?id=${equipmentId}`}
              >
                <Button
                  color="secondary"
                  variant="outlined"
                  style={{ marginTop: 20 }}
                >
                  Edit Equipment
                </Button>
              </Link>
            </div>
            <Typography variant="h6">Equipment Details</Typography>
            <Equipment equipment={equipment} />

            <div style={{ textAlign: "right" }}>
              <Link
                href={`/admin/resources/equipment/create_asset?id=${equipmentId}`}
              >
                <Button
                  color="secondary"
                  variant="outlined"
                  style={{ marginTop: 20 }}
                >
                  Create Asset
                </Button>
              </Link>
            </div>
            <Typography variant="h6">Assets</Typography>
            <DataTable
              service="equipment-assets"
              dataCols={[
                { content: "Serial", data: "serial" },
                {
                  content: "Condition",
                  data: "conditionText"
                },
                {
                  content: "Actions",
                  data: "serial",
                  render: id => {
                    return (
                      <React.Fragment>
                        <Link
                          href={`/admin/resources/equipment/asset?id=${id}`}
                        >
                          <Button variant="outlined">View</Button>
                        </Link>
                      </React.Fragment>
                    );
                  }
                }
              ]}
              findQuery={{ equipmentId }}
              disableSearch={true}
            />
            <Link passHref href="/admin/resources/equipment">
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const AdminViewEquipmentPageWithStyles = withStyles(styles)(
  AdminViewEquipmentPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  AdminActions
)(AdminViewEquipmentPageWithStyles);
