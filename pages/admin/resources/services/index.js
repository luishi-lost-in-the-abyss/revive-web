import React from "react";
import Link from "next/link";
import { withStyles, Grid, Button, Typography } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import { styles } from "../../../../src/theme";
import DataTable from "../../../../src/components/common/DataTable";

class AdminServicesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <AppContainer title="Services" appType="admin">
        <Grid container style={{ marginBottom: 20 }} justify="flex-end">
          <Grid item xs={8} md={3}>
            <Link passHref href={`/admin/resources/services/create_service`}>
              <Button color="primary" variant="outlined" fullWidth>
                Create New Service
              </Button>
            </Link>
          </Grid>
        </Grid>
        <DataTable
          service="services"
          colWidths={[150, 350, null, 100]}
          dataCols={[
            { content: "Code", data: "code" },
            { content: "Name", data: "name" },
            {
              content: "Equipment Usage",
              data: "equipment",
              render: equipment => equipment.map(({ name }) => name).join(" | ")
            },
            {
              content: "Actions",
              data: "id",
              render: id => {
                return (
                  <Link
                    passHref
                    href={`/admin/resources/services/service?id=${id}`}
                  >
                    <Button variant="outlined">View</Button>
                  </Link>
                );
              }
            }
          ]}
          searchCategory={[
            { label: "Name", value: "name" },
            { label: "Code", value: "code" }
          ]}
          defaultSearchCategory="code"
        />
      </AppContainer>
    );
  }
}

export default withStyles(styles)(AdminServicesPage);
