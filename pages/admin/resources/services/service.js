import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import {
  Typography,
  Button,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction
} from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import AdminActions from "../../../../src/actions/AdminActions";
import Service from "../../../../src/components/admin/resources/services/Service";
import DataTable from "../../../../src/components/common/DataTable";

class AdminViewServicePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, service: null };
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("services").get(this.props.serviceId);
      this.setState({ service: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { serviceId: query.id };
  }

  render() {
    const { classes, serviceId } = this.props;
    const { error, service } = this.state;

    return (
      <AppContainer
        title={`Service ${service ? service.code + ` (${service.name})` : ""}`}
        appType="admin"
      >
        {error ? (
          "No such service"
        ) : service ? (
          <React.Fragment>
            <div style={{ textAlign: "right" }}>
              <Link
                href={`/admin/resources/services/edit_service?id=${serviceId}`}
              >
                <Button
                  color="secondary"
                  variant="outlined"
                  style={{ marginTop: 20 }}
                >
                  Edit Service
                </Button>
              </Link>
            </div>
            <Typography variant="h6" gutterBottom>
              Service Details
            </Typography>
            <Service service={service} />

            <Typography variant="h6" gutterBottom style={{ marginTop: 20 }}>
              Equipment Details
            </Typography>

            <List>
              {service.equipment.map(equip => (
                <ListItem key={`equipment-${equip.id}`}>
                  <ListItemText primary={equip.name} />
                  <ListItemSecondaryAction>
                    <Link
                      href={`/admin/resources/equipment/equipment?id=${
                        equip.id
                      }`}
                    >
                      <Button variant="outlined">View</Button>
                    </Link>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </List>

            <Link passHref href="/admin/resources/services">
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const AdminViewServicePageWithStyles = withStyles(styles)(AdminViewServicePage);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  AdminActions
)(AdminViewServicePageWithStyles);
