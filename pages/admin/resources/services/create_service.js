import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateServiceForm from "../../../../src/components/admin/resources/services/CreateServiceForm";
import AdminActions from "../../../../src/actions/AdminActions";

class AdminCreateServicePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { submitError: false };
  }

  onSubmit({ name, code, description, categoryId, equipment, price }) {
    this.props.createService(
      { name, code, description, price, equipment, categoryId },
      (err, res) => {
        if (err) {
          console.log(err);
          this.setState({ submitError: true });
          return;
        }
        Router.push("/admin/resources/services");
      }
    );
  }

  render() {
    const { classes } = this.props;
    const { submitError } = this.state;

    return (
      <AppContainer title="Create New Service" appType="admin">
        <CreateServiceForm
          onSubmit={this.onSubmit.bind(this)}
          submitError={submitError}
        />
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminCreateServicePage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
