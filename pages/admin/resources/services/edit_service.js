import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
import FeathersClient from "../../../../src/api/FeathersClient";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateServiceForm from "../../../../src/components/admin/resources/services/CreateServiceForm";
import AdminActions from "../../../../src/actions/AdminActions";
import ConfirmationPopup from "../../../../src/components/common/ConfirmationPopup";

class AdminEditServicePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      service: null,
      error: false,
      submitError: false,
      openConfirmation: false,
      popperPosition: null
    };

    this.toggleConfirmation = this.toggleConfirmation.bind(this);
    this.removeService = props.removeObject.bind(
      this,
      {
        id: props.serviceId,
        type: "services"
      },
      (err, res) => {
        Router.push("/admin/resources/services");
      }
    );
  }

  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("services").get(this.props.serviceId);
      this.setState({ service: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { serviceId: query.id };
  }

  onSubmit({ name, code, description, categoryId, equipment, price }) {
    this.props.editService(
      {
        serviceId: this.props.serviceId,
        name,
        code,
        description,
        price,
        equipment,
        categoryId
      },
      (err, res) => {
        if (err) {
          console.log(err);
          this.setState({ submitError: true });
          return;
        }
        Router.push(
          "/admin/resources/services/service?id=" + this.props.serviceId
        );
      }
    );
  }

  toggleConfirmation(evt) {
    this.setState({
      openConfirmation: !this.state.openConfirmation,
      popperPosition: evt.target
    });
  }

  render() {
    const { classes, serviceId } = this.props;
    const {
      service,
      error,
      submitError,
      openConfirmation,
      popperPosition
    } = this.state;

    return (
      <AppContainer
        title={`Editing Service ${service ? service.code : ""}`}
        appType="admin"
        endComponent={
          <Button
            variant="outlined"
            className={classes.endComponentButton}
            onClick={this.toggleConfirmation}
          >
            Delete Service
          </Button>
        }
      >
        {error ? (
          "No such Service"
        ) : service ? (
          <React.Fragment>
            <ConfirmationPopup
              open={openConfirmation}
              anchorEl={popperPosition}
              onClickAway={this.toggleConfirmation}
              title="Delete Service?"
              onCancel={this.toggleConfirmation}
              onConfirm={this.removeService}
            />
            <CreateServiceForm
              onSubmit={this.onSubmit.bind(this)}
              submitError={submitError}
              editMode={true}
              service={service}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminEditServicePage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
