import React from "react";
import Link from "next/link";
import Router from "next/router";
import { connect } from "react-redux";
import FeathersClient from "../../../../src/api/FeathersClient";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateServiceCategoryForm from "../../../../src/components/admin/resources/service_category/CreateServiceCategoryForm";
import AdminActions from "../../../../src/actions/AdminActions";
import ConfirmationPopup from "../../../../src/components/common/ConfirmationPopup";

class AdminEditCategoryPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      category: null,
      error: false,
      submitError: false,
      openConfirmation: false,
      popperPosition: null
    };

    this.toggleConfirmation = this.toggleConfirmation.bind(this);
    this.removeCategory = props.removeObject.bind(
      this,
      {
        id: props.categoryId,
        type: "service-category"
      },
      (err, res) => {
        Router.push("/admin/resources/service_category");
      }
    );
  }

  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("service-category")
        .get(this.props.categoryId);
      this.setState({ category: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { categoryId: query.id };
  }

  onSubmit({ name, parentCategoryId }) {
    this.props.editServiceCategory(
      { serviceCategoryId: this.props.categoryId, name, parentCategoryId },
      (err, res) => {
        if (err) {
          console.log(err);
          return;
        }
        Router.push("/admin/resources/service_category");
      }
    );
  }

  toggleConfirmation(evt) {
    this.setState({
      openConfirmation: !this.state.openConfirmation,
      popperPosition: evt.target
    });
  }

  render() {
    const { classes, categoryId } = this.props;
    const {
      category,
      error,
      submitError,
      openConfirmation,
      popperPosition
    } = this.state;

    return (
      <AppContainer
        title={`Editing Category ${category ? category.name : ""}`}
        appType="admin"
        endComponent={
          <Button
            variant="outlined"
            className={classes.endComponentButton}
            onClick={this.toggleConfirmation}
          >
            Delete Category
          </Button>
        }
      >
        {error ? (
          "No such Category"
        ) : category ? (
          <React.Fragment>
            <ConfirmationPopup
              open={openConfirmation}
              anchorEl={popperPosition}
              onClickAway={this.toggleConfirmation}
              title="Delete Category?"
              onCancel={this.toggleConfirmation}
              onConfirm={this.removeCategory}
            />
            <CreateServiceCategoryForm
              onSubmit={this.onSubmit.bind(this)}
              submitError={submitError}
              editMode={true}
              category={category}
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminEditCategoryPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
