import React from "react";
import Link from "next/link";
import { withStyles, Grid, Button, Typography } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import { styles } from "../../../../src/theme";
import DataTable from "../../../../src/components/common/DataTable";

class AdminServiceCategoriesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentCategory: null,
      path: [null],
      reloadData: new Date().getTime()
    };
  }

  navigate(category) {
    const { path } = this.state;
    this.setState({
      currentCategory: category.id,
      path: [...path, category],
      reloadData: new Date().getTime()
    });
  }

  goBackCategory(index) {
    const { path } = this.state;
    const cat = path[index];
    this.setState({
      path: [...path.slice(0, index), cat],
      currentCategory: cat ? cat.id : null,
      reloadData: new Date().getTime()
    });
  }

  render() {
    const { currentCategory, reloadData, path } = this.state;
    return (
      <AppContainer title="Service Categories" appType="admin">
        <Grid container style={{ marginBottom: 20 }} justify="flex-end">
          <Grid item xs={8} md={3}>
            <Link
              passHref
              href={`/admin/resources/service_category/create_category`}
            >
              <Button color="primary" variant="outlined" fullWidth>
                Create New Category
              </Button>
            </Link>
          </Grid>
        </Grid>

        <Typography>Currently browsing:</Typography>
        <div
          style={{ marginBottom: 20, display: "flex", alignItems: "center" }}
        >
          {path.map((p, i) => {
            return (
              <React.Fragment key={`path-${p ? p.id : "root"}`}>
                <div
                  {...(p && p.id === currentCategory
                    ? {}
                    : {
                        style: { cursor: "pointer" },
                        onClick: this.goBackCategory.bind(this, i)
                      })}
                >
                  <Typography variant="h5">{p ? p.name : "Home"}</Typography>
                </div>
                {i !== path.length - 1 ? (
                  <div style={{ marginLeft: 5, marginRight: 5 }}>
                    <Typography variant="h5">></Typography>
                  </div>
                ) : null}
              </React.Fragment>
            );
          })}
        </div>

        <DataTable
          service="service-category"
          colWidths={[null, 400]}
          findQuery={{ parentCategoryId: currentCategory }}
          reloadData={reloadData}
          dataCols={[
            { content: "Name", data: "name" },
            {
              content: "Actions",
              data: null,
              render: cat => {
                return (
                  <React.Fragment>
                    <Link
                      href={`/admin/resources/service_category/category?id=${
                        cat.id
                      }`}
                    >
                      <Button variant="outlined">View</Button>
                    </Link>
                    <Button
                      style={{ marginLeft: 10 }}
                      variant="outlined"
                      onClick={this.navigate.bind(this, cat)}
                    >
                      Navigate
                    </Button>
                  </React.Fragment>
                );
              }
            }
          ]}
          disableSearch={true}
        />
      </AppContainer>
    );
  }
}

export default withStyles(styles)(AdminServiceCategoriesPage);
