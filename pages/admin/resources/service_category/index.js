import React from "react";
import { connect } from "react-redux";
import Link from "next/link";
import { withStyles, Grid, Button, Typography } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import { styles } from "../../../../src/theme";
import DataTable from "../../../../src/components/common/DataTable";
import CreateServiceCategoryFormModal from "../../../../src/components/admin/resources/service_category/CreateServiceCategoryFormModal";
import AdminActions from "../../../../src/actions/AdminActions";
import FeathersClient from "../../../../src/api/FeathersClient";

class AdminServiceCategoriesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false,
      currentCategory: null,
      editCategory: null,
      currentCategoryObj: null,
      path: [null],
      reloadData: new Date().getTime()
    };

    this.onSubmitCreate = this.onSubmitCreate.bind(this);
    this.onOpenForm = this.onOpenForm.bind(this);
    this.onOpenEditForm = this.onOpenEditForm.bind(this);
    this.onCloseForm = this.onCloseForm.bind(this);
    this.onDeleteCategory = this.onDeleteCategory.bind(this);
  }

  static async getInitialProps({ query }) {
    if (query.id) {
      return { goToCategory: query.id };
    }
  }

  async goToCategory() {
    //navigate to the right category
    const { goToCategory } = this.props;
    const client = FeathersClient.getClient();
    const category = await client.service("service-category").get(goToCategory);
    this.navigate(category);
  }

  componentDidMount() {
    if (this.props.goToCategory) {
      this.goToCategory();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.goToCategory !== this.props.goToCategory) {
      this.goToCategory();
    }
  }

  navigate(category) {
    const { path } = this.state;
    this.setState({
      currentCategory: category.id,
      currentCategoryObj: category,
      path: [...path, category],
      reloadData: new Date().getTime()
    });
  }

  goBackCategory(index) {
    const { path } = this.state;
    const cat = path[index];
    this.setState({
      path: [...path.slice(0, index), cat],
      currentCategory: cat ? cat.id : null,
      currentCategoryObj: cat,
      reloadData: new Date().getTime()
    });
  }

  onSubmitCreate({ name, parentCategoryId, edit }) {
    const cb = (err, res) => {
      if (err) {
        console.log(err);
        return;
      }
      this.setState({
        openForm: false,
        editCategory: null,
        reloadData: new Date().getTime()
      });
    };
    if (edit) {
      this.props.editServiceCategory({
        serviceCategoryId: edit.id,
        name,
        parentCategoryId: edit.parentCategoryId
      });
    } else {
      this.props.createServiceCategory({ name, parentCategoryId }, cb);
    }
  }

  onDeleteCategory() {
    const { editCategory } = this.state;
    this.props.removeObject(
      { id: editCategory.id, type: "service-category" },
      (err, res) => {
        if (err) {
          console.log(err);
          return;
        }
        this.setState({
          openForm: false,
          editCategory: null,
          reloadData: new Date().getTime()
        });
      }
    );
  }

  onCloseForm() {
    this.setState({ openForm: false, editCategory: null });
  }

  onOpenEditForm(cat) {
    this.setState({ openForm: true, editCategory: cat });
  }

  onOpenForm() {
    this.setState({ openForm: true });
  }

  render() {
    const {
      currentCategory,
      currentCategoryObj,
      reloadData,
      path,
      openForm,
      editCategory
    } = this.state;
    return (
      <React.Fragment>
        <CreateServiceCategoryFormModal
          open={openForm}
          onClose={this.onCloseForm}
          parentCategory={currentCategoryObj}
          onSubmit={this.onSubmitCreate}
          editMode={editCategory}
          onDelete={this.onDeleteCategory}
        />
        <AppContainer title="Service Categories" appType="admin">
          <Grid container style={{ marginBottom: 20 }} justify="flex-end">
            <Grid item xs={8} md={3}>
              <Button
                color="primary"
                variant="outlined"
                fullWidth
                onClick={this.onOpenForm}
              >
                Create New {currentCategoryObj ? "Phase" : "Service Type"}
              </Button>
            </Grid>
          </Grid>

          <Typography>Currently browsing:</Typography>
          <div
            style={{ marginBottom: 20, display: "flex", alignItems: "center" }}
          >
            {path.map((p, i) => {
              return (
                <React.Fragment key={`path-${p ? p.id : "root"}`}>
                  <div
                    {...(p && p.id === currentCategory
                      ? {}
                      : {
                          style: { cursor: "pointer" },
                          onClick: this.goBackCategory.bind(this, i)
                        })}
                  >
                    <Typography variant="h5">{p ? p.name : "Home"}</Typography>
                  </div>
                  {i !== path.length - 1 ? (
                    <div style={{ marginLeft: 5, marginRight: 5 }}>
                      <Typography variant="h5">></Typography>
                    </div>
                  ) : null}
                </React.Fragment>
              );
            })}
          </div>

          <DataTable
            service="service-category"
            colWidths={[null, 400]}
            findQuery={{ parentCategoryId: currentCategory }}
            reloadData={reloadData}
            dataCols={[
              { content: "Name", data: "name" },
              {
                content: "Actions",
                data: null,
                render: cat => {
                  return (
                    <React.Fragment>
                      <Button
                        style={{ marginRight: 10 }}
                        color="secondary"
                        variant="outlined"
                        onClick={this.onOpenEditForm.bind(this, cat)}
                      >
                        Edit
                      </Button>
                      {path.length === 1 ? (
                        <Button
                          style={{ marginLeft: 10 }}
                          variant="outlined"
                          onClick={this.navigate.bind(this, cat)}
                        >
                          View Phases
                        </Button>
                      ) : (
                        <Link
                          href={`/admin/resources/service_category/category?id=${
                            cat.id
                          }`}
                        >
                          <Button variant="outlined">View Services</Button>
                        </Link>
                      )}
                    </React.Fragment>
                  );
                }
              }
            ]}
            disableSearch={true}
          />
        </AppContainer>
      </React.Fragment>
    );
  }
}

const component = withStyles(styles)(AdminServiceCategoriesPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
