import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import {
  Typography,
  Button,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction
} from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import AdminActions from "../../../../src/actions/AdminActions";
import ServiceCategory from "../../../../src/components/admin/resources/service_category/ServiceCategory";
import DataTable from "../../../../src/components/common/DataTable";

class AdminViewServiceCategoryPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, serviceCategory: null };
  }

  async getData() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("service-category")
        .get(this.props.serviceCategoryId);
      this.setState({ serviceCategory: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }
  async componentDidMount() {
    this.getData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.serviceCategoryId !== this.props.serviceCategoryId) {
      this.getData();
    }
  }

  static async getInitialProps({ query }) {
    return { serviceCategoryId: query.id };
  }

  render() {
    const { classes, serviceCategoryId } = this.props;
    const { error, serviceCategory } = this.state;

    return (
      <AppContainer
        title={`Service Category ${
          serviceCategory ? serviceCategory.name : ""
        }`}
        appType="admin"
      >
        {error ? (
          "No such service category"
        ) : serviceCategory ? (
          <React.Fragment>
            <ServiceCategory serviceCategory={serviceCategory} />

            <Link
              href={`/admin/resources/service_category${
                serviceCategory.parentCategoryId
                  ? `?id=${serviceCategory.parentCategoryId}`
                  : ""
              }`}
            >
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const AdminViewServiceCategoryPageWithStyles = withStyles(styles)(
  AdminViewServiceCategoryPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  AdminActions
)(AdminViewServiceCategoryPageWithStyles);
