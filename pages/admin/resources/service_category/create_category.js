import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import CreateServiceCategoryForm from "../../../../src/components/admin/resources/service_category/CreateServiceCategoryForm";
import AdminActions from "../../../../src/actions/AdminActions";

class AdminCreateServiceCategoryPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onSubmit({ name, parentCategoryId }) {
    this.props.createServiceCategory({ name, parentCategoryId }, (err, res) => {
      if (err) {
        console.log(err);
        return;
      }
      Router.push("/admin/resources/service_category");
    });
  }

  render() {
    const { classes } = this.props;

    return (
      <AppContainer title="Create New Service Category" appType="admin">
        <CreateServiceCategoryForm onSubmit={this.onSubmit.bind(this)} />
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(AdminCreateServiceCategoryPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AdminActions
)(component);
