import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../src/components/common/AppContainer";
import FeathersClient from "../../../src/api/FeathersClient";
import ReviewApplicationTable from "../../../src/components/Register/ReviewApplicationTable";
import ConfirmationPopup from "../../../src/components/common/ConfirmationPopup";
import ApproveApplication from "../../../src/components/admin/users/ApproveApplication";
import AdminActions from "../../../src/actions/AdminActions";

class AdminApplicationPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      application: null,
      openApprove: false,
      openReject: false,
      popperPosition: null,
      barangays: {}
    };
  }

  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("participants")
        .get(this.props.applicationId);
      //need to model it so as to reuse component
      results["plots"] = results["farm_plots"];
      const barangays = {};
      results["plots"] = results["plots"].map(
        ({ barangay, barangayId, ...plot }) => {
          barangays[barangayId] = barangay;
          plot.barangay = barangay.id;
          return plot;
        }
      );
      if (results["socio_infos"] && results["socio_infos"].length > 0) {
        results = Object.assign({}, results["socio_infos"][0], results);
      }
      this.setState({ application: results, barangays });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    const client = FeathersClient.getClient();
    const registrationParams = await client
      .service("public-methods")
      .create({ method: "get_registration_params" });
    return { applicationId: query.id, registrationParams };
  }

  togglePopper(approve, evt) {
    const popper = approve ? "openApprove" : "openReject";
    const otherPopper = approve ? "openReject" : "openApprove";
    const currentState = this.state[popper];
    const { currentTarget } = evt;

    this.setState({
      popperPosition: currentTarget,
      [popper]: !currentState,
      [otherPopper]: false
    });
  }

  closePopups() {
    this.setState({ openApprove: false, openReject: false });
  }

  approveApplication(plotTechnicians) {
    this.props.approveApplication(
      { userId: this.state.application.userId, plotTechnicians, approve: true },
      (err, res) => {
        if (err) {
          console.log(err);
          return;
        }

        Router.push("/admin/registration/applications");
      }
    );
  }

  rejectApplication(evt) {
    this.props.approveApplication(
      { userId: this.state.application.userId, approve: false },
      (err, res) => {
        if (err) {
          console.log(err);
          return;
        }

        Router.push("/admin/registration/applications");
      }
    );
  }

  render() {
    const { classes, registrationParams, applicationId } = this.props;
    const {
      error,
      application,
      openApprove,
      openReject,
      popperPosition,
      barangays
    } = this.state;

    return (
      <AppContainer title={`Application ${applicationId}`} appType="admin">
        {error ? (
          "No such application ID"
        ) : application ? (
          openApprove ? (
            <ApproveApplication
              plots={application.plots}
              approveApplication={this.approveApplication.bind(this)}
              goBack={this.closePopups.bind(this)}
            />
          ) : (
            <React.Fragment>
              <ConfirmationPopup
                open={openReject}
                anchorEl={popperPosition}
                onClickAway={this.closePopups.bind(this)}
                title="Reject Application?"
                onCancel={this.closePopups.bind(this)}
                onConfirm={this.rejectApplication.bind(this)}
              />
              <Grid container justify="space-between" alignItems="center">
                <Grid item>
                  <Typography variant="h4" gutterBottom>
                    Application ID: {applicationId}
                  </Typography>
                </Grid>
                <Grid item>
                  <Button
                    variant="outlined"
                    color="primary"
                    onClick={this.togglePopper.bind(this, false)}
                  >
                    Reject Application
                  </Button>
                </Grid>
              </Grid>
              <ReviewApplicationTable
                {...application}
                disableGrid={true}
                registrationParams={registrationParams}
                barangays={barangays}
              />
              <div
                style={{
                  marginTop: 20,
                  display: "flex",
                  justifyContent: "space-between"
                }}
              >
                <Link passHref href="/admin/registration/applications">
                  <Button variant="outlined" color="secondary">
                    Back to Application
                  </Button>
                </Link>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.togglePopper.bind(this, true)}
                >
                  Approve Application
                </Button>
              </div>
            </React.Fragment>
          )
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const AdminApplicationPageWithStyles = withStyles(styles)(AdminApplicationPage);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  AdminActions
)(AdminApplicationPageWithStyles);
