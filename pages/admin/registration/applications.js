import React from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import { Button } from "@material-ui/core";
import AppContainer from "../../../src/components/common/AppContainer";
import FeathersClient from "../../../src/api/FeathersClient";
import DataTable from "../../../src/components/common/DataTable";

class AdminApplicationsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true
    };
  }

  async componentDidMount() {
    const client = FeathersClient.getClient();
    const results = await client
      .service("admin-methods")
      .create({ method: "get_pending_applications" });
    const { data } = results;
    console.log(results);
    this.setState({ loading: false, data });
  }

  async loadData({ query }, cb) {
    const client = FeathersClient.getClient();
    const results = await client
      .service("admin-methods")
      .create({ method: "get_pending_applications", query });
    cb(results);
  }

  render() {
    const { classes } = this.props;
    const { loading, data } = this.state;

    const mainContent = (
      <DataTable
        service="Pending Applications"
        dataCols={[
          { content: "ID", data: "id" },
          { content: "Name", data: "name" },
          {
            content: "Email",
            data: "user",
            render: data => {
              return data.email;
            }
          },
          { content: "Application Date", data: "createdAt" },
          {
            content: "Actions",
            data: "id",
            render: id => {
              return (
                <React.Fragment>
                  <Link
                    passHref
                    href={`/admin/registration/application?id=${id}`}
                  >
                    <Button variant="outlined">View</Button>
                  </Link>
                </React.Fragment>
              );
            }
          }
        ]}
        loadData={this.loadData.bind(this)}
        disableSearch={true}
      />
    );

    return (
      <AppContainer title="Pending Applications" appType="admin">
        {loading ? "Loading" : mainContent}
      </AppContainer>
    );
  }
}

export default withStyles(styles)(AdminApplicationsPage);
