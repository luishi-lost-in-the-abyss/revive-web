import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../src/theme";
import { Grid, Typography } from "@material-ui/core";
import AppContainer from "../../src/components/common/AppContainer";
import FeathersClient from "../../src/api/FeathersClient";
import { DateTime } from "luxon";
import Router from "next/router";
import {
  VictoryChart,
  VictoryLine,
  VictoryTheme,
  VictoryZoomContainer,
  VictoryLegend,
  VictoryBar,
  VictoryLabel,
  VictoryGroup,
  VictoryTooltip
} from "victory";

class AdminHomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { chartData: {}, minMax: null };
  }
  async loadData() {
    const client = FeathersClient.getClient();
    let plots = [],
      bookings = [],
      fecs = [],
      temp;
    const currentDate = DateTime.local();

    do {
      temp = await client.service("fec").find({});
      fecs = [...fecs, ...temp.data];
    } while (fecs.length !== temp.total);

    do {
      temp = await client.service("bookings").find({
        query: {
          $skip: bookings.length,
          $or: [
            {
              $and: [
                { actualEndDate: null },
                { endDate: { $gte: currentDate.startOf("year").toJSDate() } },
                { endDate: { $lte: currentDate.endOf("year").toJSDate() } }
              ]
            },
            {
              $and: [
                {
                  actualEndDate: {
                    $gte: currentDate.startOf("year").toJSDate()
                  }
                },
                {
                  actualEndDate: { $lte: currentDate.endOf("year").toJSDate() }
                }
              ]
            }
          ],
          status: { $or: ["completed", "cancel", "pending"] }
        }
      });
      bookings = [...bookings, ...temp.data];
    } while (bookings.length !== temp.total);
    do {
      temp = await client.service("farm-plots").find({
        query: {
          $skip: plots.length
        }
      });
      plots = [...plots, ...temp.data];
    } while (plots.length !== temp.total);

    const chartData = {
      totalArea: 0,
      totalFarmers: new Set(),
      totalPlots: 0,
      bookings: { completed: 0, cancel: 0, pending: 0 },
      fecs: {}
    };
    fecs.map(({ id, name }) => {
      chartData.fecs[id] = {
        id,
        name,
        bookings: {},
        area: 0,
        farmers: new Set(),
        plots: 0
      };
    });
    const minMax = { min: 99, max: 0 };

    bookings.map(booking => {
      const { status, endDate, actualEndDate, dataSnapshot } = booking;
      if (status === "pending") return;
      const eDate = DateTime.fromJSDate(
        new Date(actualEndDate ? actualEndDate : endDate)
      );
      const week = eDate.weekNumber;
      chartData.bookings[status] += 1;
      if (week > minMax.max) {
        minMax.max = week;
      }
      if (week < minMax.min) {
        minMax.min = week;
      }

      if (dataSnapshot.booking.equipment_assets[0]) {
        if (
          chartData.fecs[dataSnapshot.booking.equipment_assets[0].fecId]
            .bookings[week]
        ) {
          chartData.fecs[
            dataSnapshot.booking.equipment_assets[0].fecId
          ].bookings[week] += 1;
        } else {
          chartData.fecs[
            dataSnapshot.booking.equipment_assets[0].fecId
          ].bookings[week] = 1;
        }
      }
    });
    Object.keys(chartData.fecs).map(fecId => {
      const obj = chartData.fecs[fecId];
      for (let i = minMax.min; i <= minMax.max; i++) {
        if (!obj.bookings[i]) {
          obj.bookings[i] = 0;
        }
      }
    });

    plots.map(plot => {
      const { participantId, plantableArea, technician } = plot;
      const fecId =
        technician && technician.supervisor
          ? technician.supervisor.fecId
          : null;
      if (fecId) {
        chartData.fecs[fecId].area += parseFloat(plantableArea);
        chartData.fecs[fecId].plots += 1;
        chartData.fecs[fecId].farmers.add(participantId);
      }
      chartData.totalFarmers.add(participantId);
      chartData.totalArea += parseFloat(plantableArea);
      chartData.totalPlots += 1;
    });

    chartData.totalArea = parseFloat(chartData.totalArea).toFixed(0);

    this.setState({
      chartData,
      minMax
    });
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    const { classes } = this.props;
    const { chartData, minMax, colors } = this.state;
    const noOfData = minMax ? minMax.max - minMax.min + 2 : 0;
    const noOfFecs = minMax ? Object.keys(chartData.fecs).length : 0;
    return (
      <AppContainer title="Dashboard" appType="admin">
        {!minMax && "Fetching Data..."}
        <Grid container>
          <Grid item xs={12} md={6}>
            {minMax && (
              <div style={{ display: "flex", marginBottom: 50 }}>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Area: {chartData.totalArea} Ha
                </Typography>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Plots: {chartData.totalPlots}
                </Typography>
                <Typography variant="h6">
                  Farmers: {chartData.totalFarmers.size}
                </Typography>
              </div>
            )}
            {minMax && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      noOfFecs > 5 ? { x: [noOfFecs - 5, noOfFecs] } : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  x={125}
                  gutter={20}
                  title="AREA-FARMER-PLOT PER FEC"
                  centerTitle
                  orientation="horizontal"
                  colorScale="heatmap"
                  style={{
                    title: { fontSize: 12 }
                  }}
                  data={Object.keys(chartData.fecs).map((fecId, i) => {
                    const obj = chartData.fecs[fecId];
                    return {
                      name: obj.name,
                      id: fecId
                    };
                  })}
                  events={[
                    {
                      target: "labels",
                      eventHandlers: {
                        onClick: () => ({
                          target: "labels",
                          mutation: props => {
                            Router.push(
                              "/admin/reports/fec?id=" + props.datum.id
                            );
                            return null;
                          }
                        })
                      }
                    }
                  ]}
                />
                <VictoryGroup offset={25} colorScale="heatmap">
                  {Object.keys(chartData.fecs).map(fecId => {
                    const obj = chartData.fecs[fecId];
                    return (
                      <VictoryBar
                        labelComponent={<VictoryTooltip />}
                        key={`areafarmerplot-${fecId}`}
                        data={[
                          {
                            x: "Area",
                            y: obj.area,
                            label: parseFloat(obj.area).toFixed(0)
                          },
                          {
                            x: "Farmer",
                            y: obj.farmers.size,
                            label: obj.farmers.size
                          },
                          { x: "Plot", y: obj.plots, label: obj.plots }
                        ]}
                      />
                    );
                  })}
                </VictoryGroup>
              </VictoryChart>
            )}
          </Grid>
          <Grid item xs={12} md={6}>
            {minMax && (
              <div style={{ display: "flex", marginBottom: 50 }}>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Completed: {chartData.bookings.completed}
                </Typography>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Cancel: {chartData.bookings.cancel}
                </Typography>
                <Typography variant="h6">
                  Pending: {chartData.bookings.pending}
                </Typography>
              </div>
            )}
            {minMax && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      minMax.max - minMax.min > 5
                        ? { x: [noOfData - 5, noOfData] }
                        : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  x={125}
                  gutter={20}
                  title="BOOKING TRANSACTION PER FEC"
                  centerTitle
                  orientation="horizontal"
                  colorScale="heatmap"
                  style={{
                    title: { fontSize: 12 }
                  }}
                  data={Object.keys(chartData.fecs).map((fecId, i) => {
                    const obj = chartData.fecs[fecId];
                    return {
                      name: obj.name
                    };
                  })}
                />
                <VictoryGroup offset={25} colorScale="heatmap">
                  {Object.keys(chartData.fecs).map(fecId => {
                    const obj = chartData.fecs[fecId];
                    return (
                      <VictoryLine
                        labelComponent={<VictoryLabel />}
                        key={`bookingtransactionsperfec-${fecId}`}
                        data={Object.keys(obj.bookings).map(week => ({
                          x: "Week " + week,
                          y: obj.bookings[week],
                          label: obj.bookings[week]
                        }))}
                      />
                    );
                  })}
                </VictoryGroup>
              </VictoryChart>
            )}
          </Grid>
        </Grid>
      </AppContainer>
    );
  }
}

export default withStyles(styles)(AdminHomePage);
