import React from "react";
import Link from "next/link";
import { withStyles, Grid, Button, Typography } from "@material-ui/core";
import AppContainer from "../../../src/components/common/AppContainer";
import { styles } from "../../../src/theme";
import DataTable from "../../../src/components/common/DataTable";
import { DateTime } from "luxon";
import BookingFilter from "../../../src/components/admin/bookings/BookingFilter";
import FeathersClient from "../../../src/api/FeathersClient";

const initialFindQuery = { $sort: { startDate: -1 } };
class TechnicianBookingsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      findQuery: initialFindQuery,
      reloadData: new Date().getTime(),
      fecs: {}
    };
    this.applyFilters = this.applyFilters.bind(this);
  }

  async componentDidMount() {
    const client = FeathersClient.getClient();
    const fecArr = await client
      .service("fec")
      .find({ query: { $limit: "-1" } });
    const fecs = {};
    fecArr.map(({ id, name }) => {
      fecs[id] = name;
    });
    this.setState({ fecs, reloadData: new Date().getTime() });
  }

  applyFilters(filterQuery) {
    if (!filterQuery) {
      this.setState({
        findQuery: initialFindQuery,
        reloadData: new Date().getTime()
      });
    }
    this.setState({
      findQuery: Object.assign({}, initialFindQuery, filterQuery),
      reloadData: new Date().getTime()
    });
  }

  render() {
    const { reloadData, findQuery, fecs } = this.state;
    return (
      <AppContainer title="Bookings" appType="technician">
        <Grid container style={{ marginBottom: 20 }} justify="flex-end">
          <Grid item xs={8} md={3}>
            <Link passHref href={`/technician/bookings/create_booking`}>
              <Button color="primary" variant="outlined" fullWidth>
                Create New Booking
              </Button>
            </Link>
          </Grid>
        </Grid>
        <div style={{ marginBottom: 20 }}>
          <BookingFilter applyFilters={this.applyFilters} />
        </div>
        <DataTable
          service="bookings"
          findQuery={findQuery}
          reloadData={reloadData}
          dataCols={[
            { content: "ID", data: "id" },
            { content: "Status", data: "status" },
            {
              content: "Service Code",
              data: null,
              render: ({ dataSnapshot }) => {
                if (dataSnapshot && dataSnapshot.booking.service) {
                  return dataSnapshot.booking.service.code;
                }
                return "-";
              }
            },
            {
              content: "Participant",
              data: null,
              render: data => {
                const plot = data["farm_plot"];
                if (plot.formalClusterId) {
                  return plot["formal_cluster"]["cluster"]["code"];
                }
                return plot.participant.name;
              }
            },
            {
              content: "Orig. FEC",
              data: null,
              render: ({ dataSnapshot }) => {
                let fecId = "";
                if (dataSnapshot && dataSnapshot.booking) {
                  fecId =
                    dataSnapshot.booking.equipment_assets.length > 0
                      ? dataSnapshot.booking.equipment_assets[0].fecId
                      : "";
                }
                return fecs[fecId] ? fecs[fecId] : "-";
              }
            },
            {
              content: "Start Date",
              data: "startDate",
              render: date =>
                DateTime.fromJSDate(new Date(date)).toFormat("dd-MM-yy H:mm")
            },
            {
              content: "End Date",
              data: "endDate",
              render: date =>
                DateTime.fromJSDate(new Date(date)).toFormat("dd-MM-yy H:mm")
            },
            {
              content: "Actions",
              data: "id",
              render: id => {
                return (
                  <Link passHref href={`/technician/bookings/booking?id=${id}`}>
                    <Button variant="outlined">View</Button>
                  </Link>
                );
              }
            }
          ]}
          searchCategory={[{ label: "ID", value: "id" }]}
          defaultSearchCategory="id"
        />
      </AppContainer>
    );
  }
}

export default withStyles(styles)(TechnicianBookingsPage);
