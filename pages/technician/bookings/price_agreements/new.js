import React from "react";
import Router from "next/router";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import TechnicianActions from "../../../../src/actions/TechnicianActions";
import CreatePriceAgreementForm from "../../../../src/components/admin/bookings/priceAgreements/CreatePriceAgreementForm";

class TechnicianCreatePriceAgreement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submitError: false
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  async componentDidMount() {}

  onSubmit({ bookingId, deliveredPrice, truckingPrice, deliveryMode }) {
    this.props.createPriceAgreement(
      {
        bookingId,
        deliveredPrice,
        truckingPrice,
        deliveryMode
      },
      (err, res) => {
        if (err) return;
        Router.push("/technician/bookings/price_agreements");
      }
    );
    this.setState({ submitError: false });
  }

  render() {
    const { classes } = this.props;

    return (
      <AppContainer title="Create New Price Agreement" appType="technician">
        <CreatePriceAgreementForm
          userRole="technician"
          onSubmit={this.onSubmit}
        />
      </AppContainer>
    );
  }
}

const component = withStyles(styles)(TechnicianCreatePriceAgreement);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  TechnicianActions
)(component);
