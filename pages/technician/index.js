import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../src/theme";
import { Grid, Typography } from "@material-ui/core";
import AppContainer from "../../src/components/common/AppContainer";
import {
  VictoryChart,
  VictoryLine,
  VictoryTheme,
  VictoryZoomContainer,
  VictoryLegend,
  VictoryBar,
  VictoryTooltip,
  VictoryAxis
} from "victory";
import FeathersClient from "../../src/api/FeathersClient";
import { DateTime } from "luxon";
import Router from "next/router";

class TechnicianHomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      municipalIds: {},
      chartData: null,
      participants: null,
      numOfPlots: 0,
      bookingMonths: null,
      minMax: { min: 0, max: 5 }
    };

    this.goToMunicipal = () => ({
      target: "data",
      mutation: ({ datum }) => {
        const municipalId = this.state.municipalIds[datum.x];
        Router.push("/technician/reports/municipality?id=" + municipalId);
        return null;
      }
    });
    this.goToMunicipalLabel = () => ({
      target: "tickLabels",
      mutation: data => {
        const municipalId = this.state.municipalIds[data.text];
        Router.push("/technician/reports/municipality?id=" + municipalId);
        return null;
      }
    });
  }

  async loadData() {
    const client = FeathersClient.getClient();
    let plots = [],
      bookings = [],
      temp;
    const currentDate = DateTime.local();

    do {
      temp = await client.service("bookings").find({
        query: {
          $skip: bookings.length,
          $or: [
            {
              $and: [
                { actualEndDate: null },
                { endDate: { $gte: currentDate.startOf("year").toJSDate() } },
                { endDate: { $lte: currentDate.endOf("year").toJSDate() } }
              ]
            },
            {
              $and: [
                {
                  actualEndDate: {
                    $gte: currentDate.startOf("year").toJSDate()
                  }
                },
                {
                  actualEndDate: { $lte: currentDate.endOf("year").toJSDate() }
                }
              ]
            }
          ],
          status: { $or: ["completed", "cancel", "pending"] }
        }
      });
      bookings = [...bookings, ...temp.data];
    } while (bookings.length !== temp.total);
    do {
      temp = await client.service("farm-plots").find({
        query: {
          $skip: plots.length
        }
      });
      plots = [...plots, ...temp.data];
    } while (plots.length !== temp.total);

    const chartData = {
      area: 0,
      bookings: { completed: 0, cancel: 0, pending: 0 },
      municipalArea: {}
    };
    const bookingMonths = { completed: {}, cancel: {} };
    const participants = new Set();
    const minMax = { min: 99, max: 0 };

    bookings.map(booking => {
      const { status, endDate, actualEndDate } = booking;
      if (status === "pending") return;
      const eDate = DateTime.fromJSDate(
        new Date(actualEndDate ? actualEndDate : endDate)
      );
      const month = eDate.get("month");
      const otherStatus = status === "completed" ? "cancel" : "completed";
      if (!bookingMonths[status][month]) {
        bookingMonths[status][month] = 0;
        if (!bookingMonths[otherStatus][month]) {
          bookingMonths[otherStatus][month] = 0;
        }
      }
      bookingMonths[status][month] += 1;

      chartData.bookings[status] += 1;
      if (month > minMax.max) {
        minMax.max = month;
      }
      if (month < minMax.min) {
        minMax.min = month;
      }
    });
    const municipalIds = {};
    plots.map(plot => {
      const { participantId, plantableArea, barangay } = plot;
      const municipal = barangay.municipality.name;
      municipalIds[municipal] = barangay.municipality.id;
      if (chartData.municipalArea[municipal]) {
        chartData.municipalArea[municipal] += parseFloat(plantableArea);
      } else {
        chartData.municipalArea[municipal] = parseFloat(plantableArea);
      }
      participants.add(participantId);
      chartData.area += parseFloat(plantableArea);
    });

    Object.keys(bookingMonths).map(status => {
      const obj = bookingMonths[status];
      for (let i = minMax.min; i <= minMax.max; i++) {
        if (!obj[i]) {
          obj[i] = 0;
        }
      }
    });

    this.setState({
      municipalIds,
      chartData,
      participants,
      numOfPlots: plots.length,
      bookingMonths,
      minMax
    });
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    const { classes } = this.props;
    const {
      participants,
      numOfPlots,
      chartData,
      bookingMonths,
      minMax
    } = this.state;
    const noOfData = minMax.max - minMax.min + 2;
    const noOfMunicipalities = chartData
      ? Object.keys(chartData.municipalArea).length
      : 0;
    return (
      <AppContainer title="Dashboard" appType="technician">
        {!participants && "Fetching Data..."}
        <Grid container>
          <Grid item xs={12} md={6}>
            {participants && (
              <div style={{ display: "flex", marginBottom: 50 }}>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Area: {parseFloat(chartData.area).toFixed(0)} Ha
                </Typography>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Plots: {numOfPlots}
                </Typography>
                <Typography variant="h6">
                  Farmers: {participants.size}
                </Typography>
              </div>
            )}
            {chartData && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      noOfMunicipalities > 5
                        ? {
                            x: [noOfMunicipalities - 3, noOfMunicipalities + 2]
                          }
                        : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryAxis dependentAxis />
                <VictoryAxis
                  events={[
                    {
                      target: "tickLabels",
                      eventHandlers: { onClick: this.goToMunicipalLabel }
                    }
                  ]}
                />

                <VictoryLegend
                  title="AREA (HA) BY MUNICIPALITY"
                  x={150}
                  orientation="horizontal"
                  itemsPerRow={3}
                  centerTitle
                  style={{ title: { fontSize: 12 }, labels: { fontSize: 8 } }}
                  colorScale="qualitative"
                  data={[]}
                />
                <VictoryBar
                  barWidth={15}
                  labelComponent={<VictoryTooltip />}
                  data={Object.keys(chartData.municipalArea).map(muni => ({
                    x: muni,
                    y: chartData.municipalArea[muni],
                    label: chartData.municipalArea[muni]
                  }))}
                  events={[
                    {
                      target: "data",
                      eventHandlers: { onClick: this.goToMunicipal }
                    }
                  ]}
                />
              </VictoryChart>
            )}
          </Grid>
          <Grid item xs={12} md={6}>
            {chartData && (
              <div style={{ display: "flex", marginBottom: 50 }}>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Completed: {chartData.bookings.completed}
                </Typography>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Cancel: {chartData.bookings.cancel}
                </Typography>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Pending: {chartData.bookings.pending}
                </Typography>
              </div>
            )}
            {bookingMonths && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      minMax.max - minMax.min > 5
                        ? { x: [noOfData - 5, noOfData] }
                        : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  x={175}
                  title="COMPLETED VS CANCEL"
                  centerTitle
                  orientation="horizontal"
                  style={{
                    title: { fontSize: 12 }
                  }}
                  data={[
                    {
                      name: "Completed",
                      symbol: { fill: "#1b5e20", type: "square" }
                    },
                    {
                      name: "Cancel",
                      symbol: { fill: "#795548", type: "square" }
                    }
                  ]}
                />
                <VictoryLine
                  style={{ data: { stroke: "#1b5e20" } }}
                  data={Object.keys(bookingMonths.completed).map(month => ({
                    x: DateTime.local()
                      .set({ month })
                      .get("monthShort"),
                    y: bookingMonths.completed[month],
                    label: bookingMonths.completed[month]
                  }))}
                />
                <VictoryLine
                  style={{ data: { stroke: "#795548" } }}
                  data={Object.keys(bookingMonths.cancel).map(month => ({
                    x: DateTime.local()
                      .set({ month })
                      .get("monthShort"),
                    y: bookingMonths.cancel[month],
                    label: bookingMonths.cancel[month]
                  }))}
                />
              </VictoryChart>
            )}
          </Grid>
        </Grid>
      </AppContainer>
    );
  }
}

export default withStyles(styles)(TechnicianHomePage);
