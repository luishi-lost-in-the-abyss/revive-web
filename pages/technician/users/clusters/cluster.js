import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import TechnicianActions from "../../../../src/actions/TechnicianActions";
import Cluster from "../../../../src/components/admin/users/clusters/Cluster";
import DataTable from "../../../../src/components/common/DataTable";
import { extractLocationDetails } from "../../../../src/api/utility";

class TechnicianClusterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, cluster: null };
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("clusters").get(this.props.clusterId);
      this.setState({ cluster: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { clusterId: query.id };
  }

  render() {
    const { classes, clusterId } = this.props;
    const { error, cluster } = this.state;

    return (
      <AppContainer
        title={`Cluster ${cluster ? cluster.code : ""}`}
        appType="technician"
      >
        {error ? (
          "No such cluster"
        ) : cluster ? (
          <React.Fragment>
            <Typography variant="h6">Cluster Details</Typography>
            <Cluster cluster={cluster} />
            <Typography variant="h6">Cluster Members</Typography>
            <DataTable
              service="participants"
              dataCols={[
                { content: "ID", data: "id" },
                { content: "Name", data: "name" }
              ]}
              findQuery={{ clusterId, userStatus: "activated" }}
              searchCategory={[{ label: "Name", value: "name" }]}
              defaultSearchCategory="name"
            />
            <Typography variant="h6">Farm Plots</Typography>
            <DataTable
              offline={true}
              dataArr={
                cluster
                  ? cluster.formal_cluster
                    ? cluster.formal_cluster.farm_plots
                    : cluster.farm_plots
                  : []
              }
              service="Farm Plots"
              disableSearch={true}
              dataCols={[
                { content: "ID", data: "id" },
                { content: "Code", data: "code" },
                {
                  content: "Address",
                  data: null,
                  render: ({ barangay, purok }) => {
                    const {
                      municipality,
                      province,
                      region,
                      country
                    } = extractLocationDetails(barangay);
                    return `${country.name} ${region.name} ${province.name} ${
                      municipality.name
                    } ${barangay.name} ${purok}`;
                  }
                },
                { content: "Total Area", data: "totalArea" },
                { content: "Plantable Area", data: "plantableArea" },
                {
                  content: "Assigned Tech.",
                  data: "technician",
                  render: technician => (technician ? technician.name : "-")
                }
              ]}
            />
            <Link href="/technician/users/clusters">
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const TechnicianClusterPageWithStyles = withStyles(styles)(
  TechnicianClusterPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  TechnicianActions
)(TechnicianClusterPageWithStyles);
