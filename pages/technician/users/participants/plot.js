import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import TechnicianActions from "../../../../src/actions/TechnicianActions";
import Plot from "../../../../src/components/admin/users/participants/Plot";

class TechnicianPlotPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, plot: null };
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("farm-plots").get(this.props.plotId);
      this.setState({ plot: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { plotId: query.id };
  }

  render() {
    const { classes, plotId } = this.props;
    const { error, plot } = this.state;

    return (
      <AppContainer
        title={`Plot ${plot ? `${plot.code} (ID: ${plot.id})` : ""}`}
        appType="technician"
      >
        {error ? (
          "No such Plot"
        ) : plot ? (
          <React.Fragment>
            <Typography variant="h6">Details</Typography>
            <Plot userRole="technician" plot={plot} />
            <Link
              href={`/technician/users/participants/participant?id=${
                plot.participantId
              }`}
            >
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const TechnicianPlotPageWithStyles = withStyles(styles)(TechnicianPlotPage);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  TechnicianActions
)(TechnicianPlotPageWithStyles);
