import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import TechnicianActions from "../../../../src/actions/TechnicianActions";
import Participant from "../../../../src/components/admin/users/participants/Participant";

class TechnicianParticipantPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, participant: null };
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("participants")
        .get(this.props.participantId);
      this.setState({ participant: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { participantId: query.id };
  }

  render() {
    const { classes, participantId } = this.props;
    const { error, participant } = this.state;

    return (
      <AppContainer
        title={`Participant ${participant ? participant.name : ""}`}
        appType="technician"
      >
        {error ? (
          "No such Participant"
        ) : participant ? (
          <React.Fragment>
            <Typography variant="h6">Details</Typography>
            <Participant userRole="technician" participant={participant} />
            <Link passHref href="/technician/users/participants">
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const TechnicianParticipantPageWithStyles = withStyles(styles)(
  TechnicianParticipantPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  TechnicianActions
)(TechnicianParticipantPageWithStyles);
