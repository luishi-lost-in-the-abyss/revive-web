import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import AppContainer from "../../../src/components/common/AppContainer";
import FeathersClient from "../../../src/api/FeathersClient";
import Booking from "../../../src/components/bookings/Booking";
import { TextField, MenuItem, Grid, Typography } from "@material-ui/core";
import {
  VictoryChart,
  VictoryLine,
  VictoryTheme,
  VictoryZoomContainer,
  VictoryLegend,
  VictoryBar,
  VictoryTooltip,
  VictoryAxis,
  VictoryGroup
} from "victory";

class TechnicianViewMunicipalityReportPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      municipal: null,
      municipals: {},
      error: false
    };
  }

  async loadAllPlotsData() {
    const client = FeathersClient.getClient();
    let plots = [],
      temp;
    do {
      temp = await client.service("farm-plots").find({
        query: {
          $skip: plots.length
        }
      });
      plots = [...plots, ...temp.data];
    } while (plots.length !== temp.total);

    const municipals = {};
    plots.map(plot => {
      const { participantId, plantableArea, barangay } = plot;
      const municipalId = barangay.municipality.id;
      let obj = municipals[municipalId];
      if (!obj) {
        municipals[municipalId] = {
          id: municipalId,
          name: barangay.municipality.name,
          area: {},
          plots: {},
          farmers: {},
          totalArea: 0,
          totalFarmers: new Set(),
          totalPlots: 0
        };
        obj = municipals[municipalId];
      }
      if (!obj.area[barangay.name]) {
        obj.area[barangay.name] = parseFloat(plantableArea);
        obj.plots[barangay.name] = 1;
        obj.farmers[barangay.name] = new Set([participantId]);
      } else {
        obj.area[barangay.name] += parseFloat(plantableArea);
        obj.plots[barangay.name] += 1;
        obj.farmers[barangay.name].add(participantId);
      }
      municipals[municipalId].totalPlots += 1;
      municipals[municipalId].totalFarmers.add(participantId);
      municipals[municipalId].totalArea += parseFloat(plantableArea);
    });
    const state = { municipals };
    if (this.props.municipalityId) {
      state.municipal = municipals[this.props.municipalityId];
    }
    this.setState(state);
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.municipalId &&
      prevProps.municipalId !== this.props.municipalId
    ) {
      if (this.state.municipals[this.props.municipalId]) {
        this.setState({
          municipal: this.state.municipals[this.props.municipalId]
        });
      } else {
        this.setState({ error: true });
      }
    }
  }

  async componentDidMount() {
    this.loadAllPlotsData();
  }

  static async getInitialProps({ query }) {
    return { municipalityId: query.id };
  }

  render() {
    const { classes } = this.props;
    const { error, municipal, municipals } = this.state;

    let noOfBarangays = municipal ? Object.keys(municipal.area).length : 0;

    return (
      <AppContainer
        title={`Municipality Report ${municipal ? municipal.name : ""}`}
        appType="technician"
      >
        <TextField
          fullWidth
          label="Select a Municipal"
          variant="outlined"
          select
          value={municipal ? municipal.id : ""}
          onChange={evt => {
            this.setState({ municipal: municipals[evt.target.value] });
          }}
        >
          {Object.keys(municipals).map(id => {
            const muni = municipals[id];
            return (
              <MenuItem key={`select-${id}`} value={id}>
                {muni.name}
              </MenuItem>
            );
          })}
        </TextField>
        {municipal && (
          <div>
            <Grid container>
              <Grid item xs={12}>
                <div style={{ display: "flex", marginBottom: 50 }}>
                  <Typography variant="h6" style={{ marginRight: 20 }}>
                    Area: {parseFloat(municipal.totalArea).toFixed(0)} Ha
                  </Typography>
                  <Typography variant="h6" style={{ marginRight: 20 }}>
                    Plots: {municipal.totalPlots}
                  </Typography>
                  <Typography variant="h6">
                    Farmers: {municipal.totalFarmers.size}
                  </Typography>
                </div>
              </Grid>
              <Grid item xs={12} md={6}>
                <VictoryChart
                  domainPadding={{ x: 40, y: 20 }}
                  width={500}
                  theme={VictoryTheme.material}
                  containerComponent={
                    <VictoryZoomContainer
                      zoomDimension="x"
                      zoomDomain={
                        noOfBarangays > 5
                          ? {
                              x: [noOfBarangays - 3, noOfBarangays + 2]
                            }
                          : {}
                      }
                      allowZoom={false}
                    />
                  }
                >
                  <VictoryLegend
                    title={`${municipal.name}: AREA, HA`}
                    x={150}
                    orientation="horizontal"
                    itemsPerRow={3}
                    centerTitle
                    style={{ title: { fontSize: 12 }, labels: { fontSize: 8 } }}
                    colorScale="qualitative"
                    data={[]}
                  />
                  <VictoryBar
                    barWidth={15}
                    labelComponent={<VictoryTooltip />}
                    data={Object.keys(municipal.area).map(barangay => ({
                      x: barangay,
                      y: municipal.area[barangay],
                      label: parseFloat(municipal.area[barangay]).toFixed(0)
                    }))}
                  />
                </VictoryChart>
              </Grid>
              <Grid item xs={12} md={6}>
                <VictoryChart
                  domainPadding={{ x: 40, y: 20 }}
                  width={500}
                  theme={VictoryTheme.material}
                  containerComponent={
                    <VictoryZoomContainer
                      zoomDimension="x"
                      zoomDomain={
                        noOfBarangays > 5
                          ? {
                              x: [noOfBarangays - 3, noOfBarangays + 2]
                            }
                          : {}
                      }
                      allowZoom={false}
                    />
                  }
                >
                  <VictoryLegend
                    title={`${municipal.name}: PLOT AND FARMER`}
                    x={150}
                    orientation="horizontal"
                    centerTitle
                    style={{
                      title: { fontSize: 12 },
                      labels: { fontSize: 10 }
                    }}
                    colorScale="heatmap"
                    data={[{ name: "Plots" }, { name: "Farmers" }]}
                  />
                  <VictoryGroup offset={15} colorScale={"heatmap"}>
                    <VictoryBar
                      barWidth={15}
                      labelComponent={<VictoryTooltip />}
                      data={Object.keys(municipal.plots).map(barangay => ({
                        x: barangay,
                        y: municipal.plots[barangay],
                        label: municipal.plots[barangay]
                      }))}
                    />
                    <VictoryBar
                      barWidth={15}
                      labelComponent={<VictoryTooltip />}
                      data={Object.keys(municipal.farmers).map(barangay => ({
                        x: barangay,
                        y: municipal.farmers[barangay].size,
                        label: municipal.farmers[barangay].size
                      }))}
                    />
                  </VictoryGroup>
                </VictoryChart>
              </Grid>
            </Grid>
          </div>
        )}
      </AppContainer>
    );
  }
}

const TechnicianViewMunicipalityReportPageWithStyles = withStyles(styles)(
  TechnicianViewMunicipalityReportPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  {}
)(TechnicianViewMunicipalityReportPageWithStyles);
