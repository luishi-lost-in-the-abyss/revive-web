import {
  withStyles,
  Grid,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core";
import { styles } from "../../../../src/theme";
import AppContainer from "../../../../src/components/common/AppContainer";
import { useState } from "react";
import UserReports from "../../../../src/components/admin/reports/raw_data/UserReports";

const reports = [{ title: "Users", key: "users" }];

const component = ({ classes }) => {
  const [nav, setNav] = useState("");

  const renderReportSection = () => {
    switch (nav) {
      case "users":
        return <UserReports />;
      default:
        return "Choose a category";
    }
  };
  return (
    <AppContainer title="Raw Data Reports" appType="technician">
      <Grid container spacing={16}>
        <Grid item xs={4}>
          <List className={classes.paperList}>
            {reports.map(({ title, key }) => (
              <ListItem
                button
                selected={key === nav}
                key={`listitem-${key}`}
                onClick={() => setNav(key)}
              >
                <ListItemText primary={title} />
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid item xs={8}>
          {renderReportSection()}
        </Grid>
      </Grid>
    </AppContainer>
  );
};

export default withStyles(styles)(component);
