import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import { Grid, Paper } from "@material-ui/core";
import RegisterForm from "../../../src/components/Register";
import FeathersClient from "../../../src/api/FeathersClient";
import AppContainer from "../../../src/components/common/AppContainer";

class CreateApplicationPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static async getInitialProps({ req }) {
    const client = FeathersClient.getClient();
    const registrationParams = await client
      .service("public-methods")
      .create({ method: "get_registration_params" });
    return { registrationParams };
  }

  render() {
    const { classes, registrationParams } = this.props;
    return (
      <AppContainer title="Create New Application" appType="technician">
        <RegisterForm registrationParams={registrationParams} />
      </AppContainer>
    );
  }
}

export default withStyles(styles)(CreateApplicationPage);
