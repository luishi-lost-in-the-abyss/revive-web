import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import AppContainer from "../../../src/components/common/AppContainer";
import RegisterSuccess from "../../../src/components/Register/RegisterSuccess";

class ApplicationSubmitSuccess extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, registrationParams } = this.props;
    return (
      <AppContainer title="Create New Application" appType="technician">
        <RegisterSuccess />
      </AppContainer>
    );
  }
}

export default withStyles(styles)(ApplicationSubmitSuccess);
