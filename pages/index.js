import React from "react";
import Button from "@material-ui/core/Button";
export default class Index extends React.Component {
  render() {
    return (
      <div>
        hello world{" "}
        <Button color="primary" variant="contained">
          Test
        </Button>
      </div>
    );
  }
}
