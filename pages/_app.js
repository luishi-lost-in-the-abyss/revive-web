import App, { Container } from "next/app";
import React from "react";
import withReduxStore from "../src/store";
import { Provider } from "react-redux";
import { MuiThemeProvider, CssBaseline } from "@material-ui/core";
import JssProvider from "react-jss/lib/JssProvider";
import mainTheme from "../src/theme";
import getPageContext from "../src/theme/getPageContext";
import LoadingModal from "../src/components/common/LoadingModal";
import NotificationSnack from "../src/components/common/NotificationSnack";
import { redirectTo, initializeAuthClient } from "../src/api/utility";
import FeathersClient from "../src/api/FeathersClient";
import { ACCOUNT_LOGIN, UI_LOADING_MODAL } from "../src/api/ActionTypes";
import { API_AUTHENTICATION } from "../src/api/EndPoints";
import { Settings } from "luxon";
import getConfig from "next/config";

class MyApp extends App {
  constructor(props) {
    super(props);
    this.pageContext = getPageContext();
    this.state = { mounted: false };
    const { publicRuntimeConfig } = getConfig();
    Settings.defaultZoneName = publicRuntimeConfig.defaultZoneName;
  }

  static async getInitialProps({ ctx, Component }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    const pathname = ctx.pathname;
    return { pageProps, pathname };
  }

  async loginWeb(token) {
    //this is a workaround to force cookies to be set as websocket to support cookies
    return await fetch(API_AUTHENTICATION, {
      method: "POST",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ accessToken: token, strategy: "jwt" })
    });
  }

  async loadToken(cb) {
    const token = window.localStorage.getItem("token");
    const { reduxStore, pathname } = this.props;
    const dispatch = reduxStore.dispatch;
    if (token) {
      dispatch({ type: UI_LOADING_MODAL, payload: { open: true } });
      const client = FeathersClient.getClient();
      initializeAuthClient(client, dispatch);
      try {
        const res = await client.authenticate({
          strategy: "jwt",
          accessToken: token
        });
        await this.loginWeb(token);
        dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
        dispatch({
          type: ACCOUNT_LOGIN,
          payload: {
            token: res.accessToken,
            role: res.role
          }
        });
        cb && cb(null, res);
      } catch (err) {
        cb && cb(err, null);
        dispatch({ type: UI_LOADING_MODAL, payload: { open: false } });
        window.localStorage.removeItem("token");
      }
    } else {
      cb && cb("no token", null);
    }
  }

  async componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }

    const { reduxStore, pathname } = this.props;
    const store = reduxStore.getState();

    const loginAndCheckRole = (role, err, res) => {
      if (err || role.indexOf(res.role) < 0) {
        redirectTo("/login");
        return;
      }
    };

    //check for authenticated route
    if (pathname.indexOf("/admin") === 0) {
      //user needs to be authenticated else redirect to login page
      if (store.account.authenticated) {
        if (
          store.account.role === "admin" ||
          store.account.role === "super_admin"
        ) {
          this.setState({ mounted: true });
          return;
        } else {
          redirectTo("/login");
        }
      } else {
        await this.loadToken(
          loginAndCheckRole.bind(this, ["admin", "super_admin"])
        );
      }
    } else if (pathname.indexOf("/supervisor") === 0) {
      //user needs to be authenticated else redirect to login page
      if (store.account.authenticated) {
        if (store.account.role === "supervisor") {
          this.setState({ mounted: true });
          return;
        } else {
          redirectTo("/login");
        }
      } else {
        await this.loadToken(loginAndCheckRole.bind(this, ["supervisor"]));
      }
    } else if (pathname.indexOf("/dispatcher") === 0) {
      //user needs to be authenticated else redirect to login page
      if (store.account.authenticated) {
        if (store.account.role === "dispatcher") {
          this.setState({ mounted: true });
          return;
        } else {
          redirectTo("/login");
        }
      } else {
        await this.loadToken(loginAndCheckRole.bind(this, ["dispatcher"]));
      }
    } else if (pathname.indexOf("/technician") === 0) {
      //user needs to be authenticated else redirect to login page
      if (store.account.authenticated) {
        if (store.account.role === "technician") {
          this.setState({ mounted: true });
          return;
        } else {
          redirectTo("/login");
        }
      } else {
        await this.loadToken(loginAndCheckRole.bind(this, ["technician"]));
      }
    }

    this.setState({ mounted: true });
  }

  render() {
    const { Component, pageProps, reduxStore } = this.props;
    const { mounted } = this.state;

    return (
      <Container>
        <Provider store={reduxStore}>
          <JssProvider
            registry={this.pageContext.sheetsRegistry}
            generateClassName={this.pageContext.generateClassName}
          >
            <MuiThemeProvider theme={mainTheme}>
              <CssBaseline />
              <LoadingModal />
              <NotificationSnack />
              {mounted ? (
                <Component pageContext={this.pageContext} {...pageProps} />
              ) : (
                ""
              )}
            </MuiThemeProvider>
          </JssProvider>
        </Provider>
      </Container>
    );
  }
}

export default withReduxStore(MyApp);
