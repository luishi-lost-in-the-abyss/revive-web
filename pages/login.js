import React from "react";
import Router from "next/router";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../src/theme";
import { Grid, Paper, Button, TextField, Typography } from "@material-ui/core";
import AccountActions from "../src/actions/AccountActions";

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      error: false
    };
  }

  onInputChange(evt) {
    const { name, value } = evt.target;
    this.setState({ [name]: value });
  }

  login(evt) {
    evt.preventDefault();
    const { username, password } = this.state;
    this.setState({ error: false });
    this.props.loginUser(
      {
        username,
        password,
        role: "admin"
      },
      (err, res) => {
        if (err) {
          console.log(err);
          this.setState({ error: true });
          return;
        }
        switch (res) {
          case "super_admin":
          case "admin":
            Router.push(`/admin/`);
            break;
          case "supervisor":
            Router.push("/supervisor");
            break;
          case "technician":
            Router.push(`/technician`);
            break;
          case "dispatcher":
            Router.push(`/dispatcher`);
            break;
          default:
            break;
        }
      }
    );
  }

  render() {
    const { classes } = this.props;
    const { error } = this.state;
    return (
      <React.Fragment>
        <Grid container justify="center">
          <Grid item sm={10} md={8}>
            <Paper className={classes.rootPaper} style={{ marginTop: 50 }}>
              <Grid container justify="center" alignItems="center">
                <Grid item xs={12} sm={6} md={6}>
                  <img
                    src="/static/images/LOGO_2.jpg"
                    style={{ width: "100%" }}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                  <div
                    className={classes.rootPaper}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      flexDirection: "column"
                    }}
                  >
                    <Typography
                      variant="h5"
                      className={classes.formField}
                      align="center"
                    >
                      Login
                    </Typography>
                    <form onSubmit={this.login.bind(this)}>
                      <TextField
                        className={classes.formField}
                        label="Username"
                        variant="filled"
                        name="username"
                        onChange={this.onInputChange.bind(this)}
                        error={error}
                        fullWidth
                      />
                      <TextField
                        className={classes.formField}
                        label="Password"
                        type="password"
                        name="password"
                        variant="filled"
                        onChange={this.onInputChange.bind(this)}
                        error={error}
                        helperText={
                          error ? "Incorrect username / password" : null
                        }
                        fullWidth
                      />
                      <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        fullWidth
                      >
                        Login
                      </Button>
                    </form>
                  </div>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

const LoginPageWithStyles = withStyles(styles)(LoginPage);

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  AccountActions
)(LoginPageWithStyles);
