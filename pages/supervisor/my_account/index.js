import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import {} from "@material-ui/core";
import AppContainer from "../../../src/components/common/AppContainer";
import ChangePassword from "../../../src/components/common/ChangePassword";

class SupervisorMyAccountPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;
    return (
      <AppContainer title="My Account" appType="supervisor">
        <ChangePassword />
      </AppContainer>
    );
  }
}

export default withStyles(styles)(SupervisorMyAccountPage);
