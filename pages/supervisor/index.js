import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../src/theme";
import { Grid, Typography } from "@material-ui/core";
import AppContainer from "../../src/components/common/AppContainer";
import {
  VictoryChart,
  VictoryLine,
  VictoryTheme,
  VictoryZoomContainer,
  VictoryLegend,
  VictoryBar,
  VictoryGroup,
  VictoryTooltip,
  VictoryLabel
} from "victory";
import FeathersClient from "../../src/api/FeathersClient";
import { DateTime } from "luxon";

class SupervisorHomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData: null,
      participants: null,
      numOfPlots: 0
    };
  }

  async loadData() {
    const client = FeathersClient.getClient();
    let technicians = [],
      plots = [],
      bookings = [],
      temp;
    const currentDate = DateTime.local();

    do {
      temp = await client.service("technicians").find({
        query: {
          $skip: technicians.length
        }
      });
      technicians = [...technicians, ...temp.data];
    } while (technicians.length !== temp.total);
    do {
      temp = await client.service("bookings").find({
        query: {
          $skip: bookings.length,
          $or: [
            {
              $and: [
                { actualEndDate: null },
                { endDate: { $gte: currentDate.startOf("year").toJSDate() } },
                { endDate: { $lte: currentDate.endOf("year").toJSDate() } }
              ]
            },
            {
              $and: [
                {
                  actualEndDate: {
                    $gte: currentDate.startOf("year").toJSDate()
                  }
                },
                {
                  actualEndDate: { $lte: currentDate.endOf("year").toJSDate() }
                }
              ]
            }
          ],
          technicianId: { $in: technicians.map(({ id }) => id) },
          status: { $or: ["completed", "cancel", "pending"] }
        }
      });
      bookings = [...bookings, ...temp.data];
    } while (bookings.length !== temp.total);
    do {
      temp = await client.service("farm-plots").find({
        query: {
          $skip: plots.length
        }
      });
      plots = [...plots, ...temp.data];
    } while (plots.length !== temp.total);

    const chartData = {};
    const participants = new Set();
    technicians.map(tech => {
      chartData[tech.id] = {
        name: tech.name,
        area: 0,
        bookings: { completed: 0, cancel: 0, pending: 0 }
      };
    });
    bookings.map(booking => {
      const { technicianId, status } = booking;
      if (!technicianId) return;
      chartData[technicianId].bookings[status] += 1;
    });
    plots.map(plot => {
      const { participantId, technicianId, plantableArea } = plot;
      participants.add(participantId);
      chartData[technicianId].area += parseFloat(plantableArea);
    });
    this.setState({ chartData, participants, numOfPlots: plots.length });
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    const { classes } = this.props;
    const { participants, numOfPlots, chartData } = this.state;

    const noOfTech = chartData ? Object.keys(chartData).length : 0;
    return (
      <AppContainer title="Dashboard" appType="supervisor">
        {!participants && "Fetching Data..."}
        <Grid container>
          <Grid item xs={12} md={6}>
            {participants && (
              <div style={{ display: "flex", marginBottom: 50 }}>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Area:{" "}
                  {Object.keys(chartData)
                    .map(id => chartData[id].area)
                    .reduce((a, b) => a + b, 0)
                    .toFixed(0)}{" "}
                  Ha
                </Typography>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Plots: {numOfPlots}
                </Typography>
                <Typography variant="h6">
                  Farmers: {participants.size}
                </Typography>
              </div>
            )}
            {chartData && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      noOfTech > 5 ? { x: [noOfTech - 3, noOfTech + 2] } : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  x={150}
                  centerTitle
                  title="AREA (HA) PER TECHNICIAN"
                  data={[]}
                />
                <VictoryBar
                  barWidth={25}
                  labelComponent={<VictoryTooltip />}
                  data={Object.keys(chartData).map(id => {
                    const tech = chartData[id];
                    return {
                      x: `${tech.name}`,
                      y: tech.area,
                      label: tech.area.toFixed(2)
                    };
                  })}
                />
              </VictoryChart>
            )}
          </Grid>
          <Grid item xs={12} md={6}>
            {chartData && (
              <div style={{ display: "flex" }}>
                <Typography
                  variant="h6"
                  style={{ marginRight: 20, marginBottom: 50 }}
                >
                  Completed:{" "}
                  {Object.values(chartData)
                    .map(({ bookings }) => bookings.completed)
                    .reduce((a, b) => a + b, 0)}
                </Typography>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Cancel:{" "}
                  {Object.values(chartData)
                    .map(({ bookings }) => bookings.cancel)
                    .reduce((a, b) => a + b, 0)}
                </Typography>
                <Typography variant="h6" style={{ marginRight: 20 }}>
                  Pending:{" "}
                  {Object.values(chartData)
                    .map(({ bookings }) => bookings.pending)
                    .reduce((a, b) => a + b, 0)}
                </Typography>
              </div>
            )}
            {chartData && (
              <VictoryChart
                domainPadding={{ x: 40, y: 20 }}
                width={500}
                theme={VictoryTheme.material}
                containerComponent={
                  <VictoryZoomContainer
                    zoomDimension="x"
                    zoomDomain={
                      noOfTech > 5 ? { x: [noOfTech - 3, noOfTech + 2] } : {}
                    }
                    allowZoom={false}
                  />
                }
              >
                <VictoryLegend
                  x={180}
                  centerTitle
                  orientation="horizontal"
                  title="COMPLETED VS CANCEL"
                  data={[
                    {
                      name: "Completed",
                      symbol: { fill: "#1b5e20", type: "square" }
                    },
                    {
                      name: "Cancel",
                      symbol: { fill: "#795548", type: "square" }
                    }
                  ]}
                />
                <VictoryGroup offset={25}>
                  <VictoryLine
                    labelComponent={<VictoryLabel />}
                    style={{ data: { stroke: "#1b5e20" } }}
                    data={Object.keys(chartData).map(id => {
                      const tech = chartData[id];
                      return {
                        x: `${tech.name}`,
                        y: tech.bookings.completed,
                        label: tech.bookings.completed
                      };
                    })}
                  />
                  <VictoryLine
                    labelComponent={<VictoryLabel />}
                    style={{ data: { stroke: "#795548" } }}
                    data={Object.keys(chartData).map(id => {
                      const tech = chartData[id];
                      return {
                        x: `${tech.name}`,
                        y: tech.bookings.cancel,
                        label: tech.bookings.cancel
                      };
                    })}
                  />
                </VictoryGroup>
              </VictoryChart>
            )}
          </Grid>
        </Grid>
      </AppContainer>
    );
  }
}

export default withStyles(styles)(SupervisorHomePage);
