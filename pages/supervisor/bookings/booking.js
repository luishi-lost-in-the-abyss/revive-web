import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import AppContainer from "../../../src/components/common/AppContainer";
import FeathersClient from "../../../src/api/FeathersClient";
import Booking from "../../../src/components/bookings/Booking";

class SupervisorViewBookingPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      booking: null,
      informalClusterParticipants: null
    };
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("bookings").get(this.props.bookingId);

      const {
        dataSnapshot,
        status,
        statusReason,
        actualCompletedArea,
        actualStartDate,
        actualEndDate
      } = results;
      const { booking, participants } = dataSnapshot;
      let informalClusterParticipants = null;
      if (
        // check if informal cluster, if so retrieve the members of the informal cluster
        booking.farm_plot.clusterId &&
        !booking.individualInformal
      ) {
        informalClusterParticipants = participants;
      }
      this.setState({
        booking: Object.assign({}, booking, {
          status,
          statusReason,
          actualCompletedArea,
          actualStartDate,
          actualEndDate
        }),
        informalClusterParticipants
      });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { bookingId: query.id };
  }

  render() {
    const { classes, bookingId } = this.props;
    const { error, booking, informalClusterParticipants } = this.state;

    return (
      <AppContainer
        title={`Booking ${booking ? booking.id : ""}`}
        appType="supervisor"
      >
        {error ? (
          "No such booking"
        ) : booking ? (
          <React.Fragment>
            <Booking
              booking={booking}
              informalClusterParticipants={informalClusterParticipants}
              userRole="supervisor"
            />
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const SupervisorViewBookingPageWithStyles = withStyles(styles)(
  SupervisorViewBookingPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  {}
)(SupervisorViewBookingPageWithStyles);
