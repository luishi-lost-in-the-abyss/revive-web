import React from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import DataTable from "../../../../src/components/common/DataTable";

class SupervisorParticipantsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    const mainContent = (
      <DataTable
        service="participants"
        filterQuery={{ userStatus: "activated" }}
        dataCols={[
          { content: "ID", data: "id" },
          {
            content: "Username",
            data: null,
            render: data => data.user.username
          },
          { content: "Name", data: "name" },
          { content: "Email", data: null, render: data => data.user.email },
          {
            content: "Actions",
            data: null,
            render: ({ id, user }) => {
              return (
                <React.Fragment>
                  <Link
                    passHref
                    href={
                      user.status === "activated"
                        ? `/supervisor/users/participants/participant?id=${id}`
                        : `/supervisor/registration/application?id=${id}`
                    }
                  >
                    <Button variant="outlined">View</Button>
                  </Link>
                </React.Fragment>
              );
            }
          }
        ]}
        searchCategory={[
          { label: "Name", value: "name" },
          { label: "Username", value: "username" }
        ]}
        defaultSearchCategory="name"
      />
    );

    return (
      <AppContainer title="Participants" appType="supervisor">
        <Grid container style={{ marginBottom: 20 }} justify="flex-end">
          <Grid item xs={8} md={3}>
            {/* 
            <Link passHref href={`/supervisor/users/participants/create_participant`}>
              <Button color="primary" variant="outlined" fullWidth>
                Create New Participant
              </Button>
            </Link>*/}
          </Grid>
        </Grid>

        {mainContent}
      </AppContainer>
    );
  }
}

export default withStyles(styles)(SupervisorParticipantsPage);
