import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import SupervisorActions from "../../../../src/actions/SupervisorActions";
import Plot from "../../../../src/components/admin/users/participants/Plot";

class SupervisorPlotPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, plot: null };
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client.service("farm-plots").get(this.props.plotId);
      this.setState({ plot: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { plotId: query.id };
  }

  render() {
    const { classes, plotId } = this.props;
    const { error, plot } = this.state;

    return (
      <AppContainer
        title={`Plot ${plot ? `${plot.code} (ID: ${plot.id})` : ""}`}
        appType="supervisor"
      >
        {error ? (
          "No such Plot"
        ) : plot ? (
          <React.Fragment>
            <div style={{ textAlign: "right" }}>
              <Link
                href={`/supervisor/users/participants/edit_plot?id=${plot.id}`}
              >
                <Button
                  color="secondary"
                  variant="outlined"
                  style={{ marginTop: 20 }}
                >
                  Edit Plot
                </Button>
              </Link>
            </div>
            <Typography variant="h6">Details</Typography>
            <Plot userRole="supervisor" plot={plot} />
            <Link
              href={`/supervisor/users/participants/participant?id=${
                plot.participantId
              }`}
            >
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const SupervisorPlotPageWithStyles = withStyles(styles)(SupervisorPlotPage);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  SupervisorActions
)(SupervisorPlotPageWithStyles);
