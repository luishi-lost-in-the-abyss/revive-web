import React from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import DataTable from "../../../../src/components/common/DataTable";

class SupervisorClustersPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { clusterIds: [], reloadData: new Date().getTime() };
  }

  async componentDidMount() {
    const client = FeathersClient.getClient();
    const clusterIds = await client
      .service("supervisor-methods")
      .create({ method: "get_cluster_ids" });
    this.setState({ clusterIds, reloadData: new Date().getTime() });
  }

  render() {
    const { classes } = this.props;
    const { reloadData, clusterIds } = this.state;
    const mainContent = (
      <DataTable
        service="clusters"
        findQuery={{ id: { $in: clusterIds } }}
        reloadData={reloadData}
        dataCols={[
          { content: "Code", data: "code" },
          {
            content: "Type",
            data: "formal_cluster",
            render: formal => (formal ? "Formal" : "Informal")
          },
          {
            content: "Actions",
            data: "id",
            render: id => {
              return (
                <React.Fragment>
                  <Link
                    passHref
                    href={`/supervisor/users/clusters/cluster?id=${id}`}
                  >
                    <Button variant="outlined">View</Button>
                  </Link>
                </React.Fragment>
              );
            }
          }
        ]}
        searchCategory={[{ label: "Code", value: "code" }]}
        defaultSearchCategory="code"
      />
    );

    return (
      <AppContainer title="Clusters" appType="supervisor">
        <Grid container style={{ marginBottom: 20 }} justify="flex-end">
          <Grid item xs={8} md={3}>
            <Link passHref href={`/supervisor/users/clusters/create_cluster`}>
              <Button color="primary" variant="outlined" fullWidth>
                Create New Cluster
              </Button>
            </Link>
          </Grid>
        </Grid>

        {mainContent}
      </AppContainer>
    );
  }
}

export default withStyles(styles)(SupervisorClustersPage);
