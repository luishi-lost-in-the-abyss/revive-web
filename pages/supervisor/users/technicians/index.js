import React from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import DataTable from "../../../../src/components/common/DataTable";

class SupervisorTechniciansPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    const mainContent = (
      <DataTable
        service="technicians"
        dataCols={[
          { content: "ID", data: "id" },
          {
            content: "Username",
            data: null,
            render: data => data.user.username
          },
          { content: "Name", data: "name" },
          { content: "Contact", data: "contact" },
          { content: "Email", data: null, render: data => data.user.email },
          {
            content: "Actions",
            data: "id",
            render: id => {
              return (
                <React.Fragment>
                  <Link
                    href={`/supervisor/users/technicians/technician?id=${id}`}
                  >
                    <Button variant="outlined">View</Button>
                  </Link>
                </React.Fragment>
              );
            }
          }
        ]}
        searchCategory={[
          { label: "Name", value: "name" },
          { label: "Username", value: "username" }
        ]}
        defaultSearchCategory="name"
      />
    );

    return (
      <AppContainer title="Technicians" appType="supervisor">
        {mainContent}
      </AppContainer>
    );
  }
}

export default withStyles(styles)(SupervisorTechniciansPage);
