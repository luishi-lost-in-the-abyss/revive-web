import React from "react";
import { connect } from "react-redux";
import Router from "next/router";
import Link from "next/link";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../../src/theme";
import { Typography, Button, Grid } from "@material-ui/core";
import AppContainer from "../../../../src/components/common/AppContainer";
import FeathersClient from "../../../../src/api/FeathersClient";
import SupervisorActions from "../../../../src/actions/SupervisorActions";
import Technician from "../../../../src/components/admin/users/technicians/Technician";

class SupervisorTechnicianPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: false, technician: null };
  }
  async componentDidMount() {
    try {
      const client = FeathersClient.getClient();
      let results = await client
        .service("technicians")
        .get(this.props.technicianId);
      this.setState({ technician: results });
    } catch (err) {
      this.setState({ error: true });
    }
  }

  static async getInitialProps({ query }) {
    return { technicianId: query.id };
  }

  render() {
    const { classes, technicianId } = this.props;
    const { error, technician } = this.state;

    return (
      <AppContainer
        title={`Technician ${technician ? technician.name : ""}`}
        appType="supervisor"
      >
        {error ? (
          "No such Technician"
        ) : technician ? (
          <React.Fragment>
            <Typography variant="h6">Details</Typography>
            <Technician technician={technician} />
            <Link passHref href="/supervisor/users/technicians">
              <Button
                color="secondary"
                variant="outlined"
                style={{ marginTop: 20 }}
              >
                Go Back
              </Button>
            </Link>
          </React.Fragment>
        ) : (
          "loading"
        )}
      </AppContainer>
    );
  }
}

const SupervisorTechnicianPageWithStyles = withStyles(styles)(
  SupervisorTechnicianPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  SupervisorActions
)(SupervisorTechnicianPageWithStyles);
