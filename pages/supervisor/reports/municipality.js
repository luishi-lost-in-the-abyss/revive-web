import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../src/theme";
import AppContainer from "../../../src/components/common/AppContainer";
import FeathersClient from "../../../src/api/FeathersClient";
import Booking from "../../../src/components/bookings/Booking";
import { TextField, MenuItem, Grid, Typography } from "@material-ui/core";
import {
  VictoryChart,
  VictoryLine,
  VictoryTheme,
  VictoryZoomContainer,
  VictoryLegend,
  VictoryBar,
  VictoryTooltip,
  VictoryAxis,
  VictoryGroup
} from "victory";

class SupervisorViewMunicipalityReportPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      municipals: {},
      error: false
    };
  }

  async loadAllPlotsData() {
    const client = FeathersClient.getClient();
    let plots = [],
      temp;
    do {
      temp = await client.service("farm-plots").find({
        query: {
          $skip: plots.length
        }
      });
      plots = [...plots, ...temp.data];
    } while (plots.length !== temp.total);

    const municipals = {};
    plots.map(plot => {
      const { participantId, plantableArea, barangay } = plot;
      const municipalId = barangay.municipality.id;
      let obj = municipals[municipalId];
      if (!obj) {
        municipals[municipalId] = {
          id: municipalId,
          name: barangay.municipality.name,
          area: {},
          plots: {},
          farmers: {},
          totalArea: 0,
          totalFarmers: new Set(),
          totalPlots: 0
        };
        obj = municipals[municipalId];
      }
      municipals[municipalId].totalPlots += 1;
      municipals[municipalId].totalFarmers.add(participantId);
      municipals[municipalId].totalArea += parseFloat(plantableArea);
    });
    const state = { municipals };
    this.setState(state);
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.municipalId &&
      prevProps.municipalId !== this.props.municipalId
    ) {
      if (this.state.municipals[this.props.municipalId]) {
        this.setState({
          municipal: this.state.municipals[this.props.municipalId]
        });
      } else {
        this.setState({ error: true });
      }
    }
  }

  async componentDidMount() {
    this.loadAllPlotsData();
  }

  static async getInitialProps({ query }) {
    return { municipalityId: query.id };
  }

  render() {
    const { classes } = this.props;
    const { error, municipals } = this.state;

    let noOfMunicipality = municipals ? Object.keys(municipals).length : 0;

    return (
      <AppContainer title={`Municipality Report`} appType="supervisor">
        <Grid container>
          <Grid item xs={12} md={6}>
            <VictoryChart
              domainPadding={{ x: 40, y: 20 }}
              width={500}
              theme={VictoryTheme.material}
              containerComponent={
                <VictoryZoomContainer
                  zoomDimension="x"
                  zoomDomain={
                    noOfMunicipality > 5
                      ? {
                          x: [noOfMunicipality - 3, noOfMunicipality + 2]
                        }
                      : {}
                  }
                  allowZoom={false}
                />
              }
            >
              <VictoryLegend
                title={`AREA (HA) PER MUNICIPALITY`}
                x={150}
                orientation="horizontal"
                itemsPerRow={3}
                centerTitle
                style={{ title: { fontSize: 12 }, labels: { fontSize: 8 } }}
                colorScale="qualitative"
                data={[]}
              />
              <VictoryBar
                barWidth={15}
                labelComponent={<VictoryTooltip />}
                data={Object.keys(municipals).map(key => ({
                  x: municipals[key].name,
                  y: municipals[key].totalArea,
                  label: parseFloat(municipals[key].totalArea).toFixed(0)
                }))}
              />
            </VictoryChart>
          </Grid>
          <Grid item xs={12} md={6}>
            <VictoryChart
              domainPadding={{ x: 40, y: 20 }}
              width={500}
              theme={VictoryTheme.material}
              containerComponent={
                <VictoryZoomContainer
                  zoomDimension="x"
                  zoomDomain={
                    noOfMunicipality > 5
                      ? {
                          x: [noOfMunicipality - 3, noOfMunicipality + 2]
                        }
                      : {}
                  }
                  allowZoom={false}
                />
              }
            >
              <VictoryLegend
                title={`FARMER-PLOT PER MUNICIPALITY`}
                x={150}
                orientation="horizontal"
                centerTitle
                style={{
                  title: { fontSize: 12 },
                  labels: { fontSize: 10 }
                }}
                colorScale="heatmap"
                data={[{ name: "Plots" }, { name: "Farmers" }]}
              />
              <VictoryGroup offset={15} colorScale={"heatmap"}>
                <VictoryBar
                  barWidth={15}
                  labelComponent={<VictoryTooltip />}
                  data={Object.keys(municipals).map(key => ({
                    x: municipals[key].name,
                    y: municipals[key].totalPlots,
                    label: municipals[key].totalPlots
                  }))}
                />
                <VictoryBar
                  barWidth={15}
                  labelComponent={<VictoryTooltip />}
                  data={Object.keys(municipals).map(key => ({
                    x: municipals[key].name,
                    y: municipals[key].totalFarmers.size,
                    label: municipals[key].totalFarmers.size
                  }))}
                />
              </VictoryGroup>
            </VictoryChart>
          </Grid>
        </Grid>
      </AppContainer>
    );
  }
}

const SupervisorViewMunicipalityReportPageWithStyles = withStyles(styles)(
  SupervisorViewMunicipalityReportPage
);

function mapStateToProps(state) {
  return {};
}
export default connect(
  mapStateToProps,
  {}
)(SupervisorViewMunicipalityReportPageWithStyles);
